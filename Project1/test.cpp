//class Solution {
//public:
//    vector<vector<int>> levelOrderBottom(TreeNode* root) {
//        vector<vector<int>> res;
//        if (root == nullptr) {
//            return res;
//        }
//        queue<TreeNode*> q;
//        q.push(root);
//        while (!q.empty()) {
//            int size = q.size();
//            vector<int> level;
//            for (int i = 0; i < size; i++) {
//                TreeNode* node = q.front();
//                q.pop();
//                level.push_back(node->val);
//                if (node->left != nullptr) {
//                    q.push(node->left);
//                }
//                if (node->right != nullptr) {
//                    q.push(node->right);
//                }
//            }
//            res.push_back(level);
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> postorderTraversal(TreeNode* root)
//    {
//        vector<int> res;
//        if (root == nullptr)
//        {
//            return res;
//        }
//        stack<TreeNode*> s;
//        TreeNode* p = root;
//        TreeNode* lastVisited = nullptr;
//        while (p != nullptr || !s.empty())
//        {
//            while (p != nullptr)
//            {
//                s.push(p);
//                p = p->left;
//            }
//            TreeNode* node = s.top();
//            if (node->right == nullptr || node->right == lastVisited)
//            {
//                res.push_back(node->val);
//                s.pop();
//                lastVisited = node;
//            }
//            else
//            {
//                p = node->right;
//            }
//        }
//        return res;
//    }
//};


//struct Compare {
//    bool operator()(const pair<string, int>& a, const pair<string, int>& b) {
//        if (a.second == b.second) {
//            return a.first < b.first;
//        }
//        return a.second > b.second;
//    }
//};
//
//class Solution {
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        // 使用map记录每个单词出现的次数
//        unordered_map<string, int> countMap;
//        for (const string& word : words) {
//            countMap[word]++;
//        }
//
//        // 使用优先队列进行排序，按照自定义比较函数进行排序
//        priority_queue<pair<string, int>, vector<pair<string, int>>, Compare> pq;
//        for (const auto& it : countMap) {
//            pq.push(it);
//            if (pq.size() > k) {
//                pq.pop();
//            }
//        }
//
//        // 将结果从优先队列中取出，并按照题目要求的顺序存入结果数组中
//        vector<string> result(k);
//        for (int i = k - 1; i >= 0; i--) {
//            result[i] = pq.top().first;
//            pq.pop();
//        }
//
//        return result;
//    }
//};

//
//#include <iostream>
//#include <map>
//#include <string>
//#include <algorithm>
//#include <cctype>
//#include <sstream>
//#include <vector>
//
//using namespace std;
//
//// 辅助函数：将单词转换为小写
//string toLowercase(const string& word) {
//    string lowercaseWord = word;
//    transform(lowercaseWord.begin(), lowercaseWord.end(), lowercaseWord.begin(),
//        ::tolower);
//    return lowercaseWord;
//}
//
//int main() {
//    string sentence;
//    getline(cin, sentence);
//
//    map<string, int> wordFrequency;
//
//    for (char& c : sentence) {
//        if (ispunct(c)) {
//            c = ' ';
//        }
//    }
//
//    // 使用流提取运算符分隔并统计单词出现次数
//    istringstream iss(sentence);
//    string word;
//    while (iss >> word) {
//        string lowercaseWord = toLowercase(word);
//        wordFrequency[lowercaseWord]++;
//    }
//
//    // 使用自定义比较函数进行排序
//    auto compare = [](const pair<string, int>& a, const pair<string, int>& b) {
//        if (a.second != b.second) {
//            return a.second > b.second;
//        }
//        return a.first < b.first;
//    };
//
//    // 将单词及其出现次数存储在一个向量中，并按照指定规则排序
//    vector<pair<string, int>> sortedWords(wordFrequency.begin(),
//        wordFrequency.end());
//    sort(sortedWords.begin(), sortedWords.end(), compare);
//
//    // 输出排序后的结果
//    for (const auto& entry : sortedWords) {
//        cout << entry.first << ":" << entry.second << endl;
//    }
//
//    return 0;
//}


//#include <iostream>
//#include <set>
//using namespace std;

//int main()
//{
//	set<int> mySet = { 1, 2, 3, 4, 5 };
//
//	// 使用迭代器遍历并打印元素
//	for (set<int>::iterator it = mySet.begin(); it != mySet.end(); ++it) {
//		cout << *it << " ";
//	}
//	return 0;
//}

//int main()
//{
//	set<int> mySet = { 1, 2, 3, 4, 5 };
//
//	// 使用逆向迭代器遍历并打印元素
//	for (set<int>::reverse_iterator rit = mySet.rbegin(); rit != mySet.rend(); ++rit) {
//		cout << *rit << " ";
//	}
//
//	return 0;
//}

//int main()
//{
//	set<int> mySet = { 1, 2, 3, 4, 5 };
//
//	cout << "Size of set: " << mySet.size() << endl;
//
//	return 0;
//}

//int main()
//{
//	set<int> mySet;
//
//	cout << "Maximum size of set: " << mySet.max_size() << endl;
//
//	return 0;
//}

//int main()
//{
//	set<int> mySet;
//
//	cout << "Maximum size of set: " << mySet.max_size() << endl;
//
//	return 0;
//}

//#include <iostream>
//#include <set>
//
//int main() 
//{
//    std::set<int> mySet;
//
//    // 插入元素
//    mySet.insert(5);
//    mySet.insert(3);
//    mySet.insert(8);
//
//    // 删除元素
//    mySet.erase(3);
//
//    // 交换容器内容
//    std::set<int> anotherSet;
//    anotherSet.insert(10);
//    anotherSet.insert(20);
//    mySet.swap(anotherSet);
//
//    // 清空容器
//    mySet.clear();
//    
//    return 0;
//}


#include <iostream>
#include <map>
using namespace std;

//int main()
//{
//	map<string, string> translate{ make_pair("apple", "苹果"), make_pair("banana", "香蕉"), make_pair("bear", "梨")};
//
//    // 使用迭代器遍历translate
//    cout << "正向遍历" << endl;
//    map<string, string>::iterator it;
//    for (it = translate.begin(); it != translate.end(); ++it) {
//        cout << "Key: " << it->first << ", Value: " << it->second << endl;
//    }
//
//    //使用逆向迭代器遍历translate
//    cout << "逆向遍历" << endl;
//    map<string, string>::reverse_iterator rit;
//    for (rit = translate.rbegin(); rit != translate.rend(); ++rit) {
//        cout << "Key: " << rit->first << ", Value: " << rit->second << endl;
//    }
//	return 0;
//}

//int main()
//{
//	std::map<int, string> myMap;
//	// ...
//	std::string value = myMap[2];
//	std::cout << "Value at key 2: " << value << std::endl;
//
//	//std::map<int, std::string> myMap;
//	//// ...
//	//int count = myMap.count(2);
//	//if (count > 0) {
//	//	std::cout << "Element exists" << std::endl;
//	//}
//	//else {
//	//	std::cout << "Element does not exist" << std::endl;
//	//}
//
//	/*map<int, std::string> myMap;
//	map<int, string>::iterator it = myMap.find(2);
//	if (it != myMap.end()) {
//		cout << "Found: " << it->second << endl;
//	}
//	else {
//		cout << "Not Found" << endl;
//	}*/
//
//	return 0;
//}

//int main()
//{
//	map<int, string> myMap{make_pair(1, "a"), make_pair(2, "b"), make_pair(3, "c")};
//	map<int, string>::iterator it = myMap.find(2);
//	if (it != myMap.end()) {
//		cout << "Found: " << it->second << endl;
//	}
//	else {
//		cout << "Not Found" << endl;
//	}
//
//	return 0;
//}

//int main()
//{
//	map<int, string> myMap{ make_pair(1, "a"), make_pair(2, "b"), make_pair(3, "c") };
//	int count = myMap.count(2);
//	if (count > 0) {
//		cout << "Element exists" << endl;
//	}
//	else {
//		cout << "Element does not exist" << endl;
//	}
//
//	return 0;
//}

//int main()
//{
//	map<int, string> myMap{ make_pair(1, "a"), make_pair(2, "b"), make_pair(3, "c") };
//	string value1 = myMap[2];
//	string value2 = myMap[4];
//	cout << "Value at key 2: " << value1 << endl;
//	cout << "Value at key 4: " << value2 << endl;
//
//	return 0;
//}



int main()
{
	map<int, string> myMap{ make_pair(1, "apple"), make_pair(2, "banana"), make_pair(3, "bear") };

	myMap.clear();

	for (auto e : myMap)
		cout << e.first << " : " << e.second << endl;


	return 0;
}