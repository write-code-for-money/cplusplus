#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

class base
{
public:
	virtual void print()
	{
		std::cout << "a : " << _a << std::endl;
	}
private:
	int _a = 1;
};

class driver : public base
{
public:
	void print()
	{
		std::cout << "b : " << _b << std::endl;
	}
private:
	int _b = 2;
};

void func(base* pa)
{
	driver* d = dynamic_cast<driver*>(pa);
	pa->print();
}

int main()
{

	base b;
	func(&b);
	driver d;
	func(&d);
	//char p1 = 'c';
	//int p2 = static_cast<int>(p1);
	//double p3 = static_cast<double>(p1);
	//char* s2 = &p1;
	////char p5 = static_cast<char>(s2);
	//const int a = 2;
	//int* p = const_cast<int*>(&a);
	//*p = 3;
	//std::cout << a << std::endl;
	//std::cout << *p << std::endl;
	return 0;
}