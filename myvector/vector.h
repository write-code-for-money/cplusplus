#pragma once

#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

namespace wzh
{
    template<class T>
    class vector
    {
    public:
        // Vector的迭代器是一个原生指针
        typedef T* iterator;
        typedef const T* const_iterator;
        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;
        // construct and destroy
        vector()
            :_start(nullptr)
            , _finish(nullptr)
            , _endOfStorage(nullptr)
        {

        }

        vector(int n, const T& value = T())
            :_start(new T[n])
            , _finish(_start + n)
            , _endOfStorage(_finish)
        {
            for (int i = 0; i < n; i++)
            {
                _start[i] = value;
            }
        }

        template<class InputIterator>
        vector(InputIterator first, InputIterator last);
        vector(const vector<T>& v);
        vector<T>& operator= (vector<T> v)
        {
            swap(v);
            return *this;
        }


        ~vector()
        {
            if (_start)
            {
                delete[] _start;
                _start = _finish = _endOfStorage = nullptr;
            }
        }
        // capacity
        size_t size() const;
        size_t capacity();
        void reserve(size_t n);
        void resize(size_t n, const T& value = T());
        ///////////////access///////////////////////////////
        T& operator[](size_t pos);
        const T& operator[](size_t pos) const;
        ///////////////modify/////////////////////////////
        void push_back(const T& x);
        void pop_back();

        void swap(vector<T>& v)
        {
            std::swap(_start, v._start);
            std::swap(_finish, v._finish);
            std::swap(_endOfStorage, v._endOfStorage);
        }

        iterator insert(iterator pos, const T& x);
        iterator erase(iterator pos);
    private:
        iterator _start; // 指向数据块的开始
        iterator _finish; // 指向有效数据的尾
        iterator _endOfStorage; // 指向存储容量的尾
    };



    /*template<class T>
    template<class InputIterator>
    wzh::vector<T>::vector(InputIterator first, InputIterator last)
    {
        while (first != last)
        {
            push_back(*first);
            first++;
        }
    }

    template<class T>
    wzh::vector<T>::vector(const vector<T>& v)
    {
        vector();
        reserve(v.capacity());
        iterator it = begin();
        const_iterator cit = v.begin();
        while (cit != v.cend())
        {
            *it = *cit;
            cit++;
        }
        _finish = it;
    }*/

  

    /*template<class T>
    wzh::vector<T>::iterator wzh::vector<T>::begin()
    {
        return _start;
    }

    template<class T>
    wzh::vector<T>::iterator wzh::vector<T>::end()
    {
        return _finish;
    }

    template<class T>
    wzh::vector<T>::const_iterator wzh::vector<T>::cbegin() const
    {
        return _start;
    }

    template<class T>
    wzh::vector<T>::const_iterator wzh::vector<T>::cend() const
    {
        return _finish;
    }

    template<class T>
    size_t wzh::vector<T>::capacity()
    {
        return _endOfStorage - _start;
    }

    template<class T>
    void wzh::vector<T>::push_back(const T& x)
    {
        if (_finish == _endOfStorage)
            reserve(2 * _endOfStorage);
        *_finish++ = x;
    }

    template<class T>
    void wzh::vector<T>::reserve(size_t n)
    {
        if (n > capacity())
        {
            size_t size = size();
        }
    }

    template<class T>
    size_t wzh::vector<T>::size() const
    {
        return _finish - _start;
    }*/

}

