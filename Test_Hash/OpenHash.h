#pragma once

#include <iostream>
#include <vector>
using namespace std;

namespace OpenHash
{
	bool IsPrime(size_t num)
	{
		if (num <= 2)
			return true;

		for (size_t i = 2; i * i <= num; ++i)
		{
			if (num % i == 0)
				return false;
		}

		return true;
	}

	size_t GetNextPrime(size_t num)
	{
		while (!IsPrime(num))
			++num;

		return num;
	}

	template<class T>
	class HashFunc
	{
	public:
		size_t operator()(const T& val)
		{
			return val;
		}
	};

	template<>
	class HashFunc<string>
	{
	public:
		size_t operator()(const string& s)
		{
			const char* str = s.c_str();
			unsigned int seed = 31; // 31 131 1313 13131 131313
			unsigned int hash = 0;
			while (*str)
			{
				hash = hash * seed + (*str++);
			}

			return hash;
		}
	};

	template<class V>
	struct HashBucketNode
	{
		HashBucketNode(const V& data)
			: _pNext(nullptr), _data(data)
		{}
		HashBucketNode<V>* _pNext;
		V _data;
	};

	// 本文所实现的哈希桶中key是唯一的
	template<class V, class HF = HashFunc<V>>
	class HashBucket
	{
		typedef HashBucketNode<V> Node;
		typedef Node* PNode;

		typedef HashBucket<V, HF> Self;

	public:
		HashBucket(size_t capacity)
			: _table(GetNextPrime(capacity))
			, _size(0)
		{}

		~HashBucket()
		{
			Clear();
		}

		// 哈希桶中的元素不能重复
		Node* Insert(const V& data)
		{
			CheckCapacity();
			size_t index = HashFunc(data);
			PNode pNode = new Node(data);
			pNode->_pNext = _table[index];
			_table[index] = pNode;
			++_size;

			return pNode;
		}

		// 删除哈希桶中为data的元素(data不会重复)
		bool Erase(const V& data)
		{
			size_t index = HashFunc(data);
			PNode pCur = _table[index];
			PNode pPrev = nullptr;
			while (pCur)
			{
				if (pCur->_data == data)
				{
					if (pPrev)
						pPrev->_pNext = pCur->_pNext;
					else
						_table[index] = pCur->_pNext;

					delete pCur;
					--_size;
					return true;
				}
				pPrev = pCur;
				pCur = pCur->_pNext;
			}

			return false;
		}

		Node* Find(const V& data)
		{
			size_t index = HashFunc(data);
			PNode pCur = _table[index];
			while (pCur)
			{
				if (pCur->_data == data)
					return pCur;
				pCur = pCur->_pNext;
			}

			return nullptr;
		}

		size_t Size()const
		{
			return _size;
		}

		bool Empty()const
		{
			return 0 == _size;
		}

		void Clear()
		{
			for (size_t i = 0; i < _table.size(); ++i)
			{
				PNode pCur = _table[i];
				while (pCur)
				{
					PNode pDel = pCur;
					pCur = pCur->_pNext;
					delete pDel;
				}
				_table[i] = nullptr;
			}

			_size = 0;
		}

		size_t BucketCount()const
		{
			return _table.capacity();
		}

		void Swap(Self& ht)
		{
			_table.swap(ht._table);
			swap(_size, ht._size);
		}

	private:
		size_t HashFunc(const V& data)
		{
			return HF()(data) % _table.capacity();
		}

		void CheckCapacity()
		{
			if (_size * 10 / _table.size() >= 7)
			{
				size_t newSize = GetNextPrime(_table.size() * 2);
				Self newTable(newSize);
				for (size_t i = 0; i < _table.size(); ++i)
				{
					PNode pCur = _table[i];
					while (pCur)
					{
						PNode pNext = pCur->_pNext;
						size_t newIndex = newTable.HashFunc(pCur->_data);
						pCur->_pNext = newTable._table[newIndex];
						newTable._table[newIndex] = pCur;
						pCur = pNext;
					}
				}
				Swap(newTable);
			}
		}

	private:
		vector<Node*> _table;
		size_t _size;      // 哈希表中有效元素的个数
	};
}
