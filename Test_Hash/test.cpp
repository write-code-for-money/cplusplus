#include "OpenHash.h"  


void duplicateZeros(vector<int>& arr) {
    //1.查找最后一个复写元素
    int cur = -1, dest = -1;
    int sz = arr.size();
    while (dest < sz)
    {
        if (arr[++cur]) dest++;
        else dest += 2;
    }
    //2.处理特殊情况——dest越界
    if (dest == sz)
    {
        arr[--dest] = 0;
        cur--;
    }
    //3.从后往前复写
    while (cur >= 0)
    {
        if (arr[cur] == 0)
        {
            arr[--dest] = 0;
            arr[--dest] = 0;
            cur--;
        }
        else
        {
            arr[--dest] = arr[cur];
            cur--;
        }
    }
}


int main()
{
    vector<int> arr = { 1,0,2,3,0,0 };
    duplicateZeros(arr);
    for (auto e : arr)
    {
        cout << e << ' ';
    }
}
//int main() 
//{
//    OpenHash::HashBucket<int> hashTable(10);
//
//    // 插入元素
//    hashTable.Insert(1);
//    hashTable.Insert(5);
//    hashTable.Insert(11);
//
//    // 查找元素
//    OpenHash::HashBucketNode<int>* node = hashTable.Find(5);
//    if (node) {
//        cout << "Element 5 found in the hash bucket." << endl;
//    }
//    else {
//        cout << "Element 5 not found in the hash bucket." << endl;
//    }
//
//    // 删除元素
//    bool erased = hashTable.Erase(5);
//    if (erased) {
//        cout << "Element 5 erased from the hash bucket." << endl;
//    }
//    else {
//        cout << "Element 5 not found in the hash bucket. No erasing performed." << endl;
//    }
//
//    // 再次查找元素
//    node = hashTable.Find(5);
//    if (node) {
//        cout << "Element 5 found in the hash bucket." << endl;
//    }
//    else {
//        cout << "Element 5 not found in the hash bucket." << endl;
//    }
//
//    return 0;
//}
