#include <iostream>
#include <list> 
using namespace std;

int main() {
    std::list<int> mylist = { 1, 2, 3, 4, 5 };
    auto it = mylist.begin();

    // 将迭代器保存到外部使用
    auto outside_it = it;

    // 对list容器进行删除操作
    mylist.erase(mylist.begin());

    // 使用保存在外部的迭代器访问list容器
    std::cout << *outside_it << std::endl;

    return 0;
}

//int main() {
//    std::list<int> mylist = { 1, 2, 3, 4, 5 };
//    auto it = mylist.begin();
//
//    // 在迭代器指向的位置后面增加一个元素
//    mylist.insert(++it, 6);
//
//    // 使用迭代器遍历list容器
//    for (auto it = mylist.begin(); it != mylist.end(); ++it) {
//        std::cout << *it << " ";
//    }
//    std::cout << std::endl;
//
//    // 再次使用之前的迭代器遍历list容器
//    for (auto it = mylist.begin(); it != mylist.end(); ++it) {
//        std::cout << *it << " ";
//    }
//    std::cout << std::endl;
//
//    return 0;
//}

//int main()
//{
//	list<char> ls = {'a', 'b', 'c', 'd'};
//	list<char>::iterator it = ls.begin();
//	/*while (it != ls.end())
//	{
//		if (*it == 'b')
//		{
//			ls.erase(it);
//		}
//		cout << *it << ' ';
//		it++;
//	}*/
//	while (it != ls.end())
//	{
//		if (*it == 'b')
//		{
//			auto tmp = it++;
//			ls.erase(tmp);
//		}
//		else
//		{
//			cout << *it << ' ';
//			it++;
//		}
//	}
//	return 0;
//}

//int main()
//{
//	list<char> ls;
//	ls.push_back('A');
//	ls.push_back('B');
//	ls.push_back('C');
//	Print(ls);
//
//	ls.push_front('1');
//	ls.push_front('2');
//	ls.push_front('3');
//	Print(ls);
//
//	ls.pop_back();
//	ls.pop_front();
//	Print(ls);
//
//	string arr = "abcd";
//	ls.insert(ls.begin(), 'M');
//	ls.insert(ls.end(), arr.begin(), arr.end());
//	ls.insert(ls.begin(), 3, 'O');
//	Print(ls);
//
//	list<char> ls2(ls);
//	ls.erase(ls.begin());
//	Print(ls);
//
//	ls.swap(ls2);
//	Print(ls);
//	Print(ls2);
//
//	ls2.clear();
//	ls.erase(ls.begin(), ls.end());
//	Print(ls);
//	Print(ls2);
//	return 0;
//}


//int main()
//{
//	string arr = "abcdefg";
//	list<char> ls(arr.begin(), arr.end());
//	list<int> ls2;
//	cout << ls2.front() << "  " << ls2.back() << endl;
//	char front = ls.front();
//	char back = ls.back();
//	cout << front << "  " << back << endl;
//	return 0;
//}

//int main()
//{
//	string arr = "abcdefg";
//	list<char> ls(arr.begin(), arr.end());
//	cout << "是否为空：" << ls.empty() << endl;
//	cout << "最大存储数量：" << ls.max_size() << endl;
//	cout << "当前存储数量：" << ls.size() << endl;
//	return 0;
//}

//int main()
//{
//	string arr = "abcdefg";
//	list<char> ls(arr.begin(), arr.end());
//	list<char>::iterator it = ls.begin();
//	list<char>::reverse_iterator rit = ls.rbegin();
//	while (it != ls.end())
//	{
//		cout << *it << ' ';
//		it++;
//	}
//	cout << endl;
//	while (rit != ls.rend())
//	{
//		cout << *rit << ' ';
//		rit++;
//	}
//	cout << endl;
//	return 0;
//}

//void print(list<int> ls)
//{
//	for (auto e : ls)
//	{
//		cout << e << ' ';
//	}
//	cout << endl;
//}
//
//int main()
//{
//	list<int> ls;
//	print(ls);
//
//	list<int> ls2(10, 6);
//	print(ls2);
//
//	list<int> ls3(ls2);
//	print(ls3);
//
//	list<int> ls4(ls2.begin(), ls2.end());
//	print(ls4);	
//	return 0;
//}