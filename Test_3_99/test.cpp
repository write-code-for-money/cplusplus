#define _CRT_SECURE_NO_WARNINGS 1
#include "class.h"
class Date
{
public:
	Date()
	{
		_year = 1900;
		_month = 1;
		_day = 1;
	}
	Date(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
private:
	int _year;
	int _month;
	int _day;
};
// 以下测试函数能通过编译吗？
void Test()
{
	Date d1;
}
int main()
{
	Test();
	return 0;
}
//class Data {
//public:
//	//1.无参构造函数
//	Data()
//	{
//
//	}
//	//2.带参构造函数
//	Data(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	int _year = 0;
//	int _month = 0;
//	int _day = 0;
//};
//int main()
//{
//	Data D1; //调用无参构造函数
//	Data D2(2015, 8, 20); //调用有参构造函数
//
//	//注意：如果通过无参构造函数创建对象时，对象后面不用跟括号，否则就成了函数声明
//	//warning C4930: “Date d3(void)”: 未调用原型函数(是否是有意用变量定义的?)
//	//声明了d3函数，该函数无参，返回一个日期类型的对象
//	//Data D3();     
//	return 0;
//}
//// 1.下面程序编译运行结果是？ A、编译报错 B、运行崩溃 C、正常运行
//class A
//{
//public:
//	void Print()
//	{
//		cout << "Print()" << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A* p = nullptr;
//	p->Print();
//	return 0;
//}
//// 1.下面程序编译运行结果是？ A、编译报错 B、运行崩溃 C、正常运行
//class A
//{
//public:
//	void PrintA()
//	{
//		cout << _a << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A* p = nullptr;
//	p->PrintA();
//	return 0;
//}

//class Person
//{
//public:
//	void PrintPersonInfo();
//private:
//	char _name[20];
//	char _gender[3];
//	int  _age;
//};
//// 这里需要指定PrintPersonInfo是属于Person这个类域
//void Person::PrintPersonInfo()
//{
//	cout << _name << " " << _gender << " " << _age << endl;
//}
//void Student::Showstd()
//{
//	cout << _name << "-" << _sex << "-" << _age << endl;
//}
//
//int main()
//{
//	Student a;
//	a._age = 17;
//	a.Showstd();
//	return 0;
//}