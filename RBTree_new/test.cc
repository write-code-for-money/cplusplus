#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include "RBTree.h"


void test_RB()
{
	RBTree<int> r1;
	r1.Insert(5);
	r1.Insert(2);
	r1.Insert(4);
	r1.Insert(7);
	r1.Insert(9);
	r1.Insert(1);
	r1.Insert(1);
	r1.Insert(0);
	std::cout << r1.IsValidRBTRee() << std::endl;
	std::cout << *r1.Find(2) << std::endl;
	//std::cout << r1.Find(4)->_val << std::endl;
	std::cout << r1.LeftMost()->_val << std::endl;
	std::cout << r1.RightMost()->_val << std::endl;
	for (auto e : r1)
		std::cout << e << " ";
	std::cout << std::endl;
}

class Date 
{
public:
	Date(int year = 2012, int month = 10, int day = 2) : _year(year), _month(month), _day(day)
	{}

	~Date() {}

	void Print()
	{
		std::cout << "year : " << _year << "month : " << _month << "day : " << _day << std::endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

//void test_RB()
//{
//	Date d1(2015, 4, 7);
//	Date d2(2014, 12, 5);
//	Date d3;
//	RBTree<Date> r1;
//	r1.Insert(d1);
//	r1.Insert(d2);
//	r1.Insert(d3);
//	
//	//std::cout << r1.IsValidRBTRee() << std::endl;
//	//std::cout << *r1.Find(2) << std::endl;
//	////std::cout << r1.Find(4)->_val << std::endl;
//	//std::cout << r1.LeftMost()->_val << std::endl;
//	//std::cout << r1.RightMost()->_val << std::endl;
//	//for (auto e : r1)
//	//	std::cout << e << " ";
//	//std::cout << std::endl;
//}

int main()
{
	test_RB();
	return 0;
}