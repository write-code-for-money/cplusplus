#pragma once

// 请模拟实现红黑树的插入--注意：为了后序封装map和set，本文在实现时给红黑树多增加了一个头结点

enum Color {RED = 1, BLACK};

template<class T>
class RBTreeNode
{
	typedef RBTreeNode<T> node;
public:
	RBTreeNode(const T& val = T(), Color col = RED)
		:_left(nullptr), _right(nullptr), _parent(nullptr)
	{
		_val = val;
		_col = col;
	}
public:
	T _val;
	node* _left;
	node* _right;
	node* _parent;
	Color _col;
};

template<class T>
struct RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef RBTreeIterator<T> Self;

	RBTreeIterator(Node* pNode)
		: _pNode(pNode)
	{}

	// 让迭代器具有类似指针的行为
	T& operator*()
	{
		return _pNode->_val;
	}
	T* operator->()
	{
		return &(_pNode->_val);
	}

	// 然迭代器可以移动：前置/后置++  
	Self& operator++()
	{
		Increament();
		return *this;
	}
	Self operator++(int)
	{
		Self tmp = *this;
		Increament();
		return tmp;
	}
	// 然迭代器可以移动：前置/后置-- 
	Self& operator--()
	{
		DeIncreament();
		return *this;
	}
	Self operator--(int)
	{
		Self tmp = *this;
		DeIncreament();
		return tmp;
	}

	// 让迭代器可以比较
	bool operator!=(const Self& s)const
	{
		return _pNode != s._pNode;
	}
	bool operator==(const Self& s)const
	{
		return _pNode == s._pNode;
	}

private:
	void Increament()
	{
		//存在右节点
		if (_pNode->_right)
		{
			_pNode = _pNode->_right;
			while (_pNode->_left)
				_pNode = _pNode->_left;
		}
		else //不存在右节点
		{
			Node* parent = _pNode->_parent;
			while (parent->_right == _pNode)
			{
				_pNode = parent;
				parent = parent->_parent;
			}
			_pNode = parent;
		}
	}
	void DeIncreament()
	{
		if (_pNode->_parent->_parent == _pNode && _pNode->_col == RED) _pNode = _pNode->_right;
		//存在左子树，找左子树最大节点
		else if (_pNode->_left)
		{
			_pNode = _pNode->_left;
			while (_pNode->_right)
				_pNode = _pNode->_right;
		}
		else
		{
			Node* parent = _pNode->_parent;
			while (parent->_left == _pNode)
			{
				_pNode = parent;
				parent = parent->_parent;
			}
			_pNode = parent;
		}
	}
	Node* _pNode;
};


template<class T>
class RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef RBTreeIterator<T> iterator;

	RBTree()
	{
		_pHead = new Node;
	}

	iterator begin()
	{
		return _pHead->_left;
	}
	iterator end()
	{
		return _pHead;
	}

	// 在红黑树中插入值为data的节点，插入成功返回true，否则返回false
	// 注意：为了简单起见，本次实现红黑树不存储重复性元素
	bool Insert(const T& data)
	{
		//没有节点
		if (GetRoot() == nullptr)
		{
			_pHead->_parent = _pHead->_left = _pHead->_right = new Node(data, BLACK);
			_pHead->_parent->_parent = _pHead;			
			return true;
		}
		else
		{
			Node* cur = GetRoot(), * parent = nullptr;
			while (cur)
			{
				if (cur->_val > data)
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_val < data)
				{
					parent = cur;
					cur = cur->_right;
				}
				else return false;
			}
			//插入
			Node* node = new Node(data);
			if (parent->_val > data) parent->_left = node;
			else parent->_right = node;
			node->_parent = parent;
			cur = node;
			//更新颜色
			while (parent && parent->_col == RED) //父亲为红，爷爷就不可能为红
			{
				//确定爷爷和叔叔节点
				Node* grand = parent->_parent, * uncle;
				if (parent == grand->_left) uncle = grand->_right;
				else uncle = grand->_left;
				//叔叔存在为红
				if (uncle && uncle->_col == RED)
				{
					//p -> BLACK  g -> RED   u -> BLACK
					parent->_col = uncle->_col = BLACK;
					grand->_col = RED;
					cur = grand;
					parent = cur->_parent;
				}
				else //叔叔不存在或者为黑
				{
					if (parent == grand->_left && cur == parent->_left)
					{
						RotateR(grand);
						parent->_col = BLACK;
						grand->_col = RED;
					}
					else if (parent == grand->_right && cur == parent->_right)
					{
						RotateL(grand);
						parent->_col = BLACK;
						grand->_col = RED;
					}
					else if (parent == grand->_left && cur == parent->_right)
					{
						RotateL(parent);
						RotateR(grand);
						cur->_col = BLACK;
						grand->_col = RED;
					}
					else
					{
						RotateR(parent);
						RotateL(grand);
						cur->_col = BLACK;
						grand->_col = RED;
					}
				}
				break;
			}
			if (_pHead->_left->_val > data) _pHead->_left = node;
			if (_pHead->_right->_val < data) _pHead->_right = node;
			_pHead->_parent->_col = BLACK;
		}
	}

	// 检测红黑树中是否存在值为data的节点，存在返回该节点的地址，否则返回nullptr
	//Node* Find(const T& data)
	//{
	//	Node* root = GetRoot();
	//	if (root == nullptr) return nullptr;
	//	while (root)
	//	{
	//		if (root->_val < data) root = root->_right;
	//		else if (root->_val > data) root = root->_left;
	//		else return root;
	//	}
	//	return nullptr;
	//}

	iterator Find(const T& data)
	{
		iterator it = begin();
		while (it != end())
		{
			if (*it == data) break;
			++it;
		}
		return it;
	}

	// 获取红黑树最左侧节点
	Node* LeftMost()
	{
		return _pHead->_left;
	}

	// 获取红黑树最右侧节点
	Node* RightMost()
	{
		return _pHead->_right;
	}

	// 检测红黑树是否为有效的红黑树，注意：其内部主要依靠_IsValidRBTRee函数检测
	bool IsValidRBTRee()
	{
		Node* root = GetRoot();
		if (root == nullptr) return true;
		if (root->_col != BLACK) return false;
		int pathblack = 0;
		while (root)
		{
			if (root->_col == BLACK) pathblack++;
			root = root->_left;
		}
		return _IsValidRBTRee(GetRoot(), 0, pathblack);
	}
private:
	bool _IsValidRBTRee(Node* pRoot, size_t blackCount, size_t pathBlack)
	{
		if (pRoot == nullptr)
		{
			if (blackCount != pathBlack) return false;
			else return true;
		}

		if (pRoot->_col == BLACK) blackCount++;

		Node* parent = pRoot->_parent;
		if (parent->_col == pRoot->_col && pRoot->_col == RED) return false;

		return _IsValidRBTRee(pRoot->_left, blackCount, pathBlack) && _IsValidRBTRee(pRoot->_right, blackCount, pathBlack);
	}
	// 左单旋
	void RotateL(Node* pParent)
	{
		Node* ppnode = pParent->_parent;
		Node* RatoR = pParent->_right;
		Node* RatoRL = RatoR->_left;
		
		pParent->_right = RatoRL;
		if (RatoRL) RatoRL->_parent = pParent;

		RatoR->_left = pParent;
		pParent->_parent = RatoR;

		if (ppnode->_parent == pParent) ppnode->_parent = RatoR;
		else if (ppnode->_left == pParent) ppnode->_left = RatoR;
		else ppnode->_right = RatoR;
		RatoR->_parent = ppnode;
	}
	// 右单旋
	void RotateR(Node* pParent)
	{
		Node* ppnode = pParent->_parent;
		Node* RatoL = pParent->_left;
		Node* RatoLR = RatoL->_right;

		pParent->_left = RatoLR;
		if (RatoLR) RatoLR->_parent = pParent;

		RatoL->_right = pParent;
		pParent->_parent = RatoL;

		if (ppnode->_parent == pParent) ppnode->_parent = RatoL;
		else if (ppnode->_left == pParent) ppnode->_left = RatoL;
		else ppnode->_right = RatoL;
		RatoL->_parent = ppnode;
	}
	// 为了操作树简单起见：获取根节点
	Node*& GetRoot()
	{
		return _pHead->_parent;
	}
private:
	Node* _pHead;
};
