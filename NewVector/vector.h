#pragma once
#include <iostream>
#include <cassert>
#include "reverse_iterator.hpp"

namespace wzh
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		typedef reverse_Iterator<iterator, T&, T*> reverse_iterator;
		typedef reverse_Iterator<const_iterator, const T&, const T*> const_reverse_iterator;

	public:
		//构造析构函数///////////////////////////////
		~vector()
		{
			if(_start) delete[] _start;
			_start = _finish = _end_of_storage = nullptr;
		}
		vector() :_start(nullptr), _finish(nullptr), _end_of_storage(nullptr) {}
		vector(int n, const T& val = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		vector(vector<T>& v)
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{
			reserve(v.capacity());
			for (size_t i = 0; i < v.size(); ++i)
			{
				_start[i] = v._start[i];
			}
			_finish = _start + v.size();
			_end_of_storage = _start + v.capacity();
		}
		template<class Iterator>
		vector(Iterator first, Iterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		//数据修改/////////////////////////////////
		void push_back(const T& val)
		{
			if (_finish == _end_of_storage) reserve(capacity() == 0 ? 4 : 2 * capacity());
			*_finish = val;
			_finish++;
		}
		void pop_back()
		{
			assert(!empty());
			--_finish;
		}
		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start && pos < _finish);
			if (_finish == _end_of_storage)
			{
				int len = pos - _start;
				reserve(capacity() == 0 ? 4 : capacity() * 2);
				pos = _start + len;
			}
			iterator it = end();
			while (it != pos)
			{
				*it = *(it - 1);
				--it;
			}
			*it = val;
			_finish++;
			return pos;
		}
		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);
			iterator it = pos + 1;
			while (it != _finish)
			{
				*(it - 1) = *it;
				++it;
			}
			--_finish;
			return pos;
		}
		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}
		const T& operator[](size_t pos) const
		{
			assert(pos < size());
			return _start[pos];
		}
		//迭代器/////////////////////////////
		iterator begin()
		{
			return _start;
		}
		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator(end());
		}
		const_reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}
		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}
		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator cbegin() const
		{
			return _start;
		}
		const_iterator cend() const
		{
			return _finish;
		}

		//容量相关函数/////////////////////////////////////////
		void reserve(size_t n)
		{
			if (capacity() < n)
			{
				int sz = size();
				T* tmp = new T[n];
				if (_start)
				{
					//memcpy(tmp, _start, sizeof(T)*size());
					for (size_t i = 0; i < sz; ++i)
					{
						tmp[i] = _start[i];
					}
					delete[] _start;
				}

				_start = tmp;
				_finish = _start + sz;
				_end_of_storage = _start + n;
			}
		}
		bool empty()
		{
			return size() == 0 ? true : false;
		}
		void resize(size_t n, T val = T())
		{
			if (n <= size())
			{
				_finish = _start + n;
			}
			else
			{
				if (n > capacity())
				{
					reserve(n);
				}
				while (size() < n)
				{
					*_finish = val;
					++_finish;
				}
			}
		}
		size_t capacity()
		{
			return _end_of_storage - _start;
		}
		
		size_t size()
		{
			return _finish - _start;
		}

	private:
		iterator _start;
		iterator _finish;
		iterator _end_of_storage;
	};
}