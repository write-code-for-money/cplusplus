#define _CRT_SECURE_NO_WARNINGS 1

#include "vector.h"

class PT
{
public:
	PT()
	{
		int* tmp = new int[10];
		p = tmp;
	}
	~PT()
	{
		delete[] p;
		p = nullptr;
	}

private:
	int* p;
	int val = 10;
};

void test1()
{
	//wzh::vector<PT> p1(5, PT());
	//wzh::vector<PT> p2(p1);

	wzh::vector<int> v1(10, 5);
	wzh::vector<int> v2;
	wzh::vector<int> v3(v1);
	wzh::vector<int> v4(v1.begin(), v1.end());
	//v2.resize(10, 6);
}

void test2()
{
	wzh::vector<int> v1(5, 8);
	wzh::vector<int>::reverse_iterator rit = v1.rbegin();
	while (rit != v1.rend())
	{
		std::cout << *rit << std::endl;
		++rit;
	}
	//v1.push_back(7);

	//v1.insert(v1.begin(), 0);
	//v1.insert(v1.begin() + 6, 0);
	//v1.erase(v1.begin());
	//v1.erase(v1.end() - 1);
	//v1.pop_back();
}

int main()
{
	test2();
	return 0;
}