#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>

using namespace std;

#include <iostream>
using namespace std;
#include <vector>
//int main()
//{
//	vector<int> v{ 1, 2, 3, 4 };
//	auto it = v.begin();
//	while (it != v.end())
//	{
//		if (*it % 2 == 0)
//			v.erase(it);
//		++it;
//	}
//
//	return 0;
//}
int main()
{
	vector<int> v{ 1, 2, 3, 4 };
	auto it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
			it = v.erase(it);
		else
			++it;
	}
	return 0;
}


//int main()
//{
//	vector<int> arr{ 1, 2, 3, 4, 5 };
//	vector<int>::iterator it = arr.begin();
//	arr.erase(it);
//	while (it != arr.end())
//	{
//		cout << *it << ' ';
//		it++;
//	}
//	cout << endl;
//	return 0;
//}

//int main() 
//{
//	vector<int> arr{ 1, 2, 3, 4, 5 };
//	vector<int>::iterator it = arr.begin();
//	arr.resize(100, 0);
//	//arr.reserve(100);
//	while (it != arr.end())
//	{
//		cout << *it << ' ';
//		it++;
//	}
//	cout << endl;
//
//	return 0;
//}

//void Print(vector<int>& arr)
//{
//	for (int i = 0; i < arr.size(); i++)
//	{
//		cout << arr[i] << ' ';
//	}
//	cout << endl << "#######################" << endl;
//}
//
//int main()
//{
//	vector<int> arr;
//	arr.push_back(1);
//	arr.push_back(2);
//	arr.push_back(3);
//	arr.push_back(4);
//	arr.push_back(5);
//	Print(arr);
//	arr.pop_back();
//	arr.pop_back();
//	Print(arr);
//	//在arr.begin() + 2的位置插入一个10.
//	arr.insert(arr.begin() + 2, 10);
//	Print(arr);
//	//在arr.begin + 1的位置插入2个20。
//	arr.insert(arr.begin() + 1, 2, 20);
//	Print(arr);
//	//在arr.begin() + 1的位置之前插入arr.begin到arr.end整个区间的数据
//	arr.insert(arr.begin() + 1, arr.begin(), arr.end());
//	Print(arr);
//	arr.erase(arr.begin() + 2);
//	Print(arr);
//	arr.erase(arr.begin(), arr.begin() + 3);
//	Print(arr);
//	vector<int>::iterator it = find(arr.begin(), arr.end(), 20);
//	if (it != arr.end())
//		cout << "find success." << endl << "###################" << endl;
//	vector<int> arr_swap(5, 10);
//	arr.swap(arr_swap);
//	Print(arr);
//	return 0;
//}

//void Print(vector<int>& arr)
//{
//	cout << "size : " << arr.size() << endl << "capacity : " << arr.capacity() << endl;
//	cout << "#######################" << endl;
//}
//
//int main()
//{
//	vector<int> arr;
//	for (int i = 0; i < 20; i++)
//	{
//		if(arr.size() == arr.capacity())
//			Print(arr);
//
//		arr.push_back(i);
//	}
//	return 0;
//}

//void Print(vector<int>& arr)
//{
//	cout << "size : " << arr.size() << endl << "capacity : " << arr.capacity() << endl;
//	for (auto e : arr)
//	{
//		cout << e << ' ';
//	}
//	cout << endl << "#######################" << endl;
//}
//int main()
//{
//	vector<int> arr;
//	if (arr.empty())
//	{
//		cout << "arr中没有数据" << endl;
//	}
//	arr.push_back(1);
//	arr.push_back(2);
//	arr.push_back(3);
//	arr.push_back(4);
//	arr.push_back(5);
//	Print(arr);
//	arr.resize(10, 5);
//	Print(arr);
//	arr.reserve(20);
//	Print(arr);
//	return 0;
//}

//int main()
//{
//	vector<int> arr;
//	arr.push_back(1);
//	arr.push_back(2);
//	arr.push_back(3);
//	arr.push_back(4);
//	arr.push_back(5);
//	vector<int>::reverse_iterator rit = arr.rbegin();
//	while (rit != arr.rend())
//	{
//		cout << *rit << ' ';
//		rit++;
//	}
//	cout << endl;
//	return 0;
//}

//void Print(vector<int>& arr)
//{
//	for (auto e : arr)
//	{
//		cout << e << ' ';
//	}
//	cout << endl;
//}
//int main()
//{
//	vector<int> arr;
//	vector<int> arr2(5, 3);
//	vector<int> arr3(arr2);
//	vector<int> arr4(arr2.begin(), arr2.end());
//	Print(arr);
//	Print(arr2);
//	Print(arr3);
//	Print(arr4);
//	return 0;
//}


