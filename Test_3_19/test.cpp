#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include <stack>
using namespace std;
int main()
{
    unsigned long long M;
    int N;
    scanf("%ud %d", &M, &N);
    stack<char> s;
    if (M == 0)
        cout << 0;
    while (M != 0)
    {
        int ret = M % N;
        switch (ret)
        {
        case 10:
            s.push('A');
            break;
        case 11:
            s.push('B');
            break;
        case 12:
            s.push('C');
            break;
        case 13:
            s.push('D');
            break;
        case 14:
            s.push('E');
            break;
        case 15:
            s.push('F');
            break;
        default:
            s.push('0' + ret);
            break;
        }
        M /= N;
    }
    while (!s.empty())
    {
        cout << s.top();
        s.pop();
    }
    return 0;
}
//#include <iostream>
//#include <vector>
//#include <string>
//using namespace std;
//int main()
//{
//	string s1 = "abddddd jkfe krjekr ttt bb";
//	int ret = s1.find('x');
//	cout << ret << endl;
//	ret = s1.find('x', ret + 1);
//	cout << ret << endl;
//	return 0;
//}
//int main()
//{
//    long n = 0;
//    vector<int> v1;
//    vector<int> v_plus, v_reduce;
//    cin >> n;
//
//    /*v1.reserve(n);
//    v_plus.reserve(n);
//    v_reduce.reserve(n);*/
//    v1.resize(n, 0);
//   // v_plus.resize(n, 0);
//    //v_reduce.resize(n, 0);
//
//    for (int i = 0; i < n; i++)
//    {
//        cin >> v1[i];
//    }
//    int count = 0;
//    for (int j = 0; j < n; j++)
//    {
//        if (j == 0)
//        {
//            v_plus.push_back(v1[j]);
//            count++;
//        }
//        else if (v1[j] > v1[j - 1])
//        {
//            if (v_reduce.size() != 0)
//            {
//                v_reduce.clear();
//                count++;
//            }
//            v_plus.push_back(v1[j]);
//        }
//        else if (v1[j] < v1[j - 1])
//        {
//            if (v_plus.size() != 0)
//            {
//                v_plus.clear();
//                count++;
//            }
//            v_reduce.push_back(v1[j]);
//        }
//        else {
//            if (v_plus.size() != 0)
//                v_plus.push_back(v1[j]);
//            else
//                v_reduce.push_back(v1[j]);
//        }
//    }
//    cout << count;
//    return 0;
//}
//#include <iostream>
//#include <string>
//#include <vector>
//using namespace std;
//int main()
//{
//	/*string s1 = "shdh jdskdj";
//	cout << s1;*/
//	vector<int> v1 = { 1,2,3,4,5,6,7,8};
//	for (auto e : v1)
//	{
//		cout << e << ' ';
//	}
//	cout << '\n';
//	//v1.cout << v1;
//	v1.clear();
//	for (auto e : v1)
//	{
//		cout << e << ' ';
//	}
//	cout << '\n';
//	return 0;
//}
//class Time
//{
//    friend class Date;   // 声明日期类为时间类的友元类，则在日期类中就直接访问Time类中的私有成员变量
//public:
//    Time(int hour = 0, int minute = 0, int second = 0)
//        : _hour(hour)
//        , _minute(minute)
//        , _second(second)
//    {}
//
//private:
//    int _hour;
//    int _minute;
//    int _second;
//};
//
//class Date
//{
//    friend class Time;
//public:
//    Date(int year = 1900, int month = 1, int day = 1)
//        : _year(year)
//        , _month(month)
//        , _day(day)
//    {}
//
//private:
//    int _year;
//    int _month;
//    int _day;
//    Time _t;
//};
//
//int main()
//{
//    Date s1;
//    return 0;
//}