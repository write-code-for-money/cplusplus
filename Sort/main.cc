#define _CRT_SECURE_NO_WARNINGS 1

#include <chrono> // 用于计时
#include "Sort.hpp"
#include <functional>



//int main()
//{
//	std::vector<int> arr = { 5, 2, 7, 9, 3, 1, 6, 8, 4 };
//	//insertSort(arr);
//	//shellSort(arr);
//	//chooseSort(arr);
//	//heapSort(arr);
//	//bubbleSort(arr);
// 	quikSort(arr);
//	for (int e : arr) std::cout << e << ' ';
//	std::cout << std::endl;
//	return 0;
//}


// 生成随机数数组
std::vector<int> generateRandomArray(int size, int range) 
{
    std::vector<int> arr(size);
    srand(time(nullptr)); // 设置随机种子
    for (int i = 0; i < size; ++i) 
    {
        arr[i] = rand() % range; // 生成范围在 [0, range) 内的随机数
    }
    return arr;
}

using func_t = std::function<bool(std::vector<int>&)>;

void displayTime(std::vector<int>& arr, func_t func)
{
    // 计时开始
    auto start = std::chrono::high_resolution_clock::now();
    func(arr);
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    // 输出排序耗时
    std::cout << "排序耗时：" << elapsed_seconds.count() << " 秒" << std::endl;
}

int main() 
{
    const int size = 10000; // 数组大小
    const int range = 100; // 随机数范围
    // 生成随机数数组
    std::vector<int> arr1 = generateRandomArray(size, range);
    std::vector<int> arr2 = arr1;
    std::vector<int> arr3 = arr1;
    std::vector<int> arr4 = arr1;
    std::vector<int> arr5 = arr1;
    std::vector<int> arr6 = arr1;
    displayTime(arr1, insertSort);
    displayTime(arr2, shellSort);
    displayTime(arr3, selectSort);
    displayTime(arr4, heapSort);
    displayTime(arr5, bubbleSort);
    displayTime(arr6, quikSort);

    return 0;
}