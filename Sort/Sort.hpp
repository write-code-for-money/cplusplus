#pragma once
#include <iostream>
#include <vector>
#include <stack>

bool insertSort(std::vector<int>& arr)
{
	int n = arr.size();
	for (int i = 1; i < n; i++) 
	{
		int key = arr[i];
		int j = i - 1;
		// 将 arr[0..i-1] 中比 key 大的元素都向后移动一个位置
		while (j >= 0 && arr[j] > key) 
		{
			arr[j + 1] = arr[j];
			--j;
		}
		// 将 key 插入到合适的位置
		arr[j + 1] = key;
	}
	return true;
}

bool shellSort(std::vector<int>& arr)
{
	int gap = arr.size();
	while (gap)
	{
		for (int i = 1; i < arr.size(); i += gap)
		{
			int j = i - 1, key = arr[i];
			while (j >= 0 && arr[j] > key)
			{
				arr[j + 1] = arr[j];
				j--;
			}
			arr[j + 1] = key;
		}
		gap /= 2;
	}
	return true;
}

bool selectSort(std::vector<int>& arr)
{
	for (int i = 0; i < arr.size(); i++)
	{
		int minNum = i;
		for (int j = i + 1; j < arr.size(); j++)
		{
			if (arr[j] < arr[minNum]) minNum = j;
		}
		std::swap(arr[minNum], arr[i]);
	}
	return true;
}

void adjustDown(std::vector<int>& arr, int cur, int len)
{
	int leftChild = cur * 2 + 1;
	while (leftChild < len) 
	{
		int rightChild = leftChild + 1;
		int maxChild = (rightChild < len && arr[rightChild] > arr[leftChild]) ? rightChild : leftChild;
		if (arr[maxChild] > arr[cur])
		{
			std::swap(arr[maxChild], arr[cur]);
			cur = maxChild;
			leftChild = cur * 2 + 1;
		}
		else break;
	}
}

void createMaxHeap(std::vector<int>& arr)
{
	int len = arr.size();
	for (int i = len / 2; i >= 0; i--)
	{
		adjustDown(arr, i, len);
	}
}

bool heapSort(std::vector<int>& arr)
{
	createMaxHeap(arr);
	for (int i = arr.size() - 1; i > 0; i--)
	{
		std::swap(arr[0], arr[i]);
		adjustDown(arr, 0, i);
	}
	return true;
}

bool bubbleSort(std::vector<int>& arr)
{
	for (int i = 0; i < arr.size(); i++)
	{
		for (int j = 1; j < arr.size(); j++)
		{
			if (arr[j - 1] > arr[j]) std::swap(arr[j - 1], arr[j]);
		}
	}
	return true;
}


int getKey(std::vector<int>& arr, int left, int right)
{
	int mid = (left + right) / 2;
	if (arr[left] < arr[right])
	{
		if (arr[mid] > arr[right]) return right;
		else if (arr[mid] < arr[left]) return left;
		else return mid;
	}
	else
	{
		if (arr[mid] < arr[right]) return right;
		else if (arr[mid] > arr[left]) return left;
		else return mid;
	}
}

//hore版本 + 三数取中
int partSort1(std::vector<int>& arr, int left, int right)
{
	int key = getKey(arr, left, right);
	std::swap(arr[key], arr[left]);
	key = left;
	while (left < right)
	{
		while (left < right && arr[right] >= arr[key]) right--;
		while (left < right && arr[left] <= arr[key]) left++;
		std::swap(arr[left], arr[right]);
	}
	std::swap(arr[key], arr[left]);
	return left;
}

//挖坑法
int potholingSort(std::vector<int>& arr, int left, int right)
{
	int cur = left, val = arr[left];
	while (left < right)
	{
		while (left < right && arr[right] >= val) right--;
		arr[cur] = arr[right];
		cur = right;
		while (left < right && arr[left] <= val) left++;
		arr[cur] = arr[left];
		cur = left;
	}
	arr[left] = val;
	return left;
}

//前后指针版
int twopointSort(std::vector<int>& arr, int left, int right)
{
	int prev = left, cur = prev + 1, key = left;
	while (cur <= right)
	{
		if (arr[cur] < arr[key]) std::swap(arr[++prev], arr[cur]);
		cur++;
	}
	// 将基准值交换到它正确的位置
	std::swap(arr[key], arr[prev]);
	return prev; // 返回基准值最终的位置
}

void sort(std::vector<int>& arr, int left, int right)
{
	if (left >= right) return;
	if (right - left < 20) insertSort(arr);
	else
	{
		std::stack<std::vector<int>> st;
		st.push({ left, right });
		//int key = partSort1(arr, left, right);
		//int key = potholingSort(arr, left, right);
		while (st.size())
		{
			auto tmp = st.top();
			st.pop();
			int begin = tmp[0], end = tmp[1];
			//int key = partSort1(arr, begin, end);
			//int key = potholingSort(arr, begin, end);
			int key = twopointSort(arr, begin, end);
			if(begin < key - 1) st.push({ begin, key - 1 });
			if(key + 1 < end) st.push({ key + 1, end });
		}
		//int key = twopointSort(arr, left, right);
		//sort(arr, left, key - 1);
		//sort(arr, key + 1, right);
	}
}

bool quikSort(std::vector<int>& arr)
{
	sort(arr, 0, arr.size() - 1);
	return true;
}


void _Merger(std::vector<int>& arr, int begin, int end, std::vector<int>& tmp)
{
	if (begin >= end) return;

	int mid = (begin + end) / 2;
	_Merger(arr, begin, mid, tmp);
	_Merger(arr, mid + 1, end, tmp);

	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int cur = begin;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2]) tmp[cur++] = arr[begin1++];
		else tmp[cur++] = arr[begin2++];
	}
	while (begin1 <= end1) tmp[cur++] = arr[begin1++];
	while (begin2 <= end2) tmp[cur++] = arr[begin2++];

	memcpy(&arr[begin], &tmp[begin], sizeof(int) * (end - begin + 1));
}


void _MergerNonR(std::vector<int>& arr, std::vector<int>& tmp)
{
	int n = 1;
	while (n < arr.size())
	{
		for (int i = 0; i < arr.size(); i += 2 * n)
		{
			int begin1 = i, end1 = i + n - 1;
			int begin2 = i + n, end2 = i + 2 * n - 1;
			if (begin1 >= arr.size() || begin2 >= arr.size()) break;
			if (end2 >= arr.size()) end2 = arr.size() - 1;

			int cur = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (arr[begin1] < arr[begin2]) tmp[cur++] = arr[begin1++];
				else tmp[cur++] = arr[begin2++];
			}
			while (begin1 <= end1) tmp[cur++] = arr[begin1++];
			while (begin2 <= end2) tmp[cur++] = arr[begin2++];
			memcpy(&arr[i], &tmp[i], sizeof(int) * (end2 - i + 1));
		}
		n *= 2;
	}
}

bool MergerSort(std::vector<int>& arr)
{
	std::vector<int> tmp(arr);
	//_Merger(arr, 0, arr.size() - 1, tmp);
	_MergerNonR(arr, tmp);
	return true;
}
