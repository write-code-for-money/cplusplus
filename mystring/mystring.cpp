#define _CRT_SECURE_NO_WARNINGS 1

#include "mystring.h"
wzh::mystring::mystring(const char* str) //缺省函数声明处写，定义不需要写
{
	_size = strlen(str);
	_capacity = _size;
	_str = new char[_capacity + 1];
	strcpy(_str, str);
}

wzh::mystring::mystring(const mystring& s)
	:_size(0)
	,_capacity(0)
	,_str(nullptr)
{
	mystring tmp(s._str);
	swap(tmp);
}

void wzh::mystring::swap(mystring& s)
{
	std::swap(_str, s._str);
	std::swap(_size, s._size);
	std::swap(_capacity, s._capacity);
}

wzh::mystring::~mystring()
{
	if (_str)
	{
		delete[] _str;
		_str = nullptr;
	}
	_size = _capacity = 0;
}

wzh::mystring& wzh::mystring::operator=(mystring& s)
{
	swap(s);
	return *this;
}

wzh::mystring::iterator wzh::mystring::begin()
{
	return _str;
}

wzh::mystring::iterator wzh::mystring::end()
{
	return _str + _size;
}

void wzh::mystring::resize(size_t newsize, char c)
{
	if (newsize > _size)
	{
		if (newsize > _capacity)
		{
			reserve(newsize);
		}
		memset(_str + _size, c, newsize - _size);
	}
	_size = newsize;
	_str[newsize] = '\0';
}

void wzh::mystring::reserve(size_t newcapacity)
{
	if (newcapacity > _capacity)
	{
		char* str = new char[newcapacity + 1];
		strcpy(str, _str);
		delete[] _str;
		_str = str;
		_capacity = newcapacity;
	}
}

void wzh::mystring::push_back(char c)
{
	if (_size == _capacity) reserve(_capacity * 2);
	_str[_size++] = c;
	_str[_size] = '\0';
}

wzh::mystring& wzh::mystring::operator+=(char c)
{
	push_back(c);
	return *this;
}

wzh::mystring& wzh::mystring::operator+=(const char* str)
{
	append(str);
	return *this;
}


bool wzh::mystring::empty() const
{
	return _size == 0;
}

size_t wzh::mystring::capacity() const
{
	return _capacity;
}

size_t wzh::mystring::size() const
{
	return _size;
}

char& wzh::mystring::operator[](size_t index)
{
	assert(index < _size);
	return _str[index];
}

const char& wzh::mystring::operator[](size_t index) const
{
	assert(index < _size);
	return _str[index];
}


void wzh::mystring::append(const char* str)
{
	for (int i = 0; i < strlen(str); i++)
		push_back(str[i]);
}

void wzh::mystring::clear()
{
	_size = 0;
	_str[_size] = '\0';
}

bool wzh::mystring::operator<(const mystring& s)
{
	if (_size < s.size()) return true;
	if (_size > s.size()) return false;
	for (int i = 0; i < _size; i++)
	{
		if (_str[i] > s[i]) return false;
		if (_str[i] < s[i]) return true;
	}
	return false;
}

bool wzh::mystring::operator==(const mystring& s)
{
	if (_size != s.size()) return false;
	for (int i = 0; i < _size; i++)
	{
		if (_str[i] != s[i]) return false;
	}
	return true;
}


bool wzh::mystring::operator<=(const mystring& s)
{
	return *this < s || *this == s;
}

bool wzh::mystring::operator>(const mystring& s)
{
	return !(*this <= s);
}

bool wzh::mystring::operator>=(const mystring& s)
{
	return *this > s || *this == s;
}

bool wzh::mystring::operator!=(const mystring& s)
{
	return !(*this == s);
}

ostream& wzh::operator<<(ostream& _cout, const wzh::mystring& s)
{
	for (int i = 0; i < s.size(); i++)
		_cout << s[i];
	return _cout;
}


size_t wzh::mystring::find(char c, size_t pos) const
{
	assert(pos >= 0 && pos < _size);
	for (int i = pos; i < _size; i++)
	{
		if (_str[i] == c) return i;
	}
	return -1;
}

size_t wzh::mystring::find(const char* s, size_t pos) const
{
	assert(pos >= 0 && pos < _size);
	int tmp = 0, ti = 0;
	for (int i = pos; i < _size; i++)
	{
		ti = i;
		tmp = 0;
		while (s[tmp] == _str[ti] && tmp < strlen(s) && ti < _size)
		{
			tmp++;
			ti++;
		}
		if (tmp == strlen(s)) return i;
	}
	return -1;
}

wzh::mystring& wzh::mystring::insert(size_t pos, char c)
{
	assert(pos >= 0 && pos < _size);
	if (_size >= _capacity) reserve(_capacity * 2);
	memmove(_str + pos + 1, _str + pos, sizeof(char));
	_str[pos] = c;
	_size++;
	return *this;
}

wzh::mystring& wzh::mystring::insert(size_t pos, const char* str)
{
	assert(pos >= 0 && pos < _size);
	if (_capacity - _size < strlen(str)) reserve(_capacity + strlen(str));
	memcpy(_str + pos + strlen(str), _str + pos, sizeof(char) * (_size - pos));
	memcpy(_str + pos, str, sizeof(char) * strlen(str));
	/*for (int i = 0; i < strlen(str); i++)
		_str[pos++] = str[i];*/
	_size += strlen(str);
	return *this;
}

wzh::mystring& wzh::mystring::erase(size_t pos, size_t len)
{
	assert(pos >= 0 && pos < _size);
	if (pos + len < _size)
	{
		memcpy(_str + pos, _str + pos + len, sizeof(char) * (_size - pos - len));
		_size -= len;
	}
	else
	{
		_str[pos] = '\0';
		_size = pos;
	}
	return *this;
}


