#define _CRT_SECURE_NO_WARNINGS 1

#include "mystring.h"
//using namespace wzh;
int main()
{
	wzh::mystring s1("apple");
	wzh::mystring s2(s1);
	wzh::mystring s3;
	wzh::mystring::iterator it = s1.begin();

	while (it != s1.end())
	{
		cout << *it++ << ' ';
	}
	cout << endl;
	s1.push_back('x');
	cout << s1 << endl;
	s1.append("bear");
	cout << s1 << endl;
	s1 += "KKK";
	cout << s1 << endl;
	for (auto e : s1)
	{
		cout << e << ' ';
	}
	cout << endl;
	cout << s1.insert(0, 'p') << endl;
	cout << s1.insert(3, "TTT") << endl;
	cout << s1.erase(0, 1) << endl;
	cout << s1.erase(2, 1) << endl;
	cout << s1.insert(0, "YYY") << endl;
	return 0;
}