#pragma once
#include <iostream>
#include <assert.h>

using namespace std;
namespace wzh {
	class mystring
	{
	private:
		friend ostream& operator<<(ostream& _cout, const wzh::mystring& s);
	public:
		typedef char* iterator;
	public:
		mystring(const char* str = "");
		mystring(const mystring& s);
		~mystring();
		void swap(mystring& s);
		mystring& operator=(mystring& s);
		iterator begin();
		iterator end();

		char& operator[](size_t index);
		const char& operator[](size_t index) const;

		size_t size() const;
		size_t capacity() const;
		bool empty() const;
		void clear();
		void push_back(char c);
		mystring& operator+=(char c);
		mystring& operator+=(const char* str);
		void append(const char* str);
		void resize(size_t newsize, char c = '\0');
		void reserve(size_t newcapacity);
		
		bool operator<(const mystring& s);
		bool operator<=(const mystring& s);
		bool operator>(const mystring& s);
		bool operator>=(const mystring& s);
		bool operator==(const mystring& s);
		bool operator!=(const mystring& s);

		size_t find(char c, size_t pos = 0) const;
		size_t find(const char* s, size_t pos = 0) const;

		mystring& insert(size_t pos, char c);
		mystring& insert(size_t pos, const char* str);
		mystring& erase(size_t pos, size_t len);

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
}

