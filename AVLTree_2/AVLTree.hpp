#pragma once

#include <cassert>
#include <iostream>
//#include <cstdlib>
using namespace std;
template<class T>
struct AVLTreeNode
{
	AVLTreeNode(const T& data = T())
		: _pLeft(nullptr)
		, _pRight(nullptr)
		, _pParent(nullptr)
		, _data(data)
		, _bf(0)
	{}

	AVLTreeNode<T>* _pLeft;
	AVLTreeNode<T>* _pRight;
	AVLTreeNode<T>* _pParent;
	T _data;
	int _bf;   // 节点的平衡因子
};


// AVL: 二叉搜索树 + 平衡因子的限制
template<class T>
class AVLTree
{
	typedef AVLTreeNode<T> Node;
public:
	AVLTree()
		: _pRoot(nullptr)
	{}

	// 在AVL树中插入值为data的节点
	bool Insert(const T& data)
	{
		if (_pRoot == nullptr)
		{
			_pRoot = new Node(data);
			return true;
		}
		//找插入位置
		Node* parent = nullptr, *cur = _pRoot;
		while (cur)
		{
			if (cur->_data < data)
			{
				parent = cur;
				cur = cur->_pRight;
			}
			else if (cur->_data > data)
			{
				parent = cur;
				cur = cur->_pLeft;
			}
			else
			{
				return false;
			}
		}
		cur = new Node(data);
		//开始插入
		if (parent->_data < data)
		{
			parent->_pRight = cur;
			cur->_pParent = parent;
		}
		else
		{
			parent->_pLeft = cur;
			cur->_pParent = parent;
		}
		//更新平衡因子 + 旋转
		while (parent)
		{
			if (parent->_pLeft == cur) parent->_bf--;
			else parent->_bf++;

			if (parent->_bf == 0) return true;
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = cur->_pParent;
				parent = parent->_pParent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateR(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
				}
				else
				{
					assert(0);
				}
				break;
			}
			else
			{
				assert(0);
			}
		}
		return true;
	}

	// AVL树的验证
	bool IsAVLTree()
	{
		return _IsAVLTree(_pRoot);
	}

	void InOrder()
	{
		_InOrder(_pRoot);
		cout << endl;
	}

	int Height()
	{
		return _Height(_pRoot);
	}

private:
	void _InOrder(Node* pRoot)
	{
		if (pRoot == nullptr) return;
		_InOrder(pRoot->_pLeft);
		cout << pRoot->_data << ' ';
		_InOrder(pRoot->_pRight);
	}

	// 根据AVL树的概念验证pRoot是否为有效的AVL树
	bool _IsAVLTree(Node* pRoot)
	{
		int left = _Height(pRoot->_pLeft);
		int right = _Height(pRoot->_pRight);
		if (abs(left - right) > 1)
			return false;
		else
			return true;
	}
	size_t _Height(Node* pRoot)
	{
		if (pRoot == nullptr) return 0;
		int leftsz = _Height(pRoot->_pLeft) + 1;
		int rightsz = _Height(pRoot->_pRight) + 1;
		return max(leftsz, rightsz);
	}
	// 右单旋
	void RotateR(Node* pParent)
	{
		Node* RotaL = pParent->_pLeft;
		Node* RotaLR = RotaL->_pRight;
		Node* ppnode = pParent->_pParent;

		pParent->_pLeft = RotaLR;
		if(RotaLR) RotaLR->_pParent = pParent;	

		RotaL->_pRight = pParent;
		pParent->_pParent = RotaL;
		RotaL->_pParent = ppnode;

		if (ppnode == nullptr)
		{
			_pRoot = RotaL;
		}
		else
		{
			if (ppnode->_pLeft == pParent) ppnode->_pLeft = RotaL;
			else ppnode->_pRight = RotaL;
		}
		pParent->_bf = RotaL->_bf = 0;
	}
	// 左单旋
	void RotateL(Node* pParent)
	{
		Node* RatoR = pParent->_pRight;
		Node* RatoRL = RatoR->_pLeft;
		Node* ppnode = pParent->_pParent;

		pParent->_pRight = RatoRL;
		if (RatoRL) RatoRL->_pParent = pParent;
		RatoR->_pLeft = pParent;
		pParent->_pParent = RatoR;

		if (ppnode == nullptr)
		{
			_pRoot = RatoR;
			RatoR->_pParent = nullptr;
		}
		else
		{
			if (ppnode->_pLeft == pParent) ppnode->_pLeft = RatoR;
			else ppnode->_pRight = RatoR;
			RatoR->_pParent = ppnode;
		}
		pParent->_bf = RatoR->_bf = 0;
	}
	// 右左双旋
	void RotateRL(Node* pParent)
	{
		Node* RatoR = pParent->_pRight;
		Node* RatoRL = RatoR->_pLeft;
		int bf = RatoRL->_bf;

		RotateR(pParent->_pRight);
		RotateL(pParent);

		if (bf == 1)
		{
			RatoR->_bf = 0;
			pParent->_bf = -1;
			RatoRL->_bf = 0;
		}
		else if (bf == -1)
		{
			RatoR->_bf = 1;
			pParent->_bf = 0;
			RatoRL->_bf = 0;
		}
		else if (bf == 0)
		{
			pParent->_bf = RatoR->_bf = RatoRL->_bf = 0;
		}
		else
		{
			assert(0);
		}
	}
	// 左右双旋
	void RotateLR(Node* pParent)
	{
		Node* RatoL = pParent->_pLeft;
		Node* RatoLR = RatoL->_pRight;
		int bf = RatoLR->_bf;

		RotateL(pParent->_pLeft);
		RotateR(pParent);

		if (bf == -1)
		{
			pParent->_bf = 1;

			RatoL->_bf = RatoLR->_bf = 0;
		}
		else if (bf == 1)
		{
			pParent->_bf = RatoLR->_bf = 0;
			RatoL->_bf = -1;
		}
		else if (bf == 0)
		{
			pParent->_bf = RatoL->_bf = RatoLR->_bf = 0;
		}
		else
		{
			assert(0);
		}

	}

private:
	Node* _pRoot;
};
