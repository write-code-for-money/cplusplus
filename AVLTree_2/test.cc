#define _CRT_SECURE_NO_WARNINGS 1

#include "AVLTree.hpp"

void Test_AVLTree1()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	AVLTree<int> t1;
	for (auto e : a)
	{
		/*	if (e == 14)
			{
			int x = 0;
			}*/

		t1.Insert(e);
		cout << e << "���룺" << t1.IsAVLTree() << endl;
	}

	t1.InOrder();
	cout << t1.IsAVLTree() << endl;
}

void Test_AVLTree2()
{
	srand(time(0));
	const size_t N = 50;
	AVLTree<int> t;
	for (size_t i = 0; i < N; ++i)
	{
		size_t x = rand() + i;
		t.Insert(x);
		//cout << t.IsAVLTree() << endl;
	}

	t.InOrder();

	cout << t.IsAVLTree() << endl;
	cout << t.Height() << endl;
}

int main()
{
	Test_AVLTree2();
	return 0;
}