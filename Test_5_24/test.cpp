#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
using namespace std;

//数组中出现次数超过一半的数字
//class Solution {
//public:
//    int MoreThanHalfNum_Solution(vector<int> numbers) {
//        int cur = 0;
//        int flag = 0;
//        for (int i = 0; i < numbers.size(); i++)
//        {
//            if (flag == 0)
//            {
//                cur = i;
//                flag++;
//            }
//            else {
//                if (numbers[cur] == numbers[i]) ++flag;
//                else --flag;
//            }
//        }
//        return numbers[cur];
//    }
//};

//删除有序数组的重复项
// 
// class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        if (nums.size() == 0) return 0;
//        int sum = 1;
//        vector<int>::iterator begin = nums.begin();
//        vector<int>::iterator end = begin + 1;
//        while (end != nums.end())
//        {
//            if (*begin == *end)
//            {
//                end++;
//            }
//            else {
//                begin++;
//                sum++;
//                if (begin != end)
//                {
//                    nums.erase(begin, end);
//                    end = begin + 1;
//                }
//                else {
//                    end++;
//                }
//            }
//        }
//        return sum;
//    }
//};
// 
//class Solution {
//public:
//	int removeDuplicates(vector<int>& nums) {
//		if (nums.size() < 2) return nums.size();
//		int j = 0;
//		for (int i = 1; i < nums.size(); i++)
//			if (nums[j] != nums[i]) nums[++j] = nums[i];
//		return ++j;
//	}
//};

//只出现一次的数字||
//
//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int a = 0, b = 0;
//        for (auto x : nums) {
//            b = (b ^ x) & ~a;
//            a = (a ^ x) & ~b;
//        }
//        return b;
//    }
//};
//
//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i < 32; i++) {
//            int mask = 1 << i;
//            int cnt = 0;
//            for (int j = 0; j < nums.size(); j++) {
//                if ((nums[j] & mask) != 0) {
//                    cnt++;
//                }
//            }
//            if (cnt % 3 != 0) {
//                ret |= mask;
//            }
//        }
//        return ret;
//
//    }
//};


//电话号码字母组合
//class Solution {
//public:
//    vector<string> ret;
//    vector<string> v = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
//    string tmp;
//    vector<string> recursion(int pos, string digits)
//    {
//        if (pos == digits.size())
//        {
//            ret.push_back(tmp);
//            return ret;
//        }
//        int n = digits[pos] - '0';
//        for (int i = 0; i < v[n].size(); i++)
//        {
//            tmp.push_back(v[n][i]);
//            recursion(pos + 1, digits);
//            tmp.pop_back();
//        }
//        return ret;
//    }
//    vector<string> letterCombinations(string digits) {
//        if (digits.size() == 0) return ret;
//        int pos = 0;
//        recursion(pos, digits);
//        return ret;
//    }
//};

//只出现一次的数字III
//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int n = 0;
//        for (auto e : nums) n ^= e;
//        int i = 0;
//        for (; i < 31; i++)
//        {
//            if (n >> i & 1) break;
//        }
//        int x = 0, y = 0;
//        for (auto e : nums)
//        {
//            if (e >> i & 1) x ^= e;
//            else y ^= e;
//        }
//        vector<int> v = { x, y };
//        return v;
//    }
//};

//杨辉三角
//class Solution {
//public:
//    vector<vector<int>> generate(int numRows) {
//        vector<vector<int>> vv(numRows);
//        for (int i = 0; i < numRows; i++)
//        {
//            //vv.reserve(numRows);
//            for (int j = 0; j <= i; j++)
//            {
//                vv[i].resize(i + 1);
//                if (j == 0 || j == i)
//                    vv[i][j] = 1;
//                else {
//                    vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
//                }
//                //cout << vv[i][j] << ' ';
//            }
//        }
//        //cout << vv[0][0] << ' ';
//        return vv;
//    }
//};


//只出现一次的数字
//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int n = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            n ^= nums[i];
//        }
//        return n;
//    }
//};

//int main()
//{
//	int ar[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int n = sizeof(ar) / sizeof(int);
//	vector<int> v(ar, ar + n);
//	cout << v.size() << ":" << v.capacity() << endl;
//	v.reserve(100);
//	v.resize(20);
//	cout << v.size() << ":" << v.capacity() << endl;
//	v.reserve(50);
//	v.resize(5);
//	cout << v.size() << ":" << v.capacity() << endl;
//}