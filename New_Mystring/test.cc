#include "string.h"

//void test1()
//{
//	wzh::string s1;
//	wzh::string s2("abcd");
//	wzh::string s3(s2);
//
//	s1.print();
//	s2.print();
//	s3.print();
//	std::cout << s2.size() << " " << s2.capacity() << std::endl;
//}
//
//
//void test2()
//{
//	wzh::string s1("abcdefg");
//	std::cout << s1.capacity() << std::endl;
//
//	s1.reserve(10);
//	//s1.reserve(2);
//
//	std::cout << s1.capacity() << std::endl;
//}
//
//void test3()
//{
//	wzh::string s1("qcscsd");
//	s1.print();
//	std::cout << s1.size() << "  " <<  s1.capacity() << std::endl;
//	s1.resize(1);
//	s1.print();
//	std::cout << s1.size() << "  " <<  s1.capacity() << std::endl;
//	s1.resize(15, 'x');
//	s1.print();
//
//	std::cout << s1.size() << "  " <<  s1.capacity() << std::endl;
//
//}
//
//void test4()
//{
//	wzh::string s1("12345");
//	s1.print();
//	std::cout << s1.size() << "  " << s1.capacity() << std::endl;
//	std::cout << s1.empty() << std::endl;
//	s1.clear();
//	s1.print();
//	std::cout << s1.empty() << std::endl;
//	std::cout << s1.size() << "  " << s1.capacity() << std::endl;
//
//}
//
//void test5()
//{
//	wzh::string s1("1234556");
//	const wzh::string s2("1234556");
//	//s2[1] = 10;
//	std::cout << s2[1] << std::endl;
//
//
//	s1[1] = 10;
//	std::cout << s1[1] << std::endl;
//	std::cout << s1[0] << std::endl;
//	std::cout << s1[9] << std::endl;
//	std::cout << s1[4] << std::endl;
//}
//
//void test6()
//{
//	wzh::string s1("abcd");
//	//s1 += "1234";
//	s1.print();
//	/*s1.insert(0, "abc");
//	s1.print();*/
//
//	s1.insert(4, "abc");
//	s1.print();
//
//	s1.insert(2, "abc");
//	s1.print();
//
//	s1.print();
//
//}
//
//void test7()
//{
//	wzh::string s1("abcd");
//	s1.erase(2, 1);
//	s1.print(); 
//	s1.erase(0, 2);
//	s1.print();
//}

void test8()
{
	wzh::string s1("123456");
	wzh::string::iterator it = s1.begin();
	while (it != s1.end())
	{
		std::cout << *it << ' ';
		it++;
	}
	std::cout << std::endl;
	/*for (char ch : s1)
	{
		std::cout << ch << std::endl;
	}*/
}

void test9()
{
	std::string s1 = "1234456 789";
	wzh::string s2 = "abcd efg";
	s2[4] = '\0';
	std::cout << s2 << std::endl;
	std::cout << s2.c_str() << std::endl;
	s1[6] = '\0';
	std::cout << s1.c_str() << std::endl;
	std::cout << s1 << std::endl;
}

void test10()
{
	//std::string s1;
	//std::cin >> s1;
	//std::cout << s1 << std::endl;

	wzh::string s2("15454575");
	wzh::string s3(s2);
	//s2.push_back('5');
	//std::cin >> s2;
	std::cout << s2 << std::endl;
}

int main()
{
	test10();
	return 0;
}