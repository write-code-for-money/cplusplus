#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <cassert>
#define npos -1
namespace wzh
{
	class string
	{
		friend std::ostream& operator<< (std::ostream& os, const string& str);
		friend std::istream& operator>> (std::istream& is, string& str);
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
	public:
		//////////////////////
		string(const char* ptr = "");//默认构造
		~string();                   //析构
		string(const string& str);   //拷贝构造
		/////////  Capacity:
		size_t size() const;  
		size_t capacity() const;
		void reserve(size_t n = 0);
		void resize(size_t n, char c = '\0');
		void clear();
		bool empty() const;
		// Element access:
		char& operator[] (size_t pos);
		const char& operator[] (size_t pos) const;
		const char* c_str() const;
		// Modifiers:
		string& operator+= (const string& str);
		string& insert(size_t pos, const string& str);
		string& erase(size_t pos = 0, size_t len = npos);
		void push_back(char c);
		// iterator
		iterator begin();
		const_iterator begin() const;
		iterator end();
		const_iterator end() const;

	private:
		char* _ptr;
		int _capacity;
		int _size;
	};

	void string::push_back(char c)
	{
		if (_size == _capacity)
			reserve(_size == 0 ? 4 : 2 * _size);
		_ptr[_size++] = c;
		_ptr[_size] = '\0';
	}

	std::istream& operator>> (std::istream& is, string& str)
	{
		char ch;
		is.get(ch);
		while (ch != '\n' && ch != ' ')
		{
			str.push_back(ch);
			is.get(ch);
		}
		return is;
	}
	std::ostream& operator<< (std::ostream& os, const string& str)
	{
		for (auto e : str)
			os << e;
		return os;
	}

	const char* string::c_str() const
	{

		return _ptr;
	}

	string::iterator string::end()
	{
		return _ptr + _size;
	}

	string::const_iterator string::end() const
	{
		return _ptr + _size;
	}

	string::const_iterator string::begin() const
	{
		return _ptr;
	}

	string::iterator string::begin()
	{
		return _ptr;
	}


	string& string::erase(size_t pos, size_t len)
	{
		assert(pos >= 0 && pos < _size);
		if (len >= _size - pos)
		{
			_ptr[pos] = '\0';
			_size = 0;
		}
		else
		{
			size_t cur = pos + len;
			while (cur <= _size)
			{
				_ptr[pos++] = _ptr[cur++];
			}
			_size -= len;
		}		
		return *this;
	}

	string& string::insert(size_t pos, const string& str)
	{
		assert(pos >= 0 && pos <= _capacity);
		int sz = _size + str._size;
		if (sz > _capacity)
			reserve(sz);
		size_t cur = _size + 1;
		while (cur > pos)
		{
			_ptr[cur + str._size - 1] = _ptr[cur - 1];
			cur--;
		}
		memcpy(_ptr + pos, str._ptr, str._size);
		_size += str._size;
		return *this;
	}


	string& string::operator+= (const string& str)
	{
		size_t sz = _size + str._size;
		if (sz > _capacity)
			reserve(sz);
		strcpy(_ptr + _size, str._ptr);
		_size = sz;
		return *this;
	}


	const char& string::operator[] (size_t pos) const
	{
		assert(pos >= 0 && pos < _size);
		return _ptr[pos];
	}

	char& string::operator[] (size_t pos)
	{
		assert(pos >= 0 && pos < _size);
		return _ptr[pos];
	}

	bool string::empty() const
	{
		return _size == 0 ? true : false;
	}

	void string::clear()
	{
		if (_ptr)
		{
			resize(0);
			_capacity = 0;
		}
	}

	void string::resize(size_t n, char c)
	{
		if (n <= _capacity)
		{
			_ptr[n] = '\0';
			_size = n;
		}
		else
		{
			reserve(n);
			memset(_ptr + _size, c, n - _size);
			_size = n;
		}
	}

	void string::reserve(size_t n)
	{
		if (n > _capacity)
		{
			char* tmp = new char[n + 1];
			strcpy(tmp, _ptr);
			delete[] _ptr;
			_ptr = tmp;
			_capacity = n;
		}
	}

	size_t string::capacity() const
	{
		return _capacity;
	}

	size_t string::size() const
	{
		return _size;
	}

	string::string(const string& str)
		:_capacity(str._capacity)
		, _size(str._size)
	{
		char* tmp = new char[_capacity + 1];
		_ptr = strcpy(tmp, str._ptr);
	}

	string::string(const char* ptr)
		:_capacity(strlen(ptr))
	{
		char* tmp = new char[_capacity + 1];	
		_ptr = strcpy(tmp, ptr);
		_size = _capacity;
	}

	string::~string()
	{
		if (_ptr) delete[] _ptr;
		_capacity = _size = 0;
	}
}