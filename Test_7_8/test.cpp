﻿#include "mylist.h"


int main()
{
    wzh::list<int> l;
    for (int i = 0; i < 5; i++)
    {
        l.push_back(i);
    }
    cout << "List: ";
    for (wzh::list<int>::iterator it = l.begin(); it != l.end(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}






//#include <iostream>﻿
//#include <vector>
//#include <list>
//using namespace std;
//
//namespace bit
//{
//	// 将以下代码适配到vector和list中做反向迭代器，理解反向迭代器的原理
//	// 适配器 -- 复用
//	template<class Iterator, class Ref, class Ptr>
//	struct Reverse_iterator
//	{
//		Iterator _it;
//		typedef Reverse_iterator<Iterator, Ref, Ptr> Self;
//
//		Reverse_iterator(Iterator it)
//			:_it(it)
//		{}
//
//		Ref operator*()
//		{
//			Iterator tmp = _it;
//			return *(--tmp);
//		}
//
//		Ptr operator->()
//		{
//			return &(operator*());
//		}
//
//		Self& operator++()
//		{
//			--_it;
//			return *this;
//		}
//
//		Self& operator--()
//		{
//			++_it;
//			return *this;
//		}
//
//		bool operator!=(const Self& s)
//		{
//			return _it != s._it;
//		}
//	};
//
//	template<class T>
//	class vector
//	{
//	public:
//		typedef T* iterator;
//		typedef const T* const_iterator;
//
//		// 反向迭代器适配支持
//		typedef Reverse_iterator<iterator, T&, T*> reverse_iterator;
//		typedef Reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;
//
//		const_reverse_iterator rbegin() const
//		{
//			// list_node<int>*
//			return const_reverse_iterator(end());
//		}
//
//		const_reverse_iterator rend() const
//		{
//			return const_reverse_iterator(begin());
//		}
//
//		reverse_iterator rbegin()
//		{
//			return reverse_iterator(end());
//		}
//
//		reverse_iterator rend()
//		{
//			return reverse_iterator(begin());
//		}
//
//		iterator begin()
//		{
//			return _start;
//		}
//
//		iterator end()
//		{
//			return _finish;
//		}
//
//		const_iterator begin() const
//		{
//			return _start;
//		}
//
//		const_iterator end() const
//		{
//			return _finish;
//		}
//		// ...
//	};
//}
//
//	
//
//
////// 将以下代码适配到vector和list中做反向迭代器，理解反向迭代器的原理
////namespace wzh
////{
////	// 适配器 -- 复用
////	template<class Iterator, class Ref, class Ptr>
////	struct Reverse_iterator
////	{
////		Iterator _it;
////		Reverse_iterator(Iterator it = nullptr)
////			:_it(it)
////		{
////
////		}
////
////		Reverse_iterator& operator++()
////		{
////			--_it;
////			return *this;
////		}
////
////		Reverse_iterator& operator--()
////		{
////			++_it;
////			return *this;
////		}
////
////		Reverse_iterator& operator*()
////		{
////			return *_it;
////		}
////
////	};
////	// vector和list反向迭代器实现
////	
////}
////int main()
////{
////	vector<int> arr{1, 2, 3, 4, 5, 6};
////	wzh::Reverse_iterator<int> rit = arr.begin();
////	for (int i = 0; i < 6; i++)
////	{
////		cout << *rit << ' ';
////	}
////}
//
//
////int main()
////{
////	int ar[] = { 0,1, 2, 3, 4,  5, 6, 7, 8, 9 };
////	int n = sizeof(ar) / sizeof(int);
////	list<int> mylist(ar, ar + n);
////	list<int>::iterator pos = find(mylist.begin(), mylist.end(), 5);
////	reverse(mylist.begin(), pos);
////	reverse(pos, mylist.end());
////	list<int>::const_reverse_iterator crit = mylist.crbegin();
////	while (crit != mylist.crend())
////	{
////		cout << *crit << " ";
////		++crit;
////	}
////	cout << endl;
////}