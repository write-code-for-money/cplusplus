#include<iostream>
using namespace std;


namespace wzh
{
    template<class T>
    struct ListNode
    {
        ListNode(const T& val = T()) : _pPre(nullptr), _pNext(nullptr), _val(val) {}
        ListNode<T>* _pPre;
        ListNode<T>* _pNext;
        T _val;
    };

    template<class T, class Ref, class Ptr>
    class ListIterator
    {
        typedef ListNode<T>* PNode;
        typedef ListIterator<T, Ref, Ptr> Self;
    public:
        ListIterator(PNode pNode = nullptr) : _pNode(pNode) {}
        ListIterator(const Self& l) : _pNode(l._pNode) {}
        T& operator*() { return _pNode->_val; }
        T* operator->() { return &(_pNode->_val); }
        Self& operator++()
        {
            _pNode = _pNode->_pNext;
            return *this;
        }
        Self operator++(int)
        {
            Self tmp(*this);
            _pNode = _pNode->_pNext;
            return tmp;
        }
        Self& operator--()
        {
            _pNode = _pNode->_pPre;
            return *this;
        }
        Self& operator--(int)
        {
            Self tmp(*this);
            _pNode = _pNode->_pPre;
            return tmp;
        }
        bool operator!=(const Self& l)
        {
            return _pNode != l._pNode;
        }
        bool operator==(const Self& l)
        {
            return _pNode == l._pNode;
        }

        PNode _pNode;
    };


    template<class T>
    class list
    {
        typedef ListNode<T> Node;
        typedef Node* PNode;
    public:
        typedef ListIterator<T, T&, T*> iterator;
        typedef ListIterator<T, const T&, const T&> const_iterator;
    public:
        list()
        {
            CreateHead();
        }

        list(int n, const T& value = T())
        {
            CreateHead();
            while (n--)
            {
                push_back(value);
            }
        }

        template <class Iterator>
        list(Iterator first, Iterator last)
        {
            CreateHead();
            while (first != last)
            {
                push_back(*first);
                ++first;
            }
        }

        list(const list<T>& l)
        {
            CreateHead();
            for (auto it = l.begin(); it != l.end(); ++it)
            {
                push_back(*it);
            }
        }

        list<T>& operator=(const list<T> l)
        {
            if (this != &l)
            {
                list<T> tmp(l);
                swap(tmp);
            }
            return *this;
        }

        ~list()
        {
            clear();
            delete _pHead;
        }

        iterator begin()
        {
            return iterator(_pHead->_pNext);
        }

        iterator end()
        {
            return iterator(_pHead);
        }

        const_iterator begin() const
        {
            return const_iterator(_pHead->_pNext);
        }

        const_iterator end() const
        {
            return const_iterator(_pHead);
        }

        size_t size() const
        {
            size_t count = 0;
            for (auto it = begin(); it != end(); ++it)
            {
                ++count;
            }
            return count;
        }

        bool empty() const
        {
            return _pHead->_pNext == _pHead;
        }

        T& front()
        {
            return *begin();
        }

        const T& front() const
        {
            return *begin();
        }

        T& back()
        {
            return *(--end());
        }

        const T& back() const
        {
            return *(--end());
        }

        void push_back(const T& val)
        {
            insert(end(), val);
        }

        void pop_back()
        {
            erase(--end());
        }

        void push_front(const T& val)
        {
            insert(begin(), val);
        }

        void pop_front()
        {
            erase(begin());
        }

        iterator insert(iterator pos, const T& val)
        {
            Node* cur = new Node(val);
            Node* posNode = pos._pNode;
            Node* prevNode = posNode->_pPre;

            prevNode->_pNext = cur;
            cur->_pNext = posNode;
            cur->_pPre = prevNode;
            posNode->_pPre = cur;

            return iterator(cur);
        }

        iterator erase(iterator pos)
        {
            Node* posNode = pos._pNode;
            Node* prevNode = posNode->_pPre;
            Node* nextNode = posNode->_pNext;

            prevNode->_pNext = nextNode;
            nextNode->_pPre = prevNode;

            delete posNode;
            return iterator(nextNode);
        }

        void clear()
        {
            while (!empty())
            {
                pop_front();
            }
        }

        void swap(list<T>& l)
        {
            std::swap(_pHead, l._pHead);
        }
    private:
        void CreateHead()
        {
            _pHead = new Node;
            _pHead->_pPre = _pHead;
            _pHead->_pNext = _pHead;
        }
        PNode _pHead;
    };
};
