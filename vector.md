**参考文献：[cplusplus]([vector - C++ Reference (cplusplus.com)](https://legacy.cplusplus.com/reference/vector/vector/?kw=vector))**



#   vector简介

1. vector是表示可变大小数组的序列容器。
2. 和数组类似，vector也采用的连续存储空间来存储元素。可以采用下标对vector的元素进行访问。与数组不同的是，它的大小是可以动态改变的，而且它的大小会被容器自动处理。
3. vector会分配一些额外的空间以适应可能的增长，因为存储空间比实际需要的存储空间更大。不同的库采用不同的策略权衡空间的使用和重新分配。但是无论如何，重新分配都应该是对数增长的间隔大小，以至于在末尾插入一个元素的时候是在常数时间的复杂度完成的。
4. 与其它动态序列容器相比，vector在访问元素的时候更加高效，在末尾添加和删除元素相对高效。对于其它不在末尾的删除和插入操作，效率更低。比起`list`和`forward_list` 统一的迭代器和引用更好。



# vector使用

![image-20230521093021500](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521093021500.png)

vector的使用需要包含头文件vector。

## vector的定义

`vector()`：无参构造函数。

`vector(size_type n, const value_type& val = value_type())`：构造并初始化n个val。

`vector (const vector& x)`：拷贝构造。

`vector (InputIterator first, InputIterator last)`：使用迭代器进行初始化构造。

示例代码：

```cpp
#include <iostream>
#include <vector>

using namespace std;
void Print(vector<int>& arr)
{
	for (auto e : arr)
	{
		cout << e << ' ';
	}
	cout << endl;
}
int main()
{
	vector<int> arr;  
	vector<int> arr2(5, 3);
	vector<int> arr3(arr2);
	vector<int> arr4(arr2.begin(), arr2.end());
	Print(arr);
	Print(arr2);
	Print(arr3);
	Print(arr4);
	return 0;
}
```

运行结果：

![image-20230521100122145](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521100122145.png)

监视窗口：

![image-20230521100226347](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521100226347.png)

## vector iterator 的使用

`begin`：获取第一个数据位置的iterator/const_iterator。

```cpp
iterator begin();
const_iterator begin() const;
```

`end`： 获取最后一个数据的下一个位置的iterator/const_iterator。

```cpp
 iterator end();
const_iterator end() const;
```

> 值得注意的是，迭代器大多都是左闭右开的区间，也就是说begin是获取第一个数据的位置，而end是获取最后一个数据的下一个位置，如下图所示：
>
> ![image-20230521103503981](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521103503981.png)

示例代码：

```cpp
#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector<int> arr;
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(4);
	arr.push_back(5);
	vector<int>::iterator it = arr.begin();
	while (it != arr.end())
	{
		cout << *it << ' ';
		it++;
	}
	cout << endl;
	return 0;
}
```

运行结果：

![image-20230521102237584](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521102237584.png)

---

`rbegin`：获取最后一个数据位置的reverse_iterator。

```cpp
reverse_iterator rbegin();
const_reverse_iterator rbegin() const;
```

`rend()`：获取第一个数据前一个位置的 reverse_iterator。

```cpp
reverse_iterator rend();
const_reverse_iterator rend() const;
```

> ![image-20230521103526103](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521103526103.png)

示例代码：

```cpp
#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector<int> arr;
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(4);
	arr.push_back(5);
	vector<int>::reverse_iterator rit = arr.rbegin();
	while (rit != arr.rend())
	{
		cout << *rit << ' ';
		rit++;
	}
	cout << endl;
	return 0;
}
```

运行结果：

![image-20230521103223242](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521103223242.png)

## vector 空间函数

`size`：获取数据个数。

```cpp
size_type size() const;
```

`capacity`：获取容量大小。

```cpp
size_type capacity() const;
```

`empty`：判断是否为空。

```cpp
bool empty() const;
```

`resize`：改变vector的size。

```cpp
void resize (size_type n, value_type val = value_type());
```

`reserve`：改变vector的容量。

```cpp
void reserve (size_type n);
```

> - resize在开空间的同时还会进行初始化，会影响size。
> - reserve只负责开辟空间，如果确定知道需要用多少空间，reserve可以缓解vector增容的代价缺陷问题。
> - vector的具体增长多少是根据具体的需求定义的。（**vs下是按1.5倍增长的，g++是按2倍增长的**）

示例代码：

```cpp
#include <iostream>
#include <vector>

using namespace std;

void Print(vector<int>& arr)
{
	cout << "size : " << arr.size() << endl << "capacity : " << arr.capacity() << endl;
	for (auto e : arr)
	{
		cout << e << ' ';
	}
	cout << endl << "#######################" << endl;
}
int main()
{
	vector<int> arr;
	if (arr.empty())
	{
		cout << "arr中没有数据" << endl;
	}
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(4);
	arr.push_back(5);
	Print(arr);
	arr.resize(10, 5);
	Print(arr);
	arr.reserve(20);
	Print(arr);
	return 0;
}
```

运行结果：

![image-20230521111649022](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521111649022.png)

---

### vector的扩容问题

VS下扩容：

```cpp
#include <iostream>
#include <vector>

using namespace std;

void Print(vector<int>& arr)
{
	cout << "size : " << arr.size() << endl << "capacity : " << arr.capacity() << endl;
	cout << "#######################" << endl;
}

int main()
{
	vector<int> arr;
	for (int i = 0; i < 20; i++)
	{
		if(arr.size() == arr.capacity())
			Print(arr);

		arr.push_back(i);
	}
	return 0;
}
```

运行代码：

![image-20230521112828295](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521112828295.png)

从结果来看，vs下是1.5倍扩容，当把相同的代码放到g++下运行，结果如下：

![image-20230521113453438](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521113453438.png)

可以看出g++下是呈2倍进行扩容。

## vector 增删查改

`push_back`：尾插。

```cpp
void push_back (const value_type& val);
```

`pop_back`：尾删。

```cpp
void pop_back();
```

`insert`：在position之前插入val。

```cpp
//在position位置插入val。
iterator insert (iterator position, const value_type& val);
	//在position位置插入n个val。
    void insert (iterator position, size_type n, const value_type& val);
//在position位置之前插入first到last整个区间的数据
template <class InputIterator>
    void insert (iterator position, InputIterator first, InputIterator last);

```

`erase`：删除position位置的数据。

```cpp
iterator erase (iterator position);
iterator erase (iterator first, iterator last);
```

`swape`：交换两个vector的数据空间。

```cpp
void swap (vector& x);
```

`operator[]`：重载[]像数组一样访问。

```cpp
reference operator[] (size_type n);
const_reference operator[] (size_type n) const;
```

`find`：在一段区间内查找数据。（find不是vector的成员接口）

```cpp
template <class InputIterator, class T>
   InputIterator find (InputIterator first, InputIterator last, const T& val);
```

示例代码：

```cpp
#include <iostream>
#include <vector>

using namespace std;
void Print(vector<int>& arr)
{
	for (int i = 0; i < arr.size(); i++)
	{
		cout << arr[i] << ' ';
	}
	cout << endl << "#######################" << endl;
}

int main()
{
	vector<int> arr;
	arr.push_back(1);
	arr.push_back(2);
	arr.push_back(3);
	arr.push_back(4);
	arr.push_back(5);
	Print(arr);
	arr.pop_back();
	arr.pop_back();
	Print(arr);
	//在arr.begin() + 2的位置插入一个10.
	arr.insert(arr.begin() + 2, 10);
	Print(arr);
	//在arr.begin + 1的位置插入2个20。
	arr.insert(arr.begin() + 1, 2, 20);
	Print(arr);
	//在arr.begin() + 1的位置之前插入arr.begin到arr.end整个区间的数据
	arr.insert(arr.begin() + 1, arr.begin(), arr.end());
	Print(arr);
	arr.erase(arr.begin() + 2);
	Print(arr);
	arr.erase(arr.begin(), arr.begin() + 3);
	Print(arr);
	vector<int>::iterator it = find(arr.begin(), arr.end(), 20);
	if (it != arr.end())
		cout << "find success." << endl << "###################" << endl;
	vector<int> arr_swap(5, 10);
	arr.swap(arr_swap);
	Print(arr);
	return 0;
}
```

运行结果：
![image-20230521123707808](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521123707808.png)

## vector 迭代器失效问题

迭代器的主要作用就是让算法能够不用关心底层数据结构，其底层实际就是一个指针，或者是对指针进行封装，比如：vector的迭代器就是原生态指针T* 。因此迭代器失效，实际就是迭代器底层对应指针所指向的空间被销毁了，而使用一块已经被释放的空间，造成的后果是程序崩溃(即如果继续使用已经失效的迭代器， 程序可能会崩溃)。

对于vector可能会导致其迭代器失效的操作有：

1. 会引起其底层空间改变的操作，都有可能是迭代器失效，比如：resize、reserve、insert、assign、 push_back等。

```cpp
#include <iostream>
#include <vector>

using namespace std;

int main() 
{
	vector<int> arr{ 1, 2, 3, 4, 5 };
	vector<int>::iterator it = arr.begin();
	arr.resize(100, 0);
	//arr.reserve(100);
	while (it != arr.end())
	{
		cout << *it << ' ';
        it++;
	}
	cout << endl;

	return 0;
}
```

运行结果：

![image-20230521124830097](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521124830097.png)

**出错原因：**因为以上操作都有可能会导致vector扩容，也就是说vector底层原来的旧空间被释放掉， 而在打印时，it还使用的是释放之间的旧空间，在对it迭代器操作时，实际操作的是一块已经被释放的空间，而引起代码运行时崩溃。 

---

2. 指定位置元素的删除操作--erase

erase删除pos位置元素后，pos位置之后的元素会往前搬移，没有导致底层空间的改变，理论上讲迭代器不应该会失效，但是如果pos刚好是最后一个元素，删完之后pos刚好是end的位置，而end位置是没有元素的，那么pos就失效了。因此删除vector中任意位置上元素时，vs就认为该位置迭代器失效了。

```cpp
#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector<int> arr{ 1, 2, 3, 4, 5 };
	vector<int>::iterator it = arr.begin();
	arr.erase(it);
	while (it != arr.end())
	{
		cout << *it << ' ';
		it++;
	}
	cout << endl;
	return 0;
}
```

运行结果：

![image-20230521125742924](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230521125742924.png)

**迭代器失效解决办法：在使用前，对迭代器重新赋值即可。**
