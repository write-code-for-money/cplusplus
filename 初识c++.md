# C++命名空间

## 存在原因

在C/C++中，变量、函数和后面要学到的类都是大量存在的，这些变量、函数和类的名称将都存在于全局作用域中，可能会导致很多冲突。使用命名空间的目的是对标识符的名称进行本地化， 以避免命名冲突或名字污染，`namespace`关键字的出现就是针对这种问题的。

```cpp
#include <stdio.h>
#include <stdlib.h>
int rand = 10;
// C语言没办法解决类似这样的命名冲突问题，所以C++提出了namespace来解决
int main()
{
 printf("%d\n", rand);
return 0;
}
// 编译后后报错：error C2365: “rand”: 重定义；以前的定义是“函数”
```

## 命名空间定义

定义命名空间，需要使用到`namespace`关键字，后面跟命名空间的名字，然后接一对{}即可，{} 中即为命名空间的成员。

```cpp
// 1. 正常的命名空间定义
namespace Q
{
 // 命名空间中可以定义变量/函数/类型
 int rand = 10;
 int Add(int left, int right)
 {
 	return left + right;
 }
    struct Node
 {
 	struct Node* next;
	int val;
 };
}
//2. 命名空间可以嵌套
// test.cpp
namespace N1
{
	int a;
	int b;
	int Add(int left, int right)
 	{
     	return left + right;
 	}
	namespace N2
 	{
     	int c;
     	int d;
     	int Sub(int left, int right)
     	{
         	return left - right;
     	}
 	}
}
//3. 同一个工程中允许存在多个相同名称的命名空间,编译器最后会合成同一个命名空间中。
// ps：一个工程中的test.h和上面test.cpp中两个N1会被合并成一个(合并成一个，因此两个命名空间也不能定义相同的变量)
// test.h
namespace N1
{
	int Mul(int left, int right)
 	{
     	return left * right;
 	}
}
```

## 命名空间的3种使用方式

- 加命名空间名称及作用域限定符

```cpp
namespace N
{
	int a = 1;
}
int main()
{
    printf("%d\n", N::a);
    return 0;    
}
```

- 使用using将命名空间中某个成员引入

```cpp
namespace N
{
	int a = 1;
    int b = 2;
}
using N::b;
int main()
{
    printf("%d\n", N::a);
    printf("%d\n", b);
    return 0;    
}
```

- 使用using `namespace` 命名空间名称引入

```cpp
namespace N
{
    int a = 1;
    int b = 2;
    void Add(int a, int b)
    {
        printf("%d\n", a + b);
    }
}
using namespace N;
int main()
{
    printf("%d\n", N::a);
    printf("%d\n", b);
    Add(10, 20);
    return 0;
}
```

# C++输入&输出

std是C++标准库的命名空间名，C++将标准库的定义实现都放到这个命名空间中。

```cpp
#include<iostream>
using namespace std;
int main()
{
	cout<<"Hello world!!!"<<endl;
	return 0;
}
```

> 说明： 
>
> - 使用`cout`标准输出对象(控制台)和`cin`标准输入对象(键盘)时，必须包含`<iostream>`头文件以及按命名空间使用方法使用std。 
>
> - `cout`和`cin`是全局的流对象，`endl`是特殊的C++符号，表示换行输出，他们都包含在包含`<iostream>`头文件中。 
> - <<是流插入运算符，>>是流提取运算符。 
> - 使用C++输入输出更方便，不需要像`printf/scanf`输入输出时那样，需要手动控制格式。 C++的输入输出可以自动识别变量类型

```cpp
#include <iostream>
using namespace std;
int main()
{
   int a;
   double b;
   char c;
     
   // 可以自动识别变量的类型
   cin>>a;
   cin>>b>>c;
     
   cout<<a<<endl;
   cout<<b<<" "<<c<<endl;
   return 0;
}
```

std命名空间的使用惯例： 

> std是C++标准库的命名空间，如何展开std使用更合理呢？ 
>
> - 在日常练习中，直接`using namespace std`即可，这样就很方便。 
> - `using namespace std`展开，标准库就全部暴露出来了，如果我们定义跟库重名的类型/对象/函数，就存在冲突问题。该问题在日常练习中很少出现，但是项目开发中代码较多、规模大，就很容易出现。
> - 所以建议在项目开发中使用像`std::cout`这样使用时指定命名空间和`using std::cout`展开常用的库对象/类型等方式。

# 缺省参数

## 缺省参数概念

缺省参数是声明或定义函数时为函数的参数指定一个缺省值。在调用该函数时，如果没有指定实参则采用该形参的缺省值，否则使用指定的实参。(舔狗)

```cpp
using namespace std;
void Func(int a = 0)
{
	cout << a << endl;
}
int main()
{
	Func();     // 没有传参时，使用参数的默认值
	Func(10);   // 传参时，使用指定的实参
	return 0;
}
```

## 缺省参数分类

全缺省参数

```cpp
using namespace std;
void Func(int a = 10, int b = 20, int c = 30)
{
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;
}
int main()
{
	Func();     
	return 0;
}
```

半缺省参数

```cpp
using namespace std;
void Func(int a, int b = 20, int c = 30)
{
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;
}
int main()
{
	Func(10);     
	return 0;
}
```

> 注意:
>
> - 缺省参数必须从右往左依次来给出，不能间隔着给
>
> - **缺省参数不能在函数声明和定义中同时出现**
> - 缺省值必须是常量或者全局变量 
> - C语言不支持（编译器不支持）

```cpp
//a.h
  void Func(int a = 10);
  
  // a.cpp
  void Func(int a = 20)
 {}
  
  // 注意：如果声明与定义位置同时出现，恰巧两个位置提供的值不同，那编译器就无法确定到底该用那个缺省值。
```

# 函数重载

## 函数重载概念

函数重载：是函数的一种特殊情况，C++允许在同一作用域中声明几个功能类似的同名函数，这些同名函数的形参列表(参数个数或类型 或类型顺序)不同，常用来处理实现功能类似数据类型不同的问题。

```cpp
#include<iostream>
using namespace std;
// 1、参数类型不同
int Add(int left, int right)
{
	cout << "int Add(int left, int right)" << endl;
	return left + right;
}
double Add(double left, double right)
{
	cout << "double Add(double left, double right)" << endl;
	return left + right;
}
// 2、参数个数不同
void f()
{
	cout << "f()" << endl;
}
void f(int a)
{
	cout << "f(int a)" << endl;
}
// 3、参数类型顺序不同
void f(int a, char b)
{
	cout << "f(int a,char b)" << endl;
}
void f(char b, int a)
{
	cout << "f(char b, int a)" << endl;
}
int main()
{
	Add(10, 20);
	Add(10.1, 20.2);
	f();
	f(10);
	f(10, 'a');
	f('a', 10);
	return 0;
}
```

# 引用

## 引用概念

引用不是新定义一个变量，而是给已存在变量取了一个别名，编译器不会为引用变量开辟内存空 间，它和它引用的变量共用同一块内存空间。

**类型& 引用变量名(对象名) = 引用实体；**

> 注意：引用类型必须和引用实体是同种类型的

```cpp
#include <iostream>
using namespace std;
void TestRef()
{
    int a = 10;
    int& ra = a;//<====定义引用类型
    printf("%p\n", &a);
    printf("%p\n", &ra);
}
int main()
{
    TestRef();
    return 0;
}
```

## 引用特性

- 引用在定义时必须初始化 

- 一个变量可以有多个引用
- 引用一旦引用一个实体，再不能引用其他实体

```cpp
#include <iostream>
void TestRef()
{
    int a = 10;
    // int& ra;   // 该条语句编译时会出错
    int& ra = a;
    int& rra = a;
    printf("%p %p %p\n", &a, &ra, &rra);
}
int main()
{
    TestRef();
    return 0;
}
```

## 常引用

权限可以缩小和平移，但是不能扩大。

```cpp
using namespace std;
void TestConstRef()
{
    //权限扩大错误
    const int a = 10;
    //int& ra = a;   // 该语句编译时会出错，a为常量
    const int& ra = a;
    // int& b = 10; // 该语句编译时会出错，b为常量
    const int& b = 10;
    double d = 12.34;
    //int& rd = d; // 该语句编译时会出错，类型不同
    const int& rd = d; //类型转换会产生临时变量，rd是临时变量的别名
    cout << "a = "<< a << endl << "ra = " << ra << endl;
    cout << "d = " << d << endl << "rd = " << rd << endl;
}

int main()
{
    TestConstRef();
    return 0;
}
```

## 使用场景

- 做参数

做参数可以省去传地址

```cpp
using namespace std;
void Swap(int& left, int& right)
{
    int temp = left;
    left = right;
    right = temp;
}
int main()
{
    int a = 10;
    int b = 20;
    Swap(a, b);
    //Swap(&a, &b);
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    return 0;
}
```

- 做返回值

```cpp
int& Count()
{
    static int n = 0;
    n++;
    return n;
}
int main()
{
    int ret = Count();
    cout << ret;
    return 0;
}
```

> 如果函数返回时，出了函数作用域，如果返回对象还在(还没还给系统)，则可以使用引用返回，如果已经还给系统了，则必须使用传值返回。

错误示范

```cpp
int& Add(int a, int b)
{
    int c = a + b;
    return c;
}
int main()
{
    int& ret = Add(1, 2);
    Add(3, 4);
    cout << "Add(1, 2) is :"<< ret <<endl;
    return 0;
}
```

## 传值、传引用效率比较

**以值作为参数或者返回值类型，在传参和返回期间，函数不会直接传递实参或者将变量本身直 接返回，而是传递实参或者返回变量的一份临时的拷贝，因此用值作为参数或者返回值类型，效 率是非常低下的，尤其是当参数或者返回值类型非常大时，效率就更低。**

```cpp
#include <iostream>
using namespace std;
#include <time.h>
struct A 
{ 
	int a[10000]; 
};
void TestFunc1(A a) 
{
}
void TestFunc2(A& a) 
{
}
void TestRefAndValue()
{
	A a;
	// 以值作为函数参数
	size_t begin1 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc1(a);
	size_t end1 = clock();
	// 以引用作为函数参数
	size_t begin2 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc2(a);
	size_t end2 = clock();
	// 分别计算两个函数运行结束后的时间
	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
}
int main()
{
	TestRefAndValue();
	return 0;
}
```

## 值和引用的作为返回值类型的性能比较

```cpp
#include <iostream>
using namespace std;
#include <time.h>
struct A 
{ 
	int a[10000]; 
};
A a;
// 值返回
A TestFunc1() 
{ 
	return a; 
}
// 引用返回
A& TestFunc2() 
{ 
	return a; 
}
void TestReturnByRefOrValue()
{
	// 以值作为函数的返回值类型
	size_t begin1 = clock();
	for (size_t i = 0; i < 100000; ++i)
		TestFunc1();
	size_t end1 = clock();
	// 以引用作为函数的返回值类型
	size_t begin2 = clock();
	for (size_t i = 0; i < 100000; ++i)
		TestFunc2();
	size_t end2 = clock();
	// 计算两个函数运算完成之后的时间
	cout << "TestFunc1 time:" << end1 - begin1 << endl;
	cout << "TestFunc2 time:" << end2 - begin2 << endl;
}
int main()
{
	TestReturnByRefOrValue();
	return 0;
}
```

通过上述代码的比较，发现传值和指针在作为传参以及返回值类型上效率相差很大。

# 引用和指针的区别

在**语法概念**上引用就是一个别名，没有独立空间，和其引用实体共用同一块空间。

```cpp
#include <iostream>
using namespace std;
void TestRef()
{
    int a = 10;
    int& ra = a;
    printf("%p\n", &a);
    printf("%p\n", &ra);
}
int main()
{
    TestRef();
    return 0;
}
```

在底层实现上实际是有空间的，因为引用是按照指针方式来实现的。

```cpp
int main()
{
	int a = 10;
	int& ra = a;
	ra = 20;
	int* pa = &a;
	*pa = 20;
	return 0;
}
```

运行上面的代码，对比引用和指针的汇编代码

![image-20230307140048566](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230307140048566.png)

>引用和指针的不同点: 
>
>- 引用概念上定义一个变量的别名，指针存储一个变量地址。 
>- 引用在定义时必须初始化，指针没有要求 
>- 引用在初始化时引用一个实体后，就不能再引用其他实体，而指针可以在任何时候指向任何一个同类型实体 
>- 没有NULL引用，但有NULL指针 
>- 在`sizeof`中含义不同：引用结果为引用类型的大小，但指针始终是地址空间所占字节个数(32 位平台下占4个字节,64位平台占8个字节) 
>- 引用自加即引用的实体增加1，指针自加即指针向后偏移一个类型的大小
>- 有多级指针，但是没有多级引用 
>- 访问实体方式不同，指针需要显式解引用，引用编译器自己处理 
>- 引用比指针使用起来相对更安全

# 内联函数

## 内联函数概念

以`inline`修饰的函数叫做内联函数，编译时C++编译器会在调用内联函数的地方展开，没有函数调用建立栈帧的开销，内联函数提升程序运行的效率。

## 内联函数特性

- `inline`是一种以空间换时间的做法，如果编译器将函数当成内联函数处理，在编译阶段，会用函数体替换函数调用

  缺陷：可能会使目标文件变大，

  优势：少了调用开销，提高程序运行效率。 

- `inline`对于编译器而言只是一个建议，不同编译器关于`inline`实现机制可能不同，一般建议：将函数规模较小(即函数不是很长，具体没有准确的说法，取决于编译器内部实现)、不是递归、且频繁调用的函数采用`inline`修饰，否则编译器会忽略`inline`特性。

- `inline`不建议声明和定义分离，分离会导致链接错误。因为`inline`被展开，就没有函数地址 了，链接就会找不到。

> [面试题]
>
> 宏的优缺点？ 
>
> 优点： 1.增强代码的复用性。 2.提高性能。 
>
> 缺点： 1.不方便调试宏。（因为预编译阶段进行了替换） 2.导致代码可读性差，可维护性差，容易误用。 3.没有类型安全的检查 。
>
>  C++有哪些技术替代宏？ 
>
> 1. 常量定义换用`const enum`
> 1. 短小函数定义换用内联函数 

# auto关键字(C++11)

## 类型别名思考

随着程序越来越复杂，程序中用到的类型也越来越复杂，经常体现在：

> - 类型难于拼写 
>
> - 含义不明确导致容易出错

​	如下代码：

```cpp
#include <string>
#include <map>
int main()
{
 std::map<std::string, std::string> m{ { "apple", "苹果" }, { "orange", 
"橙子" }, 
   {"pear","梨"} };
 std::map<std::string, std::string>::iterator it = m.begin();
 while (it != m.end())
 {
 //....
 }
 return 0;
}
```

`std::map<std::string, std::string>::iterator`是一个类型，但是该类型太长了，特别容易写错。可能有人会想：可以通过typedef给类型取别名，比如：

```cpp
#include <string>
#include <map>
typedef std::map<std::string, std::string> Map;
int main()
{
 	Map m{ { "apple", "苹果" },{ "orange", "橙子" }, {"pear","梨"} };
	Map::iterator it = m.begin();
	 while (it != m.end())
 	{
 	//....
 	}
 	return 0;
}
```

使用typedef给类型取别名确实可以简化代码，但是typedef有会遇到新的难题：

```cpp
typedef char* pstring;
int main()
{
 const pstring p1;    // 编译成功还是失败？
 const pstring* p2;   // 编译成功还是失败？
 return 0;
}
```

在编程时，常常需要把表达式的值赋值给变量，这就要求在声明变量的时候清楚地知道表达式的类型。然而有时候要做到这点并非那么容易，因此C++11给auto赋予了新的含义。

## auto简介

C++11中，标准委员会赋予了auto全新的含义即：auto不再是一个存储类型指示符，而是作为一 个新的类型指示符来指示编译器，auto声明的变量必须由编译器在编译时期推导而得。

```cpp
#include <iostream>
using namespace std;
int TestAuto()
{
	return 10;
}
int main()
{
	int a = 10;
	auto b = a;
	auto c = 'a';
	auto d = TestAuto();
	cout << typeid(b).name() << endl;
	cout << typeid(c).name() << endl;
	cout << typeid(d).name() << endl;
	//auto e; //无法通过编译，使用auto定义变量时必须对其进行初始化
	return 0;
}
```

**注意**：

> 使用auto定义变量时必须对其进行初始化，在编译阶段编译器需要根据初始化表达式来推导auto 的实际类型。因此auto并非是一种“类型”的声明，而是一个类型声明时的“占位符”，编译器在编 译期会将auto替换为变量实际的类型。

## auto的使用细则

- auto与指针和引用结合起来使用

**用auto声明指针类型时，用auto和auto*没有任何区别，但用auto声明引用类型时则必须 加&**

```cpp
#include <iostream>
using namespace std;
int main()
{
    int x = 10;
    auto a = &x;
    auto* b = &x;
    auto& c = x;
    cout << typeid(a).name() << endl;
    cout << typeid(b).name() << endl;
    cout << typeid(c).name() << endl;
    *a = 20;
    *b = 30;
    c = 40;
    return 0;
}
```

- 在同一行定义多个变量

**当在同一行声明多个变量时，这些变量必须是相同的类型，否则编译器将会报错，因为编译 器实际只对第一个类型进行推导，然后用推导出来的类型定义其他变量。**

```cpp
int main()
{
    auto a = 1, b = 2;
    cout << typeid(a).name() << endl;
    cout << typeid(b).name() << endl;
    //auto c = 3, d = 4.0;  // 该行代码会编译失败，因为c和d的初始化表达式类型不同
}
```

## auto不能推导的场景

- auto不能作为函数的参数

```cpp
// 此处代码编译失败，auto不能作为形参类型，因为编译器无法对a的实际类型进行推导
void TestAuto(auto a)
{}
```

- auto不能直接用来声明数组

```cpp
void TestAuto()
{
    int a[] = {1,2,3};
    auto b[] = {4，5，6};
}
```

- 为了避免与C++98中的auto发生混淆，C++11只保留了auto作为类型指示符的用法

# 基于范围的for循环(C++11)

## 范围for的语法

在C++98中如果要遍历一个数组，可以按照以下方式进行：

```cpp
void TestFor()
{
int array[] = { 1, 2, 3, 4, 5 };
for (int i = 0; i < sizeof(array) / sizeof(array[0]); ++i)
     array[i] *= 2;
for (int* p = array; p < array + sizeof(array)/ sizeof(array[0]); ++p)
     cout << *p << endl;
}
```

对于一个有范围的集合而言，由程序员来说明循环的范围是多余的，有时候还会容易犯错误。因 此C++11中引入了基于范围的for循环。for循环后的括号由冒号“ ：”分为两部分：第一部分是范围内用于迭代的变量，第二部分则表示被迭代的范围。

```cpp
#include <iostream>
using namespace std;
int main()
{
    int array[] = { 1, 2, 3, 4, 5 };
    for (auto& e : array)
        e *= 2;
    for (auto e : array)
        cout << e << " ";
    return 0;
}
```

**注意：与普通循环类似，可以用continue来结束本次循环，也可以用break来跳出整个循环。**

## 范围for的使用条件

for循环迭代的范围必须是确定的，对于数组而言，就是数组中第一个元素和最后一个元素的范围；对于类而言，应该提供begin和end的方法，begin和end就是for循环迭代的范围。 

注意：以下代码就有问题，因为for的范围不确定。

```cpp
void TestFor(int array[])
{
    for(auto& e : array)
        cout<< e <<endl;
}
```

# 指针空值nullptr(C++11)

## C++98中的指针空值

在良好的C/C++编程习惯中，声明一个变量时最好给该变量一个合适的初始值，否则可能会出现不可预料的错误，比如未初始化的指针。如果一个指针没有合法的指向，我们基本都是按照如下方式对其进行初始化：

```cpp
void TestPtr()
{
int* p1 = NULL;
int* p2 = 0;
// ……
}
```

NULL实际是一个宏，在传统的C头文件(stddef.h)中，可以看到如下代码

```cpp
#ifndef NULL
#ifdef __cplusplus
#define NULL   0
#else
#define NULL   ((void *)0)
#endif
#endif
```

可以看到，NULL可能被定义为字面常量0，或者被定义为无类型指针(void*)的常量。不论采取何 种定义，在使用空值的指针时，都不可避免的会遇到一些麻烦，比如：

```cpp
void f(int)
{
 cout<<"f(int)"<<endl;
}
void f(int*)
{
 cout<<"f(int*)"<<endl;
}
int main()
{
 f(0);
 f(NULL);
 f((int*)NULL);
 return 0;
}
```

程序本意是想通过f(NULL)调用指针版本的f(int *)函数，但是由于NULL被定义成0，因此与程序的初衷相悖。 在C++98中，字面常量0既可以是一个整形数字，也可以是无类型的指针(void *)常量，但是编译器默认情况下将其看成是一个整形常量，如果要将其按照指针方式来使用，必须对其进行强转(void  *)0。

> 注意： 
>
> - 在使用nullptr表示指针空值时，不需要包含头文件，因为nullptr是C++11作为新关键字引入的。
> - 在C++11中，sizeof(nullptr) 与 sizeof((void*)0)所占的字节数相同。
> - 为了提高代码的健壮性，在表示指针空值时建议最好使用nullptr。













