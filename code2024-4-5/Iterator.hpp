#pragma once

namespace wzh
{
	template<class T, class Ref, class Ptr>
	struct Iterator
	{
		typedef Iterator<T, Ref, Ptr> self;
		T* _iter;

		Iterator()
			:_iter(nullptr)
		{}

		Iterator(T* pit)
			:_iter(pit)
		{}

		Iterator(const Iterator& it)
			:_iter(it._iter)
		{}

		self operator++()
		{
			_iter = _iter->_next;
			return *this;
		}

		self operator++(int)
		{
			self tmp(*this);
			_iter = _iter->_next;
			return tmp;
		}

		self operator--()
		{
			_iter = _iter->_prev;
			return *this;
		}

		self operator--(int)
		{
			self tmp(*this);
			_iter = _iter->_prev;
			return tmp;
		}

		Ref operator*()
		{
			return *_iter;
		}

		Ptr operator->()
		{
			return _iter;
		}

		Ref operator=(const self& it)
		{
			_iter = it._iter;
			return *this;
		}

		bool operator==(const self& it)
		{
			return _iter == it._iter;
		}

		bool operator!=(const self& it)
		{
			return _iter != it._iter;
		}
	};
}