**参考手册**：[cplusplus]([string - C++ Reference (cplusplus.com)](https://legacy.cplusplus.com/reference/string/string/?kw=string))

# string类简介

C语言中，字符串是以`\0`结尾的一些字符的集合，为了操作方便，C标准库中提供了一些`str`系列的库函数， 但是这些库函数与字符串是分离开的，不太符合OOP的思想（面向对象编程），而且底层空间需要用户自己管理，稍不留神可能还会越界访问。

标准库中的string类：

1. string是表示字符串的字符串类。
2. 该类的接口与常规容器的接口基本相同，再添加了一些专门用来操作string的常规操作。
3. string在底层实际是：basic_string模板类的别名，typedef basic_string  string。
4. 不能操作多字节或者变长字符的序列。

在使用string类时，必须包含`string`头文件。

![image-20230427150337583](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230427150337583.png)

# string类的常用接口说明

## string类对象常见构造函数

1. `string() `：构造空的string类对象，即空字符串。

2. `string(const char* s)`：用字符串来构造string类对象。

3. `string(size_t n, char c)`：string类对象中包含n个字符c。

4. `string(const string&s) `：拷贝构造函数。

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string arr1;
	string arr2("abcdef");
	string arr3(10, 'c');
	string arr4(arr2);
	cout << arr1 << endl << arr2 << endl << arr3 << endl << arr4;
	return 0;
}
```

![image-20230413205513486](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230413205513486.png)



## string类对象常见容量操作函数

> `size()`：**返回字符串有效字符长度。**
>
> ```cpp
> size_t size() const;
> ```
>

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string arr("apple");
	cout << arr.size() << endl;
	return 0;
}
```

![image-20230506181816255](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506181816255.png)



> `lenth()`：**返回字符串有效字符长度。**
>
> ```cpp
> size_t length() const;
> ```
>

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string arr("apple");
	cout << arr.length() << endl;
	return 0;
}
```

![image-20230506182006426](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506182006426.png)



> `capacity()`：**返回空间总大小。**
>
> ```cpp
> size_t capacity() const;
> ```
>

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string arr("apple");
	cout << arr.capacity() << endl;
	return 0;
}
```

![image-20230506182204021](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506182204021.png)



> `empty()`：**检测字符串是否为空串，是返回true，否则返回false。**
>
> ```cpp
> bool empty() const;
> ```
>

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string arr1("apple");
	string arr2;
	if (arr1.empty())
		cout << "arr1 empty" << endl;
	else
		cout << "arr1 not empty" << endl;
	if (arr2.empty())
		cout << "arr2 empty" << endl;
	else
		cout << "arr2 not empty" << endl;
	return 0;
}
```

![image-20230506182908359](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506182908359.png)



> `clear()`：**清空有效字符。**
>
> ```cpp
> void clear();
> ```
>

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string arr1("apple");
	cout << "begin : " << arr1 << endl;
	arr1.clear();
	cout << "end : " << arr1 << endl;
	return 0;
}
```

![image-20230506183131133](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506183131133.png)



> `reserve()`：**为字符串开辟空间。**
>
> ```cpp
> void reserve (size_t n = 0);
> ```
>

示例：

```cpp
int main()
{
	string arr1("apple");
	cout << arr1.capacity() << endl;
	arr1.reserve(arr1.capacity() * 2);
	cout << arr1.capacity() << endl;
	return 0;
}
```

![image-20230506183335087](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506183335087.png)



> `resize()`：**将有效字符的个数改成n个，多出的空间用字符c填充。**
>
> ```cpp
> void resize (size_t n); 
> void resize (size_t n, char c);
> ```

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string arr1("apple");
	cout << arr1.capacity() << endl;
	arr1.resize(arr1.capacity() * 2, 'X');
	cout << arr1.capacity() << endl << arr1 << endl;
	arr1.resize(arr1.capacity() / 2);
	cout << arr1.capacity() << endl << arr1 << endl;
	return 0;
}
```

![image-20230506183731573](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506183731573.png)



> 注意：
>
> 1. size()与length()方法底层实现原理完全相同，引入size()的原因是为了与其他容器的接口保持一致，一般情况下基本都是用size()。
> 2. clear()只是将string中有效字符清空，不改变底层空间大小。
> 3. reserve(size_t n = 0)函数为string预留空间，不改变有效元素个数，当reserve的参数小于string的底层空间总大小时，reserver不会改变容量大小。
>
> 4. 当resize函数的n小于原来的有效字符个数时，resize会保留n个有效字符然后再后面加上`/0`，不会改变capacity的大小。当n大于原来的有效字符个数时，resize会在未初始化的空间上进行初始化，如果传了字符c就用c初始化，没有传就用`\0`初始化，resize在改变元素个数时，如果是将元素个数增多，可能会改变底层容量的大小，如果是将元素个数减少，底层空间总大小不变。



## string类对象访问及遍历操作函数

> `operator[]()` ：**返回pos位置的字符**。
>
> ```cpp
> 	  char& operator[] (size_t pos);
> const char& operator[] (size_t pos) const;
> ```
>
> `begin()`：**获取一个字符的迭代器。**
>
> ```cpp
>       iterator begin();
> const_iterator begin() const;
> ```
>
> `end()`：**获取最后一个字符下一个位置的迭代器。**
>
> ```cpp
>       iterator end();
> const_iterator end() const;
> ```
>
> `rbegin()` ：**获取最后一个字符的迭代器。**
>
> ```cpp
>       reverse_iterator rbegin();
> const_reverse_iterator rbegin() const;
> ```
>
> `rend()`：**获取第一个字符的前一个位置的迭代器。**
>
> ```cpp
>       reverse_iterator rend();
> const_reverse_iterator rend() const;
> ```

**tips :**

1. operator[]的使用我们通常直接使用`[]`的形式，如下代码两种形式都可以：

> ```cpp
> string s1("apple");
> for (int i = 0; i < s1.size(); i++)
> {
> 	cout << s1[i] << ' ';
> }
> cout << endl;
> 	
> for (int i = 0; i < s1.size(); i++)
> {
> 	cout << s1.operator[](i) << ' ';
> }
> cout << endl;
> ```

2. 迭代器的`begin-end`和`rbegin-rend`都是左闭右开的集合。
3. 有迭代器的加持，在c++11中支持更简洁的范围for的新遍历方式，如下代码：

> ```cpp
> string s1("apple");
> 	for (auto e : s1)
> 		cout << e << ' ';
> 	cout << endl;
> ```



**示例代码：**

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string s1("hello world");
	cout << "operator[] : ";
	for (int i = 0; i < s1.size(); i++)
	{
		cout << s1[i];
	}
	cout << endl;

	cout << "begin : ";
	for (string::iterator bg = s1.begin(); bg != s1.end(); bg++)
	{
		cout << *bg;
	}
	cout << endl;

	cout << "rbegin : ";
	for (string::reverse_iterator rbg = s1.rbegin(); rbg != s1.rend(); rbg++)
	{
		cout << *rbg;
	}
	cout << endl;
	return 0;
}
```

![image-20230427214049804](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230427214049804.png)



## string类对象修改操作函数

> `push_back()`：**在字符串尾插字符c**。
>
> ```cpp
> void push_back (char c);
> ```
>
> `append()`：**在字符串后追加一个字符串str。**
>
> ```cpp
> string& append (const string& str);
> //追加字符串str从下标subpos位置开始的sublen个字符
> string& append (const string& str, size_t subpos, size_t sublen);
> string& append (const char* s);
> //追加字符串s从下标0开始n个字符
> string& append (const char* s, size_t n);
> //追加n个c字符
> string& append (size_t n, char c);
> template <class InputIterator>
> string& append (InputIterator first, InputIterator last);
> ```
>
> `operator+=`：**在字符串后追加字符串。**
>
> ```cpp
> string& operator+= (const string& str);
> string& operator+= (const char* s);
> string& operator+= (char c);
> ```
>
> tips：`s1 += "bear"`和`s1.operator+=("bear")`两种用法都可以，直接用+=更加便捷。
>
> `c_str()`：**返回C格式字符串。**
>
> ```cpp
> const char* c_str() const;
> ```
>
> `find()`：**从字符串pos位置开始往后找字符c或字符串，返回该字符在字符串中的位置。没有则返回npos(-1)。**
>
> ```cpp
> size_t find (const string& str, size_t pos = 0) const;
> size_t find (const char* s, size_t pos = 0) const;
> //从pos位置开始查找字符串s的前n个字符。
> size_t find (const char* s, size_t pos, size_t n) const;
> size_t find (char c, size_t pos = 0) const;
> ```
>
> `rfind`：**从字符串pos位置开始往前找字符c，返回该字符在字符串中的位置。没有则返回npos(-1)。**
>
> ```cpp
> size_t rfind (const string& str, size_t pos = npos) const;
> size_t rfind (const char* s, size_t pos = npos) const;
> //从pos位置开始查找字符串s的前n个字符。
> size_t rfind (const char* s, size_t pos, size_t n) const;
> size_t rfind (char c, size_t pos = npos) const;
> ```
>
> `substr()`：**在str中从pos位置开始，截取n个字符，然后将其返回。**
>
> ```cpp
> string substr (size_t pos = 0, size_t len = npos) const;
> ```
>
> 注意：
>
> - 当len大于str中pos位置后面剩余的字符时，那么就截取后面所有的字符。
> - 当不给len传参时，默认截取pos后面所有的字符。



示例代码：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string s1("apple");
	s1.push_back('T');
	cout << s1 << endl;
	s1.append("abcd", 0, 4);
	cout << s1 << endl;
	s1 += "XXX";
	cout << s1 << endl;
	int ret1 = s1.find('T', 0);
	cout << ret1 << endl;
	int ret2 = s1.rfind('T', 12);
	cout << ret2 << endl;
	cout << s1.substr(5, 5) << endl;
	cout << s1.c_str() << endl;
	return 0;
}
```

![image-20230429180221429](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230429180221429.png)



## string类非成员函数

> `operator+`：**将lhs和rhs组合成一个字符串。**
>
> ```cpp
> string operator+ (const string& lhs, const string& rhs);
> string operator+ (const string& lhs, const char*   rhs);
> string operator+ (const char*   lhs, const string& rhs);
> string operator+ (const string& lhs, char          rhs);
> string operator+ (char          lhs, const string& rhs);
> ```
>

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
int main()
{
	string s1("apple");
	string s2("bear");
	cout << s1 + s2 << endl;
	cout << operator+(s1, s2) << endl;
	return 0;
}
```

![image-20230506193758722](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230506193758722.png)

> **注意：尽量少用，因为传值返回，导致深拷贝效率低。**



> `opereator>>`：**输入运算符重载。**
>
> ```cpp
> istream& operator>> (istream& is, string& str);
> ```
>
> `operator<<`：**输出运算符重载。**
>
> ```cpp
> ostream& operator<< (ostream& os, const string& str);
> ```
>
> `getline`：**获取一行字符串（遇空格不结束）。**
>
> ```cpp
> istream& getline (istream& is, string& str, char delim);
> istream& getline (istream& is, string& str);
> ```
>
> `relational operators`：**大小比较。**
>
> ```cpp
> (1)
> bool operator== (const string& lhs, const string& rhs);
> bool operator== (const char*   lhs, const string& rhs);
> bool operator== (const string& lhs, const char*   rhs);
> (2)	
> bool operator!= (const string& lhs, const string& rhs);
> bool operator!= (const char*   lhs, const string& rhs);
> bool operator!= (const string& lhs, const char*   rhs);
> (3)	
> bool operator<  (const string& lhs, const string& rhs);
> bool operator<  (const char*   lhs, const string& rhs);
> bool operator<  (const string& lhs, const char*   rhs);
> (4)	
> bool operator<= (const string& lhs, const string& rhs);
> bool operator<= (const char*   lhs, const string& rhs);
> bool operator<= (const string& lhs, const char*   rhs);
> (5)	
> bool operator>  (const string& lhs, const string& rhs);
> bool operator>  (const char*   lhs, const string& rhs);
> bool operator>  (const string& lhs, const char*   rhs);
> (6)	
> bool operator>= (const string& lhs, const string& rhs);
> bool operator>= (const char*   lhs, const string& rhs);
> bool operator>= (const string& lhs, const char*   rhs);
> ```

string类的接口众多，并不需要全都记住，我们只需要记得会用一些常见接口即可，有必要时可以直接去查看官方文档。



# 深浅拷贝问题

**浅拷贝：**

也称位拷贝，编译器只是将对象中的值拷贝过来。如果对象中管理资源，最后就会导致多个对象共享同一份资源，当一个对象销毁时就会将该资源释放掉，而此时另一些对象不知道该资源已经被释放，以为还有效，所以当继续对资源进项操作时，就会发生发生了访问违规。

**深拷贝：**

深拷贝就是为了解决浅拷贝带来的问题，如果一个类中涉及到资源的管理，其拷贝构造函数、赋值运算符重载以及析构函数必须要显式给出，一般情况都是按照深拷贝方式提供。

示例：

```cpp
#include <iostream>
#include <string>
using namespace std;
class mystring
{
public:
	mystring(const char* str = "")
	{
		if (str == nullptr)
		{
			_ptr = nullptr;
		}
		_ptr = new char[strlen(str) + 1];
		strcpy(_ptr, str);
	}
	~mystring()
	{
		if (_ptr)
		{
			delete[] _ptr;
			_ptr = nullptr;
		}
	}

private:
	char* _ptr;
};

int main()
{
	mystring s1("apple");
	mystring s2(s1);
	return 0;
}
```

如上代码，因为没有显示定义拷贝构造，默认的拷贝构造函数为浅拷贝，所以在析构时会出现内存泄漏。

![](C:\Users\Lenovo\Desktop\string1.gif)

因此，我们需要显示定义拷贝构造函数进行深拷贝，如下代码：

```cpp
#include <iostream>
#include <string>
using namespace std;

class mystring
{
public:
	mystring(const char* str = "")
	{
		if (str == nullptr)
		{
			_ptr = nullptr;
		}
		_ptr = new char[strlen(str) + 1];
		strcpy(_ptr, str);
	}
	mystring(const mystring& s)
		:_ptr(new char[strlen(s._ptr) + 1])
	{
		strcpy(_ptr, s._ptr);
	}
	~mystring()
	{
		if (_ptr)
		{
			delete[] _ptr;
			_ptr = nullptr;
		}
	}

private:
	char* _ptr;
};

int main()
{
	mystring s1("apple");
	mystring s2(s1);
	return 0;
}
```

![](C:\Users\Lenovo\Desktop\string2.gif)





# string类模拟实现

简单模拟string类的功能，封装出一个自己的string类。

```cpp
//mystring.h
#pragma once
#include <iostream>
#include <assert.h>

using namespace std;
namespace wzh {
	class mystring
	{
	private:
		friend ostream& operator<<(ostream& _cout, const wzh::mystring& s);
	public:
		typedef char* iterator;
	public:
		mystring(const char* str = "");
		mystring(const mystring& s);
		~mystring();
		void swap(mystring& s);
		mystring& operator=(mystring& s);
		iterator begin();
		iterator end();

		char& operator[](size_t index);
		const char& operator[](size_t index) const;

		size_t size() const;
		size_t capacity() const;
		bool empty() const;
		void clear();
		void push_back(char c);
		mystring& operator+=(char c);
		mystring& operator+=(const char* str);
		void append(const char* str);
		void resize(size_t newsize, char c = '\0');
		void reserve(size_t newcapacity);
		
		bool operator<(const mystring& s);
		bool operator<=(const mystring& s);
		bool operator>(const mystring& s);
		bool operator>=(const mystring& s);
		bool operator==(const mystring& s);
		bool operator!=(const mystring& s);

		size_t find(char c, size_t pos = 0) const;
		size_t find(const char* s, size_t pos = 0) const;

		mystring& insert(size_t pos, char c);
		mystring& insert(size_t pos, const char* str);
		mystring& erase(size_t pos, size_t len);

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
}
```

```cpp
//mystring.cpp
#define _CRT_SECURE_NO_WARNINGS 1

#include "mystring.h"
wzh::mystring::mystring(const char* str) //缺省函数声明处写，定义不需要写
{
	_size = strlen(str);
	_capacity = _size;
	_str = new char[_capacity + 1];
	strcpy(_str, str);
}

wzh::mystring::mystring(const mystring& s)
	:_size(0)
	,_capacity(0)
	,_str(nullptr)
{
	mystring tmp(s._str);
	swap(tmp);
}

void wzh::mystring::swap(mystring& s)
{
	std::swap(_str, s._str);
	std::swap(_size, s._size);
	std::swap(_capacity, s._capacity);
}

wzh::mystring::~mystring()
{
	if (_str)
	{
		delete[] _str;
		_str = nullptr;
	}
	_size = _capacity = 0;
}

wzh::mystring& wzh::mystring::operator=(mystring& s)
{
	swap(s);
	return *this;
}

wzh::mystring::iterator wzh::mystring::begin()
{
	return _str;
}

wzh::mystring::iterator wzh::mystring::end()
{
	return _str + _size;
}

void wzh::mystring::resize(size_t newsize, char c)
{
	if (newsize > _size)
	{
		if (newsize > _capacity)
		{
			reserve(newsize);
		}
		memset(_str + _size, c, newsize - _size);
	}
	_size = newsize;
	_str[newsize] = '\0';
}

void wzh::mystring::reserve(size_t newcapacity)
{
	if (newcapacity > _capacity)
	{
		char* str = new char[newcapacity + 1];
		strcpy(str, _str);
		delete[] _str;
		_str = str;
		_capacity = newcapacity;
	}
}

void wzh::mystring::push_back(char c)
{
	if (_size == _capacity) reserve(_capacity * 2);
	_str[_size++] = c;
	_str[_size] = '\0';
}

wzh::mystring& wzh::mystring::operator+=(char c)
{
	push_back(c);
	return *this;
}

wzh::mystring& wzh::mystring::operator+=(const char* str)
{
	append(str);
	return *this;
}


bool wzh::mystring::empty() const
{
	return _size == 0;
}

size_t wzh::mystring::capacity() const
{
	return _capacity;
}

size_t wzh::mystring::size() const
{
	return _size;
}

char& wzh::mystring::operator[](size_t index)
{
	assert(index < _size);
	return _str[index];
}

const char& wzh::mystring::operator[](size_t index) const
{
	assert(index < _size);
	return _str[index];
}


void wzh::mystring::append(const char* str)
{
	for (int i = 0; i < strlen(str); i++)
		push_back(str[i]);
}

void wzh::mystring::clear()
{
	_size = 0;
	_str[_size] = '\0';
}

bool wzh::mystring::operator<(const mystring& s)
{
	if (_size < s.size()) return true;
	if (_size > s.size()) return false;
	for (int i = 0; i < _size; i++)
	{
		if (_str[i] > s[i]) return false;
		if (_str[i] < s[i]) return true;
	}
	return false;
}

bool wzh::mystring::operator==(const mystring& s)
{
	if (_size != s.size()) return false;
	for (int i = 0; i < _size; i++)
	{
		if (_str[i] != s[i]) return false;
	}
	return true;
}


bool wzh::mystring::operator<=(const mystring& s)
{
	return *this < s || *this == s;
}

bool wzh::mystring::operator>(const mystring& s)
{
	return !(*this <= s);
}

bool wzh::mystring::operator>=(const mystring& s)
{
	return *this > s || *this == s;
}

bool wzh::mystring::operator!=(const mystring& s)
{
	return !(*this == s);
}

ostream& wzh::operator<<(ostream& _cout, const wzh::mystring& s)
{
	for (int i = 0; i < s.size(); i++)
		_cout << s[i];
	return _cout;
}


size_t wzh::mystring::find(char c, size_t pos) const
{
	assert(pos >= 0 && pos < _size);
	for (int i = pos; i < _size; i++)
	{
		if (_str[i] == c) return i;
	}
	return -1;
}

size_t wzh::mystring::find(const char* s, size_t pos) const
{
	assert(pos >= 0 && pos < _size);
	int tmp = 0, ti = 0;
	for (int i = pos; i < _size; i++)
	{
		ti = i;
		tmp = 0;
		while (s[tmp] == _str[ti] && tmp < strlen(s) && ti < _size)
		{
			tmp++;
			ti++;
		}
		if (tmp == strlen(s)) return i;
	}
	return -1;
}

wzh::mystring& wzh::mystring::insert(size_t pos, char c)
{
	assert(pos >= 0 && pos < _size);
	if (_size >= _capacity) reserve(_capacity * 2);
	memmove(_str + pos + 1, _str + pos, sizeof(char));
	_str[pos] = c;
	_size++;
	return *this;
}

wzh::mystring& wzh::mystring::insert(size_t pos, const char* str)
{
	assert(pos >= 0 && pos < _size);
	if (_capacity - _size < strlen(str)) reserve(_capacity + strlen(str));
	memcpy(_str + pos + strlen(str), _str + pos, sizeof(char) * (_size - pos));
	memcpy(_str + pos, str, sizeof(char) * strlen(str));
	/*for (int i = 0; i < strlen(str); i++)
		_str[pos++] = str[i];*/
	_size += strlen(str);
	return *this;
}

wzh::mystring& wzh::mystring::erase(size_t pos, size_t len)
{
	assert(pos >= 0 && pos < _size);
	if (pos + len < _size)
	{
		memcpy(_str + pos, _str + pos + len, sizeof(char) * (_size - pos - len));
		_size -= len;
	}
	else
	{
		_str[pos] = '\0';
		_size = pos;
	}
	return *this;
}
```

```cpp
//test.cpp
#define _CRT_SECURE_NO_WARNINGS 1

#include "mystring.h"
//using namespace wzh;
int main()
{
	wzh::mystring s1("apple");
	wzh::mystring s2(s1);
	wzh::mystring s3;
	wzh::mystring::iterator it = s1.begin();

	while (it != s1.end())
	{
		cout << *it++ << ' ';
	}
	cout << endl;
	s1.push_back('x');
	cout << s1 << endl;
	s1.append("bear");
	cout << s1 << endl;
	s1 += "KKK";
	cout << s1 << endl;
	for (auto e : s1)
	{
		cout << e << ' ';
	}
	cout << endl;
	cout << s1.insert(0, 'p') << endl;
	cout << s1.insert(3, "TTT") << endl;
	cout << s1.erase(0, 1) << endl;
	cout << s1.erase(2, 1) << endl;
	cout << s1.insert(0, "YYY") << endl;
	return 0;
}
```

