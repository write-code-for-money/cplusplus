#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
int main()
{
	int a = 10;
	int d = static_cast<double>(a);
	static_cast<char>(a);
	const int b = 20;
	int c = const_cast<int&>(b);
	c = 30;
	std::cout << c << std::endl;

	int p = 65;
	char* ch = reinterpret_cast<char*>(&p);
	printf("%s\n", ch);
	return 0;
}