#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <stdio.h>
#include <winsock2.h>

#pragma comment(lib, "ws2_32.lib")

void error(const char* msg) {
    perror(msg);
    exit(1);
}

int main() 
{
    const char* server_ip = "139.9.40.185";
    int port = 8080;

    // 初始化 Winsock 库
    WSADATA wsa_data;
    if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0) 
    {
        error("initializing fail\n");
    }

    // 创建 socket
    SOCKET client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket == INVALID_SOCKET) 
    {
        error("create fail\n");
    }

    struct sockaddr_in server_address;

    // 初始化服务器地址结构
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = inet_addr(server_ip);
    server_address.sin_port = htons(port);

    // 连接服务器
    if (connect(client_socket, (struct sockaddr*)&server_address, sizeof(server_address)) == SOCKET_ERROR) 
    {
        error("connecting fail\n");
    }

    while (1)
    {
       char massage[1024];
       printf("#-> ");
       fgets(massage, sizeof massage, stdin);
       // scanf("%s", massage);
       if (!strcmp("quit\n", massage))
       {
           printf("exit....\n");
           break;
       }
       
       // 发送数据到服务器
       send(client_socket, massage, strlen(massage) - 1, 0);
        
       //if (send(client_socket, massage, strlen(massage), 0) < 0)
       //{
       //    printf("服务器关闭，我也退了.......\n");
       //    break;
       //}
       char buffer[1024];
       // 接收服务器的响应并打印
       int n = recv(client_socket, buffer, sizeof(buffer) - 1, 0);
       buffer[n] = '\0';
       if (n == 0)
       {
           printf("服务器关闭，我也退了.......\n");
           break;
       }
       printf("server: %s\n", buffer);
    }

    // 关闭连接
    closesocket(client_socket);

    // 释放 Winsock 库资源
    WSACleanup();

    return 0;
}
