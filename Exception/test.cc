#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//
//void divide(int num, int den) 
//{
//    if (den == 0) 
//    {
//        throw std::runtime_error("Division by zero"); // 抛出异常
//    }
//    int result = num / den;
//    std::cout << "Result: " << result << std::endl;
//}
//
//int main() 
//{
//    try 
//    {
//        divide(10, 0); // 调用可能抛出异常的函数
//    }
//    catch (std::runtime_error& e) 
//    {
//        std::cout << "Exception caught: " << e.what() << std::endl; // 捕获并处理异常
//    }
//    return 0;
//}

//#include <iostream>
//
//void funcB() 
//{
//    throw std::runtime_error("Exception in funcB");
//}
//
//void funcA() 
//{
//    funcB();
//}
//
//int main() 
//{
//    try 
//    {
//        funcA();
//    }
//    catch (std::exception& e) 
//    {
//        std::cout << "Caught exception in main: " << e.what() << std::endl;
//    }
//
//    return 0;
//}


//#include <iostream>
//
//void funcA() 
//{
//    throw std::runtime_error("Error"); // 抛出异常
//}
//
//void funcB() 
//{
//    try
//    {
//        funcA();
//    }
//    catch (std::runtime_error& e) {
//        throw;   // 重新抛出
//    }
//}
//
//int main() 
//{
//    try 
//    {
//        funcB();
//    }
//    catch (std::runtime_error& e) 
//    {
//        std::cout << "Caught exception in main: " << e.what() << std::endl;
//    }
//    return 0;
//}

//#include <iostream>
//
//void performDatabaseOperation() 
//{
//    // 模拟数据库操作引发的异常
//    throw std::runtime_error("Database exception occurred");
//}
//
//void performNetworkOperation() 
//{
//    try 
//    {
//        performDatabaseOperation();
//    }
//    catch (std::runtime_error& e) 
//    {
//        std::cout << "Caught database exception in performNetworkOperation: " << e.what() << std::endl;
//        // 处理数据库异常后，重新抛出异常
//        throw;  // 重新抛出异常
//    }
//}
//
//void processRequest() 
//{
//    try 
//    {
//        performNetworkOperation();
//    }
//    catch (std::runtime_error& e) 
//    {
//        std::cout << "Caught network exception in processRequest: " << e.what() << std::endl;
//        // 处理网络异常后，重新抛出异常
//        throw;  // 重新抛出异常
//    }
//}
//
//int main() 
//{
//    try 
//    {
//        processRequest();
//    }
//    catch (std::runtime_error& e) 
//    {
//        std::cout << "Caught exception in main: " << e.what() << std::endl;
//        // 处理最终的异常或进行传播
//    }
//
//    return 0;
//}

//
//#include <iostream>
//
//// 自定义基础异常类
//class MyAppException : public std::exception 
//{
//public:
//    MyAppException(const char* message) : message_(message) {}
//
//    const char* what() const
//    {
//        return message_;
//    }
//
//private:
//    const char* message_;
//};
//
//// 自定义具体异常类
//class MySpecificException : public MyAppException 
//{
//public:
//    MySpecificException(const char* message) : MyAppException(message) {}
//};
//
//// 使用自定义异常
//class MyClass 
//{
//public:
//    void performOperation() 
//    {
//        // 模拟发生异常的操作
//        throw MySpecificException("Something went wrong");
//    }
//};
//
//int main() 
//{
//    try 
//    {
//        MyClass obj;
//        obj.performOperation();
//    }
//    catch (const MyAppException& e) 
//    {
//        std::cout << "Caught custom exception: " << e.what() << std::endl;
//    }
//
//    return 0;
//}

#include <iostream>
#include <cmath>

int main() {
    int n = 10;  // 调整这个值以改变爱心的大小
    std::string text = "湖南工业大学65周年";  // 要添加的文本

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <= 4 * n; ++j) {
            double d1 = sqrt(pow(i - n, 2) + pow(j - n, 2));
            double d2 = sqrt(pow(i - n, 2) + pow(j - 3 * n, 2));

            if (d1 < n + 0.5 || d2 < n + 0.5) {
                std::cout << "*";
            }
            else {
                std::cout << " ";
            }
        }
        if (i == n / 2) {
            std::cout << " " << text;
        }
        std::cout << std::endl;
    }

    for (int i = 1; i < 2 * n; ++i) {
        for (int j = 0; j < i; ++j) {
            std::cout << " ";
        }

        for (int j = 0; j < 4 * n + 1 - 2 * i; ++j) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }

    return 0;
}

