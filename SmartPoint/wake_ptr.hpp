#pragma once
#include "shared_ptr.hpp"

namespace wzh 
{
	template <class T>
	class weak_ptr
	{
	public:
		weak_ptr(T* ptr = nullptr) : _ptr(ptr)
		{}

		weak_ptr(const shared_ptr<T>& sp)
			: _ptr(sp._ptr)
		{}

		weak_ptr<T>& operator=(shared_ptr<T>& sp)
		{
			if (_ptr != sp.getPtr())
			{
				_ptr = sp.getPtr();
			}
			return *this;
		}

		T* operator->()
		{
			return _ptr;
		}

		T& operator*()
		{
			return *_ptr;
		}

	private:
		T* _ptr;
	};
}