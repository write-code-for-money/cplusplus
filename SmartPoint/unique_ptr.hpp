#pragma once

namespace wzh
{
	template <class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr) : _ptr(ptr)
		{}
		~unique_ptr()
		{
			if (_ptr) delete _ptr;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		unique_ptr(const unique_ptr<T>& uptr) = delete;
		//unique_ptr(unique_ptr<T>&& uptr)
		//	:_ptr(uptr._ptr)
		//{
		//	uptr._ptr = nullptr;
		//}
		
		unique_ptr<T>& operator=(const unique_ptr<T>& uptr) = delete;
		/*unique_ptr<T>& operator=(unique_ptr<T>&& uptr)
		{
			if (this != &uptr)
			{
				if (_ptr) delete _ptr;
				_ptr = uptr._ptr;
				uptr._ptr = nullptr;
			}
		}*/
	private:
		T* _ptr;
	};
}