#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

#include "unique_ptr.hpp"
#include "auto_ptr.hpp"
#include "shared_ptr.hpp"
#include "wake_ptr.hpp"

using namespace std;


//
//#include <iostream>
//#include <memory>
//
//int main()
//{
//    std::auto_ptr<int> autoPtr1(new int(42));
//    std::auto_ptr<int> autoPtr2;
//
//    // 将 autoPtr1 的所有权转移给 autoPtr2
//    autoPtr2 = autoPtr1;
//
//    // 这将导致 autoPtr1 指向 nullptr，autoPtr2 拥有原来的资源
//    std::cout << "autoPtr1: " << *autoPtr1 << std::endl;
//    std::cout << "autoPtr2: " << *autoPtr2 << std::endl;
//
//    // 尝试使用 autoPtr1，这会导致未定义行为，因为它已经为空指针
//    // 但编译器不会发出警告
//    //   
//    std::cout << "autoPtr1: " << *autoPtr1 << std::endl;
//
//    return 0;
//}


//#include <memory>
//#include <iostream>
//
//class CustomObject 
//{
//public:
//    CustomObject(const std::string& name) : name(name) 
//    {
//        std::cout << "CustomObject " << name << " created." << std::endl;
//    }
//
//    ~CustomObject() 
//    {
//        std::cout << "CustomObject " << name << " destroyed." << std::endl;
//    }
//
//    string* operator->()
//    {
//        return &_name;
//    }
//    string operator*()
//    {
//        return _name;
//    }
//private:
//    std::string _name;
//};
//
//void customDeleter(CustomObject* obj) 
//{
//    // 在删除器中执行清理操作
//    std::cout << "Cleaning up " << obj << " using custom deleter." << std::endl;
//    delete obj;
//}
//
//int main() 
//{
//    // 创建一个自定义对象
//    CustomObject* obj = new CustomObject("Object1");
//
//    // 使用自定义删除器的 shared_ptr
//    std::shared_ptr<CustomObject> customPtr(obj, customDeleter);
//
//    // 当 shared_ptr 离开作用域时，将调用删除器进行清理
//    return 0;
//}



#include <memory>

class A;
class B;

class A 
{
public:
    wzh::weak_ptr<B> bPtr;
};

class B 
{
public:
    wzh::weak_ptr<A> aPtr;
};

int main() 
{
   
    wzh::shared_ptr<A> a(new A);
    wzh::shared_ptr<B> b(new B);

    // 形成循环引用
    a->bPtr = b;
    b->aPtr = a;

    return 0;
}





//struct ListNode
//{
//	ListNode(int data) 
//		: _data(data)
//		, _prev(nullptr)
//		, _next(nullptr)
//	{}
//	~ListNode()
//	{}
//
//	int _data;
//	wzh::shared_ptr<ListNode> _prev;
//	wzh::shared_ptr<ListNode> _next;
//};
//
//int main()
//{
//	wzh::shared_ptr<ListNode> p1(new ListNode(10));
//	wzh::shared_ptr<ListNode> p2(new ListNode(20));
//
//	p1->_next = p2;
//	p2->_prev = p1;
//
//	cout << p1->_data << " " << p2->_data << endl;
//	return 0;
//}



//int main()
//{
//	wzh::shared_ptr<int> p1(new int(5));
//	wzh::shared_ptr<int> p2(p1);
//
//	cout << *p1 << " " << *p2 << endl;
//
//	*p1 = 10;
//	*p2 = 89;
//	cout << *p1 << " " << *p2 << endl;
//
//	return 0;
//}


//
//int main()
//{
// //std::auto_ptr<int> sp1(new int);
// //std::auto_ptr<int> sp2(sp1); // 管理权转移
//
// std::unique_ptr<int> sp1(new int);
// std::unique_ptr<int> sp2(move(sp1)); // 管理权转移
//
// // sp1悬空
// *sp2 = 10;
// cout << *sp2 << endl;
// cout << *sp1 << endl;
// return 0;
//}

//int main() {
//    std::auto_ptr<int> ptr1(new int(42));
//    std::auto_ptr<int> ptr2 = ptr1;  // 拷贝构造，实际上会转移资源所有权
//    std::cout << *ptr1 << std::endl;  // 会导致未定义行为，因为 ptr1 不再拥有资源
//    return 0;
//}




//int main() {
//    std::unique_ptr<int> ptr1 = std::make_unique<int>(42);
//    std::unique_ptr<int> ptr2 = std::move(ptr1);  // ptr1 的所有权被移动到 ptr2
//    std::cout << *ptr1 << std::endl;  // 输出 42
//    // 此时 ptr1 不再拥有资源，ptr2 拥有资源
//    return 0;  // 在此处，资源会被自动释放
//}


//int main()
//{
//	wzh::unique_ptr<int> p1(new int(1));
//	cout << *p1 << endl;
//	wzh::unique_ptr<int> p2(move(p1));
//	std::unique_ptr<int> sourcePtr(new int(42));
//	std::unique_ptr<int> destinationPtr(std::move(sourcePtr)); // 移动 sourcePtr 的资源到 destinationPtr
//	//std::unique_ptr<int> destinationPtr(sourcePtr); // 移动 sourcePtr 的资源到 destinationPtr
//}

//#include <iostream>
//#include <memory>
//
//int main() 
//{
//    std::auto_ptr<int> autoPtr1(new int(42));
//    std::auto_ptr<int> autoPtr2;
//
//    // 将 autoPtr1 的所有权转移给 autoPtr2
//    autoPtr2 = autoPtr1;
//
//    // 这将导致 autoPtr1 指向 nullptr，autoPtr2 拥有原来的资源
//    std::cout << "autoPtr1: " << *autoPtr1 << std::endl;
//    std::cout << "autoPtr2: " << *autoPtr2 << std::endl;
//
//    // 尝试使用 autoPtr1，这会导致未定义行为，因为它已经为空指针
//    // 但编译器不会发出警告
//    //   
//    std::cout << "autoPtr1: " << *autoPtr1 << std::endl;
//
//    return 0;
//}




//int main()
//{
//	autoPtr<int> p(new int(2));
//	std::cout << *p << std::endl;
//	*p = 10;
//	autoPtr<int> tmp(p);
//	std::cout << *tmp << std::endl;
//	return 0;
//}


//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	// 1、如果p1这里new 抛异常会如何？
//	// 2、如果p2这里new 抛异常会如何？
//	// 3、如果div调用这里又会抛异常会如何？
//	int* p1 = new int;
//	int* p2 = new int;
//	cout << div() << endl;
//	delete p1;
//	delete p2;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}