
template <class T>
class autoPtr
{
public:
    autoPtr(T* ptr = nullptr)
        :_ptr(ptr)
    {}
    autoPtr(autoPtr<T>& aptr)
        :_ptr(aptr._ptr)
    {
        aptr._ptr = nullptr;
    }
    ~autoPtr()
    {
        if (_ptr) delete _ptr;
    }
    autoPtr<T>& operator=(autoPtr<T>& aptr)
    {
        if (this != &aptr)
        {
            if (_ptr) delete _ptr;
            _ptr = aptr._ptr;
            aptr._ptr = nullptr;
        }
        return *this;
    }

    T* operator->()
    {
        return _ptr;
    }

    T& operator*()
    {
        return *_ptr;
    }

private:
    T* _ptr;
};
