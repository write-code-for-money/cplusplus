#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <string>
//using namespace std;
//int main()
//{
//	string arr1;
//	string arr2("abcdef");
//	string arr3(10, 'c');
//	string arr4(arr2);
//	cout << arr1 << endl << arr2 << endl << arr3 << endl << arr4;
//	return 0;
//}
//#include <iostream>
//#include <string>
//int main()
//{
//	std::string str("Test string");
//	std::cout << "The size of str is " << str.size() << " bytes.\n";
//	return 0;
//}
//int main()
//{
//	std::string str("Test string");
//	std::cout << "The size of str is " << str.length() << " bytes.\n";
//	return 0;
//}

//#include <iostream>
//#include <string>
//using namespace std;
//int main()
//{
//	string str("Test string");
//	if (str.empty())
//	{
//		cout << "空" << endl;
//	}
//	else
//	{
//		cout << "非空" << endl;
//	}
//	str.clear();
//	if (str.empty())
//	{
//		cout << "空" << endl;
//	}
//	else
//	{
//		cout << "非空" << endl;
//	}
//	return 0;
//}
//void test1()
//{
//	string arr("abcdefgh");
//	string::reverse_iterator rbegin = arr.rbegin(), rend = arr.rend();
//	while (rbegin != rend)
//	{
//		cout << *rbegin;
//		rbegin++;
//	}
//}
//
//
//void test2()
//{
//	string arr("abcdefg***************X222222vvvv");
//	cout << arr.substr(0, 5) << endl;
//	cout << arr.substr() << endl;
//	cout << arr.substr(5) << endl;
//}


//void test3()
//{
//	// 动态申请一个int类型的空间
//	int* ptr4 = new int;
//
//	// 动态申请一个int类型的空间并初始化为10
//	int* ptr5 = new int(10);
//
//	// 动态申请10个int类型的空间
//	int* ptr6 = new int[3];
//	delete ptr4;
//	delete ptr5;
//	delete[] ptr6;
//}
//int main()
//{
//	test3();
//	return 0;
//}

//class A
//{
//public:
//    A(int a = 0)
//        : _a(a)
//    {
//        cout << "A():" << this << endl;
//    }
//    ~A()
//    {
//        cout << "~A():" << this << endl;
//    }
//private:
//    int _a;
//};
//int main()
//{
//    //自定义类型
//    A* p1 = (A*)malloc(sizeof(A));
//    A* p2 = new A(1);
//    free(p1);
//    delete p2;
//    // 内置类型
//    int* p3 = (int*)malloc(sizeof(int)); // C
//    int* p4 = new int;
//    free(p3);
//    delete p4;
//    A* p5 = (A*)malloc(sizeof(A) * 10);
//    A* p6 = new A[10];
//    free(p5);
//    delete[] p6;
//    return 0;
//}



//
//class A
//{
//public:
//	A(int a = 0)
//		: _a(a)
//	{
//		cout << "A():" << this << endl;
//	}
//	~A()
//	{
//		cout << "~A():" << this << endl;
//	}
//private:
//	int _a;
//};
//// 定位new/replacement new
//int main()
//{
//	// p1现在指向的只不过是与A对象相同大小的一段空间，还不能算是一个对象，因为构造函数没有执行
//	A* p1 = (A*)malloc(sizeof(A));
//	new(p1)A;  // 注意：如果A类的构造函数有参数时，此处需要传参
//	p1->~A();
//	free(p1);
//	A* p2 = (A*)operator new(sizeof(A));
//	new(p2)A(10);
//	p2->~A();
//	operator delete(p2);
//	return 0;
//}


