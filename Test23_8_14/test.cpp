//#include <iostream>
//#include <vector>
//#include <string>
//#include <unordered_map>
//using namespace std;

//vector<int> findSubstring(string s, vector<string>& words) {
//    vector<int> ret;
//    unordered_map<string, int> map_words;
//    int sz = words[0].size(), len = words.size();
//
//    for (auto& e : words) map_words[e]++; //统计words字符串出现的次数
//    int i = 0;
//    while (i < sz)
//    {
//        unordered_map<string, int> map_s;
//        int count = 0;
//        for (int left = i, right = i; right < s.size(); right += sz)
//        {
//            int offset = right + sz > s.size() ? s.size() : right + sz;
//            string str_in(s.begin() + right, s.begin() + offset);
//            if (++map_s[str_in] <= map_words[str_in]) count++; //进窗口 + 维护count
//            while ((right - left) / sz + 1 > len) //判断
//            {
//                string str_out(s.begin() + left, s.begin() + left + sz);
//                left += sz;
//                if (map_s[str_out]-- <= map_words[str_out]) count--; //出窗口 + 维护count
//            }
//            if (count == len) ret.push_back(left);
//        }
//        i++;
//    }
//
//    return ret;
//}
//
//int main()
//{
//    string s = "wordgoodgoodgoodbestword";
//    vector<string> words = { "word", "good", "best", "good" };
//    vector<int> ret = findSubstring(s, words);
//    for (int e : ret) cout << e << endl;
//    return 0;
//}

//int minOperations(vector<int>& nums, int x) {
//    //1.定义滑动窗口的左右边界
//    int begin = 0, end = 0, len = 0, sum = 0, sz = nums.size();
//    //2.依次进窗口
//    while (begin < sz)
//    {
//        int cur = end % sz;
//        sum += nums[cur];
//        end++, len++;
//        //3.判断
//        if (sum == x) return len;
//        if (sum > x)
//        {
//            //4.出窗口
//            while (sum > x && begin < nums.size())
//            {
//                sum -= nums[begin++];
//                len--;
//            }
//        }
//    }
//    return -1;
//}
//
//int main()
//{
//    vector<int> nums = { 1, 1, 4, 2, 3 };
//    int x = 5;
//    cout << minOperations(nums, x) << endl;
//    return 0;
//}

//int main()
//{
//	//int* p = &10;
//	//int* p = &(10 + 20);
//	char str[1024];
//	//gets(str);
//	cout << strlen(str) << endl;
//	return 0;
//}

//#include <iostream>
////#pragma pack(1)
//struct MyClass {
//    int a;
//    char b;
//    double e;
//    char d;
//};
////#pragma pack()
//int main() {
//    std::cout << "Size of MyClass: " << sizeof(MyClass) << " bytes" << std::endl;
//    std::cout << offsetof(MyClass, e) << std::endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//union test {
//    int a;
//    char b;
//} c;
//int main() {
//    c.a = 1;
//    if (c.b == 1) {
//        cout << "小端" << endl;
//    }
//    else {
//        cout << "大端" << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main() {
//    int n, m, q;
//    cin >> n >> m >> q;
//    vector<vector<long long>> arr(m + 1, vector<long long>(n + 1, 0)), dp(m + 1, vector<long long>(n + 1, 0));
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> arr[i][j];
//            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + arr[i][j];
//        }
//    }
//    for (int i = 0; i < q; i++)
//    {
//        int x1, y1, x2, y2;
//        cin >> x1 >> y1 >> x2 >> y2;
//        cout << dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1] << endl;
//    }
//
//    return 0;
//}


#include <iostream>
#include <vector>
using namespace std;

//int singleNumber(vector<int>& nums) {
//
//    // 0 ^ x = x,
//    // x ^ x = 0；
//    // x & ~x = 0,
//    // x & ~0 =x;
//    int a = 0, b = 0;
//    for (auto x : nums) {
//        b = (b ^ x) & ~a;
//        a = (a ^ x) & ~b;
//    }
//    return b;
//}
//
//int main()
//{
//    vector<int> arr = { 3, 1, 3, 1, 3, 1, 99 };
//    int ret = singleNumber(arr);
//}

//
//typedef char* pstring;
//int main()
//{
//	const pstring p1 = nullptr;    // 编译成功还是失败？
//	const pstring* p2;   // 编译成功还是失败？
//	cout << typeid(p1).name() << endl;
//	cout << typeid(p2).name() << endl;
//	return 0;
//}
int main()
{
    int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    for (auto element : arr) {
        cout << element << " ";
    }
    return 0;
}

