#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
using namespace std;
namespace wzh
{
    
    template<class T, class Container = vector<T>, class Compare = less<typename Container::value_type>>
    class priority_queue
    {
    public:
        priority_queue() {}

        template<class InputIterator>
        priority_queue(InputIterator first, InputIterator last)
        {
            c.reserve(last - first);
            while (first != last)
            {
                c.push_back(*first);
                ++first;
            }
            make_heap(c.begin(), c.end(), comp);
        }

        bool empty() const
        {
            return c.empty();
        }

        size_t size() const
        {
            return c.size();
        }

        const T& top() const
        {
            return c.front();
        }

        void push(const T& x)
        {
            c.push_back(x);
            push_heap(c.begin(), c.end(), comp);
        }

        void pop()
        {
            pop_heap(c.begin(), c.end(), comp);
            c.pop_back();
        }

    private:
        Container c;
        Compare comp;
    };
}

