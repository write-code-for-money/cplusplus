#include "priority_queue.h"
int main()
{
    wzh::priority_queue<int> q;

    q.push(3);
    q.push(1);
    q.push(4);
    q.push(1);

    while (!q.empty())
    {
        cout << q.top() << endl;
        q.pop();
    }
    return 0;
}

//
//#include "reverse_iteractor.h"
//using namespace std;
//int main()
//{
//    vector<int> v = { 1, 2, 3, 4, 5 };
//    list<int> l = { 1, 2, 3, 4, 5 };
//
//     //vector反向迭代器
//    wzh::vector_reverse_iterator<vector<int>::iterator> v_rbegin(v.end());
//    wzh::vector_reverse_iterator<vector<int>::iterator> v_rend(v.begin());
//    for (auto it = v_rbegin; it != v_rend; ++it)
//    {
//        cout << *it << " ";
//    }
//    cout << endl;
//
//     //list反向迭代器
//    wzh::list_reverse_iterator<list<int>::iterator> l_rbegin(l.end());
//    wzh::list_reverse_iterator<list<int>::iterator> l_rend(l.begin());
//    for (auto it = l_rbegin; it != l_rend; ++it)
//    {
//        cout << *it << " ";
//    }
//    cout << endl;
//
//    return 0;
//}


//#include <iostream>
//#include <stack>
//using namespace std;
//
//int main()
//{
//    // 创建一个空的栈
//    stack<int> s;
//    // 检查栈是否为空
//    if (s.empty())
//    {
//        cout << "栈为空" << endl;
//    }
//    // 将元素压入栈中
//    s.push(1);
//    s.push(2);
//    s.push(3);
//    // 获取栈中元素的数量
//    cout << "栈中元素的数量为：" << s.size() << endl;
//    // 获取栈顶元素的引用
//    cout << "栈顶元素为：" << s.top() << endl;
//    // 弹出栈顶元素
//    s.pop();
//    cout << "弹出栈顶元素" << endl;
//    // 获取栈中元素的数量
//    cout << "栈中元素的数量为：" << s.size() << endl;
//    // 再次获取栈顶元素的引用
//    cout << "栈顶元素为：" << s.top() << endl;
//    return 0;
//}

//#include <iostream>
//#include <queue>
//using namespace std;
//
//int main() {
//    queue<int> q;
//    q.push(1);
//    q.push(2);
//    q.push(3);
//    cout << "队列大小：" << q.size() << endl;  
//    cout << "队头元素：" << q.front() << endl;  
//    cout << "队尾元素：" << q.back() << endl;  
//    q.pop();
//    cout << "删除队头元素后，队列大小：" << q.size() << endl; 
//    cout << "新的队头元素：" << q.front() << endl;  
//    return 0;
//}

//#include <iostream>
//#include <queue>
//using namespace std;
//
//int main() {
//    priority_queue<int> pq;
//    pq.push(3);
//    pq.push(1);
//    pq.push(4);
//    pq.push(1);
//    cout << "堆顶元素：" << pq.top() << endl;  
//    pq.pop();
//    cout << "删除堆顶元素后，新的堆顶元素：" << pq.top() << endl;  
//    return 0;
//}

//#include <iostream>
//#include <queue>
//using namespace std;
//
//int main()
//{
//    priority_queue<int, vector<int>, greater<int>> minHeap;
//    minHeap.push(3);
//    minHeap.push(1);
//    minHeap.push(4);
//    minHeap.push(1);
//    cout << "堆顶元素：" << minHeap.top() << endl;
//    minHeap.pop();
//    cout << "删除堆顶元素后，新的堆顶元素：" << minHeap.top() << endl;
//    return 0;
//}