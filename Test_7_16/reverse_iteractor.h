#pragma once
#include <iostream>
#include <vector>
#include <list>

// 将以下代码适配到vector和list中做反向迭代器，理解反向迭代器的原理
namespace wzh
{
    // 适配器 -- 复用
    template<class Iterator, class Ref, class Ptr>
    struct Reverse_iterator
    {
        Iterator _it;
        typedef Reverse_iterator<Iterator, Ref, Ptr> self;

        // 构造函数
        Reverse_iterator(Iterator it) : _it(it) {}

        // 解引用操作符
        Ref operator*() const
        {
            Iterator temp = _it;
            return *(--temp);
        }

        // 箭头操作符
        Ptr operator->() const
        {
            return &(operator*());
        }

        // 前置++操作符
        self& operator++()
        {
            --_it;
            return *this;
        }

        // 后置++操作符
        self operator++(int)
        {
            self temp(*this);
            --_it;
            return temp;
        }

        // 前置--操作符
        self& operator--()
        {
            ++_it;
            return *this;
        }

        // 后置--操作符
        self operator--(int)
        {
            self temp(*this);
            ++_it;
            return temp;
        }

        // 加法操作符
        self operator+(int n) const
        {
            return self(_it - n);
        }

        // 减法操作符
        self operator-(int n) const
        {
            return self(_it + n);
        }

        // 自增操作符
        self& operator+=(int n)
        {
            _it -= n;
            return *this;
        }

        // 自减操作符
        self& operator-=(int n)
        {
            _it += n;
            return *this;
        }

        // 下标操作符
        Ref operator[](int n) const
        {
            return *(*this + n);
        }

        // 友元函数
        friend bool operator==(const self& lhs, const self& rhs)
        {
            return lhs._it == rhs._it;
        }

        friend bool operator!=(const self& lhs, const self& rhs)
        {
            return !(lhs == rhs);
        }

        friend bool operator<(const self& lhs, const self& rhs)
        {
            return lhs._it > rhs._it;
        }

        friend bool operator>(const self& lhs, const self& rhs)
        {
            return lhs._it < rhs._it;
        }

        friend bool operator<=(const self& lhs, const self& rhs)
        {
            return !(lhs > rhs);
        }

        friend bool operator>=(const self& lhs, const self& rhs)
        {
            return !(lhs < rhs);
        }
    };

    // vector反向迭代器实现
    template<class Iterator>
    class vector_reverse_iterator : public Reverse_iterator<Iterator, typename Iterator::reference, typename Iterator::pointer>
    {
    public:
        typedef Reverse_iterator<Iterator, typename Iterator::reference, typename Iterator::pointer> base;
        typedef vector_reverse_iterator<Iterator> self;

        // 构造函数
        vector_reverse_iterator(Iterator it) : base(it) {}

        // 拷贝构造函数
        vector_reverse_iterator(const self& rhs) : base(rhs._it) {}

        // 重载赋值操作符
        self& operator=(const self& rhs)
        {
            this->_it = rhs._it;
            return *this;
        }
    };

    // list反向迭代器实现
    template<class Iterator>
    class list_reverse_iterator : public Reverse_iterator<Iterator, typename Iterator::reference, typename Iterator::pointer>
    {
    public:
        typedef Reverse_iterator<Iterator, typename Iterator::reference, typename Iterator::pointer> base;
        typedef list_reverse_iterator<Iterator> self;

        // 构造函数
        list_reverse_iterator(Iterator it) : base(it) {}

        // 拷贝构造函数
        list_reverse_iterator(const self& rhs) : base(rhs._it) {}

        // 重载赋值操作符
        self& operator=(const self& rhs)
        {
            this->_it = rhs._it;
            return *this;
        }

        // 前置++操作符
        self& operator++()
        {
            --this->_it;
            return *this;
        }

        // 后置++操作符
        self operator++(int)
        {
            self temp(*this);
            --this->_it;
            return temp;
        }

        // 前置--操作符
        self& operator--()
        {
            ++this->_it;
            return *this;
        }

        // 后置--操作符
        self operator--(int)
        {
            self temp(*this);
            ++this->_it;
            return temp;
        }
    };
}