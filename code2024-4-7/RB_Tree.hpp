#pragma once

#include <iostream>

namespace wzh
{
	enum Color
	{
		RED,
		BLACK
	};

	template <class K, class V>
	struct RB_Node
	{
		typedef std::pair<K, V> Pair;
		typedef RB_Node<K, V> Node;
		RB_Node(const Pair& node)
			:_val(node)
			,_col(RED)
			,_left(nullptr)
			,_right(nullptr)
			,_parent(nullptr)
		{}

		Pair _val;
		Color _col;
		Node* _left;
		Node* _right;
		Node* _parent;
	};

	template <class K, class V>
	class RBTree
	{
		typedef RB_Node<K, V> Node;
	public:
		RBTree()
			:_root(nullptr)
		{}

		~RBTree()
		{}

		void InSert(const K& key, const V& val)
		{
			if (_root == nullptr)
			{
				_root = new Node({ key, val });
				_root->_col = BLACK;
				return;
			}

			Node* cur = _root;
			Node* prev = nullptr;
			while (cur)
			{
				if (key > cur->_val.first)
				{
					prev = cur;
					cur = cur->_right;
				}
				else if (key < cur->_val.first)
				{
					prev = cur;
					cur = cur->_left;
				}
				else return;
			}

			Node* node = new Node({ key, val });
			if (prev->_val.first > key)
			{
				prev->_left = node;
			}
			else
			{
				prev->_right = node;
			}
			node->_parent = prev;
			if (prev->_col == RED)
			{
				UpDate(prev, node);
			}
		}

		// 检查红黑树是否符合红黑树性质
		bool isRedBlackTree() const
		{
			if (!_root) {
				return true; // 空树是红黑树
			}
			// 检查根节点是否为黑色
			if (_root->_col != BLACK) {
				std::cout << "Violation: Root node is not black" << std::endl;
				return false;
			}
			// 检查根节点的父节点是否为空
			if (_root->_parent) {
				std::cout << "Violation: Root node is not the true root" << std::endl;
				return false;
			}
			// 计算黑色节点的高度
			int blackHeight = 0;
			const RB_Node<K, V>* node = _root;
			while (node) {
				if (node->_col == BLACK) {
					blackHeight++;
				}
				node = node->_left;
			}
			// 从根节点开始递归检查
			return isRedBlackTreeHelper(_root, blackHeight, 0);
		}
	private:
		// 辅助函数：递归检查红黑树的性质
		bool isRedBlackTreeHelper(const RB_Node<K, V>* node, int blackHeight, int currentHeight) const 
		{
			if (!node) {
				return blackHeight == currentHeight; // 空节点视为黑色
			}

			// 如果节点是红色，检查其子节点是否是黑色
			if (node->_col == RED) {
				if (node->_left && node->_left->_col == RED) {
					std::cout << "Violation: Red node with red left child" << std::endl;
					return false;
				}
				if (node->_right && node->_right->_col == RED) {
					std::cout << "Violation: Red node with red right child" << std::endl;
					return false;
				}
			}

			// 如果节点是黑色，则当前路径的黑色节点数加一
			if (node->_col == BLACK) {
				currentHeight++;
			}

			// 递归检查左右子树
			return isRedBlackTreeHelper(node->_left, blackHeight, currentHeight) &&
				isRedBlackTreeHelper(node->_right, blackHeight, currentHeight);
		}

		void UpDate(Node* parent, Node* cur)
		{
			while (parent && parent->_col == RED)
			{
				Node* grandparent = parent->_parent;
				Node* uncle = nullptr;
				if (grandparent->_left == parent) uncle = grandparent->_right;
				else uncle = grandparent->_left;

				if (!uncle || uncle->_col == BLACK)
				{
					if (grandparent->_left == parent && parent->_left == cur)
					{
						RevoleR(grandparent);
						parent->_col = BLACK;
						grandparent->_col = RED;
					}
					else if (grandparent->_right == parent && parent->_right == cur)
					{
						RevoleL(grandparent);
						parent->_col = BLACK;
						grandparent->_col = RED;
					}
					else if (grandparent->_left == parent && parent->_right == cur)
					{
						RevoleLR(grandparent);
						grandparent->_col = RED;
						cur->_col = BLACK;
					}
					else if (grandparent->_right == parent && parent->_left == cur)
					{
						RevoleRL(grandparent);
						grandparent->_col = RED;
						cur->_col = BLACK;
					}
					else assert(0);
					if (grandparent->_parent == nullptr) grandparent->_col = BLACK;
					return;
				}
				else if (uncle && uncle->_col == RED)
				{
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandparent->_col = RED;
					parent = grandparent->_parent;
					cur = grandparent;
				}
			}
			if (parent == nullptr)
			{
				cur->_col = BLACK;
			}
		}

		void RevoleLR(Node* grandparent)
		{
			RevoleL(grandparent->_left);
			RevoleR(grandparent);
		}

		void RevoleRL(Node* grandparent)
		{
			RevoleR(grandparent->_right);
			RevoleL(grandparent);
		}

		void RevoleL(Node* grandparent)
		{
			Node* subR = grandparent->_right;
			Node* subRL = subR->_left;
			Node* ggrand = grandparent->_parent;

			grandparent->_right = subRL;
			if (subRL) subRL->_parent = grandparent;
			subR->_left = grandparent;
			grandparent->_parent = subR;

			if (ggrand)
			{
				if (ggrand->_left == grandparent)
					ggrand->_left = subR;
				else
					ggrand->_right = subR;
			}
			else
			{
				_root = subR;
			}
			subR->_parent = ggrand;
		}

		void RevoleR(Node* grandparent)
		{
			Node* subL = grandparent->_left;
			Node* subLR = subL->_right;
			Node* ggrand = grandparent->_parent;

			grandparent->_left = subLR;
			if (subLR) subLR->_parent = grandparent;
			subL->_right = grandparent;
			grandparent->_parent = subL;

			if (ggrand)
			{
				if (ggrand->_left == grandparent)
					ggrand->_left = subL;
				else
					ggrand->_right = subL;
			}
			else
			{
				_root = subL;
			}
			subL->_parent = ggrand;
		}
	private:
		Node* _root;
	};

}