#pragma once

#include <iostream>
#include <assert.h>

namespace wzh
{
	template<class K, class V>
	struct TreeNode
	{
		TreeNode(const K& key, const V& val)
			:_key(key), _val(val)
			,_bf(0)
			,_left(nullptr)
			,_right(nullptr)
			,_parent(nullptr)
		{}

		K _key;
		V _val;
		int _bf;
		TreeNode* _left;
		TreeNode* _right;
		TreeNode* _parent;
	};

	template<class K, class V>
	class SBTree
	{
		typedef TreeNode<K, V> Node;
		typedef SBTree<K, V> self;
	public:

		SBTree()
			:_root(nullptr)
		{}

		SBTree(const self& tree)
		{
			_root = Copy(tree._root);
		}

		SBTree& operator=(const self& tree)
		{
			if (this == &tree) return *this;
			Destroy(_root);
			_root = Copy(tree._root);
			return *this;
		}

		~SBTree()
		{
			Destroy(_root);
		}

		bool InSert(const K& key, const V& val)
		{
			Node* node = new Node(key, val);
			if (_root == nullptr)
			{
				_root = node;
				return true;
			}
			Node* prev = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else
				{
					delete node;
					return true;
				}
			}
			if (prev->_key > key)
			{
				prev->_left = node;
			}
			else
			{
				prev->_right = node;
			}
			node->_parent = prev;
			upDate_In(node, prev);
			return true;
		}

		bool Erase(const K& key)
		{
			Node* cur = _root;
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_key > key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else break;
			}
			if (cur == nullptr) return false;
			// 情况一：叶子结点
			if (cur->_left == nullptr && cur->_right == nullptr)
			{
				if (prev == nullptr) _root = nullptr;
				else if (prev->_left == cur)
				{
					prev->_left = nullptr;
					prev->_bf++;
					while (prev->_right == nullptr)
					{
						Node* parent = prev->_parent;
						if (parent == nullptr) break;
						if (parent->_left == prev) parent->_bf++;
						else parent->_bf--;
						prev = parent;
					}
				}
				else
				{
					prev->_right = nullptr;
					prev->_bf--;
				}
				delete cur;
				cur = nullptr;
			}
			// 情况二：有一个孩子
			else if ((cur->_left == nullptr && cur->_right) || (cur->_left && cur->_right == nullptr))
			{
				if (prev == nullptr)
				{
					if (cur->_left) _root = cur->_left;
					else _root = cur->_right;
				}
				else if (prev->_left == cur)
				{
					if (cur->_left) prev->_left = cur->_left;
					else prev->_left = cur->_right;
					prev->_bf++;
				}
				else
				{
					if (cur->_left) prev->_right = cur->_left;
					else prev->_right = cur->_right;
					prev->_bf--;
				}
				delete cur;
				cur = nullptr;
			}
			// 情况三：有两个孩子，找右边最小节点替换，然后删除右边最小
			else
			{
				Node* scur = cur->_right;
				Node* sprev = cur;
				Node* spprev = prev;
				while (scur)
				{
					spprev = sprev;
					sprev = scur;
					scur = scur->_left;
				}
				cur->_key = sprev->_key;
				cur->_val = sprev->_val;
				if (spprev->_left == sprev)
				{
					spprev->_left = sprev->_right;
					spprev->_bf++;
				}
				else
				{
					spprev->_right = sprev->_right;
					spprev->_bf--;
				}
				delete sprev;

			}
			return true;
		}

		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key) cur = cur->_left;
				else if (cur->_key < key) cur = cur->_right;
				else return true;
			}
			return false;
		}

		void Print()
		{
			InOrder(_root);
		}

		bool IsBalance()
		{
			return _IsBalance(_root);
		}

		int Height()
		{
			return _Height(_root);
		}


	private:
		void upDate_Out(Node* cur)
		{
			if (cur->_bf == -2)
			{
				if (cur->_left->_left) RotateR(cur);
				else RotateLR(cur);
			}
			else if (cur->_bf == 2)
			{
				if (cur->_right->_right) RotateL(cur);
				else RotateRL(cur);
			}
		}

		int _Height(Node* root)
		{
			if (root == NULL)
				return 0;

			int leftH = _Height(root->_left);
			int rightH = _Height(root->_right);

			return leftH > rightH ? leftH + 1 : rightH + 1;
		}

		bool _IsBalance(Node* root)
		{
			if (root == NULL)
			{
				return true;
			}

			int leftH = _Height(root->_left);
			int rightH = _Height(root->_right);

			if (rightH - leftH != root->_bf)
			{
				std::cout << root->_key << "节点平衡因子异常" << std::endl;
				return false;
			}

			return abs(leftH - rightH) < 2
				&& _IsBalance(root->_left)
				&& _IsBalance(root->_right);
		}


		void upDate_In(Node* cur, Node* parent)
		{
			while (parent)
			{
				if (parent->_left == cur) parent->_bf--;
				else parent->_bf++;
				if (parent->_bf == 0) return;
				if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateR(parent);
					return;
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
					return;
				}
				else if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);
					return;
				}
				else if(parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
					return;
				}
				else
				{
					cur = parent;
					parent = parent->_parent;
				}
			}
		}

		void RotateL(Node* parent)
		{
			Node* subR = parent->_right;
			Node* subRL = subR->_left;

			parent->_right = subRL;
			if (subRL)
				subRL->_parent = parent;

			Node* ppnode = parent->_parent;

			subR->_left = parent;
			parent->_parent = subR;

			if (ppnode == nullptr)
			{
				_root = subR;
				_root->_parent = nullptr;
			}
			else
			{
				if (ppnode->_left == parent)
				{
					ppnode->_left = subR;
				}
				else
				{
					ppnode->_right = subR;
				}

				subR->_parent = ppnode;
			}

			parent->_bf = subR->_bf = 0;
		}

		void RotateR(Node* parent)
		{
			Node* subL = parent->_left;
			Node* subLR = subL->_right;

			parent->_left = subLR;
			if (subLR)
				subLR->_parent = parent;

			Node* ppnode = parent->_parent;

			subL->_right = parent;
			parent->_parent = subL;

			if (parent == _root)
			{
				_root = subL;
				_root->_parent = nullptr;
			}
			else
			{
				if (ppnode->_left == parent)
				{
					ppnode->_left = subL;
				}
				else
				{
					ppnode->_right = subL;
				}
				subL->_parent = ppnode;
			}

			subL->_bf = parent->_bf = 0;
		}

		void RotateLR(Node* parent)
		{
			Node* subL = parent->_left;
			Node* subLR = subL->_right;
			int bf = subLR->_bf;

			RotateL(parent->_left);
			RotateR(parent);

			if (bf == 1)
			{
				subL->_bf = -1;
			}
			else if (bf == -1)
			{
				parent->_bf = 1;
			}
		}

		void RotateRL(Node* parent)
		{
			Node* subR = parent->_right;
			Node* subRL = subR->_left;
			int bf = subRL->_bf;

			RotateR(parent->_right);
			RotateL(parent);

			if (bf == 1)
			{
				parent->_bf = -1;
			}
			else if (bf == -1)
			{
				subR->_bf = 1;
			}
		}

		void InOrder(Node* cur)
		{
			if (cur == nullptr) return;
			InOrder(cur->_left);
			std::cout << "key: " << cur->_key << " val: " << cur->_val << std::endl;
			InOrder(cur->_right);
		}

		void Destroy(Node* cur)
		{
			if (cur == nullptr) return;
			Destroy(cur->_left);
			Destroy(cur->_right);
			delete cur;
		}

		Node* Copy(Node* cur)
		{
			if (cur == nullptr) return nullptr;
			Node* node = new Node(cur->_key, cur->_val);
			node->_left = Copy(cur->_left);
			node->_right = Copy(cur->_right);
			return node;
		}

	private:
		Node* _root;
	};
}