#pragma once
#include <iostream>
#include <assert.h>

namespace wzh
{

	class MyString
	{
		friend std::ostream& operator<<(std::ostream& out, const MyString& mstr);
	public:
		MyString(const char* str)
			:_size(0), _capacity(0)
		{
			_capacity = _size = strlen(str);
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		MyString()
			:_str(new char[1]),
			_size(0), _capacity(0)
		{
			_str[0] = '\0';
		}

		MyString(const MyString& mstr)
			:_size(mstr._size),
			_capacity(mstr._capacity)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, mstr._str);
		}

		MyString& operator=(const MyString& mstr)
		{
			//自己给自己赋值
			//释放原资源
			//防止浅拷贝
			//返回引用连续赋值
			if (this != &mstr)
			{
				char* tmp = new char[mstr._capacity + 1];
				delete _str;
				_str = tmp;
				_size = mstr._size;
				_capacity = mstr._capacity;
				strcpy(_str, mstr._str);
			}
			return *this;
		}

		char operator[](size_t index)
		{
			return _str[index];
		}

		MyString& operator+=(const MyString& mstr)
		{
			InSert(_size, mstr);
			return *this;
		}

		MyString& operator+=(const char* str)
		{
			InSert(_size, str);
			return *this;
		}

		MyString& operator+=(const char ch)
		{
			InSert(_size, 1, ch);
			return *this;
		}

		~MyString()
		{
			delete _str;
			_str = nullptr;
			_size = _capacity = 0;
		}

		size_t Size()
		{
			return _size;
		}

		size_t Capacity()
		{
			return _capacity;
		}

		void ReSize(size_t size, char ch = '\0')
		{
			if (size > _size)
			{
				if (size > _capacity)
				{
					ReServer(size);
				}
				memset(_str + _size, ch, size - _size);
			}
			_str[size] = '\0';
			_size = size;
		}

		void ReServer(size_t size)
		{
			if (size > _capacity)
			{
				MyString tmp(_str);
				_str = new char[size + 1];
				_capacity = size;
				strcpy(_str, tmp._str);
			}
		}

		void Clear()
		{
			_size = _capacity = 0;
			_str[0] = '\0';
		}

		bool Empty()
		{
			return _size == 0 ? true : false;
		}

		char Front()
		{
			return _str[0];
		}

		char Back()
		{
			return _str[_size - 1];
		}

		void InSert(size_t pos, const char* str)
		{
			if (pos > _size)
			{
				std::cout << "inout error\n";
				return;
			}
			size_t len = strlen(str);
			if (_size + len > _capacity) ReServer(2 * _capacity + len);
			memmove(_str + pos + len, _str + pos, sizeof(char) * (_size - pos + 1));
			memcpy(_str + pos, str, sizeof(char) * len);
			_size += len;
		}

		void InSert(size_t pos, const MyString& mstr)
		{
			InSert(pos, mstr._str);
		}

		void InSert(size_t pos, const size_t n, const char ch)
		{
			char* str = new char[n + 1];
			for (int i = 0; i < n; i++)
			{
				str[i] = ch;
			}
			str[n] = '\0';
			InSert(pos, str);
			delete[] str;
		}

		void Push_back(const char ch)
		{
			InSert(_size, 1, ch);
		}

		void Pop_back()
		{
			Erase(_size - 1, 1);
		}
		
		void Erase(size_t pos, size_t len)
		{
			assert(pos >= 0 && pos < _size);
			memcpy(_str + pos, _str + pos + len, _size - pos + 1);
			_size -= len;
		}

		void Swap(MyString& mstr)
		{
			MyString tmp(mstr);
			mstr = *this;
			*this = tmp;
		}

		const char* C_Str()
		{
			return _str;
		}

		size_t Find(const char* str, size_t pos = 0)
		{
			for (int i = pos; i < _size; i++)
			{
				if (_str[i] == str[0] && Compare(_str + i, str))
				{
					return i;
				}
			}
			return -1;
		}

		size_t Find(const char ch, size_t pos = 0)
		{
			for (int i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			return -1;
		}

		size_t Find(const MyString& mstr, size_t pos = 0)
		{
			return Find(mstr._str, pos);
		}

		bool Compare(const char* str1, const char* str2)
		{
			assert(str1 && str2);
			while (*str1 != '\0' && *str2 != '\0' && *str1 == *str2) str1++, str2++;
			if (str2 && *str2 == '\0') return true;
			return false;
		}

		MyString SubStr(size_t pos = 0, size_t len = -1)
		{
			MyString tmp;
			for (int i = pos; i < _size && len--; i++)
			{
				tmp.Push_back(_str[i]);
			}
			return tmp;
		}
	private:
		char* _str;
		int _size;
		int _capacity;
	};

	std::ostream& operator<<(std::ostream& out, const MyString& mstr)
	{
		if(mstr._str) out << mstr._str;
		return out;
	}
}