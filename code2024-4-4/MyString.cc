#define _CRT_SECURE_NO_WARNINGS 1

#include "MyString.h"

int main()
{
	wzh::MyString str1("abcde");
	wzh::MyString str2("123456");
	wzh::MyString str3(str1);

	str2 = str1;
	std::cout << str2 << std::endl;
	//std::cout << str1.Find("bcde", 3) << std::endl;
	//std::cout << str1.Find('a') << std::endl;
	//std::cout << str1.Find(str3) << std::endl;
	//std::cout << str1.SubStr(2,2) << std::endl;

	//str1.Swap(str2);
	//std::cout << "size: " << str1.Size() << " capa: " << str1.Capacity() << std::endl;
	//std::cout << str1 << std::endl;

	//std::cout << str1.C_Str() << std::endl;
	//str.InSert(0, "123");
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.InSert(4, "000");
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.InSert(20, "000");
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//wzh::MyString tmp(str);
	//str.InSert(2, tmp);
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.InSert(2, 5, '9');
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str += str;
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str += "oooo";
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str += 'Z';
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.Push_back('V');
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.Push_front('P');
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.Erase(0, 1);
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;
	//str.Erase(0, 10);
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.Erase(3, 5);
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.Pop_back();
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;
	//str.InSert(50, "000");
	//std::cout << str << std::endl;
	//std::cout << str.Front() << " : " << str.Back() << std::endl;
	//std::cout << str[0] << std::endl;
	//std::cout << str[1] << std::endl;
	//std::cout << str[5] << std::endl;
	//std::cout << str[7] << std::endl;
	//std::cout << str[100] << std::endl;
	//std::cout << str[-1] << std::endl;

	//printf("%d %d\n", str.Size(), str.Capacity());
	//str.ReSize(20, 'a');
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.ReSize(10);
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.ReServer(5);
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//str.ReServer(25);
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//std::cout << str.Empty() << std::endl;

	//str.Clear();
	//std::cout << "size: " << str.Size() << " capa: " << str.Capacity() << std::endl;
	//std::cout << str << std::endl;

	//std::cout << str.Empty() << std::endl;
	//std::cout << str.Capacity() << str.Size() << std::endl;
	//wzh::MyString str;

	//std::cout << str << std::endl;

	//{
	//	wzh::MyString tmp(str);
	//	std::cout << tmp << std::endl;
	//}

	//wzh::MyString str2, str3("efgth");
	//str3 = str2 = str;
	//std::cout << str2 << std::endl;
	//std::cout << str3 << std::endl;

	//char* str = nullptr;
	//delete str;
	return 0;
}