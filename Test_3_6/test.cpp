#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include <time.h>
struct A
{
	int a[10000];
};
A a;
// 值返回
A TestFunc1()
{
	return a;
}
// 引用返回
A& TestFunc2()
{
	return a;
}
void TestReturnByRefOrValue()
{
	// 以值作为函数的返回值类型
	size_t begin1 = clock();
	for (size_t i = 0; i < 100000; ++i)
		TestFunc1();
	size_t end1 = clock();
	// 以引用作为函数的返回值类型
	size_t begin2 = clock();
	for (size_t i = 0; i < 100000; ++i)
		TestFunc2();
	size_t end2 = clock();
	// 计算两个函数运算完成之后的时间
	cout << "TestFunc1 time:" << end1 - begin1 << endl;
	cout << "TestFunc2 time:" << end2 - begin2 << endl;
}
int main()
{
	TestReturnByRefOrValue();
	return 0;
}
//#include <time.h>
//struct A
//{
//	int a[10000];
//};
//void TestFunc1(A a)
//{
//}
//void TestFunc2(A& a)
//{
//}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestRefAndValue();
//	return 0;
//}
//int& Add(int a, int b)
//{
//    int c = a + b;
//    return c;
//}
//int main()
//{
//    int& ret = Add(1, 2);
//    Add(3, 4);
//    cout << "Add(1, 2) is :" << ret << endl;
//    return 0;
//}
//int& Count()
//{
//    static int n = 0;
//    n++;
//    return n;
//}
//int main()
//{
//    int ret = Count();
//    cout << ret;
//    return 0;
//}
//using namespace std;
//void Swap(int& left, int& right)
//{
//    int temp = left;
//    left = right;
//    right = temp;
//}
//int main()
//{
//    int a = 10;
//    int b = 20;
//    Swap(a, b);
//    //Swap(&a, &b);
//    cout << "a = " << a << endl;
//    cout << "b = " << b << endl;
//    return 0;
//}
//using namespace std;
//void TestConstRef()
//{
//    //权限扩大错误
//    const int a = 10;
//    //int& ra = a;   // 该语句编译时会出错，a为常量
//    const int& ra = a;
//    // int& b = 10; // 该语句编译时会出错，b为常量
//    const int& b = 10;
//    double d = 12.34;
//    //int& rd = d; // 该语句编译时会出错，类型不同
//    const int& rd = d;
//    cout << "a = " << a << endl << "ra = " << ra << endl;
//    cout << "d = " << d << endl << "rd = " << rd << endl;
//}
//
//int main()
//{
//    TestConstRef();
//    return 0;
//}
//void TestRef()
//{
//    int a = 10;
//    // int& ra;   // 该条语句编译时会出错
//    int& ra = a;
//    int& rra = a;
//    printf("%p %p %p\n", &a, &ra, &rra);
//}
//int main()
//{
//    TestRef();
//    return 0;
//}
//void TestRef()
//{
//    int a = 10;
//    int& ra = a;//<====定义引用类型
//    printf("%p\n", &a);
//    printf("%p\n", &ra);
//}
//int main()
//{
//    TestRef();
//    return 0;
//}
//#include <iostream>
//using namespace std;
//#include<iostream>
//using namespace std;
//// 1、参数类型不同
//int Add(int left, int right)
//{
//	cout << "int Add(int left, int right)" << endl;
//	return left + right;
//}
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//	return left + right;
//}
//// 2、参数个数不同
//void f()
//{
//	cout << "f()" << endl;
//}
//void f(int a)
//{
//	cout << "f(int a)" << endl;
//}
//// 3、参数类型顺序不同
//void f(int a, char b)
//{
//	cout << "f(int a,char b)" << endl;
//}
//void f(char b, int a)
//{
//	cout << "f(char b, int a)" << endl;
//}
//int main()
//{
//	Add(10, 20);
//	Add(10.1, 20.2);
//	f();
//	f(10);
//	f(10, 'a');
//	f('a', 10);
//	return 0;
//}
//void Func(int a, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//}
//int main()
//{
//	Func(10);
//	return 0;
//}
//using namespace std;
//void Func(int a = 10, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//}
//int main()
//{
//	Func();
//	return 0;
//}
//void Func(int a = 0)
//{
//	cout << a << endl;
//}
//int main()
//{
//	Func();     // 没有传参时，使用参数的默认值
//	Func(10);   // 传参时，使用指定的实参
//	return 0;
//}
//int main()
//{
//    int a;
//    double b;
//    char c;
//
//    // 可以自动识别变量的类型
//    cin >> a;
//    cin >> b >> c;
//
//    cout << a << endl;
//    cout << b << " " << c << endl;
//    return 0;
//}
//namespace N
//{
//    int a = 1;
//    int b = 2;
//    void Add(int a, int b)
//    {
//        printf("%d\n", a + b);
//    }
//}
//using namespace N;
//int main()
//{
//    printf("%d\n", N::a);
//    printf("%d\n", b);
//    Add(10, 20);
//    return 0;
//}
//namespace N
//{
//    int a = 1;
//    int b = 2;
//}
//using N::b;
//int main()
//{
//    printf("%d\n", N::a);
//    printf("%d\n", b);
//    return 0;
//}
//namespace N
//{
//    int a = 1;
//}
//int main()
//{
//    printf("%d\n", N::a);
//    return 0;
//}
//void f(int)
//{
//    cout << "f(int)" << endl;
//}
//void f(int*)
//{
//    cout << "f(int*)" << endl;
//}
//int main()
//{
//    f(0);
//    f(NULL);
//    f((int*)NULL);
//    return 0;
//}
//int main()
//{
//    int array[] = { 1, 2, 3, 4, 5 };
//    for (auto& e : array)
//        e *= 2;
//    for (auto e : array)
//        cout << e << " ";
//    return 0;
//}
//void TestAuto()
//{
//    auto a = 1, b = 2;
//    cout << typeid(a).name() << endl;
//    cout << typeid(b).name() << endl;
//    //auto c = 3, d = 4.0;  // 该行代码会编译失败，因为c和d的初始化表达式类型不同
//}
//int main()
//{
//    auto a = 1, b = 2;
//    cout << typeid(a).name() << endl;
//    cout << typeid(b).name() << endl;
//    //auto c = 3, d = 4.0;  // 该行代码会编译失败，因为c和d的初始化表达式类型不同
//}
//int main()
//{
//    int x = 10;
//    auto a = &x;
//    auto* b = &x;
//    auto& c = x;
//    TestAuto();
//    cout << typeid(a).name() << endl;
//    cout << typeid(b).name() << endl;
//    cout << typeid(c).name() << endl;
//    *a = 20;
//    *b = 30;
//    c = 40;
//    return 0;
//}
//int TestAuto()
//{
//	return 10;
//}
//int main()
//{
//	int a = 10;
//	auto b = a;
//	auto c = 'a';
//	auto d = TestAuto();
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	//auto e; //无法通过编译，使用auto定义变量时必须对其进行初始化
//	return 0;
//}
//#include <string>
//#include <map>
//int main()
//{
//	std::map<std::string, std::string> m{ { "apple", "苹果" }, { "orange",
//   "橙子" },
//	  {"pear","梨"} };
//	std::map<std::string, std::string>::iterator it = m.begin();
//	while (it != m.end())
//	{
//		//....
//	}
//	return 0;
//}

//#include <time.h>
//struct A
//{
//	int a[10000];
//};
//void TestFunc1(A a)
//{
//}
//void TestFunc2(A& a)
//{
//}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestRefAndValue();
//	return 0;
//}
//#include <iostream>
//using namespace std;
//int main()
//{
//	int a = 10;
//	int& ra = a;
//	ra = 20;
//	int* pa = &a;
//	*pa = 20;
//	return 0;
//}
//#include <time.h>
//typedef struct A 
//{ 
//	int a[10000]; 
//}A;
//A a;
//// 值返回
//A TestFunc1() 
//{ 
//	return a; 
//}
//// 引用返回
//A& TestFunc2() 
//{ 
//	return a; 
//}
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1 time:" << end1 - begin1 << endl;
//	cout << "TestFunc2 time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestReturnByRefOrValue();
//	return 0;
//}
//#include <time.h>
//typedef struct A 
//{ 
//	int a[10000]; 
//}A;
//void TestFunc1(A a) 
//{
//}
//void TestFunc2(A& a) 
//{
//}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestRefAndValue();
//	return 0;
//}
//int& Add(int a, int b)
//{
//    int c = a + b;
//    return c;
//}
//int main()
//{
//    int& ret = Add(1, 2);
//    cout << "Add(1, 2) is :" << ret << endl;
//    Add(3, 4);
//    cout << "Add(1, 2) is :" << ret << endl;
//    return 0;
//}
//void TestRef()
//{
//    int a = 10;
//    int& ra = a;//<====定义引用类型
//    printf("%p\n", &a);
//    printf("%p\n", &ra);
//}
//void TestRef()
//{
//    int a = 10;
//    // int& ra;   // 该条语句编译时会出错
//    int& ra = a;
//    int& rra = a;
//    printf("%p %p %p\n", &a, &ra, &rra);
//}
//void TestConstRef()
//{
//    //权限扩大错误
//    const int a = 10;
//    //int& ra = a;   // 该语句编译时会出错，a为常量
//    const int& ra = a;
//    // int& b = 10; // 该语句编译时会出错，b为常量
//    const int& b = 10;
//    double d = 12.34;
//    double& rrd = d;
//    //int& rd = d; // 该语句编译时会出错，类型不同
//    const int& rd = d;
//    cout << "a = "<< a << endl << "ra = " << ra << endl;
//    cout << "d = " << d << endl << "rd = " << rd << endl;
//    cout << rrd;
//}
//void Swap(int& left, int& right)
//{
//    int temp = left;
//    left = right;
//    right = temp;
//}
//int& Count()
//{
//    static int n = 0;
//    n++;
//    return n;
//}
//int main()
//{
//    int ret = Count();
//    cout << ret;
//    return 0;
//}
//#include<iostream>
//using namespace std;
//// 1、参数类型不同
//int Add(int left, int right)
//{
//	cout << "int Add(int left, int right)" << endl;
//	return left + right;
//}
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//	return left + right;
//}
//// 2、参数个数不同
//void f()
//{
//	cout << "f()" << endl;
//}
//void f(int a)
//{
//	cout << "f(int a)" << endl;
//}
//// 3、参数类型顺序不同
//void f(int a, char b)
//{
//	cout << "f(int a,char b)" << endl;
//}
//void f(char b, int a)
//{
//	cout << "f(char b, int a)" << endl;
//}
//int main()
//{
//	Add(10, 20);
//	Add(10.1, 20.2);
//	f();
//	f(10);
//	f(10, 'a');
//	f('a', 10);
//	return 0;
//}
//using namespace std;
//void Func(int a, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl;
//}
//int main()
//{
//	Func(10);     
//	return 0;
//}

//#include <iostream>
// std是C++标准库的命名空间名，C++将标准库的定义实现都放到这个命名空间中
//using namespace std;
//int main()
//{
//	cout << "Hello world!!!" << endl;
//	return 0;
//}
//#include <iostream>
//using namespace std;
//int main()
//{
//    int a;
//    double b;
//    char c;
//
//    // 可以自动识别变量的类型
//    cin >> a;
//    cin >> b >> c;
//
//    cout << a << endl;
//    cout << b << " " << c << endl;
//    return 0;
//}
//namespace N
//{
//    int a = 1;
//    int b = 2;
//    void Add(int a, int b)
//    {
//        printf("%d\n", a + b);
//    }
//}
//using namespace N;
//int main()
//{
//    printf("%d\n", N::a);
//    printf("%d\n", b);
//    Add(10, 20);
//    return 0;
//}

//namespace N
//{
//    int a = 1;
//    int b = 2;
//}
//using N::b;
//int main()
//{
//    printf("%d\n", N::a);
//    printf("%d\n", b);
//    return 0;
//}