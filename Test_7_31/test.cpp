//#include <iostream>
//
//using namespace std;
//
//
//#include <iostream>
//using namespace std;
//
//class Animal 
//{
//protected:
//    string _name; // 名称
//    int _age; // 年龄
//public:
//    Animal(string name, int age) : _name(name), _age(age) 
//    {}
//    void sound() 
//    {
//        cout << "Animal sound" << endl;
//    }
//};
//
//class Cat : public Animal 
//{
//public:
//    Cat(string name, int age) : Animal(name, age) 
//    {}
//    void sound()
//    {
//        cout << "Meow" << endl;
//    }
//};
//
//class Dog : public Animal 
//{
//public:
//    Dog(string name, int age) : Animal(name, age) 
//    {}
//    void sound()
//    {
//        cout << "Woof" << endl;
//    }
//};

//void Test() {
//    Cat cat("Tom", 3);
//    Dog dog("Max", 5);
//
//    // 子类对象可以赋值给父类对象/指针/引用
//    Animal animal = cat;
//    Animal* animalPtr = &dog;
//    Animal& animalRef = cat;
//
//    // 基类对象不能赋值给派生类对象
//    // cat = animal;
//
//    // 基类的指针可以通过强制类型转换赋值给派生类的指针
//    animalPtr = &cat;
//    Cat* catPtr1 = static_cast<Cat*>(animalPtr);
//    catPtr1->sound();
//
//    animalPtr = &dog;
//    Cat* catPtr2 = static_cast<Cat*>(animalPtr);
//    catPtr2->sound(); // 这种情况转换后会存在越界访问的问题，因为实际对象是Dog而不是Cat
//}

//void Test() 
//{
//    Cat cat("Tom", 3);
//    Dog dog("Max", 5);
//
//    // 子类对象可以赋值给父类对象/指针/引用
//    Animal animal = cat;
//    Animal* animalPtr = &dog;
//    Animal& animalRef = cat;
//
//    // 基类对象不能赋值给派生类对象
//    //cat = animal;
//
//    // 基类的指针可以通过强制类型转换赋值给派生类的指针
//    animalPtr = &cat;
//    Cat* catPtr1 = (Cat*)animalPtr;
//    catPtr1->sound();
//
//    animalPtr = &dog;
//    Cat* catPtr2 = (Cat*)animalPtr;
//    catPtr2->sound(); // 这种情况转换后会存在越界访问的问题，因为实际对象是Dog而不是Cat
//}
//
//int main() 
//{
//    Test();
//    return 0;
//}




//class Person
//{
//protected:
//    string _name; // 姓名
//    string _sex;  // 性别
//    int _age; // 年龄
//};
//class Student : public Person
//{
//public:
//    int _No; // 学号
//};
//void Test()
//{
//    Student sobj;
//    // 1.子类对象可以赋值给父类对象/指针/引用
//    Person pobj = sobj;
//    Person* pp = &sobj;
//    Person& rp = sobj;
//
//    //2.基类对象不能赋值给派生类对象
//    //sobj = pobj;
//
//    // 3.基类的指针可以通过强制类型转换赋值给派生类的指针
//    pp = &sobj;
//    Student* ps1 = (Student*)pp; // 这种情况转换时可以的。
//    ps1->_No = 10;
//
//    pp = &pobj;
//    Student* ps2 = (Student*)pp; // 这种情况转换时虽然可以，但是会存在越界访问的问题
//    ps2->_No = 10;
//}

//int main()
//{
//    Test();
//    return 0;
//}


//// 定义一个基类
//class Shape
//{
//public:
//    void setWidth(int w)
//    {
//        width = w;
//    }
//    void setHeight(int h)
//    {
//        height = h;
//    }
//protected:
//    int width;
//    int height;
//};
//
//// 定义一个派生类
//class Rectangle : public Shape
//{
//public:
//    int getArea()
//    {
//        return (width * height);
//    }
//};
//
//// 主函数
//int main()
//{
//    Rectangle rect;
//
//    rect.setWidth(5);
//    rect.setHeight(7);
//
//    cout << "Area of the rectangle: " << rect.getArea() << endl;
//
//    return 0;
//}


//#include <iostream>
//#include <string>
//using namespace std;
//
//// 基类 Animal
//class Animal {
//public:
//    Animal(string name, int age)
//    {
//        _name = name;
//        _age = age;
//        cout << "Animal()" << endl;
//    }
//    ~Animal()
//    {
//        cout << "~Animal()" << endl;
//    }
//    string _name;
//    int _age;
//};
//
//// 派生类 Cat
//class Cat : virtual public Animal {
//public:
//    Cat(string name, int age)
//        :Animal(name, age)
//    {
//        cout << "Cat()" << endl;
//
//    }
//    ~Cat()
//    {
//        cout << "~Cat()" << endl;
//
//    }
//    void speak()
//    {
//        cout << "cat speak" << endl;
//    }
//};
//
//// 派生类 Dog
//class Dog : virtual public Animal {
//public:
//    Dog(string name, int age)
//        :Animal(name, age)
//    {
//        cout << "Dog()" << endl;
//
//    }
//    ~Dog()
//    {
//        cout << "~Dog()" << endl;
//
//    }
//    void speak()
//    {
//        cout << "dog speak" << endl;
//    }
//};
//
//// 派生类 Pet
//class Pet : public Cat, public Dog {
//public:
//    Pet(string name, int age)
//        :Cat(name, age), Dog(name, age), Animal(name, age)
//    {
//        cout << "Pet()" << endl;
//
//    }
//    ~Pet()
//    {
//        cout << "~Pet()" << endl;
//
//    }
//    void speak()
//    {
//        cout << "Pet speak" << endl;
//    }
//};
//
//int main() 
//{
//    Pet pet("Tom", 18);
//    pet.speak();
//    return 0;
//}

#include <iostream>
using namespace std;
//下面代码输出结果：()
class A
{
public:
	void f() { cout << "A::f()" << endl; }
	int a;
};

class B : public A
{
public:
	void f(int a) { cout << "B::f()" << endl; }
	int a;
};

int main()
{
	B b;
	b.f();
	return 0;
}
