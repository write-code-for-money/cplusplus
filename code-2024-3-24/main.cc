#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

template <class T>
int compare(T d1, T d2)
{
	if (d1 > d2) return 1;
	else if (d1 < d2) return -1;
	else return 0;
}


int main()
{
	std::cout << compare(10, 20) << std::endl;

	//int a = 1;
	//if ((char)a) std::cout << "С��\n";
	//else std::cout << "���\n";

	//union U
	//{
	//	char ch;
	//	int a;
	//};
	//U u1;
	//u1.a = 65;
	//std::cout << u1.ch << std::endl;
	//u1.ch = 'a';
	//std::cout << u1.a;

	return 0;
}