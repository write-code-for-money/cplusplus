#define _CRT_SECURE_NO_WARNINGS 1

#include "List.hpp"
#include "stack.hpp"
#include "queue.hpp"
#include "vector.h"
#include <vector>
#include <queue>
#include <list>

void test1()
{
	wzh::list<int> ls;
	for (auto e : ls)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;
	wzh::list<int> ls1(5, 4);
	for (auto e : ls1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;
}

void test2()
{
	std::string s1 = "1234567";
	wzh::list<char> l1(s1.begin(), s1.end());
	//for (auto e : l1)
	//{
	//	std::cout << e << ' ';
	//}
	//std::cout << std::endl;

	wzh::list<char> l2(l1);
	std::cout << "l2 : ";
	for (auto e : l2)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;

	wzh::list<char> l3(5, 'x');

	l3 = l2;
	//l1 = l3;
	std::cout << "l3 : ";
	for (auto e : l3)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;

	std::cout << "l2 : ";
	for (auto e : l2)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;
}

void test3()
{
	wzh::list<int> l1;
	l1.push_back(1);
	l1.push_back(2);
	l1.push_back(3);
	l1.push_back(4);
	l1.push_back(5);
	for (auto e : l1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;

	l1.pop_back();
	l1.pop_back();
	for (auto e : l1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;

	l1.push_front(7);
	l1.push_front(8);
	l1.push_front(9);
	for (auto e : l1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;


	l1.pop_front();
	l1.pop_front();
	for (auto e : l1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;

	l1.insert(l1.begin(), 5);
	for (auto e : l1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;

	l1.insert(l1.end(), 10);
	for (auto e : l1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;


	wzh::list<int>::iterator it = l1.begin();
	it++;
	l1.erase(it);
	for (auto e : l1)
	{
		std::cout << e << ' ';
	}
	std::cout << std::endl;
	//wzh::list<int>::iterator it = l1.begin();
	//wzh::list<int>::iterator it = l1.begin();

}

void test4()
{
	wzh::stack<int, std::vector<int>> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	st.push(5);

	st.pop();
	st.pop();
	std::cout << st.top() << std::endl;
	std::cout << st.size() << std::endl;
	st.pop();
	st.pop();
	st.pop();
	std::cout << st.empty() << std::endl;
}

void test5()
{
	//wzh::queue<int, std::vector<int>> qu;
	std::queue<int, std::list<int>> qu;
	qu.push(1);
	qu.push(2);
	qu.push(3);
	qu.push(4);
	qu.push(5);
	qu.pop();
	qu.pop();
	qu.pop();
	std::cout << qu.back() << std::endl;
	std::cout << qu.front() << std::endl;
	std::cout << qu.size() << std::endl;
	std::cout << qu.empty() << std::endl;
}

void test6()
{
	wzh::list<int> l1;
	l1.push_back(1);
	l1.push_back(2);
	l1.push_back(3);
	l1.push_back(4);
	l1.push_back(5);
	wzh::list<int>::reverse_iterator rit = l1.rbegin();

	wzh::list<int>::iterator it = l1.begin();
	//while (it != l1.end())
	//{
	//	std::cout << *it++ << ' ';
	//}
	//std::cout << std::endl;
	while (rit != l1.rend())
	{
		std::cout << *rit++ << std::endl;
	}
}


void test7()
{
	wzh::vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	v1.push_back(6);
	for (auto e : v1)
	{
		std::cout << e  << ' ' ;
	}
	std::cout << std::endl;

	wzh::vector<int>::reverse_iterator rit = v1.rbegin();
	while (rit != v1.rend())
	{
		std::cout << *rit++ << std::endl;
	}

}
int main()
{
	//test3();
	//wzh::test();
	//test4();
	test6();
	//test7();
	return 0;
}