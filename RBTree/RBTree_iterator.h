#include <iostream>
using namespace std;
enum color
{
    RED,
    BLANK
};

template<class T>
struct RBTreeNode
{
    RBTreeNode()
        : _value(T())
        , _pLeft(nullptr)
        , _pRight(nullptr)
        , _pParent(nullptr)
        , _COL(RED)
    {}

    T _value;
    color _COL;
    RBTreeNode<T>* _pLeft;
    RBTreeNode<T>* _pRight;
    RBTreeNode<T>* _pParent;
};

template<class T>
struct KeyOfValue {
    const T& operator()(const T& data) const {
        return data;
    }
};

template <class T>
struct RBTreeIterator
{
    typedef RBTreeNode<T> Node;
    typedef RBTreeIterator<T> Self;

    RBTreeIterator(Node* pNode)
        : _pNode(pNode)
    {}

    T& operator*() {
        return _pNode->_value;
    }

    T* operator->() {
        return &_pNode->_value;
    }

    Self& operator++() {
        Increment();
        return *this;
    }

    Self operator++(int) {
        Self tmp = *this;
        Increment();
        return tmp;
    }

    Self& operator--() {
        Decrement();
        return *this;
    }

    Self operator--(int) {
        Self tmp = *this;
        Decrement();
        return tmp;
    }

    bool operator!=(const Self& s) const {
        return _pNode != s._pNode;
    }

    bool operator==(const Self& s) const {
        return _pNode == s._pNode;
    }

private:
    void Increment() {
        if (_pNode->_pRight) {
            _pNode = _pNode->_pRight;
            while (_pNode->_pLeft)
                _pNode = _pNode->_pLeft;
        }
        else {
            Node* pParent = _pNode->_pParent;
            while (_pNode == pParent->_pRight) {
                _pNode = pParent;
                pParent = pParent->_pParent;
            }
            if (_pNode->_pRight != pParent)
                _pNode = pParent;
        }
    }

    void Decrement() {
        if (_pNode->_COL == RED && _pNode->_pParent->_pParent == _pNode)
            _pNode = _pNode->_pRight;
        else if (_pNode->_pLeft) {
            _pNode = _pNode->_pLeft;
            while (_pNode->_pRight)
                _pNode = _pNode->_pRight;
        }
        else {
            Node* pParent = _pNode->_pParent;
            while (_pNode == pParent->_pLeft) {
                _pNode = pParent;
                pParent = pParent->_pParent;
            }
            _pNode = pParent;
        }
    }

    Node* _pNode;
};

template<class T, class KeyOfValue = KeyOfValue<T>>
class RBTree
{
    typedef RBTreeNode<T> Node;
    typedef RBTreeIterator<T> iterator;
    typedef pair<iterator, bool> value_type;
public:
    RBTree()
        : _size(0)
    {
        _pHead = new Node;
        _pHead->_pLeft = _pHead;
        _pHead->_pRight = _pHead;
    }

    value_type Insert(const T& data)
    {
        
        Node* pRoot = _pHead->_pParent;
        Node* pCur = pRoot;
        Node* pParent = nullptr;
        while (pCur)
        {
            pParent = pCur;
            if (KeyOfValue()(data) < KeyOfValue()(pCur->_value))
                pCur = pCur->_pLeft;
            else if (KeyOfValue()(data) > KeyOfValue()(pCur->_value))
                pCur = pCur->_pRight;
            else
                return make_pair(iterator(pCur), false);
        }

        pCur = new Node;
        pCur->_value = data;

        pCur->_pParent = pParent;

        if (!pParent)
        {
            _pHead->_pParent = pCur;
        }
        else if (KeyOfValue()(pParent->_value) < KeyOfValue()(pCur->_value))
        {
            pParent->_pRight = pCur;
        }
        else
        {
            pParent->_pLeft = pCur;
        }

        // 调整树的平衡
        InsertFixup(pCur);

        ++_size;

        return make_pair(iterator(pCur), true);
    }


    iterator Begin() {
        return iterator(_LeftMost());
    }

    iterator End() {
        return iterator(_pHead);
    }

    bool Empty() const {
        return _size == 0;
    }

    size_t Size() const {
        return _size;
    }

    void Clear() {
        _Destroy(_pHead->_pParent);
        _pHead->_pLeft = _pHead;
        _pHead->_pRight = _pHead;
        _size = 0;
    }

    iterator Find(const T& data) {
        Node* pRoot = _pHead->_pParent;
        while (pRoot) {
            if (KeyOfValue()(data) < KeyOfValue()(pRoot->_value))
                pRoot = pRoot->_pLeft;
            else if (KeyOfValue()(data) > KeyOfValue()(pRoot->_value))
                pRoot = pRoot->_pRight;
            else
                return iterator(pRoot);
        }
        return End();
    }

private:
    void InsertFixup(Node* pNode)
    {
        while (pNode != GetRoot() && pNode->_pParent->_COL == RED)
        {
            Node* pGrandfather = pNode->_pParent->_pParent;
            if (pGrandfather == nullptr) // 祖父节点为空，将父节点设置为根节点并将其颜色设置为黑色
            {
                GetRoot()->_COL = BLANK;
                break;
            }

            if (pNode->_pParent == pGrandfather->_pLeft) // 父节点是祖父节点的左孩子
            {
                Node* pUncle = pGrandfather->_pRight;
                if (pUncle && pUncle->_COL == RED) // Case 1: 叔叔节点存在且为红色
                {
                    pNode->_pParent->_COL = BLANK;
                    pUncle->_COL = BLANK;
                    pGrandfather->_COL = RED;
                    pNode = pGrandfather;
                }
                else // Case 2: 叔叔节点不存在或为黑色
                {
                    if (pNode == pNode->_pParent->_pRight) // Case 2.1: 插入节点是父节点的右孩子
                    {
                        pNode = pNode->_pParent;
                        RotateL(pNode);
                    }
                    // Case 2.2: 插入节点是父节点的左孩子
                    pNode->_pParent->_COL = BLANK;
                    pGrandfather->_COL = RED;
                    RotateR(pGrandfather);
                }
            }
            else // 父节点是祖父节点的右孩子
            {
                Node* pUncle = pGrandfather->_pLeft;
                if (pUncle && pUncle->_COL == RED) // Case 1: 叔叔节点存在且为红色
                {
                    pNode->_pParent->_COL = BLANK;
                    pUncle->_COL = BLANK;
                    pGrandfather->_COL = RED;
                    pNode = pGrandfather;
                }
                else // Case 2: 叔叔节点不存在或为黑色
                {
                    if (pNode == pNode->_pParent->_pLeft) // Case 2.1: 插入节点是父节点的左孩子
                    {
                        pNode = pNode->_pParent;
                        RotateR(pNode);
                    }
                    // Case 2.2: 插入节点是父节点的右孩子
                    pNode->_pParent->_COL = BLANK;
                    pGrandfather->_COL = RED;
                    RotateL(pGrandfather);
                }
            }
        }
        GetRoot()->_COL = BLANK;
    }

    Node* _LeftMost() {
        Node* pRoot = _pHead->_pParent;
        if (pRoot == nullptr || pRoot->_pLeft == nullptr)
            return _pHead;
        Node* pCur = pRoot->_pLeft;
        while (pCur->_pLeft)
            pCur = pCur->_pLeft;
        return pCur;
    }

    void _Destroy(Node*& pRoot) {
        if (pRoot) {
            _Destroy(pRoot->_pLeft);
            _Destroy(pRoot->_pRight);
            delete pRoot;
            pRoot = nullptr;
        }
    }
    void RotateL(Node* pParent)
    {
        Node* pSubR = pParent->_pRight;
        Node* pSubRL = pSubR->_pLeft;

        pParent->_pRight = pSubRL;
        if (pSubRL != nullptr) {
            pSubRL->_pParent = pParent;
        }

        Node* pParentParent = pParent->_pParent;
        pSubR->_pParent = pParentParent;

        if (pParentParent == nullptr) {
            _pHead->_pParent = pSubR;
        }
        else {
            if (pParent == pParentParent->_pLeft) {
                pParentParent->_pLeft = pSubR;
            }
            else {
                pParentParent->_pRight = pSubR;
            }
        }

        pSubR->_pLeft = pParent;
        pParent->_pParent = pSubR;
    }
    void RotateR(Node* pParent)
    {
        Node* pSubL = pParent->_pLeft;
        Node* pSubLR = pSubL->_pRight;

        pParent->_pLeft = pSubLR;
        if (pSubLR != nullptr) {
            pSubLR->_pParent = pParent;
        }

        Node* pParentParent = pParent->_pParent;
        pSubL->_pParent = pParentParent;

        if (pParentParent == nullptr) {
            _pHead->_pParent = pSubL;
        }
        else {
            if (pParent == pParentParent->_pLeft) {
                pParentParent->_pLeft = pSubL;
            }
            else {
                pParentParent->_pRight = pSubL;
            }
        }

        pSubL->_pRight = pParent;
        pParent->_pParent = pSubL;
    }

    Node*& GetRoot()
    {
        return _pHead->_pParent;
    }
private:
    Node* _pHead;
    size_t _size;
};
