#pragma once
// 定义红黑树节点
template <typename Key, typename Value>
struct RBTreeNode {
    enum Color { RED, BLACK };

    Key key;                    // 节点的键值
    Value value;                // 节点的值
    RBTreeNode* left;           // 左子节点指针
    RBTreeNode* right;          // 右子节点指针
    RBTreeNode* parent;         // 父节点指针
    Color color;                // 节点的颜色

    // 构造函数
    RBTreeNode(Key k, Value v, Color c = RED)
        : key(k), value(v), left(nullptr), right(nullptr), parent(nullptr), color(c) {}
};

// 定义红黑树结构
template <class Key, class Value>
class RBTree {
public:
    // 构造函数
    RBTree() {
        header = new RBTreeNode<Key, Value>(Key(), Value(), RBTreeNode<Key, Value>::BLACK);
        header->left = header;
        header->right = header;
        header->parent = nullptr;
    }
    void insert(Key key, Value value);
    RBTreeNode<Key, Value>* findNode(Key key);
    void remove(Key key);
    // 执行中序遍历的公有函数
    void inOrderTraversal() {
        _inOrderTraversal(header->parent);
    }
private:
    void rotateLeft(RBTreeNode<Key, Value>* node);
    void insertFixup(RBTreeNode<Key, Value>* node);
    void rotateRight(RBTreeNode<Key, Value>* node);
    void removeFixup(RBTreeNode<Key, Value>* node, RBTreeNode<Key, Value>* parent);
    // 中序遍历红黑树的私有辅助函数
    void _inOrderTraversal(RBTreeNode<Key, Value>* node) {
        if (node == nullptr || node == header)
            return;

        _inOrderTraversal(node->left);
        std::cout << "Key: " << node->key << ", Value: " << node->value << std::endl;
        _inOrderTraversal(node->right);
    }
    RBTreeNode<Key, Value>* header;    // 头结点
};

// 红黑树的插入操作
template<typename Key, typename Value>
void RBTree<Key, Value>::insert(Key key, Value value) {
    RBTreeNode<Key, Value>* newNode = new RBTreeNode<Key, Value>(key, value);
    RBTreeNode<Key, Value>* parent = nullptr;
    RBTreeNode<Key, Value>* current = header->parent;

    // 找到插入位置
    while (current != nullptr) {
        parent = current;
        if (key < current->key)
            current = current->left;
        else if (key > current->key)
            current = current->right;
        else {
            // 如果已存在相同的键值，则更新节点的值
            current->value = value;
            delete newNode;
            return;
        }
    }

    newNode->parent = parent;

    // 空树，插入为根节点
    if (parent == nullptr) {
        newNode->color = RBTreeNode<Key, Value>::BLACK;
        header->parent = newNode;
        header->left = newNode;
        header->right = newNode;
    }
    else if (key < parent->key)
        parent->left = newNode;
    else
        parent->right = newNode;

    // 插入后进行调整
    insertFixup(newNode);
}

// 红黑树插入后的调整操作
template <typename Key, typename Value>
void RBTree<Key, Value>::insertFixup(RBTreeNode<Key, Value>* node) {
    while (node->parent != nullptr && node->parent->color == RBTreeNode<Key, Value>::RED) {
        if (node->parent == node->parent->parent->left) {
            RBTreeNode<Key, Value>* uncle = node->parent->parent->right;
            if (uncle != nullptr && uncle->color == RBTreeNode<Key, Value>::RED) {
                // Case 1: 叔叔节点是红色
                node->parent->color = RBTreeNode<Key, Value>::BLACK;
                uncle->color = RBTreeNode<Key, Value>::BLACK;
                node->parent->parent->color = RBTreeNode<Key, Value>::RED;
                node = node->parent->parent;
            }
            else {
                if (node == node->parent->right) {
                    // Case 2: 插入节点是父节点的右子节点
                    node = node->parent;
                    rotateLeft(node);
                }
                // Case 3: 插入节点是父节点的左子节点
                node->parent->color = RBTreeNode<Key, Value>::BLACK;
                node->parent->parent->color = RBTreeNode<Key, Value>::RED;
                rotateRight(node->parent->parent);
            }
        }
        else {
            // 与上述情况对称
            RBTreeNode<Key, Value>* uncle = node->parent->parent->left;
            if (uncle != nullptr && uncle->color == RBTreeNode<Key, Value>::RED) {
                node->parent->color = RBTreeNode<Key, Value>::BLACK;
                uncle->color = RBTreeNode<Key, Value>::BLACK;
                node->parent->parent->color = RBTreeNode<Key, Value>::RED;
                node = node->parent->parent;
            }
            else {
                if (node == node->parent->left) {
                    node = node->parent;
                    rotateRight(node);
                }
                node->parent->color = RBTreeNode<Key, Value>::BLACK;
                node->parent->parent->color = RBTreeNode<Key, Value>::RED;
                rotateLeft(node->parent->parent);
            }
        }
    }
    header->parent->color = RBTreeNode<Key, Value>::BLACK;  // 根节点始终为黑色
}

// 左旋操作
template <typename Key, typename Value>
void RBTree<Key, Value>::rotateLeft(RBTreeNode<Key, Value>* node) {
    RBTreeNode<Key, Value>* rightChild = node->right;
    node->right = rightChild->left;
    if (rightChild->left != nullptr)
        rightChild->left->parent = node;
    rightChild->parent = node->parent;
    if (node->parent == nullptr)
        header->parent = rightChild;
    else if (node == node->parent->left)
        node->parent->left = rightChild;
    else
        node->parent->right = rightChild;
    rightChild->left = node;
    node->parent = rightChild;
}

// 右旋操作
template <typename Key, typename Value>
void RBTree<Key, Value>::rotateRight(RBTreeNode<Key, Value>* node) {
    RBTreeNode<Key, Value>* leftChild = node->left;
    node->left = leftChild->right;
    if (leftChild->right != nullptr)
        leftChild->right->parent = node;
    leftChild->parent = node->parent;
    if (node->parent == nullptr)
        header->parent = leftChild;
    else if (node == node->parent->left)
        node->parent->left = leftChild;
    else
        node->parent->right = leftChild;
    leftChild->right = node;
    node->parent = leftChild;
}

// 红黑树的删除操作
template<typename Key, typename Value>
void RBTree<Key, Value>::remove(Key key) {
    RBTreeNode<Key, Value>* node = findNode(key);
    if (node == nullptr)
        return;

    RBTreeNode<Key, Value>* child;
    RBTreeNode<Key, Value>* parent;
    bool isRed = (node->color == RBTreeNode<Key, Value>::RED);

    if (node->left != nullptr && node->right != nullptr) {
        // Case 1: 被删除节点有两个子节点
        RBTreeNode<Key, Value>* replace = node->right;
        while (replace->left != nullptr)
            replace = replace->left;

        child = replace->right;
        parent = replace->parent;
        isRed = (replace->color == RBTreeNode<Key, Value>::RED);

        if (child != nullptr)
            child->parent = parent;

        if (parent != nullptr) {
            if (parent->left == replace)
                parent->left = child;
            else
                parent->right = child;
        }
        else {
            header->parent = child;
        }

        if (replace->parent == node)
            parent = replace;

        replace->parent = node->parent;
        replace->color = node->color;
        replace->left = node->left;
        replace->right = node->right;

        if (node->left != nullptr)
            node->left->parent = replace;
        if (node->right != nullptr)
            node->right->parent = replace;

        if (node->parent != nullptr) {
            if (node->parent->left == node)
                node->parent->left = replace;
            else
                node->parent->right = replace;
        }
        else {
            header->parent = replace;
        }

        delete node;
        node = replace;
    }
    else {
        // Case 2: 被删除节点无或只有一个子节点
        if (node->left != nullptr)
            child = node->left;
        else
            child = node->right;

        parent = node->parent;
        isRed = (node->color == RBTreeNode<Key, Value>::RED);

        if (child != nullptr)
            child->parent = parent;

        if (parent != nullptr) {
            if (parent->left == node)
                parent->left = child;
            else
                parent->right = child;
        }
        else {
            header->parent = child;
        }

        delete node;
    }

    if (!isRed) {
        // 删除后进行调整
        removeFixup(child, parent);
    }
}



// 红黑树删除后的调整操作
template <typename Key, typename Value>
void RBTree<Key, Value>::removeFixup(RBTreeNode<Key, Value>* node, RBTreeNode<Key, Value>* parent) {

    while (node != header->parent && (node == nullptr || node->color == RBTreeNode<Key, Value>::BLACK)) {
        if (node == parent->left) {
            RBTreeNode<Key, Value>* sibling = parent->right;
            if (sibling->color == RBTreeNode<Key, Value>::RED) {
                // Case 1: 兄弟节点是红色
                sibling->color = RBTreeNode<Key, Value>::BLACK;
                parent->color = RBTreeNode<Key, Value>::RED;
                rotateLeft(parent);
                sibling = parent->right;
            }
            if ((sibling->left == nullptr || sibling->left->color == RBTreeNode<Key, Value>::BLACK)
                && (sibling->right == nullptr || sibling->right->color == RBTreeNode<Key, Value>::BLACK)) {
                // Case 2: 兄弟节点和兄弟节点的子节点都是黑色
                sibling->color = RBTreeNode<Key, Value>::RED;
                node = parent;
                parent = node->parent;
            }
            else {
                if (sibling->right == nullptr || sibling->right->color == RBTreeNode<Key, Value>::BLACK) {
                    // Case 3: 兄弟节点的右子节点是黑色
                    if (sibling->left != nullptr)
                        sibling->left->color = RBTreeNode<Key, Value>::BLACK;
                    sibling->color = RBTreeNode<Key, Value>::RED;
                    rotateRight(sibling);
                    sibling = parent->right;
                }
                // Case 4: 兄弟节点的右子节点是红色
                sibling->color = parent->color;
                parent->color = RBTreeNode<Key, Value>::BLACK;
                if (sibling->right != nullptr)
                    sibling->right->color = RBTreeNode<Key, Value>::BLACK;
                rotateLeft(parent);
                node = header->parent;
                break;
            }
        }
        else {
            // 与上述情况对称
            RBTreeNode<Key, Value>* sibling = parent->left;
            if (sibling->color == RBTreeNode<Key, Value>::RED) {
                sibling->color = RBTreeNode<Key, Value>::BLACK;
                parent->color = RBTreeNode<Key, Value>::RED;
                rotateRight(parent);
                sibling = parent->left;
            }
            if ((sibling->left == nullptr || sibling->left->color == RBTreeNode<Key, Value>::BLACK)
                && (sibling->right == nullptr || sibling->right->color == RBTreeNode<Key, Value>::BLACK)) {
                sibling->color = RBTreeNode<Key, Value>::RED;
                node = parent;
                parent = node->parent;
            }
            else {
                if (sibling->left == nullptr || sibling->left->color == RBTreeNode<Key, Value>::BLACK) {
                    if (sibling->right != nullptr)
                        sibling->right->color = RBTreeNode<Key, Value>::BLACK;
                    sibling->color = RBTreeNode<Key, Value>::RED;
                    rotateLeft(sibling);
                    sibling = parent->left;
                }
                sibling->color = parent->color;
                parent->color = RBTreeNode<Key, Value>::BLACK;
                if (sibling->left != nullptr)
                    sibling->left->color = RBTreeNode<Key, Value>::BLACK;
                rotateRight(parent);
                node = header->parent;
                break;
            }
        }
    }

    if (node != nullptr)
        node->color = RBTreeNode<Key, Value>::BLACK;
}

// 在红黑树中查找键值对应的节点
template <typename Key, typename Value>
RBTreeNode<Key, Value>* RBTree<Key, Value>::findNode(Key key) {
    RBTreeNode<Key, Value>* current = header->parent;

    while (current != nullptr && current != header) {
        if (key == current->key)
            return current;
        else if (key < current->key)
            current = current->left;
        else
            current = current->right;
    }

    return nullptr; // 如果找不到，返回空指针
}
