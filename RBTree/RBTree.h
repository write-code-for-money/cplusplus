#pragma once

#include <iostream>

enum color
{
    RED,
    BLANK
};

template<class T>
class RBTreeNode
{
public:
    RBTreeNode()
        :_value(T())
        , _pLeft(nullptr)
        , _pRight(nullptr)
        , _pParent(nullptr)
        , _COL(RED)
    {}

    T _value;
    color _COL;
    RBTreeNode<T>* _pLeft;
    RBTreeNode<T>* _pRight;
    RBTreeNode<T>* _pParent;
};

template<class T>
class RBTree
{
    typedef RBTreeNode<T> Node;
public:
    RBTree()
    {
        _pHead = new Node;
        _pHead->_pLeft = _pHead;
        _pHead->_pRight = _pHead;
    }

    bool Insert(const T& data);
    Node* Find(const T& data);
    Node* LeftMost();
    Node* RightMost();
    bool IsValidRBTRee();

private:
    bool _IsValidRBTRee(Node* pRoot, size_t blackCount, size_t pathBlack);
    void RotateL(Node* pParent);
    void RotateR(Node* pParent);
    Node*& GetRoot();

private:
    Node* _pHead;
};

template<class T>
bool RBTree<T>::Insert(const T& data)
{
    Node* pNode = new Node;
    pNode->_value = data;

    Node* pCur = GetRoot();
    Node* pParent = nullptr;

    while (pCur != nullptr)
    {
        pParent = pCur;
        if (data < pCur->_value)
            pCur = pCur->_pLeft;
        else if (data > pCur->_value)
            pCur = pCur->_pRight;
        else
        {
            delete pNode;
            return false; // 不允许插入重复元素
        }
    }

    pNode->_pParent = pParent;
    if (pParent == nullptr)
    {
        GetRoot() = pNode; // 将新节点设置为根节点
    }
    else if (data < pParent->_value)
    {
        pParent->_pLeft = pNode;
    }
    else
    {
        pParent->_pRight = pNode;
    }

    while (pNode != GetRoot() && pNode->_pParent->_COL == RED)
    {
        Node* pGrandfather = pNode->_pParent->_pParent;
        if (pGrandfather == nullptr) // 祖父节点为空，将父节点设置为根节点并将其颜色设置为黑色
        {
            GetRoot()->_COL = BLANK;
            break;
        }

        if (pNode->_pParent == pGrandfather->_pLeft) // 父节点是祖父节点的左孩子
        {
            Node* pUncle = pGrandfather->_pRight;
            if (pUncle && pUncle->_COL == RED) // Case 1: 叔叔节点存在且为红色
            {
                pNode->_pParent->_COL = BLANK;
                pUncle->_COL = BLANK;
                pGrandfather->_COL = RED;
                pNode = pGrandfather;
            }
            else // Case 2: 叔叔节点不存在或为黑色
            {
                if (pNode == pNode->_pParent->_pRight) // Case 2.1: 插入节点是父节点的右孩子
                {
                    pNode = pNode->_pParent;
                    RotateL(pNode);
                }
                // Case 2.2: 插入节点是父节点的左孩子
                pNode->_pParent->_COL = BLANK;
                pGrandfather->_COL = RED;
                RotateR(pGrandfather);
            }
        }
        else // 父节点是祖父节点的右孩子
        {
            Node* pUncle = pGrandfather->_pLeft;
            if (pUncle && pUncle->_COL == RED) // Case 1: 叔叔节点存在且为红色
            {
                pNode->_pParent->_COL = BLANK;
                pUncle->_COL = BLANK;
                pGrandfather->_COL = RED;
                pNode = pGrandfather;
            }
            else // Case 2: 叔叔节点不存在或为黑色
            {
                if (pNode == pNode->_pParent->_pLeft) // Case 2.1: 插入节点是父节点的左孩子
                {
                    pNode = pNode->_pParent;
                    RotateR(pNode);
                }
                // Case 2.2: 插入节点是父节点的右孩子
                pNode->_pParent->_COL = BLANK;
                pGrandfather->_COL = RED;
                RotateL(pGrandfather);
            }
        }
    }

    GetRoot()->_COL = BLANK;

    return true;
}

template<class T>
typename RBTree<T>::Node* RBTree<T>::Find(const T& data)
{
    Node* pCur = GetRoot();

    while (pCur)
    {
        if (data < pCur->_value)
            pCur = pCur->_pLeft;
        else if (data > pCur->_value)
            pCur = pCur->_pRight;
        else
            return pCur;
    }

    return nullptr;
}

template<class T>
typename RBTree<T>::Node* RBTree<T>::LeftMost()
{
    Node* pCur = GetRoot();

    while (pCur->_pLeft)
        pCur = pCur->_pLeft;

    return pCur;
}

template<class T>
typename RBTree<T>::Node* RBTree<T>::RightMost()
{
    Node* pCur = GetRoot();

    while (pCur->_pRight)
        pCur = pCur->_pRight;

    return pCur;
}

template<class T>
bool RBTree<T>::IsValidRBTRee()
{
    size_t blackCount = 0;
    Node* pCur = GetRoot();

    while (pCur)
    {
        if (pCur->_COL == BLANK)
            blackCount++;
        pCur = pCur->_pLeft;
    }

    size_t pathBlack = 0;

    return _IsValidRBTRee(GetRoot(), blackCount, pathBlack);
}

template<class T>
bool RBTree<T>::_IsValidRBTRee(Node* pRoot, size_t blackCount, size_t pathBlack)
{
    if (pRoot == nullptr)
    {
        if (pathBlack != blackCount)
            return false;
        else
            return true;
    }

    if (pRoot->_COL == BLANK)
        pathBlack++;

    if (pRoot->_COL == RED && pRoot->_pParent->_COL == RED)
        return false;

    return _IsValidRBTRee(pRoot->_pLeft, blackCount, pathBlack) && _IsValidRBTRee(pRoot->_pRight, blackCount, pathBlack);
}

template<class T>
void RBTree<T>::RotateL(Node* pParent)
{
    Node* pSubR = pParent->_pRight;
    Node* pSubRL = pSubR->_pLeft;

    pParent->_pRight = pSubRL;
    if (pSubRL)
        pSubRL->_pParent = pParent;

    pSubR->_pLeft = pParent;

    if (pParent != GetRoot())
    {
        Node* pPpNode = pParent->_pParent;
        if (pParent == pPpNode->_pLeft)
            pPpNode->_pLeft = pSubR;
        else
            pPpNode->_pRight = pSubR;

        pSubR->_pParent = pPpNode;
    }
    else
    {
        pSubR->_pParent = _pHead;
        _pHead->_pParent = pSubR;
    }

    pParent->_pParent = pSubR;
}

template<class T>
void RBTree<T>::RotateR(Node* pParent)
{
    Node* pSubL = pParent->_pLeft;
    Node* pSubLR = pSubL->_pRight;

    pParent->_pLeft = pSubLR;
    if (pSubLR)
        pSubLR->_pParent = pParent;

    pSubL->_pRight = pParent;

    if (pParent != GetRoot())
    {
        Node* pPpNode = pParent->_pParent;
        if (pParent == pPpNode->_pLeft)
            pPpNode->_pLeft = pSubL;
        else
            pPpNode->_pRight = pSubL;

        pSubL->_pParent = pPpNode;
    }
    else
    {
        pSubL->_pParent = _pHead;
        _pHead->_pParent = pSubL;
    }

    pParent->_pParent = pSubL;
}

template<class T>
typename RBTree<T>::Node*& RBTree<T>::GetRoot()
{
    return _pHead->_pParent;
}
