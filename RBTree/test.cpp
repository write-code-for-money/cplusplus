#include <iostream>
#include "RBTree_test.h"



int main() {
    RBTree<int, std::string> tree;

    // �������
    tree.insert(10, "Value 10");
    tree.insert(5, "Value 5");
    tree.insert(15, "Value 15");
    tree.insert(3, "Value 3");
    tree.insert(8, "Value 8");
    tree.insert(12, "Value 12");
    tree.insert(18, "Value 18");
    tree.insert(2, "Value 2");
    tree.insert(4, "Value 4");
    tree.insert(7, "Value 7");
    tree.insert(9, "Value 9");
    tree.insert(11, "Value 11");
    tree.insert(14, "Value 14");
    tree.insert(17, "Value 17");
    tree.insert(20, "Value 20");

    // ��������
    std::cout << "begin In-order traversal:\n";
    tree.inOrderTraversal();

    // ɾ������
    tree.remove(9);
    tree.remove(12);
    tree.remove(18);

    // ��������
    std::cout << "end In-order traversal:\n";
    tree.inOrderTraversal();

    return 0;
}
