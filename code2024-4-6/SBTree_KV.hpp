#pragma once

#include <iostream>

namespace wzh
{
	template<class K, class V>
	struct TreeNode
	{
		TreeNode(const K& key, const V& val)
			:_key(key), _val(val)
			,_left(nullptr)
			,_right(nullptr)
		{}

		K _key;
		V _val;
		TreeNode* _left;
		TreeNode* _right;
	};

	template<class K, class V = K>
	class SBTree
	{
		typedef TreeNode<K, V> Node;
		typedef SBTree<K, V> self;
	public:

		SBTree()
			:_root(nullptr)
		{}

		SBTree(const self& tree)
		{
			_root = Copy(tree._root);
		}

		SBTree& operator=(const self& tree)
		{
			if (this == &tree) return *this;
			Destroy(_root);
			_root = Copy(tree._root);
			return *this;
		}

		~SBTree()
		{
			Destroy(_root);
		}

		bool InSert(const K& key, const V& val = V())
		{
			Node* node = new Node(key, val);
			if (_root == nullptr)
			{
				_root = node;
				return true;
			}
			Node* prev = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else
				{
					delete node;
					return true;
				}
			}
			if (prev->_key > key) prev->_left = node;
			else prev->_right = node;
			return true;
		}

		bool Erase(const K& key)
		{
			Node* cur = _root;
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_key > key)
				{
					prev = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					prev = cur;
					cur = cur->_right;
				}
				else break;
			}
			if (cur == nullptr) return false;
			// 情况一：叶子结点
			if (cur->_left == nullptr && cur->_right == nullptr)
			{
				if (prev == nullptr) _root = nullptr;
				else if (prev->_left == cur) prev->_left = nullptr;
				else prev->_right = nullptr;
				delete cur;
				cur = nullptr;
			}
			// 情况二：有一个孩子
			else if ((cur->_left == nullptr && cur->_right) || (cur->_left && cur->_right == nullptr))
			{
				if (prev == nullptr)
				{
					if (cur->_left) _root = cur->_left;
					else _root = cur->_right;
				}
				else if (prev->_left == cur)
				{
					if (cur->_left) prev->_left = cur->_left;
					else prev->_left = cur->_right;
				}
				else
				{
					if (cur->_left) prev->_right = cur->_left;
					else prev->_right = cur->_right;
				}
				delete cur;
				cur = nullptr;
			}
			// 情况三：有两个孩子，找右边最小节点替换，然后删除右边最小
			else
			{
				Node* scur = cur->_right;
				Node* sprev = cur;
				Node* spprev = prev;
				while (scur)
				{
					spprev = sprev;
					sprev = scur;
					scur = scur->_left;
				}
				cur->_key = sprev->_key;
				cur->_val = sprev->_val;
				if (spprev->_left == sprev) spprev->_left = sprev->_right;
				else spprev->_right = sprev->_right;

				delete sprev;
			}
			return true;
		}

		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key) cur = cur->_left;
				else if (cur->_key < key) cur = cur->_right;
				else return true;
			}
			return false;
		}

		void Print()
		{
			InOrder(_root);
			std::cout << std::endl;
		}

	private:
		void InOrder(Node* cur)
		{
			if (cur == nullptr) return;
			InOrder(cur->_left);
			std::cout << cur->_val << ' ';
			InOrder(cur->_right);
		}

		void Destroy(Node* cur)
		{
			if (cur == nullptr) return;
			Destroy(cur->_left);
			Destroy(cur->_right);
			delete cur;
		}

		Node* Copy(Node* cur)
		{
			if (cur == nullptr) return nullptr;
			Node* node = new Node(cur->_val);
			node->_left = Copy(cur->_left);
			node->_right = Copy(cur->_right);
			return node;
		}

	private:
		Node* _root;
	};
}