#define _CRT_SECURE_NO_WARNINGS 1
#include "priority_queue.hpp"
#include <vector>
int main()
{
	std::vector<int> v1;
	v1.push_back(1);
	v1.push_back(4);
	v1.push_back(8);
	v1.push_back(6);
	v1.push_back(5);
	v1.push_back(7);
	wzh::priority_queue<int> pq(v1.begin(), v1.end());
	pq.push(10);
	pq.pop();
	return 0;
}