//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include <iostream>
//using namespace std;
//
////void test_default(int a = 0, int b = 0, int c = 0)
////{
////	cout << a << " : " << b << " : " << c << endl;
////}
//
////void test_default(int a, int b, int c)
////{
////	cout << a << " : " << b << " : " << c << endl;
////}
//
//class Date
//{
//public:
//	Date()
//	{
//
//	}
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//
//	}
//	~Date()
//	{
//
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
////void test_default(char a = 'a', char b = 'p', char c = 'c')
////{
////	cout << a << " : " << b << " : " << c << endl;
////}
//
//void test_default(Date da = Date())
//{
//	//cout << a << " : " << b << " : " << c << endl;
//}
//
//int main()
//{
//	test_default();
//	//test_default(10, 2, 3);
//	//test_default('q', 'w', 'e');
//
//	return 0;
//}


//#include <iostream>
//#include <string>
//#include <mysql/mysql.h>
//
//static int callback(void* data, int argc, char** argv, char** azColName) {
//    std::cout << "用户名： ";
//    for (int i = 0; i < argc; i++) {
//        std::cout << argv[i] << " ";
//    }
//    std::cout << std::endl;
//    return 0;
//}
//
//int main() {
//    MYSQL* conn;
//    MYSQL_RES* res;
//    MYSQL_ROW row;
//    char* zErrMsg = nullptr;
//    std::string username;
//    std::string password;
//
//    conn = mysql_init(nullptr);
//
//    if (!mysql_real_connect(conn, "localhost", "root", "your_password", "test", 0, nullptr, 0)) {
//        std::cerr << "无法连接到数据库：" << mysql_error(conn) << std::endl;
//        return 1;
//    }
//    else {
//        std::cout << "成功连接到数据库" << std::endl;
//    }
//
//    std::cout << "请输入用户名： ";
//    std::cin >> username;
//    std::cout << "请输入密码： ";
//    std::cin >> password;
//
//    const char* sql = "SELECT * FROM users WHERE username='" + username + "' AND password='" + password + "';";
//
//    res = mysql_query(conn, sql);
//
//    if (res) {
//        while ((row = mysql_fetch_row(res)) != nullptr) {
//            std::cout << "登录成功，欢迎 " << row[2] << "!" << std::endl;
//        }
//    }
//    else {
//        std::cerr << "查询错误：" << mysql_error(conn) << std::endl;
//    }
//
//    mysql_free_result(res);
//    mysql_close(conn);
//    return 0;
//}

//#include <iostream>
//#include <mysqlx/xdevapi.h>
//
//using namespace std;
//using namespace mysqlx;
//
//int main()
//{
//    try
//    {
//        // 连接到MySQL数据库
//        Session session("localhost", 3306, "root", "");
//
//        // 获取游标
//        Schema schema = session.getSchema("database_name");
//        Table table = schema.getTable("table_name");
//        RowResult result = table.select().execute();
//
//        // 验证登录信息
//        if (result.next())
//        {
//            std::string username = result[0]["username"].asString();
//            std::string password = result[0]["password"].asString();
//
//            if (username == "admin" && password == "123456")
//            {
//                // 登录成功，跳转到成功界面
//                cout << "登录成功！" << endl;
//                return 0;
//            }
//            else
//            {
//                // 登录失败，显示错误信息
//                cout << "用户名或密码错误！" << endl;
//                return 1;
//            }
//        }
//        else
//        {
//            // 没有找到用户记录，登录失败
//            cout << "用户不存在！" << endl;
//            return 1;
//        }
//    }
//    catch (const Error& e)
//    {
//        cerr << "Error: " << e.what() << endl;
//        return 1;
//    }
//}

#include <iostream>
using namespace std;
class Date
{
public:
	Date(int year, int month, int day)
		:_year(year), _month(month), _day(day)
	{
		cout << "Date(int year, int month, int day)" << endl;
	}
	Date(Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
		cout << "Date(Date& d)" << endl;
	}
	~Date()
	{
		cout << "~Date()" << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1 = { 2015, 4, 7 };
	return 0;
}