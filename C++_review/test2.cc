#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;
class Date
{
public:
	Date(int year, int month, int day)
		:_year(year), _month(month), _day(day)
	{
		cout << "Date(int year, int month, int day)" << endl;
	}
	Date(Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
		cout << "Date(Date& d)" << endl;
	}
	~Date()
	{
		cout << "~Date()" << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1 = { 2015, 4, 7 };
	return 0;
}