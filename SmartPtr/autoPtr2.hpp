#pragma once

namespace wzh
{
	template <class T>
	class AutoPtr
	{
	public:
		AutoPtr(T* ptr) : _ptr(ptr) {}
		~AutoPtr()
		{
			if (_ptr) delete _ptr;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		AutoPtr(AutoPtr<T>& autoptr) : _ptr(autoptr._ptr)
		{
			autoptr._ptr = nullptr;
		}
		T& operator=(autoPtr<T>& autoptr)
		{
			if (_ptr != autoptr._ptr)
			{
				if (_ptr) delete _ptr;
				_ptr = autoptr._ptr;
				autoptr._ptr = nullptr;
			}
			return *this;
		}
	private:
		T* _ptr;
	};
}