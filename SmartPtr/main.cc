//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include "autoPtr.hpp"
//#include "UniquePtr.hpp"
//#include "SharedPtr.hpp"
//
//#include <string>
//class Dog
//{
//private:
//	std::string _name;
//public:
//	Dog(const std::string& name)
//		:_name(name)
//	{}
//
//	void print()
//	{
//		std::cout << _name << "........\n";
//	}
//};
//
//
//
//class B;
//
//class A
//{
//public:
//	A()
//		:bPtr(nullptr)
//	{}
//	wzh::SharedPtr<B> bPtr;
//};
//
//class B
//{
//public:
//	B()
//		:aPtr(nullptr)
//	{}
//
//	wzh::SharedPtr<A> aPtr;
//};
//
//
//int main()
//{
//	wzh::Autoptr<int> smartPtr1(new int(4));
//	wzh::Autoptr<int> smartPtr2;
//
//	smartPtr2 = smartPtr1;
//
//	//std::cout << "smartPtr1: " << *smartPtr1 << std::endl;
//	std::cout << "smartPtr2: " << *smartPtr2 << std::endl;
//
//	----------------------------------------
//	wzh::UniquePtr<int> ptr1(new int(100));
//	std::cout << "ptr1: " << *ptr1 << std::endl;
//	wzh::UniquePtr<Dog> ptr2(new Dog("tom"));
//	ptr2->print();
//
//
//	wzh::SharedPtr<int> ptr1(new int(10));
//	wzh::SharedPtr<int> ptr2(ptr1);
//
//	std::cout << "ptr1: " << *ptr1 << std::endl;
//	std::cout << "ptr2: " << *ptr2 << std::endl;
//
//
//	wzh::SharedPtr<A> Aptr(new A);
//	wzh::SharedPtr<B> Bptr(new B);
//	Aptr->bPtr = Bptr;
//	Bptr->aPtr = Aptr;
//
//
//	return 0;
//}

#include <iostream>
using namespace std;
class A {
public:
	A(char* s) { cout << s << endl; }
	~A() {}
};
class B :virtual public A
{
public:
	B(char* s1, char* s2) :A(s1) { cout << s2 << endl; }
};
class C :virtual public A
{
public:
	C(char* s1, char* s2) :A(s1) { cout << s2 << endl; }
};
class D :public B, public C
{
public:
	D(char* s1, char* s2, char* s3, char* s4) :B(s1, s2), C(s1, s3), A(s1)
	{
		cout << s4 << endl;
	}
};
int main() {
	D* p = new D((char*)"class A", (char*)"class B", (char*)"class C", (char*)"class D");
	delete p;
	return 0;
}