#pragma once

namespace wzh
{
	template <typename T>
	class UniquePtr
	{
	public:
		UniquePtr(T* ptr)
			:_ptr(ptr)
		{}

		~UniquePtr()
		{
			if (_ptr)
			{
				delete _ptr;
				_ptr = nullptr;
			}
		}

		UniquePtr(UniquePtr<T>& uptr) = delete;
		UniquePtr<T>& operator=(const UniquePtr<T>& uptr) = delete;

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

	private:
		T* _ptr;
	};
}