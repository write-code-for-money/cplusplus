#pragma once

namespace wzh
{
	template <class T>
	class Unique_ptr
	{
	public:
		Unique_ptr(T* ptr = nullptr) : _ptr(ptr)
		{}
		~Unique_ptr()
		{
			if (_ptr)
			{
				delete _ptr;
				_ptr = nullptr;
			}
		}
		Unique_ptr(Unique_ptr& uptr) = delete;
		T& operator=(Unique_ptr& uptr) = delete;

		T& operator*() { return *_ptr; }
		T* operator->() { return _ptr; }
	private:
		T* _ptr;
	};
}