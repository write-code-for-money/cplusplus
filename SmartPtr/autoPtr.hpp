#ifndef __MY_AUTO__
#define __MY_AUTO__

#include <iostream>

namespace wzh
{
	template <typename T>
	class Autoptr
	{
	public:
		Autoptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}

		Autoptr(Autoptr<T>& aptr)
			:_ptr(aptr._ptr)
		{
			aptr._ptr = nullptr;
		}

		~Autoptr()
		{
			if (_ptr)
			{
				delete _ptr;
				_ptr = nullptr;
			}
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		Autoptr<T>& operator=(Autoptr<T>& aptr)
		{
			if (_ptr != aptr._ptr)
			{
				delete _ptr;
				_ptr = aptr._ptr;
				aptr._ptr = nullptr;
			}
			return *this;
		}
	private:
		T* _ptr;
	};
}

#endif // !__MY_AUTO__

