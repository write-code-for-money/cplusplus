# C/C++内存分布

c和c++中程序内存区域划分一般分为内核空间，栈，内存映射段，堆，数据断，代码段，其中内核空间部分用户代码不能读写，如下图所示。

![image-20230408152216497](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230408152216497.png)

1. 栈又叫堆栈--非静态局部变量/函数参数/返回值等等，栈是向下增长的。 
2. 内存映射段是高效的I/O映射方式，用于装载一个共享的动态内存库。用户可使用系统接口 创建共享共享内存，做进程间通信。
3. 堆用于程序运行时动态内存分配，堆是可以上增长的。 
4. 数据段--存储全局数据和静态数据。 
5. 代码段--可执行的代码/只读常量。



# C语言中动态内存管理方式

C语言中3大内存申请函数：malloc，calloc，realloc，空间释放函数：free。

```cpp
void Test ()
{
int* p1 = (int*) malloc(sizeof(int));
free(p1);
// 1.malloc/calloc/realloc的区别是什么？
int* p2 = (int*)calloc(4, sizeof (int));
int* p3 = (int*)realloc(p2, sizeof(int)*10);
// 这里需要free(p2)吗？
free(p3 );
}
```

【面试题】 

1. malloc/calloc/realloc的区别？
2. malloc的实现原理？

# C++内存管理方式

C语言内存管理方式在C++中可以继续使用，但有些地方就无能为力，而且使用起来比较麻烦，因 此C++又提出了自己的内存管理方式：通过new和delete操作符进行动态内存管理。

## new/delete操作内置类型

1. 动态申请一个int类型的空间

`int* ptr4 = new int;`

释放：`delete ptr4;`

2. 动态申请一个int类型的空间并初始化为10

`int* ptr5 = new int(10);`

释放：`delete ptr5;`

3. 动态申请10个int类型的空间

`int* ptr6 = new int[3];`

释放：`delete[] ptr6;`

```cpp
void Test()
{
  // 动态申请一个int类型的空间
  int* ptr4 = new int;
  
  // 动态申请一个int类型的空间并初始化为10
  int* ptr5 = new int(10);
  
  // 动态申请10个int类型的空间
  int* ptr6 = new int[3];
  delete ptr4;
  delete ptr5;
  delete[] ptr6;
}
```

> 注意：申请和释放单个元素的空间，使用new和delete操作符，申请和释放连续的空间，使用 new[]和delete[]，注意：匹配起来使用。

## new和delete操作自定义类型

new/delete 和 malloc/free最大区别是 new/delete对于【自定义类型】除了开空间 还会调用构造函数和析构函数，但是内置类型几乎一样。

```cpp
class A
{
public:
 A(int a = 0)
 : _a(a)
 {
 cout << "A():" << this << endl;
      }
 ~A()
 {
 cout << "~A():" << this << endl;
 }
private:
 int _a;
};
int main()
{
 // new/delete 和 malloc/free最大区别是 new/delete对于【自定义类型】除了开空间还会调用构造函数和析构函数
 A* p1 = (A*)malloc(sizeof(A));
 A* p2 = new A(1);
 free(p1);
 delete p2;
 // 内置类型是几乎是一样的
 int* p3 = (int*)malloc(sizeof(int)); // C
 int* p4 = new int;
free(p3);
delete p4;
 A* p5 = (A*)malloc(sizeof(A)*10);
 A* p6 = new A[10];
 free(p5);
 delete[] p6;
 return 0;
}
```

> 注意：在申请自定义类型的空间时，new会调用构造函数，delete会调用析构函数，而malloc与 free不会。

# operator new与operator delete函数

## operator new与operator delete函数

new和delete是用户进行动态内存申请和释放的操作符，operator new 和operator delete是系统提供的全局函数，new在底层调用operator new全局函数来申请空间，delete在底层通过 operator delete全局函数来释放空间。

operator new：该函数实际通过malloc来申请空间，当malloc申请空间成功时直接返回；申请空间失败，尝试执行空间不足应对措施，如果该应对措施用户设置了，则继续申请，否则抛异常。

```cpp
void *__CRTDECL operator new(size_t size) _THROW1(_STD bad_alloc)
{
// try to allocate size bytes
void *p;
while ((p = malloc(size)) == 0)
         if (_callnewh(size) == 0)
     {
         // report no memory
         // 如果申请内存失败了，这里会抛出bad_alloc 类型异常
         static const std::bad_alloc nomem;
         _RAISE(nomem);
     }
return (p);
}
/*
operator delete: 该函数最终是通过free来释放空间的
*/
void operator delete(void *pUserData)
{
     _CrtMemBlockHeader * pHead;
     RTCCALLBACK(_RTC_Free_hook, (pUserData, 0));
     if (pUserData == NULL)
         return;
     _mlock(_HEAP_LOCK);  /* block other threads */
     __TRY
         /* get a pointer to memory block header */
         pHead = pHdr(pUserData);
          /* verify block type */
         _ASSERTE(_BLOCK_TYPE_IS_VALID(pHead->nBlockUse));
         _free_dbg( pUserData, pHead->nBlockUse );
     __FINALLY
         _munlock(_HEAP_LOCK);  /* release other threads */
     __END_TRY_FINALLY
     return;
}
/*
free的实现
*/
#define   free(p)               _free_dbg(p, _NORMAL_BLOCK)
```

# new和delete的实现原理

## 内置类型

如果申请的是内置类型的空间，new和malloc，delete和free基本类似，不同的地方是： new/delete申请和释放的是单个元素的空间，new[]和delete[]申请的是连续空间，而且new在申请空间失败时会抛异常，malloc会返回NULL。

## 自定义类型

new的原理 

> 1. 调用operator new函数申请空间 
> 2. 在申请的空间上执行构造函数，完成对象的构造

delete的原理 

> 1. 在空间上执行析构函数，完成对象中资源的清理工作 
> 2. 调用operator delete函数释放对象的空间

new T[N]的原理 

> 1. 调用operator new[]函数，在operator new[]中实际调用operator new函数完成N个对 象空间的申请 
> 2. 在申请的空间上执行N次构造函数

delete[]的原理 

> 1. 在释放的对象空间上执行N次析构函数，完成N个对象中资源的清理 
> 2. 调用operator delete[]释放空间，实际在operator delete[]中调用operator delete来释放空间



# 定位new表达式(placement-new) 

定位new表达式是在已分配的原始内存空间中调用构造函数初始化一个对象。 

使用格式： new (place_address) type或者new (place_address) type(initializer-list) 

place_address必须是一个指针，initializer-list是类型的初始化列表 

使用场景： 定位new表达式在实际中一般是配合内存池使用。因为内存池分配出的内存没有初始化，所以如果是自定义类型的对象，需要使用new的定义表达式进行显示调构造函数进行初始化。

```cpp
class A
{
public:
 A(int a = 0)
 : _a(a)
 {
 cout << "A():" << this << endl;
 }
 ~A()
 {
 cout << "~A():" << this << endl;
 }
private:
 int _a;
};
// 定位new/replacement new
int main()
{
 // p1现在指向的只不过是与A对象相同大小的一段空间，还不能算是一个对象，因为构造函数没
有执行
 A* p1 = (A*)malloc(sizeof(A));
 new(p1)A;  // 注意：如果A类的构造函数有参数时，此处需要传参
 p1->~A();
 free(p1);
 A* p2 = (A*)operator new(sizeof(A));
 new(p2)A(10);
 p2->~A();
 operator delete(p2);
  return 0;
}
```

# 常见面试题 

malloc/free和new/delete的区别 

malloc/free和new/delete的共同点是：都是从堆上申请空间，并且需要用户手动释放。

不同的地方是： 

1. malloc和free是函数，new和delete是操作符 
2. malloc申请的空间不会初始化，new可以初始化 
3. malloc申请空间时，需要手动计算空间大小并传递，new只需在其后跟上空间的类型即可， 如果是多个对象，[]中指定对象个数即可 
4. malloc的返回值为void*, 在使用时必须强转，new不需要，因为new后跟的是空间的类型 
5. malloc申请空间失败时，返回的是NULL，因此使用时必须判空，new不需要，但是new需 要捕获异常 
6. 申请自定义类型对象时，malloc/free只会开辟空间，不会调用构造函数与析构函数，而new 在申请空间后会调用构造函数完成对象的初始化，delete在释放空间前会调用析构函数完成 空间中资源的清理

# 模板初阶

## 泛型编程

泛型编程：编写与类型无关的通用代码，是代码复用的一种手段。模板是泛型编程的基础。

在C++中，存在这样一个模具，通过给这个模具中填充不同材料(类型)，来获得不同材料的铸件 (即生成具体类型的代码）

例如实现一个交换函数

```cpp
void Swap(int& left, int& right)
{
 int temp = left;
 left = right;
 right = temp;
}
void Swap(double& left, double& right)
{
 double temp = left;
 left = right;
 right = temp;
}
void Swap(char& left, char& right)
{
 char temp = left;
 left = right;
 right = temp;
}
```

在c++中如果我们需要用到交换这3种不同类型，我们虽然可以利用函数重载的特性写出这3种不同类型的交换函数，但是有一下几个不好的地方：

1. 重载的函数仅仅是类型不同，代码复用率比较低，只要有新类型出现时，就需要用户自己增加对应的函数 
2. 代码的可维护性比较低，一个出错可能所有的重载均出错

因此，就有了模板，我们只需要告诉编译器一个模子，让编译器根据不同的类型利用该模子来生成代码。

模板又分为函数模板和类模板

![image-20230409172251265](C:\Users\Lenovo\AppData\Roaming\Typora\typora-user-images\image-20230409172251265.png)

## 函数模板

函数模板概念：函数模板代表了一个函数家族，该函数模板与类型无关，在使用时被参数化，根据实参类型产生函数的特定 类型版本。

函数模板格式：

```cpp
template<typename T> //template<class T>
返回值 函数名( 参数列表 )
{
    ...
}
```



如下代码：

```cpp
template<typename T> //template<class T>
void Swap( T& left, T& right)
{
 T temp = left;
 left = right;
 right = temp;
}
```

注意：typename是用来定义模板参数关键字，也可以使用class(切记：不能使用struct代替class)

函数模板是一个蓝图，它本身并不是函数，是编译器用使用方式产生特定具体类型函数的模具。所以其实模 板就是将本来应该我们做的重复的事情交给了编译器

在编译器编译阶段，对于模板函数的使用，编译器需要根据传入的实参类型来推演生成对应类型的函数以供调用。比如：当用double类型使用函数模板时，编译器通过对实参类型的推演，将T确定为double类型，然 后产生一份专门处理double类型的代码，对于字符类型也是如此。

函数模板的实例化：用不同类型的参数使用函数模板时，称为函数模板的实例化。

模板参数实例化分为：隐式实例化和显式实例化。

> - 隐式实例化：让编译器根据实参推演模板参数的实际类型
>
> ```cpp
> template<class T>
> T Add(const T& left, const T& right)
> {
>  return left + right;
> }
> int main()
> {
>  int a1 = 10, a2 = 20;
>  double d1 = 10.0, d2 = 20.0;
>  Add(a1, a2);
>  Add(d1, d2);
>  //Add(a1, d1);
>  
>  
>  
>      return 0;
> }
> ```
>
> 但是像`Add(a1, d1);`语句不能通过编译，因为在编译期间，当编译器看到该实例化时，需要推演其实参类型，通过实参a1将T推演为int，通过实参d1将T推演为double类型，但模板参数列表中只有一个T，编译器无法确定此处到底该将T确定为int 或者 double类型而报错。此时有两种处理方式：
>
> 1. 用户自己来强制转化 （`Add(a, (int)d);`）
> 2. 使用显式实例化
>
> 注意：在模板中，编译器一般不会进行类型转换操作，因为一旦转化出问题，编译器就需要背黑锅
>
> - 显式实例化：在函数名后的<>中指定模板参数的实际类型
>
> ```cpp
> int main(void)
> {
>  int a = 10;
>  double b = 20.0;
>  
>  // 显式实例化
>  Add<int>(a, b);
>  return 0;
> }
> ```
>
> 如果类型不匹配，编译器会尝试进行隐式类型转换，如果无法转换成功编译器将会报错。

模板参数的匹配原则：

1. 一个非模板函数可以和一个同名的函数模板同时存在，而且该函数模板还可以被实例化为这个非模板函数

```cpp
// 专门处理int的加法函数
int Add(int left, int right)
{
 return left + right;
}
// 通用加法函数
template<class T>
T Add(T left, T right)
{
 return left + right;
}
void Test()
{
 Add(1, 2); // 与非模板函数匹配，编译器不需要特化
 Add<int>(1, 2); // 调用编译器特化的Add版本
}
```

2. 对于非模板函数和同名函数模板，如果其他条件都相同，在调动时会优先调用非模板函数而不会从该模板产生出一个实例。如果模板可以产生一个具有更好匹配的函数， 那么将选择模板

```cpp
// 专门处理int的加法函数
int Add(int left, int right)
{
 return left + right;
}
// 通用加法函数
template<class T1, class T2>
T1 Add(T1 left, T2 right)
{
 return left + right;
}
void Test()
{
 Add(1, 2); // 与非函数模板类型完全匹配，不需要函数模板实例化
 Add(1, 2.0); // 模板函数可以生成更加匹配的版本，编译器根据实参生成更加匹配的Add函数
}
```

3. 模板函数不允许自动类型转换，但普通函数可以进行自动类型转换

## 类模板

类模板的定义格式：

```cpp
template<class T1, class T2, ..., class Tn>
class 类模板名
{
 // 类内成员定义
}; 
```

 注意：类模板中函数放在类外进行定义时，需要加模板参数列表

如下代码：

```cpp
template<class T>
class wzh
{ 
public :
 wzh()
 {
     ...
 }
 ~Vector();
 T& operator[](size_t pos)
      {
 assert(pos < _size);
 return _pData[pos];
 } 
private:
 T* _pData;
 size_t _size;
 size_t _capacity;
};

template <class T>
Vector<T>::~Vector()
{
 ...
}
```

注意：wzh不是具体的类，是编译器根据被实例化的类型生成具体类的模具

类模板的实例化：类模板实例化与函数模板实例化不同，类模板实例化需要在类模板名字后跟<>，然后将实例化的类型放在<> 中即可，类模板名字不是真正的类，而实例化的结果才是真正的类。

```cpp
// Vector类名，Vector<int>才是类型
Vector<int> s1;
Vector<double> s2;
```





