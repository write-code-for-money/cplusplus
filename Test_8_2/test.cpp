#include <iostream>
//#include <string>
using namespace std;
//class person
//{
//public:
//	virtual void action()
//	{
//		std::cout << "我是人" << std::endl;
//	}
//
//};
//
//class student : public person
//{
//public:
//	void action()
//	{
//		std::cout << "我是学生" << std::endl;
//	}
//};
//
//class teacher : public person
//{
//public:
//	void action() 
//	{
//		std::cout << "我是老师" << std::endl;
//	}
//};
//
//int main()
//{
//	student st;
//	teacher tc;
//	person ps;
//
//	person& ps1 = tc;
//	person& ps2 = st;
//
//	ps1.action();
//	ps2.action();
//
//	std::cout << "------------" << std::endl;
//	person* ps3 = &tc;
//	person* ps4 = &st;
//
//	ps3->action();
//	ps4->action();
//
//	ps.action();
//	return 0;
//}

//class Animal {
//public:
//    virtual string getSound() {
//        return "Animal sound";
//    }
//};
//
//class Dog : public Animal {
//public:
//    const char* getSound() override {
//        return "Woof";
//    }
//};

//class Base 
//{
//public:
//    virtual Base* clone() const 
//    {
//        return new Base(*this);
//    }
//};
//
//class Derived : public Base 
//{
//public:
//    virtual Derived* clone() const override 
//    {
//        return new Derived(*this);
//    }
//};


//class Animal 
//{
//public:
//    virtual std::string getName() 
//    {
//        return "Animal";
//    }
//};
//
//class Dog : public Animal 
//{
//public:
//    virtual std::string getName() override 
//    {
//        return "Dog";
//    }
//};


//
//class person
//{
//public:
//	virtual ~person()
//	{
//		cout << "~person()" << endl;
//	}
//};
//
//class student : public person
//{
//public:
//	~student() override
//	{
//		cout << "~student()" << endl;
//	}
//};
//
//int main()
//{
//	person* ps1 = new person;
//	person* ps2 = new student;
//
//	delete ps1;
//	delete ps2;
//	return 0;
//}

//class MyClass {
//public:
//    MyClass(int num, std::string str) : number(num), name(str) {}
//
//    // 重载operator<<，使其能够输出MyClass对象
//    std::ostream& operator<<(const MyClass& obj) {
//        cout << "Number: " << obj.number << ", Name: " << obj.name;
//        return cout;
//    }
//
//private:
//    int number;
//    string name;
//};
//
//int main() {
//    MyClass obj(42, "John");
//
//    //cout << obj << endl;  // 使用重载的operator<<输出MyClass对象
//    obj << obj;
//    return 0;
//}

//class MyClass {
//public:
//    MyClass(int num, std::string str) : number(num), name(str) {}
//
//    // 重载operator<<，使其能够输出MyClass对象
//    friend std::ostream& operator<<(std::ostream& os, const MyClass& obj) {
//        os << "Number: " << obj.number << ", Name: " << obj.name;
//        return os;
//    }
//
//private:
//    int number;
//    std::string name;
//};
//
//int main() {
//    MyClass obj(42, "John");
//
//    std::cout << obj << std::endl;  // 使用重载的operator<<输出MyClass对象
//
//    return 0;
//}

//class person
//{
//public:
//	virtual void action() final
//	{
//		cout << "我不能被重写" << endl;
//	}
//};
//
//class student : public person
//{
//public:
//	void action()
//	{
//		cout << "我就要重写你" << endl;
//	}
//};

//class person
//{
//public:
//	virtual void action() 
//	{
//		cout << "我不能被重写" << endl;
//	}
//};
//
//class student : public person
//{
//public:
//	int action() override
//	{
//		cout << "我就要重写你" << endl;
//	}
//};

class Shape {
public:
    virtual void draw() const = 0;
};

class Circle : public Shape {
public:
    void draw() const override {
        cout << "绘制圆形" << endl;
    }
};

class Rectangle : public Shape {
public:
    void draw() const override {
        cout << "绘制矩形" << endl;
    }
};

int main() {
    // 编译错误：无法实例化抽象类Shape
    // Shape类包含纯虚函数draw()，不能被实例化为对象
    //Shape* shape = new Shape();

    Shape* circle = new Circle();
    circle->draw(); // Output: 绘制圆形

    Shape* rectangle = new Rectangle();
    rectangle->draw(); // Output: 绘制矩形

    delete circle;
    delete rectangle;

    return 0;
}