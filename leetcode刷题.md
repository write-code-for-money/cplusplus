#  DAY1

## [283. 移动零](https://leetcode.cn/problems/move-zeroes/)

给定一个数组 `nums`，编写一个函数将所有 `0` 移动到数组的末尾，同时保持非零元素的相对顺序。

**请注意** ，必须在不复制数组的情况下原地对数组进行操作。

**示例 1:**

```
输入: nums = [0,1,0,3,12]
输出: [1,3,12,0,0]
```

**示例 2:**

```
输入: nums = [0]
输出: [0]
```

解法一：双指针

定义双指针，前一个指针找非零元素，找到后将后一个指针++，然后交换两个元素，当前一个指针走完就结束。

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int cur = 0, dest = -1;
        while(cur < nums.size())
        {
            if(nums[cur])
                swap(nums[++dest], nums[cur]);
            cur++;
        }
    }
};
```

## [1089. 复写零](https://leetcode.cn/problems/duplicate-zeros/)

给你一个长度固定的整数数组 `arr` ，请你将该数组中出现的每个零都复写一遍，并将其余的元素向右平移。

注意：请不要在超过该数组长度的位置写入元素。请对输入的数组 **就地** 进行上述修改，不要从函数返回任何东西。

 

**示例 1：**

```
输入：arr = [1,0,2,3,0,4,5,0]
输出：[1,0,0,2,3,0,0,4]
解释：调用函数后，输入的数组将被修改为：[1,0,0,2,3,0,0,4]
```

**示例 2：**

```
输入：arr = [1,2,3]
输出：[1,2,3]
解释：调用函数后，输入的数组将被修改为：[1,2,3]
```

解法一：双指针

先找到最后一个需要写进数组的元素，然后从后往前按照条件写，注意处理特殊情况——越界

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
	void duplicateZeros(vector<int>& arr) 
    {
        //1.查找最后一个写的元素
        int cur = -1, dest = -1;
        int sz = arr.size();
        while (cur < sz)
        {           
            if (arr[++cur]) dest++;
            else dest += 2;
            if(dest >= sz - 1) break;
        }
        //2.处理特殊情况——dest越界
        if (dest == sz)
        {
            arr[--dest] = 0;
            cur--;
            dest--;
        }
        //3.从后往前复写
        while (cur >= 0)
        {      
            if(arr[cur]) arr[dest--] = arr[cur--];
            else
            {
                arr[dest--] = 0;
                arr[dest--] = 0;
                cur--;
            }
        }
    }
```

# DAY2

## [202. 快乐数](https://leetcode.cn/problems/happy-number/)

编写一个算法来判断一个数 `n` 是不是快乐数。

**「快乐数」** 定义为：

- 对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和。
- 然后重复这个过程直到这个数变为 1，也可能是 **无限循环** 但始终变不到 1。
- 如果这个过程 **结果为** 1，那么这个数就是快乐数。

如果 `n` 是 *快乐数* 就返回 `true` ；不是，则返回 `false` 。

 

**示例 1：**

```
输入：n = 19
输出：true
解释：
12 + 92 = 82
82 + 22 = 68
62 + 82 = 100
12 + 02 + 02 = 1
```

**示例 2：**

```
输入：n = 2
输出：false
```

解法一：快慢指针

对于有环的数，定义快慢指针，快指针走两步，慢指针走一步，在环中一定会相遇，最后比较相遇的数是不是1即可

> 对于不会出现环的情况【鸽巢定理】

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int square(int num)
    {
        int sum = 0;
        while(num)
        {
            int tmp = num % 10;
            sum += tmp * tmp;
            num /= 10;
        }
        return sum;
    }
    bool isHappy(int n) {
        int fast = square(n), slow = n;
        while(fast != slow)
        {
            fast = square(square(fast));
            slow = square(slow);
        } 
        return fast == 1;
    }
};
```

## [11. 盛最多水的容器](https://leetcode.cn/problems/container-with-most-water/)

给定一个长度为 `n` 的整数数组 `height` 。有 `n` 条垂线，第 `i` 条线的两个端点是 `(i, 0)` 和 `(i, height[i])` 。

找出其中的两条线，使得它们与 `x` 轴共同构成的容器可以容纳最多的水。

返回容器可以储存的最大水量。

**说明：**你不能倾斜容器。

 

**示例 1：**

![img](https://aliyun-lc-upload.oss-cn-hangzhou.aliyuncs.com/aliyun-lc-upload/uploads/2018/07/25/question_11.jpg)

```
输入：[1,8,6,2,5,4,8,3,7]
输出：49 
解释：图中垂直线代表输入数组 [1,8,6,2,5,4,8,3,7]。在此情况下，容器能够容纳水（表示为蓝色部分）的最大值为 49。
```

**示例 2：**

```
输入：height = [1,1]
输出：1
```

解法一：双指针

定义两个指针，分别指向数组的第一个和最后一个，计算出容积，然后比较两个指针所指向的元素大小，小的向里面移动。

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int maxArea(vector<int>& height) {
        int left = 0, right = height.size() - 1;
        int ret = 0;
        while(left < right)
        {
            int volume = min(height[left], height[right]) * (right - left);
            ret = max(ret, volume);
            if(height[left] > height[right]) right--;
            else left++;
        }
        return ret;
    }
};
```

# DAY3

## [611. 有效三角形的个数](https://leetcode.cn/problems/valid-triangle-number/)

给定一个包含非负整数的数组 `nums` ，返回其中可以组成三角形三条边的三元组个数。

 

**示例 1:**

```
输入: nums = [2,2,3,4]
输出: 3
解释:有效的组合是: 
2,3,4 (使用第一个 2)
2,3,4 (使用第二个 2)
2,2,3
```

**示例 2:**

```
输入: nums = [4,2,3,4]
输出: 4
```

解法一：双指针

1. 对一组数据先排序
2. 然后用找到最大的元素作为最大边
3. 在定义双指针left和right，分别指向次大元素和最小元素
4. 当最小元素+次大元素>最大元素时，这可以知道在[left，right]这个区间中都是满足条件的，统计个数然后将right--，循环统计
5. 当不满足条件时把left++，继续判断
6. 当一组数据统计完后，再把最大边--，再次判断

时间复杂度：O(n^2)

空间复杂地：O(1)

```cpp
class Solution {
public:
    int triangleNumber(vector<int>& nums) {
        //1.排序
        sort(nums.begin(), nums.end());
        //2.定义指向第三条边的指针，也是最大边
        int end = nums.size() - 1, ret = 0;
        while(end >= 2)
        {
            //3.定义指向第一二条边的指针
            int left = 0, right = end - 1;
            while(left < right)
            {
                //4.如果left + right > end，那么在[left, right]这个区间内的数据都是符合条件的
                if(nums[left] + nums[right] > nums[end])
                {
                    ret += right - left;
                    right--;
                }
                //5.否则left++
                else left++;
            }
            end--;
        } 
        return ret;
    }
};
```

## [剑指 Offer 57. 和为s的两个数字](https://leetcode.cn/problems/he-wei-sde-liang-ge-shu-zi-lcof/)

输入一个递增排序的数组和一个数字s，在数组中查找两个数，使得它们的和正好是s。如果有多对数字的和等于s，则输出任意一对即可。

 

**示例 1：**

```
输入：nums = [2,7,11,15], target = 9
输出：[2,7] 或者 [7,2]
```

**示例 2：**

```
输入：nums = [10,26,30,31,47,60], target = 40
输出：[10,30] 或者 [30,10]
```

解法一：双指针

定义两个数begin和end，分别表示为数组的第一个和最后一个，判断和是否为target，如果和等于target就将这两个数返回，如果和小于target就将begin++，如果和大于target就将end--

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        //1.定义两个数，分别表示为数组的第一个和最后一个
        int begin = 0, end = nums.size() - 1;
        while(begin < end)
        {
            //2.当两个数的和等于target时，将两个数返回
            if(nums[begin] + nums[end] == target) return {nums[begin], nums[end]};
            //3.当两个数的和小于target时，将begin++
            if(nums[begin] + nums[end] < target) begin++;
            //4.当两个数的和大于target时，将end--
            else end--;
        }
        return {0, 0};
    }
};
```



# DAY4

## [15. 三数之和](https://leetcode.cn/problems/3sum/)

给你一个整数数组 `nums` ，判断是否存在三元组 `[nums[i], nums[j], nums[k]]` 满足 `i != j`、`i != k` 且 `j != k` ，同时还满足 `nums[i] + nums[j] + nums[k] == 0` 。请

你返回所有和为 `0` 且不重复的三元组。

**注意：**答案中不可以包含重复的三元组。

 

 

**示例 1：**

```
输入：nums = [-1,0,1,2,-1,-4]
输出：[[-1,-1,2],[-1,0,1]]
解释：
nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0 。
nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0 。
nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0 。
不同的三元组是 [-1,0,1] 和 [-1,-1,2] 。
注意，输出的顺序和三元组的顺序并不重要。
```

**示例 2：**

```
输入：nums = [0,1,1]
输出：[]
解释：唯一可能的三元组和不为 0 。
```

**示例 3：**

```
输入：nums = [0,0,0]
输出：[[0,0,0]]
解释：唯一可能的三元组和为 0 。
```

解法一：双指针

利用排序先将数据整合成有序数组，然后确定最大数，在利用双指针begin和end在最小和次大数这个区间去找两个数的和加上最大数等于零。如果三个数相加大于0，将end--。如果小于0，将begin++。等于0则将begin和end分别++和--，然后进行去重操作，判断两个数与++和--之前的数是否相等，如果相等则继续++或则--。

时间复杂度：O(n^2)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        vector<vector<int>> ret;
        //1.排序优化
        sort(nums.begin(), nums.end());
        //2.找到最大数
        int i = nums.size() - 1;
        while(i >= 2)
        {
            //3.双指针遍历
            int begin = 0, end = i - 1;
            while(begin < end)
            {
                if(nums[begin] + nums[end] + nums[i] == 0)
                {
                    ret.push_back({nums[begin], nums[end], nums[i]});
                    begin++, end--;
                    //4.去重
                    while(begin < end && nums[begin] == nums[begin - 1]) begin++;
                    while(begin < end && nums[end] == nums[end + 1]) end--;
                }
                else if(nums[begin] + nums[end] + nums[i] > 0) end--;
                else begin++; 
            }
            //4.去重
            i--;
            while(i >= 2 && nums[i] == nums[i + 1]) i--;
        }
        return ret;
    }
};
```

## [18. 四数之和](https://leetcode.cn/problems/4sum/)

给你一个由 `n` 个整数组成的数组 `nums` ，和一个目标值 `target` 。请你找出并返回满足下述全部条件且**不重复**的四元组 `[nums[a], nums[b], nums[c], nums[d]]` （若两个四元组元素一一对应，则认为两个四元组重复）：

- `0 <= a, b, c, d < n`
- `a`、`b`、`c` 和 `d` **互不相同**
- `nums[a] + nums[b] + nums[c] + nums[d] == target`

你可以按 **任意顺序** 返回答案 。

 

**示例 1：**

```
输入：nums = [1,0,-1,0,-2,2], target = 0
输出：[[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
```

**示例 2：**

```
输入：nums = [2,2,2,2,2], target = 8
输出：[[2,2,2,2]]
```

解法一：双指针

同三数之和解法类似，多套一层循环

时间复杂度：O(n^3)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        vector<vector<int>> ret;
        //1.排序
        sort(nums.begin(), nums.end());
        //2.找到最大数
        int four = nums.size() - 1;
        while(four >= 3)
        {
            //3.找到次大数
            int three = four - 1;
            while(three >= 2)
            {
                //4.定义区间
                int begin = 0, end = three - 1;
                long sum1 = nums[four] + nums[three];
                while(begin < end)
                {
                    long sum2 = nums[begin] + nums[end];
                    if(sum1 + sum2 < target) begin++;
                    else if(sum1 + sum2 > target) end--;
                    else
                    {
                        ret.push_back({nums[begin], nums[end], nums[four], nums[three]});
                        begin++, end--;
                        //5.去重
                        while(begin < end && nums[begin] == nums[begin - 1]) begin++;
                        while(begin < end && nums[end] == nums[end + 1]) end--;
                    }
                }
                //5.去重
                three--;
                while(three >= 2 && nums[three] == nums[three + 1]) three--;
            }
            //5.去重
            four--;
            while(four >= 3 && nums[four] == nums[four + 1]) four--;
        }
        return ret;
    }
};
```

# DAY5

## [209. 长度最小的子数组](https://leetcode.cn/problems/minimum-size-subarray-sum/)

给定一个含有 `n` 个正整数的数组和一个正整数 `target` **。**

找出该数组中满足其和 `≥ target` 的长度最小的 **连续子数组** `[numsl, numsl+1, ..., numsr-1, numsr]` ，并返回其长度**。**如果不存在符合条件的子数组，返回 `0` 。

 

**示例 1：**

```
输入：target = 7, nums = [2,3,1,2,4,3]
输出：2
解释：子数组 [4,3] 是该条件下的长度最小的子数组。
```

**示例 2：**

```
输入：target = 4, nums = [1,4,4]
输出：1
```

**示例 3：**

```
输入：target = 11, nums = [1,1,1,1,1,1,1,1]
输出：0
```

解法一：滑动窗口

定义两个指针， 分别是这个滑动窗口的begin和end，然后将数组的第一个数进窗口，统计窗口内数据的和然后进行判断。当sum < target时，end++，继续将后面的数进窗口，如果sum>=target时，更新此时的窗口长度，然后将窗口内第一个数出窗口，循环往复。



时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int begin = 0, end = 0, sum = 0, min_sz = INT_MAX;
        while(end < nums.size())
        {
            //1.进窗口
            sum += nums[end];
            //2.判断
            while(sum >= target)
            {
                 //3.更新长度
                min_sz = min(min_sz, end - begin + 1);
                //4.出窗口
                sum -= nums[begin++];
            }
            end++;
        }
        if(begin == 0) return 0;
        return min_sz;     
    }
};
```

## [3. 无重复字符的最长子串](https://leetcode.cn/problems/longest-substring-without-repeating-characters/)

给定一个字符串 `s` ，请你找出其中不含有重复字符的 **最长连续子字符串** 的长度。

 

**示例 1:**

```
输入: s = "abcabcbb"
输出: 3 
解释: 因为无重复字符的最长子字符串是 "abc"，所以其长度为 3。
```

**示例 2:**

```
输入: s = "bbbbb"
输出: 1
解释: 因为无重复字符的最长子字符串是 "b"，所以其长度为 1。
```

**示例 3:**

```
输入: s = "pwwkew"
输出: 3
解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
```

**示例 4:**

```
输入: s = ""
输出: 0
```

解法一：滑动窗口+哈希

用哈希表映射字符位置，统计字符出现次数，先将字符进窗口，然后统计次数，当次数>1时，将字符出窗口，更新字符长度。

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int begin = 0, end = 0, len = 0;
        int hash[128] = {0};
        while(end < s.size())
        {
            //1.进窗口
            hash[s[end]]++;
            //2.判断
            while(hash[s[end]] > 1)
            {
                //3.出窗口
                hash[s[begin++]]--;
            }
            len = max(len, end - begin + 1);
            end++;
        } 
        return len;
    }
};
```

# DAY6

## [1004. 最大连续1的个数 III](https://leetcode.cn/problems/max-consecutive-ones-iii/)

给定一个二进制数组 `nums` 和一个整数 `k`，如果可以翻转最多 `k` 个 `0` ，则返回 *数组中连续 `1` 的最大个数* 。

 

**示例 1：**

```
输入：nums = [1,1,1,0,0,0,1,1,1,1,0], K = 2
输出：6
解释：[1,1,1,0,0,1,1,1,1,1,1]
粗体数字从 0 翻转到 1，最长的子数组长度为 6。
```

**示例 2：**

```
输入：nums = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], K = 3
输出：10
解释：[0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
粗体数字从 0 翻转到 1，最长的子数组长度为 10。
```

解法一：滑动窗口

从头开始将数据进数组， 当元素为1时，直接进。元素为0时，统计窗口内0的个数，当个数>k时更新长度并将元素出窗口直到k值大于0，直到遍历完整个数组。

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int longestOnes(vector<int>& nums, int k) {
        //定义窗口区间
        int begin = 0, end = 0, len = 0;
        while(end < nums.size())
        {
            //进窗口
            if(nums[end]) end++;
            else if(nums[end] == 0 && k)
            {
                end++;
                k--;
            }
            else
            {
                while(k == 0)
                {
                    //更新长度
                    len = max(len, end - begin);
                    //出窗口
                    if(nums[begin++] == 0) k++;
                }                
            }
        }
        //更新长度
        len = max(len, end - begin);
        return len;
    }
};
```

## [1658. 将 x 减到 0 的最小操作数](https://leetcode.cn/problems/minimum-operations-to-reduce-x-to-zero/)

给你一个整数数组 `nums` 和一个整数 `x` 。每一次操作时，你应当移除数组 `nums` 最左边或最右边的元素，然后从 `x` 中减去该元素的值。请注意，需要 **修改** 数组以供接下来的操作使用。

如果可以将 `x` **恰好** 减到 `0` ，返回 **最小操作数** ；否则，返回 `-1` 。

 

**示例 1：**

```
输入：nums = [1,1,4,2,3], x = 5
输出：2
解释：最佳解决方案是移除后两个元素，将 x 减到 0 。
```

**示例 2：**

```
输入：nums = [5,6,7,8,9], x = 4
输出：-1
```

**示例 3：**

```
输入：nums = [3,2,20,1,1,3], x = 10
输出：5
解释：最佳解决方案是移除后三个元素和前两个元素（总共 5 次操作），将 x 减到 0 。
```

解法一：滑动窗口

反向找一段连续数据的和等于整个数组的和减去x的值，用滑动窗口分别将数据进窗口，然后判断，当和>target时出窗口，<target时进窗口，等于target时更新值。

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int minOperations(vector<int>& nums, int x) {
        int target = x * -1, sum = 0, len = -1;
        for(int i : nums) target += i;
        if(target < 0) return -1;
        for(int begin = 0, end = 0; end < nums.size(); end++)
        {
            sum += nums[end];
            while(sum > target) sum -= nums[begin++];
            if(sum == target) len = max(len, end - begin + 1);
        }
        return len == -1 ? -1 : nums.size() - len;
    }
};
```

# DAY7

## [904. 水果成篮](https://leetcode.cn/problems/fruit-into-baskets/)

你正在探访一家农场，农场从左到右种植了一排果树。这些树用一个整数数组 `fruits` 表示，其中 `fruits[i]` 是第 `i` 棵树上的水果 **种类** 。

你想要尽可能多地收集水果。然而，农场的主人设定了一些严格的规矩，你必须按照要求采摘水果：

- 你只有 **两个** 篮子，并且每个篮子只能装 **单一类型** 的水果。每个篮子能够装的水果总量没有限制。
- 你可以选择任意一棵树开始采摘，你必须从 **每棵** 树（包括开始采摘的树）上 **恰好摘一个水果** 。采摘的水果应当符合篮子中的水果类型。每采摘一次，你将会向右移动到下一棵树，并继续采摘。
- 一旦你走到某棵树前，但水果不符合篮子的水果类型，那么就必须停止采摘。

给你一个整数数组 `fruits` ，返回你可以收集的水果的 **最大** 数目。

 

**示例 1：**

```
输入：fruits = [1,2,1]
输出：3
解释：可以采摘全部 3 棵树。
```

**示例 2：**

```
输入：fruits = [0,1,2,2]
输出：3
解释：可以采摘 [1,2,2] 这三棵树。
如果从第一棵树开始采摘，则只能采摘 [0,1] 这两棵树。
```

**示例 3：**

```
输入：fruits = [1,2,3,2,2]
输出：4
解释：可以采摘 [2,3,2,2] 这四棵树。
如果从第一棵树开始采摘，则只能采摘 [1,2] 这两棵树。
```

**示例 4：**

```
输入：fruits = [3,3,3,1,2,1,1,2,3,3,4]
输出：5
解释：可以采摘 [1,2,1,1,2] 这五棵树。
```

解法一：滑动窗口

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int totalFruit(vector<int>& fruits) {
        int box1 = -1, box2 = -1, sz1 = 0, sz2 = 0, len = 0;
        for(int begin = 0, end = 0; end < fruits.size(); )
        {
            //1.进窗口
            if(sz1 == 0 || fruits[end] == box1)
            {
                sz1++;
                box1 = fruits[end++];
            }
            else if(sz2 == 0 || fruits[end] == box2)
            {
                sz2++;
                box2 = fruits[end++];
            }
            else
            {
                //2.判断
                while(sz1 && sz2)
                {                   
                    //3.出窗口
                    if(fruits[begin] == box1) sz1--;             
                    if(fruits[begin] == box2) sz2--;
                    begin++;
                }
            }
            //4.更新
            len = max(len, sz1 + sz2);           
        }
        return len;
    }
};
```

## [LCR 015. 找到字符串中所有字母异位词](https://leetcode.cn/problems/VabMRr/)

给定两个字符串 `s` 和 `p`，找到 `s` 中所有 `p` 的 **变位词** 的子串，返回这些子串的起始索引。不考虑答案输出的顺序。

**变位词** 指字母相同，但排列不同的字符串。

 

**示例 1：**

```
输入: s = "cbaebabacd", p = "abc"
输出: [0,6]
解释:
起始索引等于 0 的子串是 "cba", 它是 "abc" 的变位词。
起始索引等于 6 的子串是 "bac", 它是 "abc" 的变位词。
```

 **示例 2：**

```
输入: s = "abab", p = "ab"
输出: [0,1,2]
解释:
起始索引等于 0 的子串是 "ab", 它是 "ab" 的变位词。
起始索引等于 1 的子串是 "ba", 它是 "ab" 的变位词。
起始索引等于 2 的子串是 "ab", 它是 "ab" 的变位词。
```

解法一：滑动窗口+哈希表

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        vector<int> ret;
        int hash_s[26] = {0}, hash_p[26] = {0}, count = 0;
        for(char ch : p) hash_p[ch - 'a']++; //统计p字符串字符出现的个数
        for(int begin = 0, end = 0; end < s.size(); end++)
        {           
            char in = s[end];
            if(++hash_s[in - 'a'] <= hash_p[in - 'a']) count++;//进窗口 + 维护count
            if(end - begin + 1 > p.size()) //判断
            {
                char out = s[begin++];
                if(hash_s[out - 'a']-- <= hash_p[out - 'a']) count--; //出窗口 + 维护count
            }   
            if(count == p.size()) ret.push_back(begin);    //更新结果       
        }
        return ret;
    }
};
```



# DAY 8

## [30. 串联所有单词的子串](https://leetcode.cn/problems/substring-with-concatenation-of-all-words/)

给定一个字符串 `s` 和一个字符串数组 `words`**。** `words` 中所有字符串 **长度相同**。

 `s` 中的 **串联子串** 是指一个包含 `words` 中所有字符串以任意顺序排列连接起来的子串。

- 例如，如果 `words = ["ab","cd","ef"]`， 那么 `"abcdef"`， `"abefcd"`，`"cdabef"`， `"cdefab"`，`"efabcd"`， 和 `"efcdab"` 都是串联子串。 `"acdbef"` 不是串联子串，因为他不是任何 `words` 排列的连接。

返回所有串联子串在 `s` 中的开始索引。你可以以 **任意顺序** 返回答案。

 

**示例 1：**

```
输入：s = "barfoothefoobarman", words = ["foo","bar"]
输出：[0,9]
解释：因为 words.length == 2 同时 words[i].length == 3，连接的子字符串的长度必须为 6。
子串 "barfoo" 开始位置是 0。它是 words 中以 ["bar","foo"] 顺序排列的连接。
子串 "foobar" 开始位置是 9。它是 words 中以 ["foo","bar"] 顺序排列的连接。
输出顺序无关紧要。返回 [9,0] 也是可以的。
```

**示例 2：**

```
输入：s = "wordgoodgoodgoodbestword", words = ["word","good","best","word"]
输出：[]
解释：因为 words.length == 4 并且 words[i].length == 4，所以串联子串的长度必须为 16。
s 中没有子串长度为 16 并且等于 words 的任何顺序排列的连接。
所以我们返回一个空数组。
```

**示例 3：**

```
输入：s = "barfoofoobarthefoobarman", words = ["bar","foo","the"]
输出：[6,9,12]
解释：因为 words.length == 3 并且 words[i].length == 3，所以串联子串的长度必须为 9。
子串 "foobarthe" 开始位置是 6。它是 words 中以 ["foo","bar","the"] 顺序排列的连接。
子串 "barthefoo" 开始位置是 9。它是 words 中以 ["bar","the","foo"] 顺序排列的连接。
子串 "thefoobar" 开始位置是 12。它是 words 中以 ["the","foo","bar"] 顺序排列的连接。
```

解法一：滑动窗口+哈希表

时间复杂度：O(n * s)（s为words单词长度）

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> findSubstring(string s, vector<string>& words) {
        vector<int> ret;
        unordered_map<string, int> hash_words;
        for(auto& e : words) hash_words[e]++; //统计words字符串出现的次数
        int sz = words[0].size(), len = words.size();
        for(int i = 0; i < sz; i++)
        {
            unordered_map<string, int> hash_s;
            for(int left = i, right = i, count = 0; right < s.size(); right += sz)
            {
                string str_in = s.substr(right, sz);
                if(++hash_s[str_in] <= hash_words[str_in]) count++; //进窗口 + 维护count
                while(right - left + 1 > len * sz) //判断
                {
                    string str_out = s.substr(left, sz);
                    left += sz;
                    if(hash_s[str_out]-- <= hash_words[str_out]) count--; //出窗口 + 维护count
                }
                if(count == len) ret.push_back(left);
            }
        }       
        return ret;
    }
};
```

## [LCR 017. 最小覆盖子串](https://leetcode.cn/problems/M1oyTv/)

给定两个字符串 `s` 和 `t` 。返回 `s` 中包含 `t` 的所有字符的最短子字符串。如果 `s` 中不存在符合条件的子字符串，则返回空字符串 `""` 。

如果 `s` 中存在多个符合条件的子字符串，返回任意一个。

 

**注意：** 对于 `t` 中重复字符，我们寻找的子字符串中该字符数量必须不少于 `t` 中该字符数量。

 

**示例 1：**

```
输入：s = "ADOBECODEBANC", t = "ABC"
输出："BANC" 
解释：最短子字符串 "BANC" 包含了字符串 t 的所有字符 'A'、'B'、'C'
```

**示例 2：**

```
输入：s = "a", t = "a"
输出："a"
```

**示例 3：**

```
输入：s = "a", t = "aa"
输出：""
解释：t 中两个字符 'a' 均应包含在 s 的子串中，因此没有符合条件的子字符串，返回空字符串。
```

解法一：滑动窗口+哈希表

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    string minWindow(string s, string t) {
        int hash_t[128] = {0};
        for(char ch : t) hash_t[ch]++; //统计t中字符出现的个数
        int hash_s[128] = {0}, count = 0;
        int start = -1, len = INT_MAX;
        for(int begin = 0, end = 0; end < s.size(); end++)
        {
            char in = s[end];
            if(++hash_s[in] <= hash_t[in]) count++;  //进窗口 + 维护
            while(count == t.size()) //判断
            {
                if(end - begin + 1 < len) //更新
                {
                    start = begin;
                    len = end - begin + 1;
                }
                char out = s[begin++];
                if(hash_s[out]-- <= hash_t[out]) count--; //出窗口 + 维护count
            }
        }
        return start == -1 ? "" : s.substr(start, len);
    }
};
```

# DAY9

## [704. 二分查找](https://leetcode.cn/problems/binary-search/)

给定一个 `n` 个元素有序的（升序）整型数组 `nums` 和一个目标值 `target` ，写一个函数搜索 `nums` 中的 `target`，如果目标值存在返回下标，否则返回 `-1`。


**示例 1:**

```
输入: nums = [-1,0,3,5,9,12], target = 9
输出: 4
解释: 9 出现在 nums 中并且下标为 4
```

**示例 2:**

```
输入: nums = [-1,0,3,5,9,12], target = 2
输出: -1
解释: 2 不存在 nums 中因此返回 -1
```

解法一：二分法

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int search(vector<int>& nums, int target) {
        for(int left = 0, right = nums.size() - 1; left <= right; )
        {
            int mid = (right - left) / 2 + left;
            if(nums[mid] < target) left = mid + 1;
            else if(nums[mid] > target) right = mid - 1;
            else return mid;
        }
        return -1;
    }
};
```

 ## [34. 在排序数组中查找元素的第一个和最后一个位置](https://leetcode.cn/problems/find-first-and-last-position-of-element-in-sorted-array/)

给你一个按照非递减顺序排列的整数数组 `nums`，和一个目标值 `target`。请你找出给定目标值在数组中的开始位置和结束位置。

如果数组中不存在目标值 `target`，返回 `[-1, -1]`。

你必须设计并实现时间复杂度为 `O(log n)` 的算法解决此问题。

 

**示例 1：**

```
输入：nums = [5,7,7,8,8,10], target = 8
输出：[3,4]
```

**示例 2：**

```
输入：nums = [5,7,7,8,8,10], target = 6
输出：[-1,-1]
```

**示例 3：**

```
输入：nums = [], target = 0
输出：[-1,-1]
```

解法一：二分查找

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        if(nums.size() == 0) return {-1, -1};
        int left = 0, right = 0, begin = 0;
        //查找区间左端点
        for(left = 0, right = nums.size() - 1; left < right;)
        {
            int mid = (right - left) / 2 + left;
            if(nums[mid] >= target) right = mid;
            else left = mid + 1;
        }
        if(nums[right] == target) 
            begin = right;
        else
            return {-1, -1};
        //查找区间右端点
        for(left = 0, right = nums.size() - 1; left < right; )
        {
            int mid = (right - left + 1) / 2 + left;
            if(nums[mid] <= target) left = mid;
            else right = mid - 1; 
        }

        return {begin, right};
    }
};
```

# DAY 10

##  [LCR 072. x 的平方根](https://leetcode.cn/problems/jJ0w9p/)

给定一个非负整数 `x` ，计算并返回 `x` 的平方根，即实现 `int sqrt(int x)` 函数。

正数的平方根有两个，只输出其中的正数平方根。

如果平方根不是整数，输出只保留整数的部分，小数部分将被舍去。

 

**示例 1:**

```
输入: x = 4
输出: 2
```

**示例 2:**

```
输入: x = 8
输出: 2
解释: 8 的平方根是 2.82842...，由于小数部分将被舍去，所以返回 2
```

解法一：二分查找

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int mySqrt(int x) {
        if(x == 0) return 0;
        int left, right;
        for(left = 1, right = x; left < right; )
        {
            int mid = left + (right - left  + 1) / 2;
            if(mid <= x / mid) left = mid;
            else right = mid - 1;
        }
        return left;
    }
};
```

## [LCR 068. 搜索插入位置](https://leetcode.cn/problems/N6YdxV/)

给定一个排序的整数数组 `nums` 和一个整数目标值` target` ，请在数组中找到 `target `，并返回其下标。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。

请必须使用时间复杂度为 `O(log n)` 的算法。

 

**示例 1:**

```
输入: nums = [1,3,5,6], target = 5
输出: 2
```

**示例 2:**

```
输入: nums = [1,3,5,6], target = 2
输出: 1
```

**示例 3:**

```
输入: nums = [1,3,5,6], target = 7
输出: 4
```

**示例 4:**

```
输入: nums = [1,3,5,6], target = 0
输出: 0
```

**示例 5:**

```
输入: nums = [1], target = 0
输出: 0
```

解法一：二分查找

时间复杂度：O(logn)

空间复杂度：O(1)

```cp
class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int left = 0, right = nums.size() - 1;
        while(left < right)
        {
            int mid = (right - left) / 2 + left;
            if(nums[mid] >= target) right = mid;
            else left = mid + 1;
        }
        return nums[left] < target ? left + 1 : left;
    }
};
```

# DAY 11

## [LCR 069. 山脉数组的峰顶索引](https://leetcode.cn/problems/B1IidL/)

符合下列属性的数组 `arr` 称为 **山峰数组**（**山脉数组）** ：

- `arr.length >= 3`

- 存在

  ```
  i（0 < i < arr.length - 1）使得：
  ```

  - `arr[0] < arr[1] < ... arr[i-1] < arr[i] `
  - `arr[i] > arr[i+1] > ... > arr[arr.length - 1]`

给定由整数组成的山峰数组 `arr` ，返回任何满足 `arr[0] < arr[1] < ... arr[i - 1] < arr[i] > arr[i + 1] > ... > arr[arr.length - 1]` 的下标 `i` ，即山峰顶部。

 

**示例 1：**

```
输入：arr = [0,1,0]
输出：1
```

**示例 2：**

```
输入：arr = [1,3,5,4,2]
输出：2
```

**示例 3：**

```
输入：arr = [0,10,5,2]
输出：1
```

**示例 4：**

```
输入：arr = [3,4,5,1]
输出：2
```

**示例 5：**

```
输入：arr = [24,69,100,99,79,78,67,36,26,19]
输出：2
```

解法一：二分法

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int peakIndexInMountainArray(vector<int>& arr) {
        int left = 1, right = arr.size() - 2;
        while(left < right)
        {
            int mid = left + (right - left) / 2;
            if(arr[mid - 1] > arr[mid]) right = mid - 1;
            else if(arr[mid + 1] > arr[mid]) left = mid + 1;
            else return mid;
        }
        return left;
    }
};
```

## [162. 寻找峰值](https://leetcode.cn/problems/find-peak-element/)

峰值元素是指其值严格大于左右相邻值的元素。

给你一个整数数组 `nums`，找到峰值元素并返回其索引。数组可能包含多个峰值，在这种情况下，返回 **任何一个峰值** 所在位置即可。

你可以假设 `nums[-1] = nums[n] = -∞` 。

你必须实现时间复杂度为 `O(log n)` 的算法来解决此问题。

**示例 1：**

```
输入：nums = [1,2,3,1]
输出：2
解释：3 是峰值元素，你的函数应该返回其索引 2。
```

**示例 2：**

```
输入：nums = [1,2,1,3,5,6,4]
输出：1 或 5 
解释：你的函数可以返回索引 1，其峰值元素为 2；
     或者返回索引 5， 其峰值元素为 6。
```

解法一：二分法

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        int left = 0, right = nums.size() - 1;
        while(left < right)
        {
            int mid = left + (right - left) / 2;
            if(mid - 1 >= left && nums[mid - 1] > nums[mid]) right = mid - 1;
            else if(mid + 1 <= right && nums[mid + 1] > nums[mid]) left = mid + 1;
            else return mid;
        }
        return left;
    }
};
```

# DAY 12

## [153. 寻找旋转排序数组中的最小值](https://leetcode.cn/problems/find-minimum-in-rotated-sorted-array/)

已知一个长度为 `n` 的数组，预先按照升序排列，经由 `1` 到 `n` 次 **旋转** 后，得到输入数组。例如，原数组 `nums = [0,1,2,4,5,6,7]` 在变化后可能得到：

- 若旋转 `4` 次，则可以得到 `[4,5,6,7,0,1,2]`
- 若旋转 `7` 次，则可以得到 `[0,1,2,4,5,6,7]`

注意，数组 `[a[0], a[1], a[2], ..., a[n-1]]` **旋转一次** 的结果为数组 `[a[n-1], a[0], a[1], a[2], ..., a[n-2]]` 。

给你一个元素值 **互不相同** 的数组 `nums` ，它原来是一个升序排列的数组，并按上述情形进行了多次旋转。请你找出并返回数组中的 **最小元素** 。

你必须设计一个时间复杂度为 `O(log n)` 的算法解决此问题。

 

**示例 1：**

```
输入：nums = [3,4,5,1,2]
输出：1
解释：原数组为 [1,2,3,4,5] ，旋转 3 次得到输入数组。
```

**示例 2：**

```
输入：nums = [4,5,6,7,0,1,2]
输出：0
解释：原数组为 [0,1,2,4,5,6,7] ，旋转 4 次得到输入数组。
```

**示例 3：**

```
输入：nums = [11,13,15,17]
输出：11
解释：原数组为 [11,13,15,17] ，旋转 4 次得到输入数组。
```

解法一：二分法

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int findMin(vector<int>& nums) {     
        if(nums.size() == 1) return nums[0];  
        int left = 0, right = nums.size() - 1, sz = nums.size() - 1;
        while(left < right)
        {
            int mid = left + (right - left) / 2;
            if(nums[mid] > nums[sz]) left = mid + 1;
            else right = mid;
        }
        return nums[left];
    }
};
```

## [剑指 Offer 53 - II. 0～n-1中缺失的数字](https://leetcode.cn/problems/que-shi-de-shu-zi-lcof/)

一个长度为n-1的递增排序数组中的所有数字都是唯一的，并且每个数字都在范围0～n-1之内。在范围0～n-1内的n个数字中有且只有一个数字不在该数组中，请找出这个数字。

 

**示例 1:**

```
输入: [0,1,3]
输出: 2
```

**示例 2:**

```
输入: [0,1,2,3,4,5,6,7,9]
输出: 8
```

解法一：二分法

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int left = 0, right = nums.size() - 1;
        while(left < right)
        {
            int mid = left + (right - left) / 2;
            if(nums[mid] == mid) left = mid + 1;
            else right = mid;
        }
        return nums[left] == left ? left + 1 : left;
    }
};
```

# DAY13

## [**DP34** **【模板】前缀和**]([【模板】前缀和_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/acead2f4c28c401889915da98ecdc6bf?tpId=230&tqId=38960&ru=/exam/oj))

描述

给定一个长度为n的数组a1,a2,....an.

接下来有q次查询, 每次查询有两个参数l, r.

对于每个询问, 请输a<sub>l</sub>+a<sub>l + 1</sub>+.....+a<sub>r</sub>。



输入描述：

第一行包含两个整数n和q.

第二行包含n个整数, 表示a1,a2,....an.

接下来q行,每行包含两个整数  l和r.



输出描述：

输出q行,每行代表一次查询的结果.



示例1

输入：

```
3 2
1 2 4
1 2
2 3
```

输出：

```
3
6
```

解法一：前缀和

时间复杂度：O(n) + O(q)

空间复杂度：O(n)

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n, q;
    cin >> n >> q;
    vector<long long> arr(n + 1), dp(n + 1);
    for(int i = 1; i <= n; i++)
    {
        cin >> arr[i];
        dp[i] = dp[i - 1] + arr[i]; //统计前n个数和
    }
    for(int i = 0; i < q; i++)
    {
        int l, r;
        cin >> l >> r;
        cout << dp[r] - dp[l - 1] << endl;
    }
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [**DP35** **【模板】二维前缀和**](https://www.nowcoder.com/practice/99eb8040d116414ea3296467ce81cbbc?tpId=230&tqId=38962&ru=/exam/oj)

描述

给你一个 n 行 m 列的矩阵 A ，下标从1开始。

接下来有 q 次查询，每次查询输入 4 个参数 x1 , y1 , x2 , y2

请输出以 (x1, y1) 为左上角 , (x2,y2) 为右下角的子矩阵的和



输入描述：

第一行包含三个整数n,m,q.

接下来n行，每行m个整数，代表矩阵的元素

接下来q行，每行4个整数x1, y1, x2, y2，分别代表这次查询的参数



输出描述：

输出q行，每行表示查询结果。

示例1

输入：

```
3 4 3
1 2 3 4
3 2 1 0
1 5 7 8
1 1 2 2
1 1 3 3
1 2 3 4
```



输出：

```
8
25
32
```

解法一：前缀和

时间复杂度：O(n + m) + O(q)

空间复杂度：O(n + m)

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n, m, q;
    cin >> n >> m >> q;
    vector<vector<long long>> arr(n + 1, vector<long long>(m + 1, 0)), dp(n + 1, vector<long long>(m + 1, 0));
    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= m; j++) 
        {
            cin >> arr[i][j];
            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + arr[i][j];
        } 
    }
    for(int i = 0; i < q; i++)
    {
        int x1, y1, x2, y2;
        cin >> x1 >> y1 >> x2 >> y2;
        cout << dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1] << endl;
    }        

    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY14

## [LCR 012. 寻找数组的中心下标](https://leetcode.cn/problems/tvdfij/)

给你一个整数数组 `nums` ，请计算数组的 **中心下标** 。

数组 **中心下标** 是数组的一个下标，其左侧所有元素相加的和等于右侧所有元素相加的和。

如果中心下标位于数组最左端，那么左侧数之和视为 `0` ，因为在下标的左侧不存在元素。这一点对于中心下标位于数组最右端同样适用。

如果数组有多个中心下标，应该返回 **最靠近左边** 的那一个。如果数组不存在中心下标，返回 `-1` 。

 

**示例 1：**

```
输入：nums = [1,7,3,6,5,6]
输出：3
解释：
中心下标是 3 。
左侧数之和 sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11 ，
右侧数之和 sum = nums[4] + nums[5] = 5 + 6 = 11 ，二者相等。
```

**示例 2：**

```
输入：nums = [1, 2, 3]
输出：-1
解释：
数组中不存在满足此条件的中心下标。
```

**示例 3：**

```
输入：nums = [2, 1, -1]
输出：0
解释：
中心下标是 0 。
左侧数之和 sum = 0 ，（下标 0 左侧不存在元素），
右侧数之和 sum = nums[1] + nums[2] = 1 + -1 = 0 。
```

解法一：前缀和

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        vector<int> tmp(nums.size() + 1, 0);
        int sz = tmp.size() - 1;
        for(int i = 1; i <= sz; i++)
            tmp[i] = tmp[i - 1] + nums[i - 1]; 

        for(int i = 0; i < sz; i++)
            if(tmp[i] == tmp[sz] - tmp[i + 1]) return i;

        return -1;
    }
};
```

## [238. 除自身以外数组的乘积](https://leetcode.cn/problems/product-of-array-except-self/)

给你一个整数数组 `nums`，返回 *数组 `answer` ，其中 `answer[i]` 等于 `nums` 中除 `nums[i]` 之外其余各元素的乘积* 。

题目数据 **保证** 数组 `nums`之中任意元素的全部前缀元素和后缀的乘积都在 **32 位** 整数范围内。

请**不要使用除法，**且在 `O(*n*)` 时间复杂度内完成此题。

 

**示例 1:**

```
输入: nums = [1,2,3,4]
输出: [24,12,8,6]
```

**示例 2:**

```
输入: nums = [-1,1,0,-3,3]
输出: [0,0,9,0,0]
```

解法一：前缀和+后缀和

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int sz = nums.size() + 1;
        vector<int> prefix(sz, 1), suffix(sz, 1), ret(sz - 1, 0);
        for(int i = 1; i < sz; i++)
            prefix[i] = prefix[i - 1] * nums[i - 1];
        for(int i = sz - 3; i >= 0; i--)
            suffix[i] = suffix[i + 1] * nums[i + 1];
        for(int i = 0; i < sz - 1; i++)
            ret[i] = prefix[i] * suffix[i];
        return ret;
    }
};
```

解法二：左右累乘

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        
        int n=nums.size();
        int left=1,right=1;     //left：从左边累乘，right：从右边累乘
        vector<int> res(n,1);
        
        for(int i=0;i<n;++i)    //最终每个元素其左右乘积进行相乘得出结果
        {
            res[i]*=left;       //乘以其左边的乘积
            left*=nums[i];
            
            res[n-1-i]*=right;  //乘以其右边的乘积
            right*=nums[n-1-i];
        }       
        return res;  
    }
};
```

# DAY15

## [LCR 010. 和为 K 的子数组](https://leetcode.cn/problems/QTMn0o/)

给定一个整数数组和一个整数 `k` **，**请找到该数组中和为 `k` 的连续子数组的个数。

 

**示例 1：**

```
输入:nums = [1,1,1], k = 2
输出: 2
解释: 此题 [1,1] 与 [1,1] 为两种不同的情况
```

**示例 2：**

```
输入:nums = [1,2,3], k = 3
输出: 2
```

解法一：前缀和+哈希表

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        unordered_map<int, int> hash;
        hash[0]++;
        int sum = 0, ret = 0;
        for(auto e : nums)
        {
            sum += e;
            if(hash.count(sum - k)) ret += hash[sum - k];
            hash[sum]++;
        }
        return ret;
    }
};
```

## [974. 和可被 K 整除的子数组](https://leetcode.cn/problems/subarray-sums-divisible-by-k/)

给定一个整数数组 `nums` 和一个整数 `k` ，返回其中元素之和可被 `k` 整除的（连续、非空） **子数组** 的数目。

**子数组** 是数组的 **连续** 部分。

 

**示例 1：**

```
输入：nums = [4,5,0,-2,-3,1], k = 5
输出：7
解释：
有 7 个子数组满足其元素之和可被 k = 5 整除：
[4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]
```

**示例 2:**

```
输入: nums = [5], k = 9
输出: 0
```

解法一：前缀和+哈希

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    int subarraysDivByK(vector<int>& nums, int k) {
        unordered_map<int, int> hash;
        hash[0]++;
        int sum = 0, ret = 0;
        for(auto e : nums)
        {
            sum += e;
            int cur = (sum % k + k) % k;
            if(hash.count(cur)) ret += hash[cur];
            hash[cur]++;
        }
        return ret;
    }
};
```

# DAY16

## [LCR 011. 连续数组](https://leetcode.cn/problems/A1NYOS/)

给定一个二进制数组 `nums` , 找到含有相同数量的 `0` 和 `1` 的最长连续子数组，并返回该子数组的长度。

 

**示例 1：**

```
输入: nums = [0,1]
输出: 2
说明: [0, 1] 是具有相同数量 0 和 1 的最长连续子数组。
```

**示例 2：**

```
输入: nums = [0,1,0]
输出: 2
说明: [0, 1] (或 [1, 0]) 是具有相同数量 0 和 1 的最长连续子数组。
```

解法一：前缀和 + 哈希表

时间复杂度：O(n)

空间复杂度:O(n)

```cpp
class Solution {
public:
    int findMaxLength(vector<int>& nums) {       
        unordered_map<int, int> hash;
        hash[0] = -1;
        int sum = 0, ret = 0;
        for(int i = 0; i < nums.size(); i++)
        {
            sum += nums[i] == 0 ? -1 : 1;
            if(hash.count(sum)) ret = max(ret, i - hash[sum]);
            else hash[sum] = i;
        }
        return ret;
    }
};
```

## [1314. 矩阵区域和](https://leetcode.cn/problems/matrix-block-sum/)

给你一个 `m x n` 的矩阵 `mat` 和一个整数 `k` ，请你返回一个矩阵 `answer` ，其中每个 `answer[i][j]` 是所有满足下述条件的元素 `mat[r][c]` 的和： 

- `i - k <= r <= i + k, `
- `j - k <= c <= j + k` 且
- `(r, c)` 在矩阵内。

 

**示例 1：**

```
输入：mat = [[1,2,3],[4,5,6],[7,8,9]], k = 1
输出：[[12,21,16],[27,45,33],[24,39,28]]
```

**示例 2：**

```
输入：mat = [[1,2,3],[4,5,6],[7,8,9]], k = 2
输出：[[45,45,45],[45,45,45],[45,45,45]]
```

解法一：二维矩阵和

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
        vector<vector<int>> prefix(mat.size() + 1, vector<int>(mat[0].size() + 1, 0));
        for(int i = 1; i <= mat.size(); i++)
        {
            for(int j = 1; j <= mat[0].size(); j++)
            {
                prefix[i][j] = prefix[i - 1][j] + prefix[i][j - 1] - prefix[i - 1][j - 1] + mat[i - 1][j - 1];
            }
        }
        vector<vector<int>> ret(mat.size(), vector<int>(mat[0].size(), 0));
        for(int i = 0; i < ret.size(); i++)
        {
            for(int j = 0; j < ret[0].size(); j++)
            {
                int ri = i + k + 1 >= prefix.size() ? prefix.size() - 1 : i + k + 1; 
                int rj = j + k + 1 >= prefix[0].size() ? prefix[0].size() - 1 : j + k + 1;
                int li = i - k < 0 ? 0 : i - k;
                int lj = j - k < 0 ? 0 : j - k;
                ret[i][j] = prefix[ri][rj] - prefix[ri][lj] - prefix[li][rj] + prefix[li][lj];
            }
        }
        return ret;
    }
};
```

# DAY17

## [191. 位1的个数](https://leetcode.cn/problems/number-of-1-bits/)

编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为[汉明重量](https://baike.baidu.com/item/汉明重量)）。

 

**提示：**

- 请注意，在某些语言（如 Java）中，没有无符号整数类型。在这种情况下，输入和输出都将被指定为有符号整数类型，并且不应影响您的实现，因为无论整数是有符号的还是无符号的，其内部的二进制表示形式都是相同的。
- 在 Java 中，编译器使用[二进制补码](https://baike.baidu.com/item/二进制补码/5295284)记法来表示有符号整数。因此，在 **示例 3** 中，输入表示有符号整数 `-3`。

 

**示例 1：**

```
输入：n = 00000000000000000000000000001011
输出：3
解释：输入的二进制串 00000000000000000000000000001011 中，共有三位为 '1'。
```

**示例 2：**

```
输入：n = 00000000000000000000000010000000
输出：1
解释：输入的二进制串 00000000000000000000000010000000 中，共有一位为 '1'。
```

**示例 3：**

```
输入：n = 11111111111111111111111111111101
输出：31
解释：输入的二进制串 11111111111111111111111111111101 中，共有 31 位为 '1'。
```

解法一：位运算

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int hammingWeight(uint32_t n) {
        int ret = 0;
        while(n)
        {
            n &= (n - 1);
            ret++;
        }
        return ret;
    }
};
```

## [338. 比特位计数](https://leetcode.cn/problems/counting-bits/)

给你一个整数 `n` ，对于 `0 <= i <= n` 中的每个 `i` ，计算其二进制表示中 **`1` 的个数** ，返回一个长度为 `n + 1` 的数组 `ans` 作为答案。

 

**示例 1：**

```
输入：n = 2
输出：[0,1,1]
解释：
0 --> 0
1 --> 1
2 --> 10
```

**示例 2：**

```
输入：n = 5
输出：[0,1,1,2,1,2]
解释：
0 --> 0
1 --> 1
2 --> 10
3 --> 11
4 --> 100
5 --> 101
```

解法一：位运算+动规

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> countBits(int n) {
        vector<int> ret(n + 1, 0);
        for(int i = 1; i <= n; i++)
            ret[i] = ret[i & (i - 1)] + 1;
        return ret;
    }
};
```

## [461. 汉明距离](https://leetcode.cn/problems/hamming-distance/)

两个整数之间的 [汉明距离](https://baike.baidu.com/item/汉明距离) 指的是这两个数字对应二进制位不同的位置的数目。

给你两个整数 `x` 和 `y`，计算并返回它们之间的汉明距离。

 

**示例 1：**

```
输入：x = 1, y = 4
输出：2
解释：
1   (0 0 0 1)
4   (0 1 0 0)
       ↑   ↑
上面的箭头指出了对应二进制位不同的位置。
```

**示例 2：**

```
输入：x = 3, y = 1
输出：1
```

解法一：位运算

时间复杂度：O(logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int hammingDistance(int x, int y) {
        int tmp = x ^ y, ret = 0;
        while(tmp)
        {
            tmp &= tmp - 1;
            ret++;
        }
        return ret;
    }
};
```

## [136. 只出现一次的数字](https://leetcode.cn/problems/single-number/)

给你一个 **非空** 整数数组 `nums` ，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。

你必须设计并实现线性时间复杂度的算法来解决此问题，且该算法只使用常量额外空间。

 

**示例 1 ：**

```
输入：nums = [2,2,1]
输出：1
```

**示例 2 ：**

```
输入：nums = [4,1,2,1,2]
输出：4
```

**示例 3 ：**

```
输入：nums = [1]
输出：1
```

解法一：位运算

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ret = 0;
        for(int e : nums)
            ret ^= e;
        return ret;
    }
};
```

## [260. 只出现一次的数字 III](https://leetcode.cn/problems/single-number-iii/)

给你一个整数数组 `nums`，其中恰好有两个元素只出现一次，其余所有元素均出现两次。 找出只出现一次的那两个元素。你可以按 **任意顺序** 返回答案。

你必须设计并实现线性时间复杂度的算法且仅使用常量额外空间来解决此问题。

 

**示例 1：**

```
输入：nums = [1,2,1,3,2,5]
输出：[3,5]
解释：[5, 3] 也是有效的答案。
```

**示例 2：**

```
输入：nums = [-1,0]
输出：[-1,0]
```

**示例 3：**

```
输入：nums = [0,1]
输出：[1,0]
```

解法一：位运算

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int tmp = 0;
        for(int e : nums)
            tmp ^= e;
        int cur = 0;
        while(!((tmp >> cur) & 1))
            cur++;
        int x = 0, y = 0;
        for(int e : nums)
        {
            if(e >> cur & 1) x ^= e;
            else y ^= e;   
        }
        return {x, y};
    }
};
```

## [面试题 01.01. 判定字符是否唯一](https://leetcode.cn/problems/is-unique-lcci/)

实现一个算法，确定一个字符串 `s` 的所有字符是否全都不同。

**示例 1：**

```
输入: s = "leetcode"
输出: false 
```

**示例 2：**

```
输入: s = "abc"
输出: true
```

**限制：**

- `0 <= len(s) <= 100 `
- `s[i]`仅包含小写字母
- 如果你不使用额外的数据结构，会很加分。

解法一：位运算

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    bool isUnique(string astr) {
        if(astr.size() > 26) return false;
        size_t i = 0;
        for(char e : astr)
        {
            int cur = e - 'a';
            int tmp = 1 << cur;
            if(tmp & i) return false;
            else i |= tmp;
        }
        return true;
    }
};
```

# DAY18

## [268. 丢失的数字](https://leetcode.cn/problems/missing-number/)

给定一个包含 `[0, n]` 中 `n` 个数的数组 `nums` ，找出 `[0, n]` 这个范围内没有出现在数组中的那个数。



 

**示例 1：**

```
输入：nums = [3,0,1]
输出：2
解释：n = 3，因为有 3 个数字，所以所有的数字都在范围 [0,3] 内。2 是丢失的数字，因为它没有出现在 nums 中。
```

**示例 2：**

```
输入：nums = [0,1]
输出：2
解释：n = 2，因为有 2 个数字，所以所有的数字都在范围 [0,2] 内。2 是丢失的数字，因为它没有出现在 nums 中。
```

**示例 3：**

```
输入：nums = [9,6,4,2,3,5,7,0,1]
输出：8
解释：n = 9，因为有 9 个数字，所以所有的数字都在范围 [0,9] 内。8 是丢失的数字，因为它没有出现在 nums 中。
```

**示例 4：**

```
输入：nums = [0]
输出：1
解释：n = 1，因为有 1 个数字，所以所有的数字都在范围 [0,1] 内。1 是丢失的数字，因为它没有出现在 nums 中。
```

解法一：位运算

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int ret = nums.size();
        for(int i = 0; i < nums.size(); i++)
            ret ^= nums[i] ^ i;
        return ret;
    }
};
```

## [371. 两整数之和](https://leetcode.cn/problems/sum-of-two-integers/)

给你两个整数 `a` 和 `b` ，**不使用** 运算符 `+` 和 `-` ，计算并返回两整数之和。

 

**示例 1：**

```
输入：a = 1, b = 2
输出：3
```

**示例 2：**

```
输入：a = 2, b = 3
输出：5
```

解法一：位运算

时间复杂度：O(logb)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int getSum(int a, int b) {
        while(b)
        {
            int tmp_a = a, tmp_b = b;
            a = tmp_a ^ tmp_b;
            b = (tmp_a & tmp_b) << 1;
        }
        return a;
    }
};
```

# DAY19

## [LCR 004. 只出现一次的数字 II](https://leetcode.cn/problems/WGki4K/)

给你一个整数数组 `nums` ，除某个元素仅出现 **一次** 外，其余每个元素都恰出现 **三次 。**请你找出并返回那个只出现了一次的元素。

 

**示例 1：**

```
输入：nums = [2,2,3,2]
输出：3
```

**示例 2：**

```
输入：nums = [0,1,0,1,0,1,100]
输出：100
```

解法一：位运算

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ret = 0;
        for(int i = 0; i < 32; i++)
        {
            int sum = 0;
            for(int x : nums)
                if((x >> i) & 1 == 1) sum++;
            if(sum % 3 == 1) ret |= 1 << i;
        }
        return ret;
    }
};
```

## [面试题 17.19. 消失的两个数字](https://leetcode.cn/problems/missing-two-lcci/)

给定一个数组，包含从 1 到 N 所有的整数，但其中缺了两个数字。你能在 O(N) 时间内只用 O(1) 的空间找到它们吗？

以任意顺序返回这两个数字均可。

**示例 1:**

```
输入: [1]
输出: [2,3]
```

**示例 2:**

```
输入: [2,3]
输出: [1,4]
```

解法一：位运算

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> missingTwo(vector<int>& nums) {
        int tmp = 0;
        for(int i = 0; i < nums.size(); i++)
            tmp ^= nums[i];
        for(int i = 1; i <= nums.size() + 2; i++)
            tmp ^= i;
        int cur = 0;
        for(; cur < 32; cur++)
            if((tmp >> cur) & 1) break;
        int ret1 = 0, ret2 = 0;
        for(int e : nums)
        {
            if((e >> cur) & 1) ret1 ^= e;
            else ret2 ^= e;
        }
        for(int i = 1; i <= nums.size() + 2; i++)
        {
            if((i >> cur) & 1) ret1 ^= i;
            else ret2 ^= i;
        }
        return {ret1, ret2};
    }
};
```

# DAY20

## [1576. 替换所有的问号](https://leetcode.cn/problems/replace-all-s-to-avoid-consecutive-repeating-characters/)

给你一个仅包含小写英文字母和 `'?'` 字符的字符串 `s`，请你将所有的 `'?'` 转换为若干小写字母，使最终的字符串不包含任何 **连续重复** 的字符。

注意：你 **不能** 修改非 `'?'` 字符。

题目测试用例保证 **除** `'?'` 字符 **之外**，不存在连续重复的字符。

在完成所有转换（可能无需转换）后返回最终的字符串。如果有多个解决方案，请返回其中任何一个。可以证明，在给定的约束条件下，答案总是存在的。

 

**示例 1：**

```
输入：s = "?zs"
输出："azs"
解释：该示例共有 25 种解决方案，从 "azs" 到 "yzs" 都是符合题目要求的。只有 "z" 是无效的修改，因为字符串 "zzs" 中有连续重复的两个 'z' 。
```

**示例 2：**

```
输入：s = "ubv?w"
输出："ubvaw"
解释：该示例共有 24 种解决方案，只有替换成 "v" 和 "w" 不符合题目要求。因为 "ubvvw" 和 "ubvww" 都包含连续重复的字符。
```

解法一：模拟

时间复杂度：O(n * c)（n为字符串长度，c为替换字符长度）

空间复杂度：O(1)

```cpp
class Solution {
public:
    string modifyString(string s) {
        for(int i = 0; i < s.size(); i++)
        {
            if(s[i] == '?')
            {
                char left = i == 0 ? '?' : s[i - 1];
                char right = i == s.size() - 1 ? '?' : s[i + 1];
                char tmp = 'a';
                while(tmp <= 'z')
                {
                    if(tmp != left && tmp != right)
                    {
                        s[i] = tmp;
                        break;
                    }
                    tmp++;
                }
            }
        }
        return s;
    }
};
```

## [495. 提莫攻击](https://leetcode.cn/problems/teemo-attacking/)

在《英雄联盟》的世界中，有一个叫 “提莫” 的英雄。他的攻击可以让敌方英雄艾希（编者注：寒冰射手）进入中毒状态。

当提莫攻击艾希，艾希的中毒状态正好持续 `duration` 秒。

正式地讲，提莫在 `t` 发起攻击意味着艾希在时间区间 `[t, t + duration - 1]`（含 `t` 和 `t + duration - 1`）处于中毒状态。如果提莫在中毒影响结束 **前** 再次攻击，中毒状态计时器将会 **重置** ，在新的攻击之后，中毒影响将会在 `duration` 秒后结束。

给你一个 **非递减** 的整数数组 `timeSeries` ，其中 `timeSeries[i]` 表示提莫在 `timeSeries[i]` 秒时对艾希发起攻击，以及一个表示中毒持续时间的整数 `duration` 。

返回艾希处于中毒状态的 **总** 秒数。

 

**示例 1：**

```
输入：timeSeries = [1,4], duration = 2
输出：4
解释：提莫攻击对艾希的影响如下：
- 第 1 秒，提莫攻击艾希并使其立即中毒。中毒状态会维持 2 秒，即第 1 秒和第 2 秒。
- 第 4 秒，提莫再次攻击艾希，艾希中毒状态又持续 2 秒，即第 4 秒和第 5 秒。
艾希在第 1、2、4、5 秒处于中毒状态，所以总中毒秒数是 4 。
```

**示例 2：**

```
输入：timeSeries = [1,2], duration = 2
输出：3
解释：提莫攻击对艾希的影响如下：
- 第 1 秒，提莫攻击艾希并使其立即中毒。中毒状态会维持 2 秒，即第 1 秒和第 2 秒。
- 第 2 秒，提莫再次攻击艾希，并重置中毒计时器，艾希中毒状态需要持续 2 秒，即第 2 秒和第 3 秒。
艾希在第 1、2、3 秒处于中毒状态，所以总中毒秒数是 3 。
```

解法一：模拟

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
        int ret = 0, flag = 0;
        for(int i = 0; i < timeSeries.size(); i++)
        {
            if(flag == 0 || timeSeries[i] > flag) ret += duration;                
            else ret += timeSeries[i] - timeSeries[i - 1];               
            flag = timeSeries[i] + duration - 1;
        }
        return ret;
    }
};
```

# DAY21

## [6. N 字形变换](https://leetcode.cn/problems/zigzag-conversion/)

将一个给定字符串 `s` 根据给定的行数 `numRows` ，以从上往下、从左到右进行 Z 字形排列。

比如输入字符串为 `"PAYPALISHIRING"` 行数为 `3` 时，排列如下：

```
P   A   H   N
A P L S I I G
Y   I   R
```

之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如：`"PAHNAPLSIIGYIR"`。

请你实现这个将字符串进行指定行数变换的函数：

```
string convert(string s, int numRows);
```

 

**示例 1：**

```
输入：s = "PAYPALISHIRING", numRows = 3
输出："PAHNAPLSIIGYIR"
```

**示例 2：**

```
输入：s = "PAYPALISHIRING", numRows = 4
输出："PINALSIGYAHRPI"
解释：
P     I    N
A   L S  I G
Y A   H R
P     I
```

**示例 3：**

```
输入：s = "A", numRows = 1
输出："A"
```

解法一：模拟

时间复杂度：O(n)

空间复杂度：o(1)

```cpp
class Solution {
public:
    string convert(string s, int numRows) {
        if(numRows == 1) return s;
        string ret;
        int d = 2 * (numRows - 1);
        //第一行
        for(int i = 0; i < s.size(); i += d)
            ret += s[i];
        //中间行
        for(int i = 1; i < numRows - 1; i++)
        {
            for(int j = i, k = d - i; j < s.size() || k < s.size(); j += d, k += d)
            {
                if(j < s.size()) ret += s[j];
                if(k < s.size()) ret += s[k];
            }
        }
        //末行
        for(int i = numRows - 1; i < s.size(); i += d)
            ret += s[i];
        return ret;
    }
};
```

## [38. 外观数列](https://leetcode.cn/problems/count-and-say/)

给定一个正整数 `n` ，输出外观数列的第 `n` 项。

「外观数列」是一个整数序列，从数字 1 开始，序列中的每一项都是对前一项的描述。

你可以将其视作是由递归公式定义的数字字符串序列：

- `countAndSay(1) = "1"`
- `countAndSay(n)` 是对 `countAndSay(n-1)` 的描述，然后转换成另一个数字字符串。

前五项如下：

```
1.     1
2.     11
3.     21
4.     1211
5.     111221
第一项是数字 1 
描述前一项，这个数是 1 即 “ 一 个 1 ”，记作 "11"
描述前一项，这个数是 11 即 “ 二 个 1 ” ，记作 "21"
描述前一项，这个数是 21 即 “ 一 个 2 + 一 个 1 ” ，记作 "1211"
描述前一项，这个数是 1211 即 “ 一 个 1 + 一 个 2 + 二 个 1 ” ，记作 "111221"
```

要 **描述** 一个数字字符串，首先要将字符串分割为 **最小** 数量的组，每个组都由连续的最多 **相同字符** 组成。然后对于每个组，先描述字符的数量，然后描述字符，形成一个描述组。要将描述转换为数字字符串，先将每组中的字符数量用数字替换，再将所有描述组连接起来。

例如，数字字符串 `"3322251"` 的描述如下图：

![img](https://pic.leetcode-cn.com/1629874763-TGmKUh-image.png)



 

**示例 1：**

```
输入：n = 1
输出："1"
解释：这是一个基本样例。
```

**示例 2：**

```
输入：n = 4
输出："1211"
解释：
countAndSay(1) = "1"
countAndSay(2) = 读 "1" = 一 个 1 = "11"
countAndSay(3) = 读 "11" = 二 个 1 = "21"
countAndSay(4) = 读 "21" = 一 个 2 + 一 个 1 = "12" + "11" = "1211"
```

解法一：模拟 + 双指针

时间复杂度：o(n ^ 2)

空间复杂度：O(1)

```cpp
class Solution {
public:
    string countAndSay(int n) {
        string ret("1");        
        for(int i = 2; i <= n; i++)
        {
            int left = 0, right = 0;
            string tmp;
            for(; right < ret.size();)
            {
                
                if(ret[left] == ret[right]) right++;
                else
                {
                    tmp += (right - left) + '0';
                    tmp += ret[left];
                    left = right;
                }
            }
            tmp += (right - left) + '0';
            tmp += ret[left];
            ret = tmp;
        }
        return ret;
    }
};
```

# DAY22

## [1419. 数青蛙](https://leetcode.cn/problems/minimum-number-of-frogs-croaking/)

给你一个字符串 `croakOfFrogs`，它表示不同青蛙发出的蛙鸣声（字符串 `"croak"` ）的组合。由于同一时间可以有多只青蛙呱呱作响，所以 `croakOfFrogs` 中会混合多个 `“croak”` *。*

请你返回模拟字符串中所有蛙鸣所需不同青蛙的最少数目。

要想发出蛙鸣 "croak"，青蛙必须 **依序** 输出 `‘c’, ’r’, ’o’, ’a’, ’k’` 这 5 个字母。如果没有输出全部五个字母，那么它就不会发出声音。如果字符串 `croakOfFrogs` 不是由若干有效的 "croak" 字符混合而成，请返回 `-1` 。

 

**示例 1：**

```
输入：croakOfFrogs = "croakcroak"
输出：1 
解释：一只青蛙 “呱呱” 两次
```

**示例 2：**

```
输入：croakOfFrogs = "crcoakroak"
输出：2 
解释：最少需要两只青蛙，“呱呱” 声用黑体标注
第一只青蛙 "crcoakroak"
第二只青蛙 "crcoakroak"
```

**示例 3：**

```
输入：croakOfFrogs = "croakcrook"
输出：-1
解释：给出的字符串不是 "croak" 的有效组合。
```

 

**提示：**

- `1 <= croakOfFrogs.length <= 105`
- 字符串中的字符只有 `'c'`, `'r'`, `'o'`, `'a'` 或者 `'k'`

解法一：模拟 + 哈希

时间复杂度：O(n)

空间复杂度：O(m)（叫声字符串长度）

```cpp
class Solution {
public:
    int minNumberOfFrogs(string croakOfFrogs) {
        string arr = "croak";
        int sz = arr.size();
        vector<int> hash(sz);
        unordered_map<char, int> index;
        for(int i = 0; i < sz; i++)
            index[arr[i]] = i;
        for(char ch : croakOfFrogs)
        {
            if(ch == 'c')
            {
                if(hash[sz - 1]) hash[sz - 1]--;
                hash[0]++;
            }
            else
            {
                int i = index[ch];
                if(!hash[i - 1]) return -1;
                hash[i - 1]--;
                hash[i]++;
            }
        }
        for(int i = 0; i < sz - 1; i++)
            if(hash[i]) return -1;
        return hash[sz - 1];
    }
};
```

## [75. 颜色分类](https://leetcode.cn/problems/sort-colors/)

给定一个包含红色、白色和蓝色、共 `n` 个元素的数组 `nums` ，**[原地](https://baike.baidu.com/item/原地算法)**对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。

我们使用整数 `0`、 `1` 和 `2` 分别表示红色、白色和蓝色。



必须在不使用库内置的 sort 函数的情况下解决这个问题。

 

**示例 1：**

```
输入：nums = [2,0,2,1,1,0]
输出：[0,0,1,1,2,2]
```

**示例 2：**

```
输入：nums = [2,0,1]
输出：[0,1,2]
```

解法一：三指针

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    void sortColors(vector<int>& nums) {
        int left = -1, right = nums.size();
        for(int i = 0; i < right; )
        {
            if(nums[i] == 2)
                swap(nums[--right], nums[i]);
            else if(nums[i] == 0)
                swap(nums[++left], nums[i++]);
            else i++;
        } 
    }
};
```

# DAY23

## [912. 排序数组](https://leetcode.cn/problems/sort-an-array/)

给你一个整数数组 `nums`，请你将该数组升序排列。

 



**示例 1：**

```
输入：nums = [5,2,3,1]
输出：[1,2,3,5]
```

**示例 2：**

```
输入：nums = [5,1,1,2,0,0]
输出：[0,0,1,1,2,5]
```

解法一：快排

时间复杂度：O(nlogn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> sortArray(vector<int>& nums) {
        srand(time(NULL));
        quick_sort(nums, 0, nums.size() - 1);
        return nums;
    }
    void quick_sort(vector<int>& arr, int l, int r)
    {
        if(l >= r) return;
        int key = arr[rand() % (r - l + 1) + l];
        int left = l - 1, right = r + 1;
        for(int i = l; i < right; )
        {
            if(arr[i] > key) swap(arr[i], arr[--right]);
            else if(arr[i] < key) swap(arr[i++], arr[++left]);
            else i++;
        }
        quick_sort(arr, l, left);
        quick_sort(arr, right, r);
    }
};
```

## [215. 数组中的第K个最大元素](https://leetcode.cn/problems/kth-largest-element-in-an-array/)

给定整数数组 `nums` 和整数 `k`，请返回数组中第 `**k**` 个最大的元素。

请注意，你需要找的是数组排序后的第 `k` 个最大的元素，而不是第 `k` 个不同的元素。

你必须设计并实现时间复杂度为 `O(n)` 的算法解决此问题。

 

**示例 1:**

```
输入: [3,2,1,5,6,4], k = 2
输出: 5
```

**示例 2:**

```
输入: [3,2,3,1,2,4,5,5,6], k = 4
输出: 4
```

解法一：快速选择

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int quickselect(vector<int> &nums, int l, int r, int k) {
        if (l == r) return nums[l];
        int left = l - 1, right = r + 1;
        int key = nums[rand() % (r - l + 1) + l];
        for(int i = l; i < right;)
        {
            if(key < nums[i]) swap(nums[i], nums[--right]);
            else if(key > nums[i]) swap(nums[i++], nums[++left]);
            else i++;
        }
        int c = r - right + 1, b = right - left - 1;
        if(k <= c) return quickselect(nums, right, r, k);
        else if(k <= c + b) return key;
        else return quickselect(nums, l, left, k - c - b);
    }

    int findKthLargest(vector<int> &nums, int k) {
        srand(time(NULL));
        return quickselect(nums, 0, nums.size() - 1, k);
    }
};

```

# DAY24

## [剑指 Offer 40. 最小的k个数](https://leetcode.cn/problems/zui-xiao-de-kge-shu-lcof/)

输入整数数组 `arr` ，找出其中最小的 `k` 个数。例如，输入4、5、1、6、2、7、3、8这8个数字，则最小的4个数字是1、2、3、4。

 

**示例 1：**

```
输入：arr = [3,2,1], k = 2
输出：[1,2] 或者 [2,1]
```

**示例 2：**

```
输入：arr = [0,1,2,1], k = 1
输出：[0]
```

解法一：快速选择

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    vector<int> getLeastNumbers(vector<int>& nums, int k) {
        if(k == 0) return {};
        srand(time(NULL));       
        quick_sort(nums, 0, nums.size() - 1, k);
        return {nums.begin(), nums.begin() + k};
    }
    void quick_sort(vector<int>& nums, int l, int r, int k)
    {
        if(r == l) return;
        int left = l - 1, right = r + 1;
        int key = nums[rand() % (r - l + 1) + l];
        for(int i = l; i < right;)
        {
            if(nums[i] > key) swap(nums[i], nums[--right]);
            else if(nums[i] < key) swap(nums[i++], nums[++left]);
            else i++;
        }
        int a = left - l + 1, b = right - left - 1;
        if(k <= a) return quick_sort(nums, l, left, k);
        else if(k <= a + b) return;
        else return quick_sort(nums, right, r, k - a - b);
    }
};
```

## [912. 排序数组](https://leetcode.cn/problems/sort-an-array/)

给你一个整数数组 `nums`，请你将该数组升序排列。

 



**示例 1：**

```
输入：nums = [5,2,3,1]
输出：[1,2,3,5]
```

**示例 2：**

```
输入：nums = [5,1,1,2,0,0]
输出：[0,0,1,1,2,5]
```

解法一：归并

时间复杂度：O(nlogn)

空间复杂度：O(n)

```cpp
class Solution {   
public:
    vector<int> tmp;
    vector<int> sortArray(vector<int>& nums) {
        tmp.resize(nums.size());
        merge_sort(nums, 0, nums.size() - 1);
        return nums;
    }
    void merge_sort(vector<int>& nums, int l, int r)
    {
        if(l >= r) return;
        int mid = (r - l) / 2 + l;
        merge_sort(nums, l, mid);
        merge_sort(nums, mid + 1, r);
        //合并
        int left = l, right = mid + 1;
        int i = 0;
        while(left <= mid && right <= r)
            tmp[i++] = nums[left] > nums[right] ? nums[right++] : nums[left++];
        while(left <= mid)
            tmp[i++] = nums[left++];
        while(right <= r)
            tmp[i++] = nums[right++];   
        for(int i = l; i <= r; i++)
        {
            nums[i] = tmp[i - l];
        }
    }
};
```



# DAY25

## [剑指 Offer 51. 数组中的逆序对](https://leetcode.cn/problems/shu-zu-zhong-de-ni-xu-dui-lcof/)

在数组中的两个数字，如果前面一个数字大于后面的数字，则这两个数字组成一个逆序对。输入一个数组，求出这个数组中的逆序对的总数。

 

**示例 1:**

```
输入: [7,5,6,4]
输出: 5
```

解法一：归并

时间复杂度：O(nlogn)

空间复杂度：O(n)

```cpp
class Solution {
    vector<int> tmp;
public:
    int reversePairs(vector<int>& nums) {
        tmp.resize(nums.size());        
        return merge_sort(nums, 0, nums.size() - 1);
    }
    int merge_sort(vector<int>& nums, int left, int right)
    {
        if(left >= right) return 0;
        int ret = 0;
        int mid = (left + right) >> 1;
        ret += merge_sort(nums, left, mid);
        ret += merge_sort(nums, mid + 1, right);
        int cur1 = left, cur2 = mid + 1, i = 0;
        while(cur1 <= mid && cur2 <= right)
        {
            if(nums[cur1] <= nums[cur2])
                tmp[i++] = nums[cur1++];
            else
            {
                ret += mid - cur1 + 1;
                tmp[i++] = nums[cur2++];
            }
        }
        while(cur1 <= mid) tmp[i++] = nums[cur1++];
        while(cur2 <= right) tmp[i++] = nums[cur2++];
        for(int j = left; j <= right; j++)
            nums[j] = tmp[j -left];
        return ret;
    }
};
```

## [315. 计算右侧小于当前元素的个数](https://leetcode.cn/problems/count-of-smaller-numbers-after-self/)

给你一个整数数组 `nums` ，按要求返回一个新数组 `counts` 。数组 `counts` 有该性质： `counts[i]` 的值是 `nums[i]` 右侧小于 `nums[i]` 的元素的数量。

 

**示例 1：**

```
输入：nums = [5,2,6,1]
输出：[2,1,1,0] 
解释：
5 的右侧有 2 个更小的元素 (2 和 1)
2 的右侧仅有 1 个更小的元素 (1)
6 的右侧有 1 个更小的元素 (1)
1 的右侧有 0 个更小的元素
```

**示例 2：**

```
输入：nums = [-1]
输出：[0]
```

**示例 3：**

```
输入：nums = [-1,-1]
输出：[0,0]
```

解法一：归并

时间复杂度：O(nlogn)

空间复杂度：O(n)

```cpp
class Solution {
    vector<int> ret;
    vector<int> index;
    vector<int> numTmp;
    vector<int> indexTmp;
public:
    vector<int> countSmaller(vector<int>& nums) {       
        ret.resize(nums.size());
        index.resize(nums.size());
        numTmp.resize(nums.size());
        indexTmp.resize(nums.size());
        for(int i = 0; i < nums.size(); i++) index[i] = i;
        merge_sort(nums, 0, nums.size() - 1);
        return ret;
    }
    void merge_sort(vector<int>& nums, int left, int right)
    {
        if(right <= left) return;
        int mid = (right + left) >> 1;
        merge_sort(nums, left, mid);
        merge_sort(nums, mid + 1, right);
        int cur1 = left, cur2 = mid + 1, i = 0;

        while(cur1 <= mid && cur2 <= right)
        {
            if(nums[cur1] <= nums[cur2])
            {
                numTmp[i] = nums[cur2];
                indexTmp[i++] = index[cur2++];
            }
            else 
            {
                ret[index[cur1]] += right - cur2 + 1;
                numTmp[i] = nums[cur1];
                indexTmp[i++] = index[cur1++];
            }
        }
        while(cur1 <= mid)
        {
            numTmp[i] = nums[cur1];
            indexTmp[i++] = index[cur1++];
        }
        while(cur2 <= right)
        {
            numTmp[i] = nums[cur2];
            indexTmp[i++] = index[cur2++];
        }

        for(int j = left; j <= right; j++)
        {
            nums[j] = numTmp[j - left];
            index[j] = indexTmp[j - left];
        }       
    }
};
```

# DAY26

## [493. 翻转对](https://leetcode.cn/problems/reverse-pairs/)

给定一个数组 `nums` ，如果 `i < j` 且 `nums[i] > 2*nums[j]` 我们就将 `(i, j)` 称作一个***重要翻转对\***。

你需要返回给定数组中的重要翻转对的数量。

**示例 1:**

```
输入: [1,3,2,3,1]
输出: 2
```

**示例 2:**

```
输入: [2,4,3,5,1]
输出: 3
```

**注意:**

1. 给定数组的长度不会超过`50000`。
2. 输入数组中的所有数字都在32位整数的表示范围内。

解法一：归并

时间复杂度：O(nlogn)

空间复杂度：O(n)

```cpp
class Solution {
    vector<int> tmp;
public:
    int reversePairs(vector<int>& nums) {
        tmp.resize(nums.size());
        return merge_sort(nums, 0, nums.size() - 1);
    }

    int merge_sort(vector<int>& nums, int left, int right)
    {
        if(right <= left) return 0;
        int mid = (right - left) / 2 + left;
        int ret = 0;
        ret += merge_sort(nums, left, mid);
        ret += merge_sort(nums, mid + 1, right);
        int cur1 = left, cur2 = mid + 1;
        while(cur1 <= mid && cur2 <= right)
        {
            if(nums[cur1] / 2.0 <= nums[cur2]) cur2++;
            else
            {
                ret += right - cur2 + 1;
                cur1++;
            }
        }
        int i = 0;
        cur1 = left, cur2 = mid + 1;
        while(cur1 <= mid && cur2 <= right)
        {
            if(nums[cur1] <= nums[cur2]) tmp[i++] = nums[cur2++];
            else tmp[i++] = nums[cur1++];
        }
        while(cur1 <= mid) tmp[i++] = nums[cur1++];
        while(cur2 <= right) tmp[i++] = nums[cur2++];
        for(int j = left; j <= right; j++)
        {
            nums[j] = tmp[j - left];
        }
        return ret;
    }
};
```

## [2. 两数相加](https://leetcode.cn/problems/add-two-numbers/)

给你两个 **非空** 的链表，表示两个非负的整数。它们每位数字都是按照 **逆序** 的方式存储的，并且每个节点只能存储 **一位** 数字。

请你将两个数相加，并以相同形式返回一个表示和的链表。

你可以假设除了数字 0 之外，这两个数都不会以 0 开头。

 

**示例 1：**

![img](https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2021/01/02/addtwonumber1.jpg)

```
输入：l1 = [2,4,3], l2 = [5,6,4]
输出：[7,0,8]
解释：342 + 465 = 807.
```

**示例 2：**

```
输入：l1 = [0], l2 = [0]
输出：[0]
```

**示例 3：**

```
输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
输出：[8,9,9,9,0,0,0,1]
```

解法一：链表

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        //定义头尾节点
        ListNode* head = nullptr, *end = nullptr;
        int carry = 0;
        while(l1 || l2)
        {
            int sum = 0;
            //定义新节点
            ListNode* node = new ListNode;
            node->next = nullptr;
            //计算 + 进位
            if(l1)
            {
                sum += l1->val;
                l1 = l1->next;
            }
            if(l2) 
            {
                sum += l2->val;
                l2 = l2->next;
            }
            sum += carry;
            //存入节点 + 存进位
            node->val = sum % 10;
            carry = sum / 10;
            //链接
            if(head == nullptr) 
            {
                head = node;
                end = node;
            }
            else
            {
                end->next = node;
                end = node; 
            }
        }
        if(carry)
        {
            ListNode* node = new ListNode;
            node->val = carry;
            node->next = nullptr;
            end->next = node;
        }
        return head;
    }
};
```

# DAY27

## [24. 两两交换链表中的节点](https://leetcode.cn/problems/swap-nodes-in-pairs/)

给你一个链表，两两交换其中相邻的节点，并返回交换后链表的头节点。你必须在不修改节点内部的值的情况下完成本题（即，只能进行节点交换）。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/10/03/swap_ex1.jpg)

```
输入：head = [1,2,3,4]
输出：[2,1,4,3]
```

**示例 2：**

```
输入：head = []
输出：[]
```

**示例 3：**

```
输入：head = [1]
输出：[1]
```

 

**提示：**

- 链表中节点的数目在范围 `[0, 100]` 内
- `0 <= Node.val <= 100`

解法一：链表

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        //当为空链表或者只有一个节点时直接返回
        if(head == nullptr || head->next == nullptr) return head;
        //定义一个虚拟头节点
        ListNode* tmpHead = new ListNode(0);
        tmpHead->next = head;       
        ListNode* cur1 = head, *cur2 = head->next, *tmp = tmpHead;
        while(cur1 && cur2)
        {
            //交换
            tmp->next = cur2;
            cur1->next = cur2->next;
            cur2->next = cur1;
            //移动指针
            tmp = cur1;
            cur1 = cur1->next;
            cur2 = cur1 == nullptr ? cur1 : cur1->next;
        }
        return tmpHead->next;
    }
};
```

## [LCR 026. 重排链表](https://leetcode.cn/problems/LGjMqU/)

给定一个单链表 `L` 的头节点 `head` ，单链表 `L` 表示为：

` L0 → L1 → … → Ln-1 → Ln `
请将其重新排列后变为：

```
L0 → Ln → L1 → Ln-1 → L2 → Ln-2 → …
```

不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。

 

**示例 1:**

![img](https://pic.leetcode-cn.com/1626420311-PkUiGI-image.png)

```
输入: head = [1,2,3,4]
输出: [1,4,2,3]
```

**示例 2:**

![img](https://pic.leetcode-cn.com/1626420320-YUiulT-image.png)

```
输入: head = [1,2,3,4,5]
输出: [1,5,2,4,3]
```

 

**提示：**

- 链表的长度范围为 `[1, 5 * 104]`
- `1 <= node.val <= 1000`

解法一：模拟

时间复杂度：O(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    void reorderList(ListNode* head) {
        if(head == nullptr && head->next == nullptr && head->next->next) return;
        //找中间节点
        ListNode* fast = head, *slow = head;
        while(fast && fast->next)
        {
            slow = slow->next;
            fast = fast->next->next;
        }
        //逆序后一半
        ListNode* cur = slow->next;
        ListNode* newhead = new ListNode(0);
        newhead->next = nullptr;
        slow->next = nullptr; 
        while(cur)
        {
            ListNode* tmp = cur->next;
            cur->next = newhead->next;
            newhead->next = cur;
            cur = tmp;    
        }
        //合并
        ListNode* cur1 = head, *cur2 = newhead->next;
        while(cur1 && cur2)
        {
            ListNode* tmp1 = cur1->next;
            ListNode* tmp2 = cur2->next;
            cur1->next = cur2;
            cur2->next = tmp1;
            cur1 = tmp1;
            cur2 = tmp2;
        }
        return;
    }
};
```

# DAY28

## [LCR 078. 合并 K 个升序链表](https://leetcode.cn/problems/vvXgSW/)

给定一个链表数组，每个链表都已经按升序排列。

请将所有链表合并到一个升序链表中，返回合并后的链表。

 

**示例 1：**

```
输入：lists = [[1,4,5],[1,3,4],[2,6]]
输出：[1,1,2,3,4,4,5,6]
解释：链表数组如下：
[
  1->4->5,
  1->3->4,
  2->6
]
将它们合并到一个有序链表中得到。
1->1->2->3->4->4->5->6
```

**示例 2：**

```
输入：lists = []
输出：[]
```

**示例 3：**

```
输入：lists = [[]]
输出：[]
```

 

**提示：**

- `k == lists.length`
- `0 <= k <= 10^4`
- `0 <= lists[i].length <= 500`
- `-10^4 <= lists[i][j] <= 10^4`
- `lists[i]` 按 **升序** 排列
- `lists[i].length` 的总和不超过 `10^4`

解法一：优先级队列

时间复杂度：O(n * k * logk)

空间复杂度：O(k)

```cpp
class Solution {
    struct cmp
    {
        bool operator()(ListNode* p, ListNode* q)
        {
            return p->val > q->val;
        }       
    };
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        //定义一个小根堆
        priority_queue<ListNode*, vector<ListNode*>, cmp> min_heap;
        //定义一个虚拟头节点
        ListNode* head = new ListNode(0), *prev = head;
        head->next = nullptr;
        //将k个链表的第一个数进堆
        for(ListNode* cur : lists)
            if(cur) min_heap.push(cur);
        //将堆中最小节点取出来加入到链表中
        while(!min_heap.empty())
        {
            prev->next = min_heap.top();
            prev = prev->next;
            min_heap.pop();
            if(prev->next) min_heap.push(prev->next);
        }
        prev = head->next;
        delete head;
        return prev;
    }
};
```

解法二：分治，递归

时间复杂度：O(n * k *logk)

空间复杂度：O(logk)

```cpp
lass Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        return merge(lists, 0, lists.size() - 1);
    }
    ListNode* merge(vector<ListNode*>& lists, int left, int right)
    {
        if(left > right) return nullptr;
        if(left == right) return lists[left];

        int mid = ((right - left) >> 1) + left;
        ListNode* l1 = merge(lists, left, mid);
        ListNode* l2 = merge(lists, mid + 1, right);

        return combine(l1, l2);
    }
    ListNode* combine(ListNode* l1, ListNode* l2)
    {
        ListNode head, *prev = &head;
        while(l1 && l2)
        {
            if(l1->val > l2->val)
            {
                prev->next = l2;
                prev = l2;
                l2 = l2->next;
            }
            else
            {
                prev->next = l1;
                prev = l1;
                l1 = l1->next;
            }
        }
        if(l1) prev->next = l1;
        if(l2) prev->next = l2;
        return head.next;
    }
};
```

## [25. K 个一组翻转链表](https://leetcode.cn/problems/reverse-nodes-in-k-group/)

给你链表的头节点 `head` ，每 `k` 个节点一组进行翻转，请你返回修改后的链表。

`k` 是一个正整数，它的值小于或等于链表的长度。如果节点总数不是 `k` 的整数倍，那么请将最后剩余的节点保持原有顺序。

你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/10/03/reverse_ex1.jpg)

```
输入：head = [1,2,3,4,5], k = 2
输出：[2,1,4,3,5]
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2020/10/03/reverse_ex2.jpg)

```
输入：head = [1,2,3,4,5], k = 3
输出：[3,2,1,4,5]
```

 

**提示：**

- 链表中的节点数目为 `n`
- `1 <= k <= n <= 5000`
- `0 <= Node.val <= 1000`

解法一：模拟

时间复杂度：o(n)

空间复杂度：O(1)

```cpp
class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
        //定义一个临时链表
        ListNode* tmp = new ListNode(0);
        ListNode* ret = new ListNode(0), *cur_end = ret;
        while(1)
        {
            ListNode* prev = head;
            int flag = 1;
            //找到需要逆序的节点结合
            while(prev && flag % (k + 1))
            {
                prev = prev->next;
                flag++;
            }
            //判断当剩余节点数 < k时不逆序
            if(flag <= k)
            {
                cur_end->next = head;
                break;
            }
            else
            {
                ListNode* tmp_end = head;
                //开始逆序
                while(head != prev)
                {
                    ListNode* cur = head->next;                   
                    head->next = tmp->next;
                    tmp->next = head;
                    head = cur;
                }
                //合并到返回链表
                cur_end->next = tmp->next;
                cur_end = tmp_end;
                tmp->next = nullptr;
            }           
        }
        return ret->next;
    }
};
```

# DAY29

## [1. 两数之和](https://leetcode.cn/problems/two-sum/)

给定一个整数数组 `nums` 和一个整数目标值 `target`，请你在该数组中找出 **和为目标值** *`target`* 的那 **两个** 整数，并返回它们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

你可以按任意顺序返回答案。

 

**示例 1：**

```
输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
```

**示例 2：**

```
输入：nums = [3,2,4], target = 6
输出：[1,2]
```

**示例 3：**

```
输入：nums = [3,3], target = 6
输出：[0,1]
```

 

**提示：**

- `2 <= nums.length <= 104`
- `-109 <= nums[i] <= 109`
- `-109 <= target <= 109`
- **只会存在一个有效答案**

解法一：哈希

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int, int> hash;
        for(int i = 0; i < nums.size(); i++)
        {
            int tmp = target - nums[i];
            if(hash.count(tmp)) return {hash[tmp], i};
            hash[nums[i]] = i;
        }
        return {0, 0};
    }
};
```

## [面试题 01.02. 判定是否互为字符重排](https://leetcode.cn/problems/check-permutation-lcci/)

给定两个由小写字母组成的字符串 `s1` 和 `s2`，请编写一个程序，确定其中一个字符串的字符重新排列后，能否变成另一个字符串。

**示例 1：**

```
输入: s1 = "abc", s2 = "bca"
输出: true 
```

**示例 2：**

```
输入: s1 = "abc", s2 = "bad"
输出: false
```

**说明：**

- `0 <= len(s1) <= 100 `
- `0 <= len(s2) <= 100 `

解法一：哈希表

时间复杂度：O(n)

空间复杂度：O(k)

```cpp
class Solution {
public:
    bool CheckPermutation(string s1, string s2) {
        if(s1.size() != s2.size()) return false;
        char hash[27] = {0};
        //将第一个字符串全部放入哈希表
        for(char ch : s1) 
            hash[ch - 'a']++;
        //将第二个字符串在哈希表中删除
        for(char ch : s2)
            hash[ch - 'a']--;
        //查看哈希表中是否还有数据
        for(int i = 0; i < 27; i++)
            if(hash[i]) return false;
        return true;
    }
};
```

# DAY30

## [217. 存在重复元素]([217. 存在重复元素 - 力扣（LeetCode）](https://leetcode.cn/problems/contains-duplicate/description/))

给你一个整数数组 `nums` 。如果任一值在数组中出现 **至少两次** ，返回 `true` ；如果数组中每个元素互不相同，返回 `false` 。

 

**示例 1：**

```
输入：nums = [1,2,3,1]
输出：true
```

**示例 2：**

```
输入：nums = [1,2,3,4]
输出：false
```

**示例 3：**

```
输入：nums = [1,1,1,3,3,4,3,2,4,2]
输出：true
```

 

**提示：**

- `1 <= nums.length <= 105`
- `-109 <= nums[i] <= 109`

解法一：哈希表

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        //创建一个哈希表
        unordered_map<int, int> hash;
        //遍历数据放进哈希中
        for(int& e : nums)
        {
            if(hash.count(e)) return true;
            hash[e]++;
        }
        return false;
    }
};
```

## [219. 存在重复元素 II - 力扣（LeetCode）](https://leetcode.cn/problems/contains-duplicate-ii/description/)

给你一个整数数组 `nums` 和一个整数 `k` ，判断数组中是否存在两个 **不同的索引** `i` 和 `j` ，满足 `nums[i] == nums[j]` 且 `abs(i - j) <= k` 。如果存在，返回 `true` ；否则，返回 `false` 。

 

**示例 1：**

```
输入：nums = [1,2,3,1], k = 3
输出：true
```

**示例 2：**

```
输入：nums = [1,0,1,1], k = 1
输出：true
```

**示例 3：**

```
输入：nums = [1,2,3,1,2,3], k = 2
输出：false
```

 

 

**提示：**

- `1 <= nums.length <= 105`
- `-109 <= nums[i] <= 109`
- `0 <= k <= 105`

解法一：哈希表

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
        //1.创建哈希表
        int sz = nums.size();
        unordered_map<int, int> hash;
        hash.reserve(sz);
        //2.遍历nums，查看nums[i]在哈希表中是否存在
        for(int i = 0; i < sz; i++)
        {
            //存在且当前下标和哈希表中的下标差值 <= k，返回true
            if(hash.count(nums[i]) && (i - hash[nums[i]]) <= k)
                return true;            
            //else 更新哈希表中i值
            hash[nums[i]] = i;
        }
        //返回false
        return false;
    }
};
```

# DAY31

## [LCR 033. 字母异位词分组 - 力扣（LeetCode）](https://leetcode.cn/problems/sfvd7V/description/)

给定一个字符串数组 `strs` ，将 **变位词** 组合在一起。 可以按任意顺序返回结果列表。

**注意：**若两个字符串中每个字符出现的次数都相同，则称它们互为变位词。

 

**示例 1:**

```
输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
输出: [["bat"],["nat","tan"],["ate","eat","tea"]]
```

**示例 2:**

```
输入: strs = [""]
输出: [[""]]
```

**示例 3:**

```
输入: strs = ["a"]
输出: [["a"]]
```

 

**提示：**

- `1 <= strs.length <= 104`
- `0 <= strs[i].length <= 100`
- `strs[i]` 仅包含小写字母

解法一：哈希 + 快排

时间复杂度：O(n * klog(k))

空间复杂度：O(n * k)

```cpp
class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> hash;

        for(auto& e : strs)
        {
            string tmp = e;
            sort(tmp.begin(), tmp.end());
            hash[tmp].push_back(e);
        }

        vector<vector<string>> ret;
        for(auto& [x, y] : hash)
        {
            ret.push_back(y);
        }
        return ret;
    }
};
```

## [14. 最长公共前缀 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-common-prefix/description/)

编写一个函数来查找字符串数组中的最长公共前缀。

如果不存在公共前缀，返回空字符串 `""`。

 

**示例 1：**

```
输入：strs = ["flower","flow","flight"]
输出："fl"
```

**示例 2：**

```
输入：strs = ["dog","racecar","car"]
输出：""
解释：输入不存在公共前缀。
```

 

**提示：**

- `1 <= strs.length <= 200`
- `0 <= strs[i].length <= 200`
- `strs[i]` 仅由小写英文字母组成

解法一：模拟

时间复杂度：O(n * k)

空间复杂度：O(1)

```cpp
class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        string ret;
        for(int i = 0; i < strs[0].size(); i++)
        {
            char ch = strs[0][i];
            int j = 1;
            for(; j < strs.size(); j++)
            {
                if(ch != strs[j][i]) break;
            }
            if(j == strs.size()) ret += ch;
            else break;
        }
        return ret;
    }
};
```

# DAY32

## [5. 最长回文子串 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-palindromic-substring/description/)

给你一个字符串 `s`，找到 `s` 中最长的回文子串。

如果字符串的反序与原始字符串相同，则该字符串称为回文字符串。

 

**示例 1：**

```
输入：s = "babad"
输出："bab"
解释："aba" 同样是符合题意的答案。
```

**示例 2：**

```
输入：s = "cbbd"
输出："bb"
```

 

**提示：**

- `1 <= s.length <= 1000`
- `s` 仅由数字和英文字母组成

解法一：中心扩展算法

时间复杂度：O(n ^ 2)

空间复杂度：O(1)

```cpp
class Solution {
public:
    string longestPalindrome(string s) {
        string ret;
        int len = 0;
        for(int i = 0; i < s.size(); i++)
        {
            int left = i, right = i;
            while(left >= 0 && right < s.size() && s[left] == s[right])
            {
                left--;
                right++;
            }
            if(right - left - 1 > len)
            {
                len = right - left - 1;
                ret = s.substr(left + 1, len);
            }

            left = i, right = i + 1;
            while(left >= 0 && right < s.size() && s[left] == s[right])
            {
                left--;
                right++;
            }
            if(right - left - 1 > len)
            {
                len = right - left - 1;
                ret = s.substr(left + 1, len);
            }
        }
        return ret;
    }
};
```

## [LCR 002. 二进制求和 - 力扣（LeetCode）](https://leetcode.cn/problems/JFETK5/description/)

给定两个 01 字符串 `a` 和 `b` ，请计算它们的和，并以二进制字符串的形式输出。

输入为 **非空** 字符串且只包含数字 `1` 和 `0`。

 

**示例 1:**

```
输入: a = "11", b = "10"
输出: "101"
```

**示例 2:**

```
输入: a = "1010", b = "1011"
输出: "10101"
```

 

**提示：**

- 每个字符串仅由字符 `'0'` 或 `'1'` 组成。
- `1 <= a.length, b.length <= 10^4`
- 字符串如果不是 `"0"` ，就都不含前导零。

解法一：模拟

时间复杂度：O(k)（最长字符串长度）

空间复杂度：O(1)

```cpp
class Solution {
public:
    string addBinary(string a, string b) {
        string ret;
        int sz_a = a.size() - 1, sz_b = b.size() - 1;
        int carry = 0;
        while(sz_a >= 0 || sz_b >= 0 || carry)
        {
            if(sz_a >= 0) carry += a[sz_a--] - '0';
            if(sz_b >= 0) carry += b[sz_b--] - '0';
            ret += carry % 2 + '0';
            carry /= 2;
        }
        reverse(ret.begin(), ret.end());
        return ret;
    }
};
```

# DAY33

## [43. 字符串相乘 - 力扣（LeetCode）](https://leetcode.cn/problems/multiply-strings/description/)

给定两个以字符串形式表示的非负整数 `num1` 和 `num2`，返回 `num1` 和 `num2` 的乘积，它们的乘积也表示为字符串形式。

**注意：**不能使用任何内置的 BigInteger 库或直接将输入转换为整数。

 

**示例 1:**

```
输入: num1 = "2", num2 = "3"
输出: "6"
```

**示例 2:**

```
输入: num1 = "123", num2 = "456"
输出: "56088"
```

 

**提示：**

- `1 <= num1.length, num2.length <= 200`
- `num1` 和 `num2` 只能由数字组成。
- `num1` 和 `num2` 都不包含任何前导零，除了数字0本身。

解法一：无进位相乘相加

时间复杂度：O(m * n)

空间复杂度：O(m + n)

```cpp
class Solution {
public:
    string multiply(string num1, string num2) {
        if(num1[0] == '0' || num2[0] == '0') return "0";
        //逆序num1 num2
        reverse(num1.begin(), num1.end());
        reverse(num2.begin(), num2.end());
        //遍历num1元素 去乘以num2中的元素
        vector<int> tmp(num1.size() + num2.size() - 1, 0);
        for(int i = 0; i < num1.size(); i++)
        {
            int int_num1 = num1[i] - '0';
            for(int j = 0; j < num2.size(); j++)
            {
                tmp[i + j] += int_num1 * (num2[j] - '0');
            }
        }
        string ret;
        int carry = 0;
        for(int e : tmp)
        {
            int t = (e + carry) % 10;
            ret.push_back(t + '0');
            carry = (e + carry) / 10;
        }
        if(carry) ret.push_back(carry + '0');
        reverse(ret.begin(), ret.end());
        return ret;
    }
};
```

## [1047. 删除字符串中的所有相邻重复项 - 力扣（LeetCode）](https://leetcode.cn/problems/remove-all-adjacent-duplicates-in-string/description/)

给出由小写字母组成的字符串 `S`，**重复项删除操作**会选择两个相邻且相同的字母，并删除它们。

在 S 上反复执行重复项删除操作，直到无法继续删除。

在完成所有重复项删除操作后返回最终的字符串。答案保证唯一。

 

**示例：**

```
输入："abbaca"
输出："ca"
解释：
例如，在 "abbaca" 中，我们可以删除 "bb" 由于两字母相邻且相同，这是此时唯一可以执行删除操作的重复项。之后我们得到字符串 "aaca"，其中又只有 "aa" 可以执行重复项删除操作，所以最后的字符串为 "ca"。
```

 

**提示：**

1. `1 <= S.length <= 20000`
2. `S` 仅由小写英文字母组成。

解法一：栈

时间复杂度：O(n)

空间复杂度：O(s)

```cpp
class Solution {
public:
    string removeDuplicates(string s) {
        string ret;
        for(auto ch : s)
        {
            int end = ret.size() - 1;
            if(ret.size() == 0) ret.push_back(ch);
            else if(ret[end] == ch) ret.pop_back();
            else ret.push_back(ch);
        }
        return ret;
    }
};
```

# DAY34

## [844. 比较含退格的字符串 - 力扣（LeetCode）](https://leetcode.cn/problems/backspace-string-compare/description/)

给定 `s` 和 `t` 两个字符串，当它们分别被输入到空白的文本编辑器后，如果两者相等，返回 `true` 。`#` 代表退格字符。

**注意：**如果对空文本输入退格字符，文本继续为空。

 

**示例 1：**

```
输入：s = "ab#c", t = "ad#c"
输出：true
解释：s 和 t 都会变成 "ac"。
```

**示例 2：**

```
输入：s = "ab##", t = "c#d#"
输出：true
解释：s 和 t 都会变成 ""。
```

**示例 3：**

```
输入：s = "a#c", t = "b"
输出：false
解释：s 会变成 "c"，但 t 仍然是 "b"。
```

 

**提示：**

- `1 <= s.length, t.length <= 200`
- `s` 和 `t` 只含有小写字母以及字符 `'#'`

解法一：栈

时间复杂度：O(n)

空间复杂度：O(n + m)

```cpp
class Solution {
public:
    bool backspaceCompare(string s, string t) {
        vector<char> st1, st2;
        for(auto ch : s)
        {
            if(ch == '#' && !st1.empty())
                st1.pop_back();
            if(ch != '#') 
                st1.push_back(ch);
        }
        for(auto ch : t)
        {
            if(ch == '#' && !st2.empty())
                st2.pop_back();
            if(ch != '#') 
                st2.push_back(ch);
        }
        if(st1.size() != st2.size()) return false;
        for(int i = 0; i < st1.size(); i++)
        {
            if(st1[i] != st2[i]) return false;
        }
        return true;
    }
};
```



## [227. 基本计算器 II - 力扣（LeetCode）](https://leetcode.cn/problems/basic-calculator-ii/description/)

给你一个字符串表达式 `s` ，请你实现一个基本计算器来计算并返回它的值。

整数除法仅保留整数部分。

你可以假设给定的表达式总是有效的。所有中间结果将在 `[-231, 231 - 1]` 的范围内。

**注意：**不允许使用任何将字符串作为数学表达式计算的内置函数，比如 `eval()` 。

 

**示例 1：**

```
输入：s = "3+2*2"
输出：7
```

**示例 2：**

```
输入：s = " 3/2 "
输出：1
```

**示例 3：**

```
输入：s = " 3+5 / 2 "
输出：5
```

 

**提示：**

- `1 <= s.length <= 3 * 105`
- `s` 由整数和算符 `('+', '-', '*', '/')` 组成，中间由一些空格隔开
- `s` 表示一个 **有效表达式**
- 表达式中的所有整数都是非负整数，且在范围 `[0, 231 - 1]` 内
- 题目数据保证答案是一个 **32-bit 整数**

解法一：栈

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    int calculate(string s) {
        vector<int> st;
        char op = '+';  
        for(int i = 0; i < s.size(); )
        {
            if(s[i] == ' ') i++;           
            else if(s[i] >= '0' && s[i] <= '9')
            {
                int tmp = 0;
                while(i < s.size() && s[i] >= '0' && s[i] <= '9')
                    tmp = tmp * 10 + (s[i++] - '0'); 
                if(op == '+') st.push_back(tmp);
                else if(op == '-') st.push_back(-1 * tmp);   
                else if(op == '*') st.back() *= tmp;
                else st.back() /= tmp;        
            }
            else 
                op = s[i++];      
        }
        int sum = 0;
        for(auto e : st)
            sum += e;
        return sum;
    }
};
```

# DAY35

## [394. 字符串解码 - 力扣（LeetCode）](https://leetcode.cn/problems/decode-string/)

给定一个经过编码的字符串，返回它解码后的字符串。

编码规则为: `k[encoded_string]`，表示其中方括号内部的 `encoded_string` 正好重复 `k` 次。注意 `k` 保证为正整数。

你可以认为输入字符串总是有效的；输入字符串中没有额外的空格，且输入的方括号总是符合格式要求的。

此外，你可以认为原始数据不包含数字，所有的数字只表示重复的次数 `k` ，例如不会出现像 `3a` 或 `2[4]` 的输入。

 

**示例 1：**

```
输入：s = "3[a]2[bc]"
输出："aaabcbc"
```

**示例 2：**

```
输入：s = "3[a2[c]]"
输出："accaccacc"
```

**示例 3：**

```
输入：s = "2[abc]3[cd]ef"
输出："abcabccdcdcdef"
```

**示例 4：**

```
输入：s = "abc3[cd]xyz"
输出："abccdcdcdxyz"
```

 

**提示：**

- `1 <= s.length <= 30`
- `s` 由小写英文字母、数字和方括号 `'[]'` 组成
- `s` 保证是一个 **有效** 的输入。
- `s` 中所有整数的取值范围为 `[1, 300]` 

解法一：栈

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    string decodeString(string s) {
        //2个栈分别放数字,字符
        stack<string> chara; 
        stack<int> num;
        chara.push("");
        //遍历字符串
        int n = s.size();
        for(int i = 0; i < n; )
        {      
            //遇到数字,提取出来直接入数字栈
            if(s[i] >= '0' && s[i] <= '9')
            {
                int tmp = 0;
                while(s[i] >= '0' && s[i] <= '9')
                    tmp = tmp * 10 + (s[i++] - '0');
                num.push(tmp);
            }
            //遇到'[',将后面的字符提取出来入字符串栈
            else if(s[i] == '[')
            {
                i++;    
                string ch = "";
                while(s[i] >= 'a' && s[i] <= 'z')
                    ch += s[i++];
                chara.push(ch);
            }
            //遇到']'解析,放入字符串栈顶后面
            else if(s[i] == ']')
            {
                string ch = chara.top();
                int t = num.top();
                chara.pop();
                num.pop();
                for(;t > 0; t--)
                    chara.top() += ch;
                i++;
            } 
            //遇到字符，提取出来放入栈顶元素后面
            else
            {
                string ch;
                while(i < n && s[i] >= 'a' && s[i] <= 'z')
                    ch += s[i++];
                chara.top() += ch;
            }
        }
        return chara.top();
    }
};
```

## [946. 验证栈序列 - 力扣（LeetCode）](https://leetcode.cn/problems/validate-stack-sequences/)

给定 `pushed` 和 `popped` 两个序列，每个序列中的 **值都不重复**，只有当它们可能是在最初空栈上进行的推入 push 和弹出 pop 操作序列的结果时，返回 `true`；否则，返回 `false` 。

 

**示例 1：**

```
输入：pushed = [1,2,3,4,5], popped = [4,5,3,2,1]
输出：true
解释：我们可以按以下顺序执行：
push(1), push(2), push(3), push(4), pop() -> 4,
push(5), pop() -> 5, pop() -> 3, pop() -> 2, pop() -> 1
```

**示例 2：**

```
输入：pushed = [1,2,3,4,5], popped = [4,3,5,1,2]
输出：false
解释：1 不能在 2 之前弹出。
```

 

**提示：**

- `1 <= pushed.length <= 1000`
- `0 <= pushed[i] <= 1000`
- `pushed` 的所有元素 **互不相同**
- `popped.length == pushed.length`
- `popped` 是 `pushed` 的一个排列

解法一：栈

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
        int sz_push = pushed.size();
        int sz_pop = popped.size();
        if(sz_pop != sz_push) return false;
        //定义一个栈序列
        stack<int> st;
        //遍历push序列
        for(int i = 0, j = 0; i < sz_push; i++)
        {
            st.push(pushed[i]);
            while(j < sz_pop && !st.empty() && st.top() == popped[j])
            {
                j++;
                st.pop();
            }
        }
        if(st.empty()) return true;
        return false;
    }
};
```

# DAY36

## [429. N 叉树的层序遍历 - 力扣（LeetCode）](https://leetcode.cn/problems/n-ary-tree-level-order-traversal/)

给定一个 N 叉树，返回其节点值的*层序遍历*。（即从左到右，逐层遍历）。

树的序列化输入是用层序遍历，每组子节点都由 null 值分隔（参见示例）。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png)

```
输入：root = [1,null,3,2,4,null,5,6]
输出：[[1],[3,2,4],[5,6]]
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png)

```
输入：root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
输出：[[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
```

 

**提示：**

- 树的高度不会超过 `1000`
- 树的节点总数在 `[0, 10^4]` 之间

解法一：队列

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int>> ret;
        queue<Node*> qu;
        if(root == nullptr) return ret;

        qu.push(root);
        while(qu.size())
        {
            int sz = qu.size();
            vector<int> tmp;
            for(int i = 0; i < sz; i++)
            {
                Node* cur = qu.front();
                qu.pop();
                tmp.push_back(cur->val);
                for(auto child : cur->children)
                {
                    qu.push(child);
                }
            }
            ret.push_back(tmp);
        }
        return ret;
    }
};
```

## [103. 二叉树的锯齿形层序遍历 - 力扣（LeetCode）](https://leetcode.cn/problems/binary-tree-zigzag-level-order-traversal/submissions/466988274/)

给你二叉树的根节点 `root` ，返回其节点值的 **锯齿形层序遍历** 。（即先从左往右，再从右往左进行下一层遍历，以此类推，层与层之间交替进行）。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/02/19/tree1.jpg)

```
输入：root = [3,9,20,null,null,15,7]
输出：[[3],[20,9],[15,7]]
```

**示例 2：**

```
输入：root = [1]
输出：[[1]]
```

**示例 3：**

```
输入：root = []
输出：[]
```

 

**提示：**

- 树中节点数目在范围 `[0, 2000]` 内
- `-100 <= Node.val <= 100`

解法一：队列

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> ret;
        if(root == nullptr) return ret;

        queue<TreeNode*> qu;
        qu.push(root);
        int flag = 1;
        while(qu.size())
        {
            vector<int> tmp;
            int sz = qu.size();
            for(int i = 0; i < sz; i++)
            {
                TreeNode* cur = qu.front();
                qu.pop();
                tmp.push_back(cur->val);
                if(cur->left) qu.push(cur->left);
                if(cur->right) qu.push(cur->right);
            }
            if(flag % 2 == 0) reverse(tmp.begin(), tmp.end()); 
            ret.push_back(tmp);
            flag++;
        }
        return ret;
    }
};
```

# DAY37

## [104. 二叉树的最大深度 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-depth-of-binary-tree/)

给定一个二叉树 `root` ，返回其最大深度。

二叉树的 **最大深度** 是指从根节点到最远叶子节点的最长路径上的节点数。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/11/26/tmp-tree.jpg)

 

```
输入：root = [3,9,20,null,null,15,7]
输出：3
```

**示例 2：**

```
输入：root = [1,null,2]
输出：2
```

 

**提示：**

- 树中节点的数量在 `[0, 104]` 区间内。
- `-100 <= Node.val <= 100`

解法一：递归

时间复杂度：O(n)

空间复杂度：O(height)

```cpp
class Solution {
public:
    int maxDepth(TreeNode* root) {
        if(root == nullptr)
            return 0;
        int leftDepth = maxDepth(root->left) + 1;
        int rightDepth = maxDepth(root->right) + 1;
        return max(leftDepth, rightDepth);
    }
};
```

解法二：队列

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    int maxDepth(TreeNode* root) {
        if(root == nullptr) return 0;
        queue<TreeNode*> qu;
        qu.push(root);
        int ret = 0;
        while(qu.size())
        {
            int sz = qu.size();
            for(int i = 0; i < sz; i++)
            {
                TreeNode* p = qu.front();
                qu.pop();
                if(p->left) qu.push(p->left);
                if(p->right) qu.push(p->right);
            }
            ret++;
        }
        return ret;
    }
};
```

## [LCR 044. 在每个树行中找最大值 - 力扣（LeetCode）](https://leetcode.cn/problems/hPov7L/)

给定一棵二叉树的根节点 `root` ，请找出该二叉树中每一层的最大值。

 

**示例1：**

```
输入: root = [1,3,2,5,3,null,9]
输出: [1,3,9]
解释:
          1
         / \
        3   2
       / \   \  
      5   3   9 
```

**示例2：**

```
输入: root = [1,2,3]
输出: [1,3]
解释:
          1
         / \
        2   3
```

**示例3：**

```
输入: root = [1]
输出: [1]
```

**示例4：**

```
输入: root = [1,null,2]
输出: [1,2]
解释:      
           1 
            \
             2     
```

**示例5：**

```
输入: root = []
输出: []
```

 

**提示：**

- 二叉树的节点个数的范围是 `[0,104]`
- `-231 <= Node.val <= 231 - 1`

解法一：队列

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    vector<int> largestValues(TreeNode* root) {
        if(root == nullptr) return {};
        queue<TreeNode*> qu;
        qu.push(root);
        vector<int> ret;
        while(qu.size())
        {
            int sz = qu.size(), _max = INT_MIN;
            for(int i = 0; i < sz; i++)
            {
                TreeNode* p = qu.front();
                qu.pop();
                _max = max(_max, p->val);
                if(p->left) qu.push(p->left);
                if(p->right) qu.push(p->right);
            }
            ret.push_back(_max);
        }
        return ret;
    }
};
```

# DAY38

## [662. 二叉树最大宽度 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-width-of-binary-tree/)

给你一棵二叉树的根节点 `root` ，返回树的 **最大宽度** 。

树的 **最大宽度** 是所有层中最大的 **宽度** 。

每一层的 **宽度** 被定义为该层最左和最右的非空节点（即，两个端点）之间的长度。将这个二叉树视作与满二叉树结构相同，两端点间会出现一些延伸到这一层的 `null` 节点，这些 `null` 节点也计入长度。

题目数据保证答案将会在 **32 位** 带符号整数范围内。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/05/03/width1-tree.jpg)

```
输入：root = [1,3,2,5,3,null,9]
输出：4
解释：最大宽度出现在树的第 3 层，宽度为 4 (5,3,null,9) 。
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2022/03/14/maximum-width-of-binary-tree-v3.jpg)

```
输入：root = [1,3,2,5,null,null,9,6,null,7]
输出：7
解释：最大宽度出现在树的第 4 层，宽度为 7 (6,null,null,null,null,null,7) 。
```

**示例 3：**

![img](https://assets.leetcode.com/uploads/2021/05/03/width3-tree.jpg)

```
输入：root = [1,3,2,5]
输出：2
解释：最大宽度出现在树的第 2 层，宽度为 2 (3,2) 。
```

 

**提示：**

- 树中节点的数目范围是 `[1, 3000]`
- `-100 <= Node.val <= 100`

解法一：队列

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root) {
        queue<pair<TreeNode*, unsigned int>> qu;
        qu.push(make_pair(root, 1));
        unsigned int ret = 0;
        while(qu.size())
        {
            unsigned int start = qu.front().second;
            unsigned int end = qu.back().second;
            unsigned int tmp = end - start + 1;
            ret = max(ret, tmp);
            unsigned int sz = qu.size();
            for(int i = 0; i < sz; i++)
            {
                TreeNode* n = qu.front().first;
                unsigned int t = qu.front().second;
                qu.pop();
                if(n->left) qu.push(make_pair(n->left, 2 * t));
                if(n->right) qu.push(make_pair(n->right, 2 * t + 1));
            }
        }
        return ret;
    }
};
```

## [515. 在每个树行中找最大值 - 力扣（LeetCode）](https://leetcode.cn/problems/find-largest-value-in-each-tree-row/)

给定一棵二叉树的根节点 `root` ，请找出该二叉树中每一层的最大值。

 

**示例1：**

![img](https://assets.leetcode.com/uploads/2020/08/21/largest_e1.jpg)

```
输入: root = [1,3,2,5,3,null,9]
输出: [1,3,9]
```

**示例2：**

```
输入: root = [1,2,3]
输出: [1,3]
```

 

**提示：**

- 二叉树的节点个数的范围是 `[0,104]`
- `-231 <= Node.val <= 231 - 1`

解法一：队列

时间复杂度：O(n)

空间复杂度：O(n)

```cp
class Solution {
public:
    vector<int> largestValues(TreeNode* root) {
        vector<int> ret;
        if(root == nullptr) return ret;
        queue<TreeNode*> qu;
        qu.push(root);
        while(qu.size())
        {
            int sz = qu.size();
            int tmp = INT_MIN;
            for(int i = 0; i < sz; i++)
            {
                TreeNode* q = qu.front();
                qu.pop();
                tmp = max(tmp, q->val);
                if(q->left) qu.push(q->left);
                if(q->right) qu.push(q->right);
            }
            ret.push_back(tmp);
        }
        return ret; 
    }
};
```

# DAY39

## [1046. 最后一块石头的重量 - 力扣（LeetCode）](https://leetcode.cn/problems/last-stone-weight/description/)

有一堆石头，每块石头的重量都是正整数。

每一回合，从中选出两块 **最重的** 石头，然后将它们一起粉碎。假设石头的重量分别为 `x` 和 `y`，且 `x <= y`。那么粉碎的可能结果如下：

- 如果 `x == y`，那么两块石头都会被完全粉碎；
- 如果 `x != y`，那么重量为 `x` 的石头将会完全粉碎，而重量为 `y` 的石头新重量为 `y-x`。

最后，最多只会剩下一块石头。返回此石头的重量。如果没有石头剩下，就返回 `0`。

 

**示例：**

```
输入：[2,7,4,1,8,1]
输出：1
解释：
先选出 7 和 8，得到 1，所以数组转换为 [2,4,1,1,1]，
再选出 2 和 4，得到 2，所以数组转换为 [2,1,1,1]，
接着是 2 和 1，得到 1，所以数组转换为 [1,1,1]，
最后选出 1 和 1，得到 0，最终数组转换为 [1]，这就是最后剩下那块石头的重量。
```

 

**提示：**

- `1 <= stones.length <= 30`
- `1 <= stones[i] <= 1000`

解法一：模拟+优先级队列

时间复杂度：O(n * logn)

空间复杂度：O(n)

```cpp
class Solution {
public:
    int lastStoneWeight(vector<int>& stones) {
        priority_queue<int> pq;
        for(int i : stones) pq.push(i);

        while(pq.size() > 1)
        {
            int y = pq.top();
            pq.pop();
            int x = pq.top();
            pq.pop();
            if(y != x) pq.push(y - x);
        }
        if(pq.size()) return pq.top();
        else return 0;
    }
};
```

解法二：模拟

时间复杂度：O(n * logn)

空间复杂度：O(1)

```cpp
class Solution {
public:
    int lastStoneWeight(vector<int>& stones) {
        
        while(stones.size() > 1)
        {
            sort(stones.begin(), stones.end());
            int y = stones.back();
            stones.pop_back();
            int x = stones.back();
            stones.pop_back();
            if(x != y)
            {
                stones.push_back(y - x);
            }
        }
        if(stones.size()) return stones[0];
        else return 0;
    }
};
```

## [LCR 059. 数据流中的第 K 大元素 - 力扣（LeetCode）](https://leetcode.cn/problems/jBjn9C/)

设计一个找到数据流中第 `k` 大元素的类（class）。注意是排序后的第 `k` 大元素，不是第 `k` 个不同的元素。

请实现 `KthLargest` 类：

- `KthLargest(int k, int[] nums)` 使用整数 `k` 和整数流 `nums` 初始化对象。
- `int add(int val)` 将 `val` 插入数据流 `nums` 后，返回当前数据流中第 `k` 大的元素。

 

**示例：**

```
输入：
["KthLargest", "add", "add", "add", "add", "add"]
[[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
输出：
[null, 4, 5, 5, 8, 8]

解释：
KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
kthLargest.add(3);   // return 4
kthLargest.add(5);   // return 5
kthLargest.add(10);  // return 5
kthLargest.add(9);   // return 8
kthLargest.add(4);   // return 8
```

 

**提示：**

- `1 <= k <= 104`
- `0 <= nums.length <= 104`
- `-104 <= nums[i] <= 104`
- `-104 <= val <= 104`
- 最多调用 `add` 方法 `104` 次
- 题目数据保证，在查找第 `k` 大元素时，数组中至少有 `k` 个元素

解法一：优先级队列

时间复杂度：O(n)

空间复杂度：O(k)

```cpp
class KthLargest {
    priority_queue<int, vector<int>, greater<int>> _pq;
    int _k;
public:
    KthLargest(int k, vector<int>& nums) 
        :_k(k)
    {
        for(int i : nums)
        {
            _pq.push(i);
            if(_pq.size() > k) _pq.pop();
        }
    }
    
    int add(int val) {
        _pq.push(val);
        if(_pq.size() > _k) _pq.pop();
        return _pq.top();
    }
};
```

# DAY 40

## [692. 前K个高频单词 - 力扣（LeetCode）](https://leetcode.cn/problems/top-k-frequent-words/)

给定一个单词列表 `words` 和一个整数 `k` ，返回前 `k` 个出现次数最多的单词。

返回的答案应该按单词出现频率由高到低排序。如果不同的单词有相同出现频率， **按字典顺序** 排序。

 

**示例 1：**

```
输入: words = ["i", "love", "leetcode", "i", "love", "coding"], k = 2
输出: ["i", "love"]
解析: "i" 和 "love" 为出现次数最多的两个单词，均为2次。
    注意，按字母顺序 "i" 在 "love" 之前。
```

**示例 2：**

```
输入: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
输出: ["the", "is", "sunny", "day"]
解析: "the", "is", "sunny" 和 "day" 是出现次数最多的四个单词，
    出现次数依次为 4, 3, 2 和 1 次。
```

 

**注意：**

- `1 <= words.length <= 500`
- `1 <= words[i] <= 10`
- `words[i]` 由小写英文字母组成。
- `k` 的取值范围是 `[1, **不同** words[i] 的数量]`

解法一：哈希 + 优先级队列

时间复杂度：O(n*logk)

空间复杂度：O(k)

```cpp

class Solution {
    struct compare
    {
        bool operator()(pair<string, int> t1, pair<string, int> t2)
        {
            if(t1.second == t2.second) return t1.first < t2.first;
            return t1.second > t2.second;
        }
    };
public:
    vector<string> topKFrequent(vector<string>& words, int k) {
        unordered_map<string, int> word_count;
        for(string str : words)
            word_count[str]++;
        
        priority_queue<pair<string, int>, vector<pair<string, int>>, compare> pq;
        for(auto& e : word_count)
        {
            pq.push(e);
            if(pq.size() > k) pq.pop();
        }
        vector<string> ret(k);
        for(int i = k - 1; i >= 0; i--)
        {
            ret[i] = pq.top().first;
            pq.pop();
        }
        return ret;
    }
};
```

## [LCR 160. 数据流中的中位数 - 力扣（LeetCode）](https://leetcode.cn/problems/shu-ju-liu-zhong-de-zhong-wei-shu-lcof/)

**中位数** 是有序整数列表中的中间值。如果列表的大小是偶数，则没有中间值，中位数是两个中间值的平均值。

例如，
`[2,3,4]` 的中位数是 `3`
`[2,3]` 的中位数是 `(2 + 3) / 2 = 2.5`
设计一个支持以下两种操作的数据结构：

- `void addNum(int num)` - 从数据流中添加一个整数到数据结构中。
- `double findMedian()` - 返回目前所有元素的中位数。

**示例 1：**

```
输入：
["MedianFinder","addNum","addNum","findMedian","addNum","findMedian"]
[[],[1],[2],[],[3],[]]
输出：[null,null,null,1.50000,null,2.00000]
```

**示例 2：**

```
输入：
["MedianFinder","addNum","findMedian","addNum","findMedian"]
[[],[2],[],[3],[]]
输出：[null,null,2.00000,null,2.50000]
```

 

**提示：**

- 最多会对 `addNum、findMedian` 进行 `50000` 次调用。

解法一：优先级队列

时间复杂度：ADD O(logn)

空间复杂度：O(n)

```cpp
class MedianFinder {
public:
    /** initialize your data structure here. */
    MedianFinder() 
        :_size(0)
    {

    }
    
    void addNum(int num) 
    {
        _size++;
        if(min_heap.size() == max_heap.size())
        {
            max_heap.push(num);
            min_heap.push(max_heap.top());
            max_heap.pop();
        }
        else
        {
            min_heap.push(num);
            max_heap.push(min_heap.top());
            min_heap.pop();
        }
    }
    
    double findMedian() 
    {
        if(_size % 2 == 0) return (min_heap.top() + max_heap.top()) / 2.0;
        else return min_heap.top();
    }
private:
    priority_queue<int, vector<int>, less<int>> max_heap;
    priority_queue<int, vector<int>, greater<int>> min_heap;
    int _size;
};
```

# DAY41

## [面试题 08.06. 汉诺塔问题 - 力扣（LeetCode）](https://leetcode.cn/problems/hanota-lcci/)

在经典汉诺塔问题中，有 3 根柱子及 N 个不同大小的穿孔圆盘，盘子可以滑入任意一根柱子。一开始，所有盘子自上而下按升序依次套在第一根柱子上(即每一个盘子只能放在更大的盘子上面)。移动圆盘时受到以下限制:
(1) 每次只能移动一个盘子;
(2) 盘子只能从柱子顶端滑出移到下一根柱子;
(3) 盘子只能叠在比它大的盘子上。

请编写程序，用栈将所有盘子从第一根柱子移到最后一根柱子。

你需要原地修改栈。

**示例1:**

```
 输入：A = [2, 1, 0], B = [], C = []
 输出：C = [2, 1, 0]
```

**示例2:**

```
 输入：A = [1, 0], B = [], C = []
 输出：C = [1, 0]
```

**提示:**

1. A中盘子的数目不大于14个。

解法一：递归

时间复杂度：O(2 ^ n - 1)

空间复杂度：O(1)

```cpp
class Solution {
public:
    void hanota(vector<int>& A, vector<int>& B, vector<int>& C) {
        return move(A.size(), A, B, C);
    }

    void move(int sz, vector<int>& a, vector<int>& b, vector<int>& c)
    {
        if(sz == 1)
        {
            c.push_back(a.back());
            a.pop_back();
            return;
        }
        //通过空c将上面 n - 1个移动到b
        move(sz - 1, a, c, b);
        //将最下面的移动到c
        c.push_back(a.back());
        a.pop_back();
        //将n - 1个通过空a移动到c
        move(sz - 1, b, a, c);
    }
};
```

## [21. 合并两个有序链表 - 力扣（LeetCode）](https://leetcode.cn/problems/merge-two-sorted-lists/)

将两个升序链表合并为一个新的 **升序** 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/10/03/merge_ex1.jpg)

```
输入：l1 = [1,2,4], l2 = [1,3,4]
输出：[1,1,2,3,4,4]
```

**示例 2：**

```
输入：l1 = [], l2 = []
输出：[]
```

**示例 3：**

```
输入：l1 = [], l2 = [0]
输出：[0]
```

 

**提示：**

- 两个链表的节点数目范围是 `[0, 50]`
- `-100 <= Node.val <= 100`
- `l1` 和 `l2` 均按 **非递减顺序** 排列

时间复杂度：O(m + n)

空间复杂度：O(m + n)

```cpp
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
        //当其中一个链表为空时，返回另一个链表
        if(list1 == nullptr) return list2;
        if(list2 == nullptr) return list1;
        //都不为空时，比较两个val大小，返回小的节点
        if(list1->val > list2->val) 
        {
            list2->next = mergeTwoLists(list1, list2->next);
            return list2;
        }
        else
        {
            list1->next = mergeTwoLists(list1->next, list2);
            return list1;
        }
        
    }
};

```

# DAY42

## [24. 两两交换链表中的节点 - 力扣（LeetCode）](https://leetcode.cn/problems/swap-nodes-in-pairs/description/)

给你一个链表，两两交换其中相邻的节点，并返回交换后链表的头节点。你必须在不修改节点内部的值的情况下完成本题（即，只能进行节点交换）。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/10/03/swap_ex1.jpg)

```
输入：head = [1,2,3,4]
输出：[2,1,4,3]
```

**示例 2：**

```
输入：head = []
输出：[]
```

**示例 3：**

```
输入：head = [1]
输出：[1]
```

 

**提示：**

- 链表中节点的数目在范围 `[0, 100]` 内
- `0 <= Node.val <= 100`

解法一：递归

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
    ListNode* _swap(ListNode* first, ListNode* second)
    {
        if(first == nullptr) return nullptr;
        if(second == nullptr) return first;

        ListNode* tmp_first = second->next;
        ListNode* tmp_second = tmp_first == nullptr ? nullptr : tmp_first->next;
        
        first->next = _swap(tmp_first, tmp_second);
        second->next = first;
        return second;
    }
public:
    ListNode* swapPairs(ListNode* head) {
        if(head == nullptr) return nullptr;
        return _swap(head, head->next);
    }
};
```

## [LCR 024. 反转链表 - 力扣（LeetCode）](https://leetcode.cn/problems/UHnkqh/description/)

给定单链表的头节点 `head` ，请反转链表，并返回反转后的链表的头节点。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/02/19/rev1ex1.jpg)

```
输入：head = [1,2,3,4,5]
输出：[5,4,3,2,1]
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2021/02/19/rev1ex2.jpg)

```
输入：head = [1,2]
输出：[2,1]
```

**示例 3：**

```
输入：head = []
输出：[]
```

 

**提示：**

- 链表中节点的数目范围是 `[0, 5000]`
- `-5000 <= Node.val <= 5000`

解法一：递归

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
    void _reverse(ListNode* head, ListNode* prev, ListNode*& ret)
    {        
        if(head == nullptr)
        {
            ret = prev;
            return;
        }
        _reverse(head->next, head, ret);      
        head->next = prev;        
    }
public:
    ListNode* reverseList(ListNode* head) {
        if(head == nullptr) return nullptr;
        ListNode* ret;
        _reverse(head, nullptr, ret);
        return ret;
    }
};
```

# DAY43

##  [50. Pow(x, n) - 力扣（LeetCode）](https://leetcode.cn/problems/powx-n/)

实现 [pow(*x*, *n*)](https://www.cplusplus.com/reference/valarray/pow/) ，即计算 `x` 的整数 `n` 次幂函数（即，`xn` ）。

 

**示例 1：**

```
输入：x = 2.00000, n = 10
输出：1024.00000
```

**示例 2：**

```
输入：x = 2.10000, n = 3
输出：9.26100
```

**示例 3：**

```
输入：x = 2.00000, n = -2
输出：0.25000
解释：2-2 = 1/22 = 1/4 = 0.25
```

 

**提示：**

- `-100.0 < x < 100.0`
- `-231 <= n <= 231-1`
- `n` 是一个整数
- 要么 `x` 不为零，要么 `n > 0` 。
- `-104 <= xn <= 104`

时间复杂度：O(logn)

空间复杂度：O(logn)

```cpp
class Solution {
public:
    double myPow(double x, int n) {      
       return n < 0 ? 1.0 / pow(x, n) : pow(x, n);
    }
    double pow (double x, int n)
    {
        if(n == 0) return 1;
        double tmp = pow(x, n / 2);       
        return n % 2 == 0 ? tmp * tmp : tmp * tmp * x;
    }
};
```

## [2331. 计算布尔二叉树的值 - 力扣（LeetCode）](https://leetcode.cn/problems/evaluate-boolean-binary-tree/description/)

给你一棵 **完整二叉树** 的根，这棵树有以下特征：

- **叶子节点** 要么值为 `0` 要么值为 `1` ，其中 `0` 表示 `False` ，`1` 表示 `True` 。
- **非叶子节点** 要么值为 `2` 要么值为 `3` ，其中 `2` 表示逻辑或 `OR` ，`3` 表示逻辑与 `AND` 。

**计算** 一个节点的值方式如下：

- 如果节点是个叶子节点，那么节点的 **值** 为它本身，即 `True` 或者 `False` 。
- 否则，**计算** 两个孩子的节点值，然后将该节点的运算符对两个孩子值进行 **运算** 。

返回根节点 `root` 的布尔运算值。

**完整二叉树** 是每个节点有 `0` 个或者 `2` 个孩子的二叉树。

**叶子节点** 是没有孩子的节点。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2022/05/16/example1drawio1.png)

```
输入：root = [2,1,3,null,null,0,1]
输出：true
解释：上图展示了计算过程。
AND 与运算节点的值为 False AND True = False 。
OR 运算节点的值为 True OR False = True 。
根节点的值为 True ，所以我们返回 true 。
```

**示例 2：**

```
输入：root = [0]
输出：false
解释：根节点是叶子节点，且值为 false，所以我们返回 false 。
```

解法一：递归

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
public:
    bool evaluateTree(TreeNode* root) {
        if(root->val == 1) return true;
        if(root->val == 0) return false;

        if(root->val == 2)
            return evaluateTree(root->left) || evaluateTree(root->right);
        else 
            return evaluateTree(root->left) && evaluateTree(root->right);
    }
};
```

# DAY44

## [129. 求根节点到叶节点数字之和 - 力扣（LeetCode）](https://leetcode.cn/problems/sum-root-to-leaf-numbers/)

给你一个二叉树的根节点 `root` ，树中每个节点都存放有一个 `0` 到 `9` 之间的数字。

每条从根节点到叶节点的路径都代表一个数字：

- 例如，从根节点到叶节点的路径 `1 -> 2 -> 3` 表示数字 `123` 。

计算从根节点到叶节点生成的 **所有数字之和** 。

**叶节点** 是指没有子节点的节点。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/02/19/num1tree.jpg)

```
输入：root = [1,2,3]
输出：25
解释：
从根到叶子节点路径 1->2 代表数字 12
从根到叶子节点路径 1->3 代表数字 13
因此，数字总和 = 12 + 13 = 25
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2021/02/19/num2tree.jpg)

```
输入：root = [4,9,0,5,1]
输出：1026
解释：
从根到叶子节点路径 4->9->5 代表数字 495
从根到叶子节点路径 4->9->1 代表数字 491
从根到叶子节点路径 4->0 代表数字 40
因此，数字总和 = 495 + 491 + 40 = 1026
```

 

**提示：**

- 树中节点的数目在范围 `[1, 1000]` 内
- `0 <= Node.val <= 9`
- 树的深度不超过 `10`

解法一：递归

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
    int _sumnum(TreeNode* root, int ret)
    {
        if(root == nullptr) return 0;
        if(root->left == nullptr && root->right == nullptr)
            return ret * 10 + root->val;       
        return _sumnum(root->left, ret * 10 + root->val) + _sumnum(root->right, ret * 10 + root->val);
    }
public:
    int sumNumbers(TreeNode* root) {
        return _sumnum(root, 0);
    }
};
```



## [LCR 047. 二叉树剪枝 - 力扣（LeetCode）](https://leetcode.cn/problems/pOCWxh/)

给定一个二叉树 **根节点** `root` ，树的每个节点的值要么是 `0`，要么是 `1`。请剪除该二叉树中所有节点的值为 `0` 的子树。

节点 `node` 的子树为 `node` 本身，以及所有 `node` 的后代。

 

**示例 1:**

```
输入: [1,null,0,0,1]
输出: [1,null,0,null,1] 
解释: 
只有红色节点满足条件“所有不包含 1 的子树”。
右图为返回的答案。
```

**示例 2:**

```
输入: [1,0,1,0,0,0,1]
输出: [1,null,1,null,1]
解释: 
```

**示例 3:**

```
输入: [1,1,0,1,1,0,1,0]
输出: [1,1,0,1,1,null,1]
解释: 
```

 

**提示:**

- 二叉树的节点个数的范围是 `[1,200]`
- 二叉树节点的值只会是 `0` 或 `1`

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
    bool _pruneTree(TreeNode* root)
    {
        if(root == nullptr) return true;

        bool left = _pruneTree(root->left);
        bool right = _pruneTree(root->right);

        if(left) root->left = nullptr;
        if(right) root->right = nullptr;
        //if(root->val == 0) return true;
        return left && right && !root->val;
    }
public:
    TreeNode* pruneTree(TreeNode* root) {
        if(root == nullptr) return root;
        if(_pruneTree(root)) return nullptr;
        return root;
    }
};
```

# DAY45

## [98. 验证二叉搜索树 - 力扣（LeetCode）](https://leetcode.cn/problems/validate-binary-search-tree/)

给你一个二叉树的根节点 `root` ，判断其是否是一个有效的二叉搜索树。

**有效** 二叉搜索树定义如下：

- 节点的左子树只包含 **小于** 当前节点的数。
- 节点的右子树只包含 **大于** 当前节点的数。
- 所有左子树和右子树自身必须也是二叉搜索树。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/12/01/tree1.jpg)

```
输入：root = [2,1,3]
输出：true
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2020/12/01/tree2.jpg)

```
输入：root = [5,1,4,null,null,3,6]
输出：false
解释：根节点的值是 5 ，但是右子节点的值是 4 。
```

 

**提示：**

- 树中节点数目范围在`[1, 104]` 内
- `-231 <= Node.val <= 231 - 1`

解法一：递归

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
    long prev = LONG_MIN;
public:
    bool isValidBST(TreeNode* root) {
        if(root == nullptr) return true;
        bool l = isValidBST(root->left);
        if(root->val > prev) 
        {
            prev = root->val;
        }
        else return false;
        bool r = isValidBST(root->right);
        return l && r;
    }
};
```

## [230. 二叉搜索树中第K小的元素 - 力扣（LeetCode）](https://leetcode.cn/problems/kth-smallest-element-in-a-bst/)

给定一个二叉搜索树的根节点 `root` ，和一个整数 `k` ，请你设计一个算法查找其中第 `k` 个最小元素（从 1 开始计数）。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/01/28/kthtree1.jpg)

```
输入：root = [3,1,4,null,2], k = 1
输出：1
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2021/01/28/kthtree2.jpg)

```
输入：root = [5,3,6,2,4,null,null,1], k = 3
输出：3
```

 

 

**提示：**

- 树中的节点数为 `n` 。
- `1 <= k <= n <= 104`
- `0 <= Node.val <= 104`

解法一：递归

时间复杂度：O(n)

空间复杂度：O(n)

```cpp
class Solution {
    int ret, _k;
    void _kthSmallest(TreeNode* root)
    {
        if(root == nullptr) return;
        _kthSmallest(root->left);
        if(--_k == 0)
        {
            ret = root->val;
            return;
        }
        _kthSmallest(root->right);
    }
public:
    int kthSmallest(TreeNode* root, int k) {
        if(root == nullptr) return 0;
        _k = k;
        _kthSmallest(root);
        return ret;
    }
};
```



# DAY46

## [257. 二叉树的所有路径 - 力扣（LeetCode）](https://leetcode.cn/problems/binary-tree-paths/)



给你一个二叉树的根节点 `root` ，按 **任意顺序** ，返回所有从根节点到叶子节点的路径。

**叶子节点** 是指没有子节点的节点。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/03/12/paths-tree.jpg)

```
输入：root = [1,2,3,null,5]
输出：["1->2->5","1->3"]
```

**示例 2：**

```
输入：root = [1]
输出：["1"]
```

 

**提示：**

- 树中节点的数目在范围 `[1, 100]` 内
- `-100 <= Node.val <= 100`

解法一：递归+回溯

时间复杂度：O(n *n!)

空间复杂度：O(n)

```cpp
class Solution {
    void _binaryTreePaths(TreeNode* root, vector<string>& ret, string str)
    {
        str += to_string(root->val) + "->";
        if(root->left == nullptr && root->right == nullptr)
        {
            str.pop_back();
            str.pop_back();
            ret.push_back(str);
            return;
        }        
        if(root->left) _binaryTreePaths(root->left, ret, str);
        if(root->right) _binaryTreePaths(root->right, ret, str);
    }
public:
    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> ret;
        if(root == nullptr) return ret;
        _binaryTreePaths(root, ret, "");
        return ret;
    }
};
```

## [LCR 083. 全排列 - 力扣（LeetCode）](https://leetcode.cn/problems/VvJkup/)

给定一个不含重复数字的整数数组 `nums` ，返回其 **所有可能的全排列** 。可以 **按任意顺序** 返回答案。

 

**示例 1：**

```
输入：nums = [1,2,3]
输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
```

**示例 2：**

```
输入：nums = [0,1]
输出：[[0,1],[1,0]]
```

**示例 3：**

```
输入：nums = [1]
输出：[[1]]
```

 

**提示：**

- `1 <= nums.length <= 6`
- `-10 <= nums[i] <= 10`
- `nums` 中的所有整数 **互不相同**

解法一：递归+回溯

时间复杂度：O(n*n!)

空间复杂度：O(n)

```cpp
class Solution {
    vector<vector<int>> ret;
    vector<int> path;
    bool check[6];
    void _permute(vector<int>& nums)
    {
        if(path.size() == nums.size())
        {
            ret.push_back(path);
            return;
        }
        for(int i = 0; i < nums.size(); i++)
        {
            if(check[i] == false)
            {
                path.push_back(nums[i]);
                check[i] = true;
                _permute(nums);
                path.pop_back();
                check[i] = false;
            }           
        }
    }
public:
    vector<vector<int>> permute(vector<int>& nums) {
        _permute(nums);
        return ret;
    }
};
```

# DAY47

## [LCR 079. 子集 - 力扣（LeetCode）](https://leetcode.cn/problems/TVdhkn/)

给定一个整数数组 `nums` ，数组中的元素 **互不相同** 。返回该数组所有可能的子集（幂集）。

解集 **不能** 包含重复的子集。你可以按 **任意顺序** 返回解集。

 

**示例 1：**

```
输入：nums = [1,2,3]
输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
```

**示例 2：**

```
输入：nums = [0]
输出：[[],[0]]
```

 

**提示：**

- `1 <= nums.length <= 10`
- `-10 <= nums[i] <= 10`
- `nums` 中的所有元素 **互不相同**

解法一：递归+回溯

时间复杂度：O(n*n!)

空间复杂度：O(n)

```cpp
class Solution {
    vector<vector<int>> ret;
    vector<int> path;
    void _subsets(vector<int>& nums, int sz)
    {
       ret.push_back(path);
       for(int i = sz; i < nums.size(); i++)
       {
           path.push_back(nums[i]);
           _subsets(nums, i + 1);
           path.pop_back();
       }
       
    }
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        _subsets(nums, 0);
        return ret;
    }
};
```

## [1863. 找出所有子集的异或总和再求和 - 力扣（LeetCode）](https://leetcode.cn/problems/sum-of-all-subset-xor-totals/)

一个数组的 **异或总和** 定义为数组中所有元素按位 `XOR` 的结果；如果数组为 **空** ，则异或总和为 `0` 。

- 例如，数组 `[2,5,6]` 的 **异或总和** 为 `2 XOR 5 XOR 6 = 1` 。

给你一个数组 `nums` ，请你求出 `nums` 中每个 **子集** 的 **异或总和** ，计算并返回这些值相加之 **和** 。

**注意：**在本题中，元素 **相同** 的不同子集应 **多次** 计数。

数组 `a` 是数组 `b` 的一个 **子集** 的前提条件是：从 `b` 删除几个（也可能不删除）元素能够得到 `a` 。

 

**示例 1：**

```
输入：nums = [1,3]
输出：6
解释：[1,3] 共有 4 个子集：
- 空子集的异或总和是 0 。
- [1] 的异或总和为 1 。
- [3] 的异或总和为 3 。
- [1,3] 的异或总和为 1 XOR 3 = 2 。
0 + 1 + 3 + 2 = 6
```

**示例 2：**

```
输入：nums = [5,1,6]
输出：28
解释：[5,1,6] 共有 8 个子集：
- 空子集的异或总和是 0 。
- [5] 的异或总和为 5 。
- [1] 的异或总和为 1 。
- [6] 的异或总和为 6 。
- [5,1] 的异或总和为 5 XOR 1 = 4 。
- [5,6] 的异或总和为 5 XOR 6 = 3 。
- [1,6] 的异或总和为 1 XOR 6 = 7 。
- [5,1,6] 的异或总和为 5 XOR 1 XOR 6 = 2 。
0 + 5 + 1 + 6 + 4 + 3 + 7 + 2 = 28
```

**示例 3：**

```
输入：nums = [3,4,5,6,7,8]
输出：480
解释：每个子集的全部异或总和值之和为 480 。
```

 

**提示：**

- `1 <= nums.length <= 12`
- `1 <= nums[i] <= 20`

解法一：递归+回溯

时间复杂度：O(n * n!)

空间复杂度：O(n)

```cpp
class Solution {
    vector<int> path;
    int ret;
    void _subsetXORSum(vector<int>& nums, int pos)
    {
        int tmp = 0;
        for(int e : path) tmp ^= e;
        ret += tmp;
        for(int i = pos; i < nums.size(); i++)
        {
            path.push_back(nums[i]);
            _subsetXORSum(nums, i + 1);
            path.pop_back();
        }
    }

public:
    int subsetXORSum(vector<int>& nums) {
        _subsetXORSum(nums, 0);
        return ret;
    }
};
```

# DAY48

## [LCR 084. 全排列 II - 力扣（LeetCode）](https://leetcode.cn/problems/7p8L0Z/)

给定一个可包含重复数字的整数集合 `nums` ，**按任意顺序** 返回它所有不重复的全排列。

 

**示例 1：**

```
输入：nums = [1,1,2]
输出：
[[1,1,2],
 [1,2,1],
 [2,1,1]]
```

**示例 2：**

```
输入：nums = [1,2,3]
输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
```

 

**提示：**

- `1 <= nums.length <= 8`
- `-10 <= nums[i] <= 10`

解法一：递归+回溯

时间复杂度：O(n * n!)

空间复杂度：O(n)

```cpp
class Solution {
    vector<vector<int>> ret;
    vector<int> path;
    bool check[8];
    void _permuteUnique(vector<int>& nums)
    {
        if(path.size() == nums.size())
        {
            ret.push_back(path);
            return;
        }
        for(int i = 0; i < nums.size(); i++)
        {
            if(check[i] == true || (i != 0 && nums[i] == nums[i - 1] && check[i - 1] == false))
                continue;
            path.push_back(nums[i]);
            check[i] = true;
            _permuteUnique(nums);
            path.pop_back();
            check[i] = false;
        }
    }
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) 
    {
        sort(nums.begin(), nums.end());
        _permuteUnique(nums);
        return ret;
    }
};
```

## [17. 电话号码的字母组合 - 力扣（LeetCode）](https://leetcode.cn/problems/letter-combinations-of-a-phone-number/description/)

给定一个仅包含数字 `2-9` 的字符串，返回所有它能表示的字母组合。答案可以按 **任意顺序** 返回。

给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。

![img](https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2021/11/09/200px-telephone-keypad2svg.png)

 

**示例 1：**

```
输入：digits = "23"
输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
```

**示例 2：**

```
输入：digits = ""
输出：[]
```

**示例 3：**

```
输入：digits = "2"
输出：["a","b","c"]
```

 

**提示：**

- `0 <= digits.length <= 4`
- `digits[i]` 是范围 `['2', '9']` 的一个数字。

时间复杂度：O(3^n * 4^m)

空间复杂度：O(m + n)

```cpp
class Solution {
    char* nums[10] = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    vector<string> ret;
    string path;
    bool check[4];
    void _letterCombinations(string& digits, int pos)
    {
        if(path.size() == digits.size())
        {
            ret.push_back(path);
            return;
        }
        int t = digits[pos] - '0';
        for(int i = 0; nums[t][i] != '\0'; i++)
        {
            path += nums[t][i];
            _letterCombinations(digits, pos + 1);
            path.pop_back();
        }
    }

public:
    vector<string> letterCombinations(string digits) {
        if(digits.size() == 0) return ret;
        _letterCombinations(digits, 0);
        return ret;
    }
};
```

# DAY49

## [LCR 085. 括号生成 - 力扣（LeetCode）](https://leetcode.cn/problems/IDBivT/)

正整数 `n` 代表生成括号的对数，请设计一个函数，用于能够生成所有可能的并且 **有效的** 括号组合。

 

**示例 1：**

```
输入：n = 3
输出：["((()))","(()())","(())()","()(())","()()()"]
```

**示例 2：**

```
输入：n = 1
输出：["()"]
```

 

**提示：**

- `1 <= n <= 8`

解法一：递归+回溯

```cpp
class Solution {
    vector<string> ret;
    string path;
    int left_bracket;
    bool check[16];
    void _generateParenthesis(const string& nums)
    {
        if(path.size() == nums.size())
        {
            ret.push_back(path);
            return;
        }
        for(int i = 0; i < nums.size(); i++)
        {
            if(check[i] == true || (i != 0 && nums[i] == nums[i - 1] && check[i - 1] == false))
                continue;    
                          
            if(left_bracket || (left_bracket == 0 && nums[i] == '('))
            {
                path += nums[i];
                check[i] = true;
                if(nums[i] == '(') left_bracket++;
                else left_bracket--;
                _generateParenthesis(nums);
                if(path.back() == '(') left_bracket--;
                else left_bracket++;
                path.pop_back();
                check[i] = false;
            }
        }
    }
public:
    vector<string> generateParenthesis(int n) {
        string nums;
        for(int i = 0; i < 2 * n; i++)
        {
            if(i < n) nums += '(';
            else nums += ')';
        }
        cout << nums << endl;
        _generateParenthesis(nums);
        return ret;
    }
};
```

## [LCR 080. 组合 - 力扣（LeetCode）](https://leetcode.cn/problems/uUsW3B/)

给定两个整数 `n` 和 `k`，返回 `1 ... n` 中所有可能的 `k` 个数的组合。

 

**示例 1:**

```
输入: n = 4, k = 2
输出:
[
  [2,4],
  [3,4],
  [2,3],
  [1,2],
  [1,3],
  [1,4],
]
```

**示例 2:**

```
输入: n = 1, k = 1
输出: [[1]]
```

 

**提示:**

- `1 <= n <= 20`
- `1 <= k <= n`

解法一：递归+回溯

```cpp
class Solution {
    vector<vector<int>> ret;
    vector<int> path;
    void _combine(int n, int k, int pos)
    {
        if(path.size() == k)
        {
            ret.push_back(path);
            return;
        }
        for(int i = pos; i <= n; i++)
        {
            path.push_back(i);
            _combine(n, k, i + 1);
            path.pop_back();
        }
    }
public:
    vector<vector<int>> combine(int n, int k) {
        _combine(n, k, 1);
        return ret;
    }
};
```



# DAY50

## [LCR 102. 目标和 - 力扣（LeetCode）](https://leetcode.cn/problems/YaVDxD/)

给定一个正整数数组 `nums` 和一个整数 `target` 。

向数组中的每个整数前添加 `'+'` 或 `'-'` ，然后串联起所有整数，可以构造一个 **表达式** ：

- 例如，`nums = [2, 1]` ，可以在 `2` 之前添加 `'+'` ，在 `1` 之前添加 `'-'` ，然后串联起来得到表达式 `"+2-1"` 。

返回可以通过上述方法构造的、运算结果等于 `target` 的不同 **表达式** 的数目。

 

**示例 1：**

```
输入：nums = [1,1,1,1,1], target = 3
输出：5
解释：一共有 5 种方法让最终目标和为 3 。
-1 + 1 + 1 + 1 + 1 = 3
+1 - 1 + 1 + 1 + 1 = 3
+1 + 1 - 1 + 1 + 1 = 3
+1 + 1 + 1 - 1 + 1 = 3
+1 + 1 + 1 + 1 - 1 = 3
```

**示例 2：**

```
输入：nums = [1], target = 1
输出：1
```

 

**提示：**

- `1 <= nums.length <= 20`
- `0 <= nums[i] <= 1000`
- `0 <= sum(nums[i]) <= 1000`
- `-1000 <= target <= 1000`

解法一：递归+回溯

时间复杂度：O(2 ^ n)

空间复杂度：O(n)

```cpp
class Solution {
    int ret;
    void _findTargetSumWays(vector<int>& nums, int target, int pos, int sum)
    {
        if(pos == nums.size())
        {
            if(sum == target) ret++;     
            return;
        }
        _findTargetSumWays(nums, target, pos + 1, sum + nums[pos]);
        _findTargetSumWays(nums, target, pos + 1, sum - nums[pos]);
    }
public:
    int findTargetSumWays(vector<int>& nums, int target) {         
        _findTargetSumWays(nums, target, 0, 0);
        return ret;
    }
};
```



## [LCR 081. 组合总和 - 力扣（LeetCode）](https://leetcode.cn/problems/Ygoe9J/)

给定一个**无重复元素**的正整数数组 `candidates` 和一个正整数 `target` ，找出 `candidates` 中所有可以使数字和为目标数 `target` 的唯一组合。

`candidates` 中的数字可以无限制重复被选取。如果至少一个所选数字数量不同，则两种组合是不同的。 

对于给定的输入，保证和为 `target` 的唯一组合数少于 `150` 个。

 

**示例 1：**

```
输入: candidates = [2,3,6,7], target = 7
输出: [[7],[2,2,3]]
```

**示例 2：**

```
输入: candidates = [2,3,5], target = 8
输出: [[2,2,2,2],[2,3,3],[3,5]]
```

**示例 3：**

```
输入: candidates = [2], target = 1
输出: []
```

**示例 4：**

```
输入: candidates = [1], target = 1
输出: [[1]]
```

**示例 5：**

```
输入: candidates = [1], target = 2
输出: [[1,1]]
```

 

**提示：**

- `1 <= candidates.length <= 30`
- `1 <= candidates[i] <= 200`
- `candidate` 中的每个元素都是独一无二的。
- `1 <= target <= 500`

解法一：递归+回溯

```cpp
class Solution {
    vector<vector<int>> ret;
    vector<int> path;
    void _combinationSum(vector<int>& candidates, int target, int sum, int pos)
    {
        if(sum >= target)
        {
            if(sum == target) ret.push_back(path);
            return;
        }
        for(int i = pos; i < candidates.size(); i++)
        {
            path.push_back(candidates[i]);
            _combinationSum(candidates, target, sum + candidates[i], i);
            path.pop_back();
        }
    }
public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        _combinationSum(candidates, target, 0, 0);
        return ret;
    }
};
```



# DAY51

## [784. 字母大小写全排列 - 力扣（LeetCode）](https://leetcode.cn/problems/letter-case-permutation/description/)

给定一个字符串 `s` ，通过将字符串 `s` 中的每个字母转变大小写，我们可以获得一个新的字符串。

返回 *所有可能得到的字符串集合* 。以 **任意顺序** 返回输出。

 

**示例 1：**

```
输入：s = "a1b2"
输出：["a1b2", "a1B2", "A1b2", "A1B2"]
```

**示例 2:**

```
输入: s = "3z4"
输出: ["3z4","3Z4"]
```

 

**提示:**

- `1 <= s.length <= 12`
- `s` 由小写英文字母、大写英文字母和数字组成

解法一：递归+回溯

```cpp
class Solution {
    vector<string> ret;
    string path;

    void _letterCasePermutation(string& s, int pos)
    {
        if(pos == s.size())
        {
            ret.push_back(path);
            return;
        }
        path += s[pos];
        _letterCasePermutation(s, pos + 1);
        path.pop_back();
        if(s[pos] >= 'a' && s[pos] <= 'z')
        {
            path += s[pos] - 32;
            _letterCasePermutation(s, pos + 1);
            path.pop_back();
        }
        if(s[pos] >= 'A' && s[pos] <= 'Z')
        {
            path += s[pos] + 32;
            _letterCasePermutation(s, pos + 1);
            path.pop_back();
        }
    }
public:
    vector<string> letterCasePermutation(string s) {
        _letterCasePermutation(s, 0);
        return ret;
    }
};
```

## [526. 优美的排列 - 力扣（LeetCode）](https://leetcode.cn/problems/beautiful-arrangement/)

假设有从 1 到 n 的 n 个整数。用这些整数构造一个数组 `perm`（**下标从 1 开始**），只要满足下述条件 **之一** ，该数组就是一个 **优美的排列** ：

- `perm[i]` 能够被 `i` 整除
- `i` 能够被 `perm[i]` 整除

给你一个整数 `n` ，返回可以构造的 **优美排列** 的 **数量** 。

 

**示例 1：**

```
输入：n = 2
输出：2
解释：
第 1 个优美的排列是 [1,2]：
    - perm[1] = 1 能被 i = 1 整除
    - perm[2] = 2 能被 i = 2 整除
第 2 个优美的排列是 [2,1]:
    - perm[1] = 2 能被 i = 1 整除
    - i = 2 能被 perm[2] = 1 整除
```

**示例 2：**

```
输入：n = 1
输出：1
```

 

**提示：**

- `1 <= n <= 15`

解法一：递归+回溯

```cpp
class Solution {
    int ret;
    bool check[16];
    void _countArrangement(int n, int pos)
    {
        if(pos == n + 1)
        {
            ret++;
            return;
        }
        for(int i = 1; i <= n; i++)
        {
            if(check[i] == false && (i % pos == 0 || pos % i == 0))
            {
                check[i] = true;
                _countArrangement(n, pos + 1);
                check[i] = false;
            }
        }
    }
public:
    int countArrangement(int n) {
        _countArrangement(n, 1);
        return ret;
    }
};
```



# DAY52

## [51. N 皇后 - 力扣（LeetCode）](https://leetcode.cn/problems/n-queens/)

按照国际象棋的规则，皇后可以攻击与之处在同一行或同一列或同一斜线上的棋子。

**n 皇后问题** 研究的是如何将 `n` 个皇后放置在 `n×n` 的棋盘上，并且使皇后彼此之间不能相互攻击。

给你一个整数 `n` ，返回所有不同的 **n 皇后问题** 的解决方案。

每一种解法包含一个不同的 **n 皇后问题** 的棋子放置方案，该方案中 `'Q'` 和 `'.'` 分别代表了皇后和空位。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/11/13/queens.jpg)

```
输入：n = 4
输出：[[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
解释：如上图所示，4 皇后问题存在两个不同的解法。
```

**示例 2：**

```
输入：n = 1
输出：[["Q"]]
```

 

**提示：**

- `1 <= n <= 9`

解法一：递归+回溯

```cpp
class Solution {
    vector<vector<string>> ret;
    bool col[9], diag[18], back_diag[18];
    vector<string> path;
    int n;
    void _solveNQueens(int row)
    {
        if(row == n)
        {
            ret.push_back(path);
            return;
        }       
        for(int i = 0; i < n; i++)
        {
            string tmp(n, '.');
            if( !col[i] && !diag[row - i + n] && !back_diag[row + i])
            {
                tmp[i] = 'Q';
                col[i] = diag[row - i + n] = back_diag[row + i] = true;
                path.push_back(tmp);
                _solveNQueens(row + 1);
                path.pop_back();
                col[i] = diag[row - i + n] = back_diag[row + i] = false;               
            }
        }
    }
public:
    vector<vector<string>> solveNQueens(int _n) {
        n = _n;
        _solveNQueens(0);
        return ret;
    }
};
```



## [36. 有效的数独 - 力扣（LeetCode）](https://leetcode.cn/problems/valid-sudoku/)

请你判断一个 `9 x 9` 的数独是否有效。只需要 **根据以下规则** ，验证已经填入的数字是否有效即可。

1. 数字 `1-9` 在每一行只能出现一次。
2. 数字 `1-9` 在每一列只能出现一次。
3. 数字 `1-9` 在每一个以粗实线分隔的 `3x3` 宫内只能出现一次。（请参考示例图）

 

**注意：**

- 一个有效的数独（部分已被填充）不一定是可解的。
- 只需要根据以上规则，验证已经填入的数字是否有效即可。
- 空白格用 `'.'` 表示。

 

**示例 1：**

![img](https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2021/04/12/250px-sudoku-by-l2g-20050714svg.png)

```
输入：board = 
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
输出：true
```

**示例 2：**

```
输入：board = 
[["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
输出：false
解释：除了第一行的第一个数字从 5 改为 8 以外，空格内其他数字均与 示例1 相同。 但由于位于左上角的 3x3 宫内有两个 8 存在, 因此这个数独是无效的。
```

 

**提示：**

- `board.length == 9`
- `board[i].length == 9`
- `board[i][j]` 是一位数字（`1-9`）或者 `'.'`



解法一：递归

```cpp
class Solution {
    bool row[9][10];
    bool col[9][10];
    bool board_tmp[3][3][10];
    bool _isValidSudoku(vector<vector<char>>& board)
    {
        for(int i = 0; i < 9; i++)
        {
            for(int j = 0; j < 9; j++)
            {
                if(board[i][j] != '.')
                {
                    int num = board[i][j] - '0';
                    if(row[i][num] || col[j][num] || board_tmp[i / 3][j / 3][num])
                    {
                        return false;
                    }
                    row[i][num] = col[j][num] = board_tmp[i / 3][j / 3][num] = true;
                    _isValidSudoku(board);
                    row[i][num] = col[j][num] = board_tmp[i / 3][j / 3][num] = true;
                }
            }
        }
        return true;
    }
public:
    bool isValidSudoku(vector<vector<char>>& board)
    {
        return _isValidSudoku(board);
    }
};
```

## [37. 解数独 - 力扣（LeetCode）](https://leetcode.cn/problems/sudoku-solver/)

编写一个程序，通过填充空格来解决数独问题。

数独的解法需 **遵循如下规则**：

1. 数字 `1-9` 在每一行只能出现一次。
2. 数字 `1-9` 在每一列只能出现一次。
3. 数字 `1-9` 在每一个以粗实线分隔的 `3x3` 宫内只能出现一次。（请参考示例图）

数独部分空格内已填入了数字，空白格用 `'.'` 表示。

 

**示例 1：**

![img](https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2021/04/12/250px-sudoku-by-l2g-20050714svg.png)

```
输入：board = [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]
输出：[["5","3","4","6","7","8","9","1","2"],["6","7","2","1","9","5","3","4","8"],["1","9","8","3","4","2","5","6","7"],["8","5","9","7","6","1","4","2","3"],["4","2","6","8","5","3","7","9","1"],["7","1","3","9","2","4","8","5","6"],["9","6","1","5","3","7","2","8","4"],["2","8","7","4","1","9","6","3","5"],["3","4","5","2","8","6","1","7","9"]]
解释：输入的数独如上图所示，唯一有效的解决方案如下所示：
```

 

**提示：**

- `board.length == 9`
- `board[i].length == 9`
- `board[i][j]` 是一位数字或者 `'.'`
- 题目数据 **保证** 输入数独仅有一个解

解法一：递归

```cpp
class Solution {
    bool row[9][10];
    bool col[9][10];
    bool board_tmp[3][3][10];
    bool _solveSudoku(vector<vector<char>>& board)
    {
        for(int i = 0; i < 9; i++)
        {
            for(int j = 0; j < 9; j++)
            {
                if(board[i][j] == '.')
                {                                   
                    for(int k = 1; k <= 9; k++)
                    {
                        if(!row[i][k] && !col[j][k] && !board_tmp[i / 3][j / 3][k])
                        {
                            board[i][j] = k + '0';
                            row[i][k] = col[j][k] = board_tmp[i / 3][j / 3][k] = true;
                            if(_solveSudoku(board)) return true;
                            board[i][j] = '.';
                            row[i][k] = col[j][k] = board_tmp[i / 3][j / 3][k] = false;
                        }
                    }
                    return false;
                }            
            }
        }
        return true;
    }
public:
    void solveSudoku(vector<vector<char>>& board) {
        for(int i = 0; i < 9; i++)
        {
            for(int j = 0; j < 9; j++)
            {
                if(board[i][j] != '.')
                {
                    int num = board[i][j] - '0';
                    row[i][num] = col[j][num] = board_tmp[i / 3][j / 3][num] = true;
                }
            }
        }
        _solveSudoku(board);
    }
};
```

# DAY 53

## [79. 单词搜索 - 力扣（LeetCode）](https://leetcode.cn/problems/word-search/)

给定一个 `m x n` 二维字符网格 `board` 和一个字符串单词 `word` 。如果 `word` 存在于网格中，返回 `true` ；否则，返回 `false` 。

单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/11/04/word2.jpg)

```
输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
输出：true
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2020/11/04/word-1.jpg)

```
输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
输出：true
```

**示例 3：**

![img](https://assets.leetcode.com/uploads/2020/10/15/word3.jpg)

```
输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
输出：false
```

 

**提示：**

- `m == board.length`
- `n = board[i].length`
- `1 <= m, n <= 6`
- `1 <= word.length <= 15`
- `board` 和 `word` 仅由大小写英文字母组成

解法一：递归

```cpp
class Solution {
    int m, n;
    bool check[6][6];
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0 , 0};
    bool _exist(const vector<vector<char>>& board, int row, int col, const string& word, int pos)
    {
        if(pos == word.size()) return true;
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i], y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && !check[x][y] && board[x][y] == word[pos])
            {
                check[x][y] = true;
                if(_exist(board, x, y, word, pos + 1)) return true;
                check[x][y] = false;
            }
        }
        return false;
    }
public:
    bool exist(vector<vector<char>>& board, string word) {
        m = board.size();
        n = board[0].size();
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(board[i][j] == word[0])
                {
                    check[i][j] = true;
                    if(_exist(board, i, j, word, 1)) return true;
                    check[i][j] = false;
                }
            }
        }
        return false;
    }   
};
```

## [1219. 黄金矿工 - 力扣（LeetCode）](https://leetcode.cn/problems/path-with-maximum-gold/)

你要开发一座金矿，地质勘测学家已经探明了这座金矿中的资源分布，并用大小为 `m * n` 的网格 `grid` 进行了标注。每个单元格中的整数就表示这一单元格中的黄金数量；如果该单元格是空的，那么就是 `0`。

为了使收益最大化，矿工需要按以下规则来开采黄金：

- 每当矿工进入一个单元，就会收集该单元格中的所有黄金。
- 矿工每次可以从当前位置向上下左右四个方向走。
- 每个单元格只能被开采（进入）一次。
- **不得开采**（进入）黄金数目为 `0` 的单元格。
- 矿工可以从网格中 **任意一个** 有黄金的单元格出发或者是停止。

 

**示例 1：**

```
输入：grid = [[0,6,0],[5,8,7],[0,9,0]]
输出：24
解释：
[[0,6,0],
 [5,8,7],
 [0,9,0]]
一种收集最多黄金的路线是：9 -> 8 -> 7。
```

**示例 2：**

```
输入：grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
输出：28
解释：
[[1,0,7],
 [2,0,6],
 [3,4,5],
 [0,3,0],
 [9,0,20]]
一种收集最多黄金的路线是：1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7。
```

 

**提示：**

- `1 <= grid.length, grid[i].length <= 15`
- `0 <= grid[i][j] <= 100`
- 最多 **25** 个单元格中有黄金。



解法一：递归

```cpp
class Solution {
    int m, n;
    int max;
    bool check[15][15];
public:
    int dx[4] = {0, 0, -1, 1};
    int dy[4] = {1, -1, 0, 0};
    void _getMaximumGold(vector<vector<int>>& grid, int row, int col, int sum)
    {
        if(sum > max) max = sum;
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i];
            int y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && !check[x][y] && grid[x][y] != 0)
            {
                sum += grid[x][y];
                check[x][y] = true;
                _getMaximumGold(grid, x, y, sum);
                check[x][y] = false;
                sum -= grid[x][y];
            }
        }
    }

    int getMaximumGold(vector<vector<int>>& grid) {
        m = grid.size();
        n = grid[0].size();
        int sum = 0;
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(grid[i][j] != 0)
                {
                    check[i][j] = true;
                    sum += grid[i][j];
                    _getMaximumGold(grid, i, j, sum);
                    check[i][j] = false;
                    sum -= grid[i][j];
                }
            }
        }
        return max;
    }
};
```

# DAY54

## [980. 不同路径 III - 力扣（LeetCode）](https://leetcode.cn/problems/unique-paths-iii/)

在二维网格 `grid` 上，有 4 种类型的方格：

- `1` 表示起始方格。且只有一个起始方格。
- `2` 表示结束方格，且只有一个结束方格。
- `0` 表示我们可以走过的空方格。
- `-1` 表示我们无法跨越的障碍。

返回在四个方向（上、下、左、右）上行走时，从起始方格到结束方格的不同路径的数目**。**

**每一个无障碍方格都要通过一次，但是一条路径中不能重复通过同一个方格**。

 

**示例 1：**

```
输入：[[1,0,0,0],[0,0,0,0],[0,0,2,-1]]
输出：2
解释：我们有以下两条路径：
1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)
```

**示例 2：**

```
输入：[[1,0,0,0],[0,0,0,0],[0,0,0,2]]
输出：4
解释：我们有以下四条路径： 
1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)
```

**示例 3：**

```
输入：[[0,1],[2,0]]
输出：0
解释：
没有一条路能完全穿过每一个空的方格一次。
请注意，起始和结束方格可以位于网格中的任意位置。
```

 

**提示：**

- `1 <= grid.length * grid[0].length <= 20`

解法一：递归+回溯

```cpp
class Solution {
    int m, n, zero;
    bool check[20][20];
    int ret;
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    void _uniquePathsIII(vector<vector<int>>& grid, int row, int col, int count)
    {
        if(grid[row][col] == 2)
        {
            if(count == zero)
                ret++;
            return;
        }
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i];
            int y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && !check[x][y] && grid[x][y] != -1)
            {
                check[x][y] = true;
                if(grid[x][y] == 2)
                    _uniquePathsIII(grid, x, y, count);
                else 
                    _uniquePathsIII(grid, x, y, count + 1);
                check[x][y] = false;
            }
        }
    }
public:
    int uniquePathsIII(vector<vector<int>>& grid) {
        m = grid.size();
        n = grid[0].size();
        for(auto e : grid) 
        {
            for(auto t : e)
            {
                if(t == 0) zero++;
            }
        }
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(grid[i][j] == 1)
                {
                    check[i][j] = true;
                    _uniquePathsIII(grid, i, j, 0);
                    return ret;
                }
            }
        }
        return 0;
    }
};
```

## [733. 图像渲染 - 力扣（LeetCode）](https://leetcode.cn/problems/flood-fill/)

有一幅以 `m x n` 的二维整数数组表示的图画 `image` ，其中 `image[i][j]` 表示该图画的像素值大小。

你也被给予三个整数 `sr` , `sc` 和 `newColor` 。你应该从像素 `image[sr][sc]` 开始对图像进行 上色**填充** 。

为了完成 **上色工作** ，从初始像素开始，记录初始坐标的 **上下左右四个方向上** 像素值与初始坐标相同的相连像素点，接着再记录这四个方向上符合条件的像素点与他们对应 **四个方向上** 像素值与初始坐标相同的相连像素点，……，重复该过程。将所有有记录的像素点的颜色值改为 `newColor` 。

最后返回 *经过上色渲染后的图像* 。

 

**示例 1:**

![img](https://assets.leetcode.com/uploads/2021/06/01/flood1-grid.jpg)

```
输入: image = [[1,1,1],[1,1,0],[1,0,1]]，sr = 1, sc = 1, newColor = 2
输出: [[2,2,2],[2,2,0],[2,0,1]]
解析: 在图像的正中间，(坐标(sr,sc)=(1,1)),在路径上所有符合条件的像素点的颜色都被更改成2。
注意，右下角的像素没有更改为2，因为它不是在上下左右四个方向上与初始点相连的像素点。
```

**示例 2:**

```
输入: image = [[0,0,0],[0,0,0]], sr = 0, sc = 0, newColor = 2
输出: [[2,2,2],[2,2,2]]
```

 

**提示:**

- `m == image.length`
- `n == image[i].length`
- `1 <= m, n <= 50`
- `0 <= image[i][j], newColor < 216`
- `0 <= sr < m`
- `0 <= sc < n`

解法一：递归

```cpp
class Solution {
    int m, n;
    bool check[50][50];
    vector<vector<int>> ret;
    int dx[4] = {0, 0, -1, 1};
    int dy[4] = {1, -1, 0, 0};
    int sorce;
    void _floodFill(vector<vector<int>>& image, int row, int col, int color)
    {
        image[row][col] = color;
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i];
            int y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && image[x][y] == sorce)
            {
                _floodFill(image, x, y, color);
            }
        }
    }
public:
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
        m = image.size();
        n = image[0].size();
        sorce = image[sr][sc];
        if(sorce == color) return image;
        _floodFill(image, sr, sc, color);
        return image;
    }
};
```



# DAY55

## [200. 岛屿数量 - 力扣（LeetCode）](https://leetcode.cn/problems/number-of-islands/)

给你一个由 `'1'`（陆地）和 `'0'`（水）组成的的二维网格，请你计算网格中岛屿的数量。

岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。

此外，你可以假设该网格的四条边均被水包围。

 

**示例 1：**

```
输入：grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
输出：1
```

**示例 2：**

```
输入：grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
输出：3
```

 

**提示：**

- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 300`
- `grid[i][j]` 的值为 `'0'` 或 `'1'`

解法一：递归

```cpp
class Solution {
    int m, n;
    int ret;
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    bool check[300][300];
    void _numIslands(vector<vector<char>>& grid, int row, int col)
    {
        if(grid[row][col] == '0')
        {
            return;
        }
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i];
            int y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == '1' && !check[x][y])
            {
                check[x][y] = true;
                _numIslands(grid, x, y);
            }
        }
    }
public:
    int numIslands(vector<vector<char>>& grid) {
        m = grid.size();
        n = grid[0].size();
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(grid[i][j] == '1' && !check[i][j])
                {
                    ++ret;
                    check[i][j] = true;
                    _numIslands(grid, i, j);
                }
            }
        }
        return ret;
    }
};
```

## [LCR 105. 岛屿的最大面积 - 力扣（LeetCode）](https://leetcode.cn/problems/ZL6zAn/)

给定一个由 `0` 和 `1` 组成的非空二维数组 `grid` ，用来表示海洋岛屿地图。

一个 **岛屿** 是由一些相邻的 `1` (代表土地) 构成的组合，这里的「相邻」要求两个 `1` 必须在水平或者竖直方向上相邻。你可以假设 `grid` 的四个边缘都被 `0`（代表水）包围着。

找到给定的二维数组中最大的岛屿面积。如果没有岛屿，则返回面积为 `0` 。

 

**示例 1:**

![img](https://pic.leetcode-cn.com/1626667010-nSGPXz-image.png)

```
输入: grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,1,1,0,1,0,0,0,0,0,0,0,0],[0,1,0,0,1,1,0,0,1,0,1,0,0],[0,1,0,0,1,1,0,0,1,1,1,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0]]
输出: 6
解释: 对于上面这个给定矩阵应返回 6。注意答案不应该是 11 ，因为岛屿只能包含水平或垂直的四个方向的 1 。
```

**示例 2:**

```
输入: grid = [[0,0,0,0,0,0,0,0]]
输出: 0
```

 

**提示：**

- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 50`
- `grid[i][j] is either 0 or 1`

解法一：递归

```cpp
class Solution {
    int m, n;
    bool check[50][50];
    int ret;
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    void _maxAreaOfIsland(vector<vector<int>>& grid, int row, int col, int& sum)
    {  
        if(grid[row][col] == 0)
        {
            return;
        }
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i];
            int y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && grid[x][y] && !check[x][y])
            {
                check[x][y] = true;
                _maxAreaOfIsland(grid, x, y, ++sum);
            }
        }
    }
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        m = grid.size();
        n = grid[0].size();
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(grid[i][j] == 1 && !check[i][j])
                {
                    int sum = 1;
                    check[i][j] = true;
                    _maxAreaOfIsland(grid, i, j, sum);
                    ret = max(ret, sum);
                }
            }
        }
        return ret;
    }
};
```



# DAY56

## [130. 被围绕的区域 - 力扣（LeetCode）](https://leetcode.cn/problems/surrounded-regions/)

给你一个 `m x n` 的矩阵 `board` ，由若干字符 `'X'` 和 `'O'` ，找到所有被 `'X'` 围绕的区域，并将这些区域里所有的 `'O'` 用 `'X'` 填充。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/02/19/xogrid.jpg)

```
输入：board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
输出：[["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
解释：被围绕的区间不会存在于边界上，换句话说，任何边界上的 'O' 都不会被填充为 'X'。 任何不在边界上，或不与边界上的 'O' 相连的 'O' 最终都会被填充为 'X'。如果两个元素在水平或垂直方向相邻，则称它们是“相连”的。
```

**示例 2：**

```
输入：board = [["X"]]
输出：[["X"]]
```

 

**提示：**

- `m == board.length`
- `n == board[i].length`
- `1 <= m, n <= 200`
- `board[i][j]` 为 `'X'` 或 `'O'`

解法一：floodfill

```cpp
class Solution {
    int m, n;
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    void _solve(vector<vector<char>>& board, int row, int col)
    {
        for(int i = 0; i < 4; i++)
        {
            int x = dx[i] + row;
            int y = dy[i] + col;
            if(x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O')
            {
                board[x][y] = '.';
                _solve(board, x, y);
            }
        }
    }
public:
    void solve(vector<vector<char>>& board) { 
        m = board.size();
        n = board[0].size();
       for(int i = 0; i < m; i++)
       {
           if(board[i][0] == 'O')
           {
               board[i][0] = '.';
               _solve(board, i, 0);
           }
           if(board[i][n - 1] == 'O')
           {
               board[i][n - 1] = '.';
               _solve(board, i, n - 1);
           }
       }
       for(int j = 0; j < n; j++)
       {
           if(board[0][j] == 'O')
           {
               board[0][j] = '.';
               _solve(board, 0, j);
           }
           if(board[m - 1][j] == 'O')
           {
               board[m - 1][j] = '.';
               _solve(board, m - 1, j);
           }
       }
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(board[i][j] == '.')
                    board[i][j] = 'O';
                else if(board[i][j] == 'O')
                    board[i][j] = 'X';
            }
        }

    }
};
```



## [417. 太平洋大西洋水流问题 - 力扣（LeetCode）](https://leetcode.cn/problems/pacific-atlantic-water-flow/)

有一个 `m × n` 的矩形岛屿，与 **太平洋** 和 **大西洋** 相邻。 **“太平洋”** 处于大陆的左边界和上边界，而 **“大西洋”** 处于大陆的右边界和下边界。

这个岛被分割成一个由若干方形单元格组成的网格。给定一个 `m x n` 的整数矩阵 `heights` ， `heights[r][c]` 表示坐标 `(r, c)` 上单元格 **高于海平面的高度** 。

岛上雨水较多，如果相邻单元格的高度 **小于或等于** 当前单元格的高度，雨水可以直接向北、南、东、西流向相邻单元格。水可以从海洋附近的任何单元格流入海洋。

返回网格坐标 `result` 的 **2D 列表** ，其中 `result[i] = [ri, ci]` 表示雨水从单元格 `(ri, ci)` 流动 **既可流向太平洋也可流向大西洋** 。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/06/08/waterflow-grid.jpg)

```
输入: heights = [[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]
输出: [[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]
```

**示例 2：**

```
输入: heights = [[2,1],[1,2]]
输出: [[0,0],[0,1],[1,0],[1,1]]
```

 

**提示：**

- `m == heights.length`
- `n == heights[r].length`
- `1 <= m, n <= 200`
- `0 <= heights[r][c] <= 105`

解法一：floodfill

```cpp
class Solution {
    int m, n;
    vector<vector<int>> ret;
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    void _pacificAtlantic(vector<vector<int>>& heights, vector<vector<bool>>& ocean, int row, int col)
    {
        ocean[row][col] = true;
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i], y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && !ocean[x][y] && heights[x][y] >= heights[row][col])
            {
                _pacificAtlantic(heights, ocean, x, y);
            }
        }
    }
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
        m = heights.size();
        n = heights[0].size();
        vector<vector<bool>> Po(m, vector<bool>(n, false));
        vector<vector<bool>> Ao(m, vector<bool>(n, false));
        for(int j = 0; j < n; j++)
        {
            _pacificAtlantic(heights, Po, 0, j);
            _pacificAtlantic(heights, Ao, m - 1, j);
        }
        for(int i = 0; i < m; i++)
        {
            _pacificAtlantic(heights, Po, i, 0);
            _pacificAtlantic(heights, Ao, i, n - 1);
        }
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(Ao[i][j] && Po[i][j])
                {
                    ret.push_back({i, j});
                }
            }
        }
        return ret;
    }
};
```

# DAY57

## [529. 扫雷游戏 - 力扣（LeetCode）](https://leetcode.cn/problems/minesweeper/)

让我们一起来玩扫雷游戏！

给你一个大小为 `m x n` 二维字符矩阵 `board` ，表示扫雷游戏的盘面，其中：

- `'M'` 代表一个 **未挖出的** 地雷，
- `'E'` 代表一个 **未挖出的** 空方块，
- `'B'` 代表没有相邻（上，下，左，右，和所有4个对角线）地雷的 **已挖出的** 空白方块，
- **数字**（`'1'` 到 `'8'`）表示有多少地雷与这块 **已挖出的** 方块相邻，
- `'X'` 则表示一个 **已挖出的** 地雷。

给你一个整数数组 `click` ，其中 `click = [clickr, clickc]` 表示在所有 **未挖出的** 方块（`'M'` 或者 `'E'`）中的下一个点击位置（`clickr` 是行下标，`clickc` 是列下标）。

根据以下规则，返回相应位置被点击后对应的盘面：

1. 如果一个地雷（`'M'`）被挖出，游戏就结束了- 把它改为 `'X'` 。
2. 如果一个 **没有相邻地雷** 的空方块（`'E'`）被挖出，修改它为（`'B'`），并且所有和其相邻的 **未挖出** 方块都应该被递归地揭露。
3. 如果一个 **至少与一个地雷相邻** 的空方块（`'E'`）被挖出，修改它为数字（`'1'` 到 `'8'` ），表示相邻地雷的数量。
4. 如果在此次点击中，若无更多方块可被揭露，则返回盘面。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2023/08/09/untitled.jpeg)

```
输入：board = [["E","E","E","E","E"],["E","E","M","E","E"],["E","E","E","E","E"],["E","E","E","E","E"]], click = [3,0]
输出：[["B","1","E","1","B"],["B","1","M","1","B"],["B","1","1","1","B"],["B","B","B","B","B"]]
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2023/08/09/untitled-2.jpeg)

```
输入：board = [["B","1","E","1","B"],["B","1","M","1","B"],["B","1","1","1","B"],["B","B","B","B","B"]], click = [1,2]
输出：[["B","1","E","1","B"],["B","1","X","1","B"],["B","1","1","1","B"],["B","B","B","B","B"]]
```

 

**提示：**

- `m == board.length`
- `n == board[i].length`
- `1 <= m, n <= 50`
- `board[i][j]` 为 `'M'`、`'E'`、`'B'` 或数字 `'1'` 到 `'8'` 中的一个
- `click.length == 2`
- `0 <= clickr < m`
- `0 <= clickc < n`
- `board[clickr][clickc]` 为 `'M'` 或 `'E'`

解法一：递归

```cpp
class Solution {
    int m, n;
    void _updateBoard(vector<vector<char>>& board, int row, int col)
    {
        int sum = 0;
        for(int i = row - 1; i <= row + 1; i++)
            for(int j = col - 1; j <= col + 1; j++)
                if(i >= 0 && i < m && j >= 0 && j < n && board[i][j] == 'M')
                    sum++;        
        if(sum > 0)
        {
            board[row][col] = sum + '0';
            return;
        }
        board[row][col] = 'B';
        for(int i = row - 1; i <= row + 1; i++)
            for(int j = col - 1; j <= col + 1; j++)
                if(i >= 0 && i < m && j >= 0 && j < n && board[i][j] != 'B')               
                    _updateBoard(board, i, j);                                                 
    }
public:
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        m = board.size(), n = board[0].size();
        int x = click[0], y = click[1];
        if(x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'M')
        {
            board[x][y] = 'X';
            return board;
        }
        board[x][y] = 'B';
        _updateBoard(board, x, y);
        return board;
    }
};
```



## [LCR 130. 衣橱整理 - 力扣（LeetCode）](https://leetcode.cn/problems/ji-qi-ren-de-yun-dong-fan-wei-lcof/)

家居整理师将待整理衣橱划分为 `m x n` 的二维矩阵 `grid`，其中 `grid[i][j]` 代表一个需要整理的格子。整理师自 `grid[0][0]` 开始 **逐行逐列** 地整理每个格子。

整理规则为：在整理过程中，可以选择 **向右移动一格** 或 **向下移动一格**，但不能移动到衣柜之外。同时，不需要整理 `digit(i) + digit(j) > cnt` 的格子，其中 `digit(x)` 表示数字 `x` 的各数位之和。

请返回整理师 **总共需要整理多少个格子**。

 

**示例 1：**

```
输入：m = 4, n = 7, cnt = 5
输出：18
```

 

**提示：**

- `1 <= n, m <= 100`
- `0 <= cnt <= 20`

解法一：递归

```cpp
class Solution {
    int m, n, ret, cnt;
    bool check[100][100];
    int dx[2] = {1, 0};
    int dy[2] = {0, 1}; 
    void _wardrobeFinishing(int row, int col)
    {
        check[row][col] = true;
        if(digit(row) + digit(col) > cnt) return;
        ret++;
        for(int i = 0; i < 2; i++)
        {
            int x = row + dx[i], y = col + dy[i];
            if(x < m && y < n && !check[x][y])
            {
                _wardrobeFinishing(x, y);
            }
        }
    }
public:
    int wardrobeFinishing(int _m, int _n, int _cnt) {
        m = _m, n = _n, cnt = _cnt;
        _wardrobeFinishing(0, 0);     
        return ret;
    }

    int digit(int i)
    {
        int sum = 0;
        while(i)
        {
            sum += i % 10;
            i /= 10;
        }
        return sum;
    }
};
```



# DAY58

## [LCR 098. 不同路径 - 力扣（LeetCode）](https://leetcode.cn/problems/2AoeFn/)

一个机器人位于一个 `m x n` 网格的左上角 （起始点在下图中标记为 “Start” ）。

机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。

问总共有多少条不同的路径？

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2018/10/22/robot_maze.png)

```
输入：m = 3, n = 7
输出：28
```

**示例 2：**

```
输入：m = 3, n = 2
输出：3
解释：
从左上角开始，总共有 3 条路径可以到达右下角。
1. 向右 -> 向下 -> 向下
2. 向下 -> 向下 -> 向右
3. 向下 -> 向右 -> 向下
```

**示例 3：**

```
输入：m = 7, n = 3
输出：28
```

**示例 4：**

```
输入：m = 3, n = 3
输出：6
```

 

**提示：**

- `1 <= m, n <= 100`
- 题目数据保证答案小于等于 `2 * 109`

解法一：递归

```cpp
class Solution {
    int _uniquePaths(int m, int n, vector<vector<int>>& sum)
    {
        if(m == 0 || n == 0) return 0;
        if(m == 1 && n == 1) 
        {
            sum[m][n] = 1;
            return 1;
        }
        if(sum[m][n] == 0)
        {
            sum[m][n] = _uniquePaths(m - 1, n, sum) + _uniquePaths(m, n - 1, sum);
        }
        return sum[m][n];
    }
public:
    int uniquePaths(int m, int n) {
        vector<vector<int>> sum(m + 1, vector<int>(n + 1, 0));
        return _uniquePaths(m, n, sum);
    }
};
```



## [300. 最长递增子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-increasing-subsequence/)

给你一个整数数组 `nums` ，找到其中最长严格递增子序列的长度。

**子序列** 是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。例如，`[3,6,2,7]` 是数组 `[0,3,1,6,2,2,7]` 的子序列。

 

**示例 1：**

```
输入：nums = [10,9,2,5,3,7,101,18]
输出：4
解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
```

**示例 2：**

```
输入：nums = [0,1,0,3,2,3]
输出：4
```

**示例 3：**

```
输入：nums = [7,7,7,7,7,7,7]
输出：1
```

 

**提示：**

- `1 <= nums.length <= 2500`
- `-104 <= nums[i] <= 104`

 

**进阶：**

- 你能将算法的时间复杂度降低到 `O(n log(n))` 吗?

解法一：递归

```cpp
class Solution {
    int ret;
    int _lengthOfLIS(vector<int>& nums, int pos, vector<int>& sum)
    {
        if(sum[pos] != 0) return sum[pos] + 1;
        int ret = 0;
        for(int i = pos + 1; i < nums.size(); i++)
        {
            if(nums[i] > nums[pos])
            {
                int tmp = _lengthOfLIS(nums, i, sum);
                ret = max(ret, tmp);
            }
        }
        sum[pos] = ret;
        return ret + 1;
    }
public:
    int lengthOfLIS(vector<int>& nums) {
        int ret = 0;
        vector<int> sum(nums.size(), 0);
        for(int i = 0; i < nums.size(); i++)
        {
            int tmp = _lengthOfLIS(nums, i, sum);
            ret = max(ret, tmp);
        }
        for(auto e : sum) cout << e << ' ';
        return ret;
    }
};
```

# DAY59

## [375. 猜数字大小 II - 力扣（LeetCode）](https://leetcode.cn/problems/guess-number-higher-or-lower-ii/)

我们正在玩一个猜数游戏，游戏规则如下：

1. 我从 `1` 到 `n` 之间选择一个数字。
2. 你来猜我选了哪个数字。
3. 如果你猜到正确的数字，就会 **赢得游戏** 。
4. 如果你猜错了，那么我会告诉你，我选的数字比你的 **更大或者更小** ，并且你需要继续猜数。
5. 每当你猜了数字 `x` 并且猜错了的时候，你需要支付金额为 `x` 的现金。如果你花光了钱，就会 **输掉游戏** 。

给你一个特定的数字 `n` ，返回能够 **确保你获胜** 的最小现金数，**不管我选择那个数字** 。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/09/10/graph.png)

```
输入：n = 10
输出：16
解释：制胜策略如下：
- 数字范围是 [1,10] 。你先猜测数字为 7 。
    - 如果这是我选中的数字，你的总费用为 $0 。否则，你需要支付 $7 。
    - 如果我的数字更大，则下一步需要猜测的数字范围是 [8,10] 。你可以猜测数字为 9 。
        - 如果这是我选中的数字，你的总费用为 $7 。否则，你需要支付 $9 。
        - 如果我的数字更大，那么这个数字一定是 10 。你猜测数字为 10 并赢得游戏，总费用为 $7 + $9 = $16 。
        - 如果我的数字更小，那么这个数字一定是 8 。你猜测数字为 8 并赢得游戏，总费用为 $7 + $9 = $16 。
    - 如果我的数字更小，则下一步需要猜测的数字范围是 [1,6] 。你可以猜测数字为 3 。
        - 如果这是我选中的数字，你的总费用为 $7 。否则，你需要支付 $3 。
        - 如果我的数字更大，则下一步需要猜测的数字范围是 [4,6] 。你可以猜测数字为 5 。
            - 如果这是我选中的数字，你的总费用为 $7 + $3 = $10 。否则，你需要支付 $5 。
            - 如果我的数字更大，那么这个数字一定是 6 。你猜测数字为 6 并赢得游戏，总费用为 $7 + $3 + $5 = $15 。
            - 如果我的数字更小，那么这个数字一定是 4 。你猜测数字为 4 并赢得游戏，总费用为 $7 + $3 + $5 = $15 。
        - 如果我的数字更小，则下一步需要猜测的数字范围是 [1,2] 。你可以猜测数字为 1 。
            - 如果这是我选中的数字，你的总费用为 $7 + $3 = $10 。否则，你需要支付 $1 。
            - 如果我的数字更大，那么这个数字一定是 2 。你猜测数字为 2 并赢得游戏，总费用为 $7 + $3 + $1 = $11 。
在最糟糕的情况下，你需要支付 $16 。因此，你只需要 $16 就可以确保自己赢得游戏。
```

**示例 2：**

```
输入：n = 1
输出：0
解释：只有一个可能的数字，所以你可以直接猜 1 并赢得游戏，无需支付任何费用。
```

**示例 3：**

```
输入：n = 2
输出：1
解释：有两个可能的数字 1 和 2 。
- 你可以先猜 1 。
    - 如果这是我选中的数字，你的总费用为 $0 。否则，你需要支付 $1 。
    - 如果我的数字更大，那么这个数字一定是 2 。你猜测数字为 2 并赢得游戏，总费用为 $1 。
最糟糕的情况下，你需要支付 $1 。
```

 

**提示：**

- `1 <= n <= 200`

解法一：递归

```cpp
class Solution {
    int memo[201][201];
    int _getMoneyAmount(int l, int r)
    {
        if(l >= r) return 0;
        if(memo[l][r]) return memo[l][r];
        int ret = INT_MAX;
        for(int i = l; i <= r; i++)
        {
            int x = _getMoneyAmount(l, i - 1);
            if(x) memo[l][i - 1] = x;
            int y = _getMoneyAmount(i + 1, r);
            if(y) memo[i + 1][r] = y;
            ret = min(ret, i + max(x, y));
        }
        return ret;
    }
public:
    int getMoneyAmount(int n) {       
        return _getMoneyAmount(1, n);
    }
};
```

## [329. 矩阵中的最长递增路径 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-increasing-path-in-a-matrix/description/)

给定一个 `m x n` 整数矩阵 `matrix` ，找出其中 **最长递增路径** 的长度。

对于每个单元格，你可以往上，下，左，右四个方向移动。 你 **不能** 在 **对角线** 方向上移动或移动到 **边界外**（即不允许环绕）。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/01/05/grid1.jpg)

```
输入：matrix = [[9,9,4],[6,6,8],[2,1,1]]
输出：4 
解释：最长递增路径为 [1, 2, 6, 9]。
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2021/01/27/tmp-grid.jpg)

```
输入：matrix = [[3,4,5],[3,2,6],[2,2,1]]
输出：4 
解释：最长递增路径是 [3, 4, 5, 6]。注意不允许在对角线方向上移动。
```

**示例 3：**

```
输入：matrix = [[1]]
输出：1
```

 

**提示：**

- `m == matrix.length`
- `n == matrix[i].length`
- `1 <= m, n <= 200`
- `0 <= matrix[i][j] <= 231 - 1`

解法一：递归

```cpp
class Solution {
    int m, n;
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    int memo[200][200];
    int _longestIncreasingPath(vector<vector<int>>& matrix, int row, int col)
    {
        if(memo[row][col]) return memo[row][col];
        int ret = 0;
        for(int i = 0; i < 4; i++)
        {
            int x = row + dx[i];
            int y = col + dy[i];
            if(x >= 0 && x < m && y >= 0 && y < n && matrix[row][col] < matrix[x][y])
            {
               int tmp = _longestIncreasingPath(matrix, x, y);
               memo[x][y] = tmp;
               ret = max(ret, tmp);
            }
        }
        return ret + 1;
    }
public:
    int longestIncreasingPath(vector<vector<int>>& matrix) {
        m = matrix.size();
        n = matrix[0].size();

        int ret = 0;
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                int tmp = _longestIncreasingPath(matrix, i, j);
                memo[i][j] = tmp;
                ret = max(ret, tmp);
            }
        }
        return ret;
    }
};
```





# DAY60

## [1137. 第 N 个泰波那契数 - 力扣（LeetCode）](https://leetcode.cn/problems/n-th-tribonacci-number/submissions/)

泰波那契序列 Tn 定义如下： 

T0 = 0, T1 = 1, T2 = 1, 且在 n >= 0 的条件下 Tn+3 = Tn + Tn+1 + Tn+2

给你整数 `n`，请返回第 n 个泰波那契数 Tn 的值。

 

**示例 1：**

```
输入：n = 4
输出：4
解释：
T_3 = 0 + 1 + 1 = 2
T_4 = 1 + 1 + 2 = 4
```

**示例 2：**

```
输入：n = 25
输出：1389537
```

 

**提示：**

- `0 <= n <= 37`
- 答案保证是一个 32 位整数，即 `answer <= 2^31 - 1`。

解法一：动规

```cp
class Solution {
public:
    int tribonacci(int n) {
        if(n < 3) return n == 0 ? 0 : 1;         
        int arr[3] = {0, 1, 1};
        for(int i = 3; i <= n; i++)
        {
            int tmp = arr[0] + arr[1] + arr[2];
            arr[0] = arr[1];
            arr[1] = arr[2];
            arr[2] = tmp;
        }
        return arr[2];
    }
};
```

## [面试题 08.01. 三步问题 - 力扣（LeetCode）](https://leetcode.cn/problems/three-steps-problem-lcci/description/)

三步问题。有个小孩正在上楼梯，楼梯有n阶台阶，小孩一次可以上1阶、2阶或3阶。实现一种方法，计算小孩有多少种上楼梯的方式。结果可能很大，你需要对结果模1000000007。

**示例1:**

```
 输入：n = 3 
 输出：4
 说明: 有四种走法
```

**示例2:**

```
 输入：n = 5
 输出：13
```

**提示:**

1. n范围在[1, 1000000]之间

解法一：动归

```cpp
class Solution {
public:
    int waysToStep(int n) {
        if(n < 3) return n;
        long long arr[3] = {1, 1, 2};
        for(int i = 3; i <= n; i++)
        {
            long long tmp = (arr[0] + arr[1] + arr[2]) % 1000000007;
            arr[0] = arr[1], arr[1] = arr[2], arr[2] = tmp;
        }
        return arr[2];
    }
};
```

# DAY61

## [LCR 088. 使用最小花费爬楼梯 - 力扣（LeetCode）](https://leetcode.cn/problems/GzCJIP/)

数组的每个下标作为一个阶梯，第 `i` 个阶梯对应着一个非负数的体力花费值 `cost[i]`（下标从 `0` 开始）。

每当爬上一个阶梯都要花费对应的体力值，一旦支付了相应的体力值，就可以选择向上爬一个阶梯或者爬两个阶梯。

请找出达到楼层顶部的最低花费。在开始时，你可以选择从下标为 0 或 1 的元素作为初始阶梯。

 

**示例 1：**

```
输入：cost = [10, 15, 20]
输出：15
解释：最低花费是从 cost[1] 开始，然后走两步即可到阶梯顶，一共花费 15 。
```

 **示例 2：**

```
输入：cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
输出：6
解释：最低花费方式是从 cost[0] 开始，逐个经过那些 1 ，跳过 cost[3] ，一共花费 6 。
```

 

**提示：**

- `2 <= cost.length <= 1000`
- `0 <= cost[i] <= 999`

解法一：动规

```cpp
class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        int Count[2] = {cost[0], cost[1]};
        for(int i = 2; i < cost.size(); i++)
        {
            int tmp1 = cost[i] + Count[0];
            int tmp2 = cost[i] + Count[1];
            Count[0] = Count[1];
            Count[1] = min(tmp1, tmp2);
        }
        return min(Count[0], Count[1]);
    }
};
```

## [91. 解码方法 - 力扣（LeetCode）](https://leetcode.cn/problems/decode-ways/)

一条包含字母 `A-Z` 的消息通过以下映射进行了 **编码** ：

```
'A' -> "1"
'B' -> "2"
...
'Z' -> "26"
```

要 **解码** 已编码的消息，所有数字必须基于上述映射的方法，反向映射回字母（可能有多种方法）。例如，`"11106"` 可以映射为：

- `"AAJF"` ，将消息分组为 `(1 1 10 6)`
- `"KJF"` ，将消息分组为 `(11 10 6)`

注意，消息不能分组为 `(1 11 06)` ，因为 `"06"` 不能映射为 `"F"` ，这是由于 `"6"` 和 `"06"` 在映射中并不等价。

给你一个只含数字的 **非空** 字符串 `s` ，请计算并返回 **解码** 方法的 **总数** 。

题目数据保证答案肯定是一个 **32 位** 的整数。

 

**示例 1：**

```
输入：s = "12"
输出：2
解释：它可以解码为 "AB"（1 2）或者 "L"（12）。
```

**示例 2：**

```
输入：s = "226"
输出：3
解释：它可以解码为 "BZ" (2 26), "VF" (22 6), 或者 "BBF" (2 2 6) 。
```

**示例 3：**

```
输入：s = "06"
输出：0
解释："06" 无法映射到 "F" ，因为存在前导零（"6" 和 "06" 并不等价）。
```

 

**提示：**

- `1 <= s.length <= 100`
- `s` 只包含数字，并且可能包含前导零。

解法一：动规

```cpp
class Solution {
public:
    int numDecodings(string s) {
        int Count[2] = { 0 };
        if(s[0] == '0') return 0;
        Count[0] = 1;
        if(s.size() == 1) return Count[0];       

        if(s[1] != '0') Count[1] += 1;           
        if((s[0] - '0') * 10 + (s[1] - '0') <= 26) Count[1] += 1;
        
        for(int i = 2; i < s.size(); i++)
        {
            int sum = 0;
            if(s[i] != '0') sum += Count[1];
            int tmp = (s[i - 1] - '0') * 10 + (s[i] - '0');
            if(tmp <= 26 && tmp >= 10) sum += Count[0];
            Count[0] = Count[1];
            Count[1] = sum;
        }
        return Count[1];
    }
};
```

# DAY62

## [62. 不同路径 - 力扣（LeetCode）](https://leetcode.cn/problems/unique-paths/)

一个机器人位于一个 `m x n` 网格的左上角 （起始点在下图中标记为 “Start” ）。

机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。

问总共有多少条不同的路径？

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2018/10/22/robot_maze.png)

```
输入：m = 3, n = 7
输出：28
```

**示例 2：**

```
输入：m = 3, n = 2
输出：3
解释：
从左上角开始，总共有 3 条路径可以到达右下角。
1. 向右 -> 向下 -> 向下
2. 向下 -> 向下 -> 向右
3. 向下 -> 向右 -> 向下
```

**示例 3：**

```
输入：m = 7, n = 3
输出：28
```

**示例 4：**

```
输入：m = 3, n = 3
输出：6
```

 

**提示：**

- `1 <= m, n <= 100`
- 题目数据保证答案小于等于 `2 * 109`

解法一：动规

```cpp
class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));      
        for(int i = 1; i <= m; i++)
            for(int j = 1; j <= n; j++)
            {
                if(i == 1 && j == 1) dp[1][1] = 1;
                else dp[i][j] = dp[i - 1][j] + dp[i][j - 1];                   
            }
        return dp[m][n];
    }
};
```

## [63. 不同路径 II - 力扣（LeetCode）](https://leetcode.cn/problems/unique-paths-ii/description/)

一个机器人位于一个 `m x n` 网格的左上角 （起始点在下图中标记为 “Start” ）。

机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish”）。

现在考虑网格中有障碍物。那么从左上角到右下角将会有多少条不同的路径？

网格中的障碍物和空位置分别用 `1` 和 `0` 来表示。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/11/04/robot1.jpg)

```
输入：obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
输出：2
解释：3x3 网格的正中间有一个障碍物。
从左上角到右下角一共有 2 条不同的路径：
1. 向右 -> 向右 -> 向下 -> 向下
2. 向下 -> 向下 -> 向右 -> 向右
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2020/11/04/robot2.jpg)

```
输入：obstacleGrid = [[0,1],[0,0]]
输出：1
```

 

**提示：**

- `m == obstacleGrid.length`
- `n == obstacleGrid[i].length`
- `1 <= m, n <= 100`
- `obstacleGrid[i][j]` 为 `0` 或 `1`

解法一：动规

```cpp
class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        int m = obstacleGrid.size();
        int n = obstacleGrid[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(obstacleGrid[i - 1][j - 1] == 1) dp[i][j] = 0;
                else if(i == 1 && j == 1) dp[i][j] = 1;
                else dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m][n];
    }
};
```

# DAY63

## [LCR 166. 珠宝的最高价值 - 力扣（LeetCode）](https://leetcode.cn/problems/li-wu-de-zui-da-jie-zhi-lcof/description/)

现有一个记作二维矩阵 `frame` 的珠宝架，其中 `frame[i][j]` 为该位置珠宝的价值。拿取珠宝的规则为：

- 只能从架子的左上角开始拿珠宝
- 每次可以移动到右侧或下侧的相邻位置
- 到达珠宝架子的右下角时，停止拿取

注意：珠宝的价值都是大于 0 的。除非这个架子上没有任何珠宝，比如 `frame = [[0]]`。

 

**示例 1:**

```
输入: frame = [[1,3,1],[1,5,1],[4,2,1]]
输出: 12
解释: 路径 1→3→5→2→1 可以拿到最高价值的珠宝
```

 

**提示：**

- `0 < frame.length <= 200`
- `0 < frame[0].length <= 200`

解法一：动规

```cpp
class Solution {
public:
    int jewelleryValue(vector<vector<int>>& frame) {
        int m = frame.size(), n = frame[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                dp[i][j] = frame[i - 1][j - 1] + max(dp[i][j - 1], dp[i - 1][j]);
            }
        }
        return dp[m][n];
    }
};
```




## [931. 下降路径最小和 - 力扣（LeetCode）](https://leetcode.cn/problems/minimum-falling-path-sum/)

给你一个 `n x n` 的 **方形** 整数数组 `matrix` ，请你找出并返回通过 `matrix` 的**下降路径** 的 **最小和** 。

**下降路径** 可以从第一行中的任何元素开始，并从每一行中选择一个元素。在下一行选择的元素和当前行所选元素最多相隔一列（即位于正下方或者沿对角线向左或者向右的第一个元素）。具体来说，位置 `(row, col)` 的下一个元素应当是 `(row + 1, col - 1)`、`(row + 1, col)` 或者 `(row + 1, col + 1)` 。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/11/03/failing1-grid.jpg)

```
输入：matrix = [[2,1,3],[6,5,4],[7,8,9]]
输出：13
解释：如图所示，为和最小的两条下降路径
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2021/11/03/failing2-grid.jpg)

```
输入：matrix = [[-19,57],[-40,-5]]
输出：-59
解释：如图所示，为和最小的下降路径
```

 

**提示：**

- `n == matrix.length == matrix[i].length`
- `1 <= n <= 100`
- `-100 <= matrix[i][j] <= 100`

解法一：动规

```cpp
class Solution {
public:
    int minFallingPathSum(vector<vector<int>>& matrix) {
        int m = matrix.size(), n = matrix[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(j + 1 > n) dp[i][j] = matrix[i - 1][j - 1] + min(dp[i - 1][j - 1], dp[i - 1][j]);
                else if(j - 1 <= 0) dp[i][j] = matrix[i - 1][j - 1] + min(dp[i - 1][j], dp[i - 1][j + 1]);
                else dp[i][j] = matrix[i - 1][j - 1] + min(min(dp[i - 1][j - 1], dp[i - 1][j]), dp[i - 1][j + 1]);
            }
        }
        int ret = dp[m][1];
        for(int j = 1; j <= n; j++)
            ret = min(ret, dp[m][j]);
        return ret;
    }
};
```



# DAY64

## [LCR 099. 最小路径和 - 力扣（LeetCode）](https://leetcode.cn/problems/0i0mDW/description/)

给定一个包含非负整数的 `*m* x *n*` 网格 `grid` ，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。

**说明：**一个机器人每次只能向下或者向右移动一步。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/11/05/minpath.jpg)

```
输入：grid = [[1,3,1],[1,5,1],[4,2,1]]
输出：7
解释：因为路径 1→3→1→1→1 的总和最小。
```

**示例 2：**

```
输入：grid = [[1,2,3],[4,5,6]]
输出：12
```

 

**提示：**

- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 200`
- `0 <= grid[i][j] <= 100`

解法一：动规

```cpp
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));

        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(i == 1) dp[i][j] = dp[i][j - 1] + grid[i - 1][j - 1];
                else if(j == 1) dp[i][j] = dp[i - 1][j] + grid[i - 1][j - 1];
                else dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
            }
        }
        return dp[m][n];
    }
};
```

## [174. 地下城游戏 - 力扣（LeetCode）](https://leetcode.cn/problems/dungeon-game/submissions/474430975/)

恶魔们抓住了公主并将她关在了地下城 `dungeon` 的 **右下角** 。地下城是由 `m x n` 个房间组成的二维网格。我们英勇的骑士最初被安置在 **左上角** 的房间里，他必须穿过地下城并通过对抗恶魔来拯救公主。

骑士的初始健康点数为一个正整数。如果他的健康点数在某一时刻降至 0 或以下，他会立即死亡。

有些房间由恶魔守卫，因此骑士在进入这些房间时会失去健康点数（若房间里的值为*负整数*，则表示骑士将损失健康点数）；其他房间要么是空的（房间里的值为 *0*），要么包含增加骑士健康点数的魔法球（若房间里的值为*正整数*，则表示骑士将增加健康点数）。

为了尽快解救公主，骑士决定每次只 **向右** 或 **向下** 移动一步。

返回确保骑士能够拯救到公主所需的最低初始健康点数。

**注意：**任何房间都可能对骑士的健康点数造成威胁，也可能增加骑士的健康点数，包括骑士进入的左上角房间以及公主被监禁的右下角房间。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/03/13/dungeon-grid-1.jpg)

```
输入：dungeon = [[-2,-3,3],[-5,-10,1],[10,30,-5]]
输出：7
解释：如果骑士遵循最佳路径：右 -> 右 -> 下 -> 下 ，则骑士的初始健康点数至少为 7 。
```

**示例 2：**

```
输入：dungeon = [[0]]
输出：1
```

 

**提示：**

- `m == dungeon.length`
- `n == dungeon[i].length`
- `1 <= m, n <= 200`
- `-1000 <= dungeon[i][j] <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        int m = dungeon.size();
        int n = dungeon[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
        dp[m][n - 1] = dp[m  -1][n] = 1;

        for(int i = m - 1; i >= 0; i--)
            for(int j = n - 1; j >= 0; j--)
            {
                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
                dp[i][j] = max(1, dp[i][j]);
            }
        return dp[0][0];
    }
};
```

# DAY65

## [面试题 17.16. 按摩师 - 力扣（LeetCode）](https://leetcode.cn/problems/the-masseuse-lcci/submissions/474548615/)

一个有名的按摩师会收到源源不断的预约请求，每个预约都可以选择接或不接。在每次预约服务之间要有休息时间，因此她不能接受相邻的预约。给定一个预约请求序列，替按摩师找到最优的预约集合（总预约时间最长），返回总的分钟数。

**注意：**本题相对原题稍作改动

 

**示例 1：**

```
输入： [1,2,3,1]
输出： 4
解释： 选择 1 号预约和 3 号预约，总时长 = 1 + 3 = 4。
```

**示例 2：**

```
输入： [2,7,9,3,1]
输出： 12
解释： 选择 1 号预约、 3 号预约和 5 号预约，总时长 = 2 + 9 + 1 = 12。
```

**示例 3：**

```
输入： [2,1,4,5,3,1,1,3]
输出： 12
解释： 选择 1 号预约、 3 号预约、 5 号预约和 8 号预约，总时长 = 2 + 4 + 3 + 3 = 12。
```

解法一：动规

```cpp
class Solution {
public:
    int massage(vector<int>& nums) {
        int sz = nums.size();
        if(sz == 0) return 0;
        if(sz == 1) return nums[0];
        vector<int> dp(sz, 0);
        dp[0] = nums[0], dp[1] = nums[1];
        int tmp = dp[0];
        for(int i = 2; i < sz; i++)
        {
            dp[i] = nums[i] + tmp;
            tmp = max(tmp, dp[i - 1]);
        }
        return max(dp[sz - 1], dp[sz - 2]);
    }
};
```

## [LCR 089. 打家劫舍 - 力扣（LeetCode）](https://leetcode.cn/problems/Gu0c2T/)

一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响小偷偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，**如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警**。

给定一个代表每个房屋存放金额的非负整数数组 `nums` ，请计算 **不触动警报装置的情况下** ，一夜之内能够偷窃到的最高金额。

 

**示例 1：**

```
输入：nums = [1,2,3,1]
输出：4
解释：偷窃 1 号房屋 (金额 = 1) ，然后偷窃 3 号房屋 (金额 = 3)。
     偷窃到的最高金额 = 1 + 3 = 4 。
```

**示例 2：**

```
输入：nums = [2,7,9,3,1]
输出：12
解释：偷窃 1 号房屋 (金额 = 2), 偷窃 3 号房屋 (金额 = 9)，接着偷窃 5 号房屋 (金额 = 1)。
     偷窃到的最高金额 = 2 + 9 + 1 = 12 。
```

 

**提示：**

- `1 <= nums.length <= 100`
- `0 <= nums[i] <= 400`

解法一：动规

```cpp
class Solution {
public:
    int rob(vector<int>& nums) {
        int sz = nums.size();
        if(sz == 1) return nums[0];
        vector<int> dp(sz, 0);
        dp[0] = nums[0], dp[1] = nums[1];
        int tmp = dp[0];
        for(int i = 2; i < sz; i++)
        {
            dp[i] = nums[i] + tmp;
            tmp = max(tmp, dp[i - 1]);
        }
        return max(dp[sz - 1], dp[sz - 2]);
    }
};
```

## [LCR 090. 打家劫舍 II - 力扣（LeetCode）](https://leetcode.cn/problems/PzWKhm/description/)

一个专业的小偷，计划偷窃一个环形街道上沿街的房屋，每间房内都藏有一定的现金。这个地方所有的房屋都 **围成一圈** ，这意味着第一个房屋和最后一个房屋是紧挨着的。同时，相邻的房屋装有相互连通的防盗系统，**如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警** 。

给定一个代表每个房屋存放金额的非负整数数组 `nums` ，请计算 **在不触动警报装置的情况下** ，今晚能够偷窃到的最高金额。

 

**示例 1：**

```
输入：nums = [2,3,2]
输出：3
解释：你不能先偷窃 1 号房屋（金额 = 2），然后偷窃 3 号房屋（金额 = 2）, 因为他们是相邻的。
```

**示例 2：**

```
输入：nums = [1,2,3,1]
输出：4
解释：你可以先偷窃 1 号房屋（金额 = 1），然后偷窃 3 号房屋（金额 = 3）。
     偷窃到的最高金额 = 1 + 3 = 4 。
```

**示例 3：**

```
输入：nums = [0]
输出：0
```

 

**提示：**

- `1 <= nums.length <= 100`
- `0 <= nums[i] <= 1000`

解法一：动规

```cpp
class Solution {
    int rob1(vector<int>& nums, int start, int end)
    {
        if(start > end) return 0;
        if(start == end) return nums[end];
        int sz = nums.size();
        vector<int> dp(sz, 0);
        dp[start] = nums[start], dp[start + 1] = nums[start + 1];
        int tmp = dp[start];
        for(int i = start + 2; i <= end; i++)
        {
            dp[i] = nums[i] + tmp;
            tmp = max(tmp, dp[i - 1]);
        }
        return max(dp[end - 1], dp[end]);
    }
public:
    int rob(vector<int>& nums) {
        int sz = nums.size();
        return max(nums[0] + rob1(nums, 2, sz - 2), rob1(nums, 1, sz - 1));
    }
};
```

# DAY66

## [740. 删除并获得点数 - 力扣（LeetCode）](https://leetcode.cn/problems/delete-and-earn/submissions/474981350/)

给你一个整数数组 `nums` ，你可以对它进行一些操作。

每次操作中，选择任意一个 `nums[i]` ，删除它并获得 `nums[i]` 的点数。之后，你必须删除 **所有** 等于 `nums[i] - 1` 和 `nums[i] + 1` 的元素。

开始你拥有 `0` 个点数。返回你能通过这些操作获得的最大点数。

 

**示例 1：**

```
输入：nums = [3,4,2]
输出：6
解释：
删除 4 获得 4 个点数，因此 3 也被删除。
之后，删除 2 获得 2 个点数。总共获得 6 个点数。
```

**示例 2：**

```
输入：nums = [2,2,3,3,3,4]
输出：9
解释：
删除 3 获得 3 个点数，接着要删除两个 2 和 4 。
之后，再次删除 3 获得 3 个点数，再次删除 3 获得 3 个点数。
总共获得 9 个点数。
```

 

**提示：**

- `1 <= nums.length <= 2 * 104`
- `1 <= nums[i] <= 104`

解法一：动规

```cpp
class Solution {
public:
    int deleteAndEarn(vector<int>& nums) {
        int maxValue = 0;
        for(int e : nums) maxValue = max(maxValue, e);

        vector<int> tmp(maxValue + 1, 0), dp(maxValue + 1, 0);
        for(int e : nums) tmp[e] += e;

        dp[0] = tmp[0];
        int maxNum = 0;
        for(int i = 1; i <= maxValue; i++)
        {
            dp[i] = tmp[i] + maxNum;
            maxNum = max(maxNum, dp[i - 1]); 
        }
        return max(dp[maxValue], dp[maxValue - 1]);
    }
};
```

## [LCR 091. 粉刷房子 - 力扣（LeetCode）](https://leetcode.cn/problems/JEj789/description/)

假如有一排房子，共 `n` 个，每个房子可以被粉刷成红色、蓝色或者绿色这三种颜色中的一种，你需要粉刷所有的房子并且使其相邻的两个房子颜色不能相同。

当然，因为市场上不同颜色油漆的价格不同，所以房子粉刷成不同颜色的花费成本也是不同的。每个房子粉刷成不同颜色的花费是以一个 `n x 3` 的正整数矩阵 `costs` 来表示的。

例如，`costs[0][0]` 表示第 0 号房子粉刷成红色的成本花费；`costs[1][2]` 表示第 1 号房子粉刷成绿色的花费，以此类推。

请计算出粉刷完所有房子最少的花费成本。

 

**示例 1：**

```
输入: costs = [[17,2,17],[16,16,5],[14,3,19]]
输出: 10
解释: 将 0 号房子粉刷成蓝色，1 号房子粉刷成绿色，2 号房子粉刷成蓝色。
     最少花费: 2 + 5 + 3 = 10。
```

**示例 2：**

```
输入: costs = [[7,6,2]]
输出: 2
```

 

**提示:**

- `costs.length == n`
- `costs[i].length == 3`
- `1 <= n <= 100`
- `1 <= costs[i][j] <= 20`

解法一：动规

```cpp
class Solution {
public:
    int minCost(vector<vector<int>>& costs) {
        int m = costs.size();
        vector<vector<int>> dp(m, vector<int>(3, 0));
        int minCost[3] = {0, 0, 0};
        
        for(int i = 0; i < m; i++) 
        {
            for(int j = 0; j < 3; j++)
            {
                dp[i][j] = costs[i][j] + minCost[j];
            }               
            minCost[0] = min(dp[i][1], dp[i][2]);
            minCost[1] = min(dp[i][0], dp[i][2]);
            minCost[2] = min(dp[i][1], dp[i][0]);
        }

        int ret = minCost[0] > minCost[1] ? minCost[1] : minCost[0];
        return min(ret, minCost[2]);
    }
};
```

# DAY67

## [309. 买卖股票的最佳时机含冷冻期 - 力扣（LeetCode）](https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-with-cooldown/description/)

给定一个整数数组`prices`，其中第 `prices[i]` 表示第 `*i*` 天的股票价格 。

设计一个算法计算出最大利润。在满足以下约束条件下，你可以尽可能地完成更多的交易（多次买卖一支股票）:

- 卖出股票后，你无法在第二天买入股票 (即冷冻期为 1 天)。

**注意：**你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。

 

**示例 1:**

```
输入: prices = [1,2,3,0,2]
输出: 3 
解释: 对应的交易状态为: [买入, 卖出, 冷冻期, 买入, 卖出]
```

**示例 2:**

```
输入: prices = [1]
输出: 0
```

 

**提示：**

- `1 <= prices.length <= 5000`
- `0 <= prices[i] <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int sz = prices.size();
        vector<vector<int>> dp(sz, vector<int>(3, 0));

        dp[0][0] = -prices[0];
        for(int i = 1; i < sz; i++)
        {
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
            dp[i][2] = dp[i - 1][0] + prices[i];
        }
        return max(dp[sz - 1][1], dp[sz - 1][2]);
    }
};
```

## [714. 买卖股票的最佳时机含手续费 - 力扣（LeetCode）](https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/submissions/475141495/)

给定一个整数数组 `prices`，其中 `prices[i]`表示第 `i` 天的股票价格 ；整数 `fee` 代表了交易股票的手续费用。

你可以无限次地完成交易，但是你每笔交易都需要付手续费。如果你已经购买了一个股票，在卖出它之前你就不能再继续购买股票了。

返回获得利润的最大值。

**注意：**这里的一笔交易指买入持有并卖出股票的整个过程，每笔交易你只需要为支付一次手续费。

 

**示例 1：**

```
输入：prices = [1, 3, 2, 8, 4, 9], fee = 2
输出：8
解释：能够达到的最大利润:  
在此处买入 prices[0] = 1
在此处卖出 prices[3] = 8
在此处买入 prices[4] = 4
在此处卖出 prices[5] = 9
总利润: ((8 - 1) - 2) + ((9 - 4) - 2) = 8
```

**示例 2：**

```
输入：prices = [1,3,7,5,10,3], fee = 3
输出：6
```

 

**提示：**

- `1 <= prices.length <= 5 * 104`
- `1 <= prices[i] < 5 * 104`
- `0 <= fee < 5 * 104`



解法一：动规

```cpp
class Solution {
public:
    int maxProfit(vector<int>& prices, int fee) {
        int sz = prices.size();
        vector<vector<int>> dp(sz, vector<int>(2, 0));       
        dp[0][0] = 0;
        dp[0][1] = -prices[0];

        for(int i = 1; i < sz; i++)
        {
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] + prices[i] - fee);
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
        }
        return dp[sz - 1][0];
    }
};
```

# DAY68

## [123. 买卖股票的最佳时机 III - 力扣（LeetCode）](https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-iii/submissions/475491848/)

给定一个数组，它的第 `i` 个元素是一支给定的股票在第 `i` 天的价格。

设计一个算法来计算你所能获取的最大利润。你最多可以完成 **两笔** 交易。

**注意：**你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。

 

**示例 1:**

```
输入：prices = [3,3,5,0,0,3,1,4]
输出：6
解释：在第 4 天（股票价格 = 0）的时候买入，在第 6 天（股票价格 = 3）的时候卖出，这笔交易所能获得利润 = 3-0 = 3 。
     随后，在第 7 天（股票价格 = 1）的时候买入，在第 8 天 （股票价格 = 4）的时候卖出，这笔交易所能获得利润 = 4-1 = 3 。
```

**示例 2：**

```
输入：prices = [1,2,3,4,5]
输出：4
解释：在第 1 天（股票价格 = 1）的时候买入，在第 5 天 （股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4 。   
     注意你不能在第 1 天和第 2 天接连购买股票，之后再将它们卖出。   
     因为这样属于同时参与了多笔交易，你必须在再次购买前出售掉之前的股票。
```

**示例 3：**

```
输入：prices = [7,6,4,3,1] 
输出：0 
解释：在这个情况下, 没有交易完成, 所以最大利润为 0。
```

**示例 4：**

```
输入：prices = [1]
输出：0
```

 

**提示：**

- `1 <= prices.length <= 105`
- `0 <= prices[i] <= 105`

解法一：动规

```cpp
class Solution {
    const int INF = 0x3f3f3f3f;
public:
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        vector<vector<int>> get(n, vector<int>(3, -100000));
        vector<vector<int>> post(n, vector<int>(3, -100000));

        post[0][0] = -prices[0], get[0][0] = 0;
 
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                post[i][j] = max(post[i - 1][j], get[i - 1][j] - prices[i]);
                get[i][j] = get[i - 1][j];
                if(j >= 1) get[i][j] = max(get[i][j], post[i - 1][j - 1] + prices[i]);
            }
        }
        return max(get[n - 1][0], max(get[n - 1][1], get[n - 1][2]));
    }
};
```

## [188. 买卖股票的最佳时机 IV - 力扣（LeetCode）](https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-iv/)

给你一个整数数组 `prices` 和一个整数 `k` ，其中 `prices[i]` 是某支给定的股票在第 `i` 天的价格。

设计一个算法来计算你所能获取的最大利润。你最多可以完成 `k` 笔交易。也就是说，你最多可以买 `k` 次，卖 `k` 次。

**注意：**你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。

 

**示例 1：**

```
输入：k = 2, prices = [2,4,1]
输出：2
解释：在第 1 天 (股票价格 = 2) 的时候买入，在第 2 天 (股票价格 = 4) 的时候卖出，这笔交易所能获得利润 = 4-2 = 2 。
```

**示例 2：**

```
输入：k = 2, prices = [3,2,6,5,0,3]
输出：7
解释：在第 2 天 (股票价格 = 2) 的时候买入，在第 3 天 (股票价格 = 6) 的时候卖出, 这笔交易所能获得利润 = 6-2 = 4 。
     随后，在第 5 天 (股票价格 = 0) 的时候买入，在第 6 天 (股票价格 = 3) 的时候卖出, 这笔交易所能获得利润 = 3-0 = 3 。
```

 

**提示：**

- `1 <= k <= 100`
- `1 <= prices.length <= 1000`
- `0 <= prices[i] <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
        int n = prices.size();
        vector<vector<int>> get(n, vector<int>(k + 1, -1001));
        auto post = get;

        get[0][0] = 0, post[0][0] = -prices[0];
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j <= k; j++)
            {
                post[i][j] = max(post[i - 1][j], get[i - 1][j] - prices[i]);
                get[i][j] = get[i - 1][j];
                if(j >= 1) get[i][j] = max(get[i][j], post[i - 1][j - 1] + prices[i]);
            }
        }
        int ret = 0;
        for(int j = 0; j <= k; j++)
        {
            ret = max(ret, get[n - 1][j]);
        }
        return ret;
    } 
};
```

# DAY69

## [53. 最大子数组和 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-subarray/submissions/475758219/)

给你一个整数数组 `nums` ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

**子数组** 是数组中的一个连续部分。

 

**示例 1：**

```
输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
```

**示例 2：**

```
输入：nums = [1]
输出：1
```

**示例 3：**

```
输入：nums = [5,4,-1,7,8]
输出：23
```

 

**提示：**

- `1 <= nums.length <= 105`
- `-104 <= nums[i] <= 104`

 

**进阶：**如果你已经实现复杂度为 `O(n)` 的解法，尝试使用更为精妙的 **分治法** 求解。

解法一：动规

```cpp
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
        vector<int> dp(n, 0);
        dp[0] = nums[0];
        int ret = dp[0];
        for(int i = 1; i < n; i++)
        {
            dp[i] = max(dp[i - 1] + nums[i], nums[i]);
            ret = max(ret, dp[i]);
        }
        return ret;
    }
};
```

## [918. 环形子数组的最大和 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-sum-circular-subarray/description/)

给定一个长度为 `n` 的**环形整数数组** `nums` ，返回 *`nums` 的非空 **子数组** 的最大可能和* 。

**环形数组** 意味着数组的末端将会与开头相连呈环状。形式上， `nums[i]` 的下一个元素是 `nums[(i + 1) % n]` ， `nums[i]` 的前一个元素是 `nums[(i - 1 + n) % n]` 。

**子数组** 最多只能包含固定缓冲区 `nums` 中的每个元素一次。形式上，对于子数组 `nums[i], nums[i + 1], ..., nums[j]` ，不存在 `i <= k1, k2 <= j` 其中 `k1 % n == k2 % n` 。

 

**示例 1：**

```
输入：nums = [1,-2,3,-2]
输出：3
解释：从子数组 [3] 得到最大和 3
```

**示例 2：**

```
输入：nums = [5,-3,5]
输出：10
解释：从子数组 [5,5] 得到最大和 5 + 5 = 10
```

**示例 3：**

```
输入：nums = [3,-2,2,-3]
输出：3
解释：从子数组 [3] 和 [3,-2,2] 都可以得到最大和 3
```

 

**提示：**

- `n == nums.length`
- `1 <= n <= 3 * 104`
- `-3 * 104 <= nums[i] <= 3 * 104`

解法一：递归

```cpp
class Solution {
public:
    int maxSubarraySumCircular(vector<int>& nums) {
        int n = nums.size();
        vector<int> dp_max(n, 0), dp_min(n, 0);
        dp_max[0] = dp_min[0] = nums[0];
        int ret_max = dp_max[0], ret_min = dp_min[0], sum = nums[0];
        for(int i = 1; i < n; i++)
        {
            dp_max[i] = max(dp_max[i - 1] + nums[i], nums[i]);
            dp_min[i] = min(dp_min[i - 1] + nums[i], nums[i]);
            ret_max = max(ret_max, dp_max[i]);
            ret_min = min(ret_min, dp_min[i]);
            sum += nums[i];
        }
        if(sum == ret_min) return ret_max;
        return max(ret_max, sum - ret_min);
    }
};
```

# DAY70

## [152. 乘积最大子数组 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-product-subarray/submissions/476139188/)

给你一个整数数组 `nums` ，请你找出数组中乘积最大的非空连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。

测试用例的答案是一个 **32-位** 整数。

**子数组** 是数组的连续子序列。

 

**示例 1:**

```
输入: nums = [2,3,-2,4]
输出: 6
解释: 子数组 [2,3] 有最大乘积 6。
```

**示例 2:**

```
输入: nums = [-2,0,-1]
输出: 0
解释: 结果不能为 2, 因为 [-2,-1] 不是子数组。
```

 

**提示:**

- `1 <= nums.length <= 2 * 104`
- `-10 <= nums[i] <= 10`
- `nums` 的任何前缀或后缀的乘积都 **保证** 是一个 **32-位** 整数

解法一：动规

```cpp
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int n = nums.size();
        vector<int> dp_max(n + 1, 0);
        auto dp_min = dp_max;
        dp_max[0] = dp_min[0] = 1;
        int ret = INT_MIN;
        for(int i = 1; i <= n; i++)
        {
            int x = nums[i - 1], y = dp_max[i - 1] * nums[i - 1], z = dp_min[i - 1] * nums[i - 1];
            dp_max[i] = max(x, max(y, z));
            dp_min[i] = min(x, min(y, z));
            ret = max(ret, dp_max[i]);
        }
        return ret;
    }
};
```

## [1567. 乘积为正数的最长子数组长度 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-length-of-subarray-with-positive-product/)

给你一个整数数组 `nums` ，请你求出乘积为正数的最长子数组的长度。

一个数组的子数组是由原数组中零个或者更多个连续数字组成的数组。

请你返回乘积为正数的最长子数组长度。

 

**示例 1：**

```
输入：nums = [1,-2,-3,4]
输出：4
解释：数组本身乘积就是正数，值为 24 。
```

**示例 2：**

```
输入：nums = [0,1,-2,-3,-4]
输出：3
解释：最长乘积为正数的子数组为 [1,-2,-3] ，乘积为 6 。
注意，我们不能把 0 也包括到子数组中，因为这样乘积为 0 ，不是正数。
```

**示例 3：**

```
输入：nums = [-1,-2,-3,0,1]
输出：2
解释：乘积为正数的最长子数组是 [-1,-2] 或者 [-2,-3] 。
```

 

**提示：**

- `1 <= nums.length <= 10^5`
- `-10^9 <= nums[i] <= 10^9`

解法一：动规

```cpp
class Solution {
public:
    int getMaxLen(vector<int>& nums) {
        int n = nums.size();
        vector<int> f(n + 1, 0), g(n + 1, 0);
        f[0] = g[0] = 0;
        int ret = 0;
        for(int i = 1; i <= n; i++)
        {
            if(nums[i - 1]  > 0)
            {
                f[i] = f[i - 1] + 1;
                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
            }
            else if(nums[i - 1] < 0)
            {
                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
                g[i] = f[i - 1] + 1;
            }
            else
            {
                f[i] = g[i] = 0;
            }
            ret = max(ret, f[i]);
        }
        return ret;
    }
};
```

# DAY71

## [413. 等差数列划分 - 力扣（LeetCode）](https://leetcode.cn/problems/arithmetic-slices/submissions/476172774/)

如果一个数列 **至少有三个元素** ，并且任意两个相邻元素之差相同，则称该数列为等差数列。

- 例如，`[1,3,5,7,9]`、`[7,7,7,7]` 和 `[3,-1,-5,-9]` 都是等差数列。

给你一个整数数组 `nums` ，返回数组 `nums` 中所有为等差数组的 **子数组** 个数。

**子数组** 是数组中的一个连续序列。

 

**示例 1：**

```
输入：nums = [1,2,3,4]
输出：3
解释：nums 中有三个子等差数组：[1, 2, 3]、[2, 3, 4] 和 [1,2,3,4] 自身。
```

**示例 2：**

```
输入：nums = [1]
输出：0
```

 

**提示：**

- `1 <= nums.length <= 5000`
- `-1000 <= nums[i] <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        int n = nums.size();
        if(n < 3) return 0;
        vector<int> dp(n, 0);
        dp[0] = dp[1] = 0;
        int dif = nums[1] - nums[0], len = 2, ret = 0;
        for(int i = 2; i < n; i++)
        {
            if(nums[i] - nums[i - 1] == dif)
            {
                dp[i] = dp[i - 1] + (++len) - 2;
            }
            else
            {
                dif = nums[i] - nums[i - 1];
                len = 2;
                ret += dp[i - 1];
            }
        }
        return ret + dp[n - 1];
    }
};
```

## [978. 最长湍流子数组 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-turbulent-subarray/)

给定一个整数数组 `arr` ，返回 `arr` 的 *最大湍流子数组的**长度*** 。

如果比较符号在子数组中的每个相邻元素对之间翻转，则该子数组是 **湍流子数组** 。

更正式地来说，当 `arr` 的子数组 `A[i], A[i+1], ..., A[j]` 满足仅满足下列条件时，我们称其为*湍流子数组*：

- 若 

  ```
  i <= k < j
  ```

   ：

  - 当 `k` 为奇数时， `A[k] > A[k+1]`，且
  - 当 `k` 为偶数时，`A[k] < A[k+1]`；

- 或 

  若 

  ```
  i <= k < j
  ```

   ：

  - 当 `k` 为偶数时，`A[k] > A[k+1]` ，且
  - 当 `k` 为奇数时， `A[k] < A[k+1]`。

 

**示例 1：**

```
输入：arr = [9,4,2,10,7,8,8,1,9]
输出：5
解释：arr[1] > arr[2] < arr[3] > arr[4] < arr[5]
```

**示例 2：**

```
输入：arr = [4,8,12,16]
输出：2
```

**示例 3：**

```
输入：arr = [100]
输出：1
```

 

**提示：**

- `1 <= arr.length <= 4 * 104`
- `0 <= arr[i] <= 109`

解法一：动规

```cpp
class Solution {
public:
    int maxTurbulenceSize(vector<int>& arr) {
        int n = arr.size();
        vector<int> dp(n, 0);
        dp[0] = 1;
        int ret = 1, flag = -1;

        for(int i = 1; i < n; i++)
        {
            int tmp = arr[i] - arr[i - 1];
            if((flag == -1 && tmp) || ((tmp > 0 && !flag) || (tmp < 0 && flag)))
            {
                dp[i] = dp[i - 1] + 1;
                if(flag == -1 && tmp) flag = tmp > 0 ? 1 : 0;
                else flag ^= 1;
            }
            else if(tmp == 0)
            {
                dp[i] = 1;
                flag = -1;
            }
            else
            {
                dp[i] = 2;
            }
            ret = max(ret, dp[i]);
        }
        return ret;
    }
};
```

# DAY72

## [139. 单词拆分 - 力扣（LeetCode）](https://leetcode.cn/problems/word-break/)

给你一个字符串 `s` 和一个字符串列表 `wordDict` 作为字典。请你判断是否可以利用字典中出现的单词拼接出 `s` 。

**注意：**不要求字典中出现的单词全部都使用，并且字典中的单词可以重复使用。

 

**示例 1：**

```
输入: s = "leetcode", wordDict = ["leet", "code"]
输出: true
解释: 返回 true 因为 "leetcode" 可以由 "leet" 和 "code" 拼接成。
```

**示例 2：**

```
输入: s = "applepenapple", wordDict = ["apple", "pen"]
输出: true
解释: 返回 true 因为 "applepenapple" 可以由 "apple" "pen" "apple" 拼接成。
     注意，你可以重复使用字典中的单词。
```

**示例 3：**

```
输入: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
输出: false
```

 

**提示：**

- `1 <= s.length <= 300`
- `1 <= wordDict.length <= 1000`
- `1 <= wordDict[i].length <= 20`
- `s` 和 `wordDict[i]` 仅由小写英文字母组成
- `wordDict` 中的所有字符串 **互不相同**

解法一：动规

```cpp
class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        unordered_set<string> tmp;
        for(auto& e : wordDict) tmp.insert(e);

        int n = s.size();
        vector<bool> dp(n + 1);
        dp[0] = true;
        s = ' ' + s;

        for(int i = 1; i <= n; i++)
        {
            for(int j = i; j >= 1; j--)
            {
                if(dp[j - 1] && tmp.count(s.substr(j, i - j + 1)))
                {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }
};
```

## [467. 环绕字符串中唯一的子字符串 - 力扣（LeetCode）](https://leetcode.cn/problems/unique-substrings-in-wraparound-string/submissions/476614709/)

定义字符串 `base` 为一个 `"abcdefghijklmnopqrstuvwxyz"` 无限环绕的字符串，所以 `base` 看起来是这样的：

- `"...zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd...."`.

给你一个字符串 `s` ，请你统计并返回 `s` 中有多少 **不同****非空子串** 也在 `base` 中出现。

 

**示例 1：**

```
输入：s = "a"
输出：1
解释：字符串 s 的子字符串 "a" 在 base 中出现。
```

**示例 2：**

```
输入：s = "cac"
输出：2
解释：字符串 s 有两个子字符串 ("a", "c") 在 base 中出现。
```

**示例 3：**

```
输入：s = "zab"
输出：6
解释：字符串 s 有六个子字符串 ("z", "a", "b", "za", "ab", and "zab") 在 base 中出现。
```

 

**提示：**

- `1 <= s.length <= 105`
- s 由小写英文字母组成

解法一：动规

```cpp
class Solution {
public:
    int findSubstringInWraproundString(string s) {
        int n = s.size();     
        vector<int> dp(n, 1);
        dp[0] = 1;
        for(int i = 1; i < n; i++)
        {
            if((s[i - 1] + 1 == s[i]) || (s[i - 1] == 'z' && s[i] == 'a')) dp[i] += dp[i - 1];
        }
        int hash[26] = {0};
        for(int i = 0; i < n; i++)
        {
            int cur = s[i] - 'a';
            hash[cur] = max(hash[cur], dp[i]);
        }
        int ret = 0;
        for(int e : hash) ret += e;
        return ret;
    }
};
```

# DAY73

## [300. 最长递增子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-increasing-subsequence/description/)

给你一个整数数组 `nums` ，找到其中最长严格递增子序列的长度。

**子序列** 是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。例如，`[3,6,2,7]` 是数组 `[0,3,1,6,2,2,7]` 的子序列。

 

**示例 1：**

```
输入：nums = [10,9,2,5,3,7,101,18]
输出：4
解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
```

**示例 2：**

```
输入：nums = [0,1,0,3,2,3]
输出：4
```

**示例 3：**

```
输入：nums = [7,7,7,7,7,7,7]
输出：1
```

 

**提示：**

- `1 <= nums.length <= 2500`
- `-104 <= nums[i] <= 104`

解法一：动规

```cpp
class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int n = nums.size();
        vector<int> dp(n, 1);
        int ret = 1;
        for(int i = 1; i < n; i++)
        {
            for(int j = i - 1; j >= 0; j--)
            {
                if(nums[j] < nums[i]) dp[i] = max(dp[j] + 1, dp[i]);
            }
            ret = max(dp[i], ret);
        }
        return ret;
    }
};
```

## [376. 摆动序列 - 力扣（LeetCode）](https://leetcode.cn/problems/wiggle-subsequence/)

如果连续数字之间的差严格地在正数和负数之间交替，则数字序列称为 **摆动序列 。**第一个差（如果存在的话）可能是正数或负数。仅有一个元素或者含两个不等元素的序列也视作摆动序列。

- 例如， `[1, 7, 4, 9, 2, 5]` 是一个 **摆动序列** ，因为差值 `(6, -3, 5, -7, 3)` 是正负交替出现的。
- 相反，`[1, 4, 7, 2, 5]` 和 `[1, 7, 4, 5, 5]` 不是摆动序列，第一个序列是因为它的前两个差值都是正数，第二个序列是因为它的最后一个差值为零。

**子序列** 可以通过从原始序列中删除一些（也可以不删除）元素来获得，剩下的元素保持其原始顺序。

给你一个整数数组 `nums` ，返回 `nums` 中作为 **摆动序列** 的 **最长子序列的长度** 。

 

**示例 1：**

```
输入：nums = [1,7,4,9,2,5]
输出：6
解释：整个序列均为摆动序列，各元素之间的差值为 (6, -3, 5, -7, 3) 。
```

**示例 2：**

```
输入：nums = [1,17,5,10,13,15,10,5,16,8]
输出：7
解释：这个序列包含几个长度为 7 摆动序列。
其中一个是 [1, 17, 10, 13, 10, 16, 8] ，各元素之间的差值为 (16, -7, 3, -3, 6, -8) 。
```

**示例 3：**

```
输入：nums = [1,2,3,4,5,6,7,8,9]
输出：2
```

 

**提示：**

- `1 <= nums.length <= 1000`
- `0 <= nums[i] <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        int n = nums.size();
        vector<int> hash(n, 0), dp(n, 0);
        dp[0] = 1;
        for(int i = 1; i < n; i++)
        {
            if(nums[i] > nums[i - 1]) hash[i] = 1;
            else if(nums[i] < nums[i - 1]) hash[i] = -1;
            else hash[i] = hash[i - 1];
        }

        for(int i = 1; i < n; i++)
        {
            if(hash[i] != hash[i - 1]) dp[i] = dp[i - 1] + 1;
            else dp[i] = dp[i - 1];
        }
        return dp[n - 1];
    }
};
```

# DAY74

## [673. 最长递增子序列的个数 - 力扣（LeetCode）](https://leetcode.cn/problems/number-of-longest-increasing-subsequence/submissions/477072754/)

给定一个未排序的整数数组 `nums` ， *返回最长递增子序列的个数* 。

**注意** 这个数列必须是 **严格** 递增的。

 

**示例 1:**

```
输入: [1,3,5,4,7]
输出: 2
解释: 有两个最长递增子序列，分别是 [1, 3, 4, 7] 和[1, 3, 5, 7]。
```

**示例 2:**

```
输入: [2,2,2,2,2]
输出: 5
解释: 最长递增子序列的长度是1，并且存在5个子序列的长度为1，因此输出5。
```

 

**提示:** 



- `1 <= nums.length <= 2000`
- `-106 <= nums[i] <= 106`

解法一：动规

```cpp
class Solution {
public:
    int findNumberOfLIS(vector<int>& nums) {
        int n = nums.size();
        vector<int> len(n, 1), count(n, 1);
        int retlen = 1, retcount = 1;
        for(int i = 1; i < n; i++)
        {
            for(int j = i - 1; j >= 0; j--)
            {
                if(nums[i] > nums[j]) 
                {
                    if(len[j] + 1 == len[i]) count[i] += count[j];
                    else if(len[j] + 1 > len[i]) len[i] = len[j] + 1, count[i] = count[j]; 
                }
            }
            if(retlen == len[i]) retcount += count[i];
            else if(retlen < len[i]) retlen = len[i], retcount = count[i];
        }
        return retcount;
    }
};
```

## [646. 最长数对链 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-length-of-pair-chain/submissions/477078628/)

给你一个由 `n` 个数对组成的数对数组 `pairs` ，其中 `pairs[i] = [lefti, righti]` 且 `lefti < righti` 。

现在，我们定义一种 **跟随** 关系，当且仅当 `b < c` 时，数对 `p2 = [c, d]` 才可以跟在 `p1 = [a, b]` 后面。我们用这种形式来构造 **数对链** 。

找出并返回能够形成的 **最长数对链的长度** 。

你不需要用到所有的数对，你可以以任何顺序选择其中的一些数对来构造。

 

**示例 1：**

```
输入：pairs = [[1,2], [2,3], [3,4]]
输出：2
解释：最长的数对链是 [1,2] -> [3,4] 。
```

**示例 2：**

```
输入：pairs = [[1,2],[7,8],[4,5]]
输出：3
解释：最长的数对链是 [1,2] -> [4,5] -> [7,8] 。
```

 

**提示：**

- `n == pairs.length`
- `1 <= n <= 1000`
- `-1000 <= lefti < righti <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int findLongestChain(vector<vector<int>>& pairs) {
        int n = pairs.size();
        sort(pairs.begin(), pairs.end());
        int ret = 1;
        vector<int> dp(n, 1);
        for(int i = 1; i < n; i++)
        {
            for(int j = i - 1; j >= 0; j--)
            {
                if(pairs[i][0] > pairs[j][1]) 
                {
                    dp[i] = max(dp[j] + 1, dp[i]);
                    break;
                }
            }
            ret = max(ret, dp[i]);
        }
        return ret;
    }
};
```

# DAY75

## [1218. 最长定差子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-arithmetic-subsequence-of-given-difference/)

给你一个整数数组 `arr` 和一个整数 `difference`，请你找出并返回 `arr` 中最长等差子序列的长度，该子序列中相邻元素之间的差等于 `difference` 。

**子序列** 是指在不改变其余元素顺序的情况下，通过删除一些元素或不删除任何元素而从 `arr` 派生出来的序列。

 

**示例 1：**

```
输入：arr = [1,2,3,4], difference = 1
输出：4
解释：最长的等差子序列是 [1,2,3,4]。
```

**示例 2：**

```
输入：arr = [1,3,5,7], difference = 1
输出：1
解释：最长的等差子序列是任意单个元素。
```

**示例 3：**

```
输入：arr = [1,5,7,8,5,3,4,2,1], difference = -2
输出：4
解释：最长的等差子序列是 [7,5,3,1]。
```

 

**提示：**

- `1 <= arr.length <= 105`
- `-104 <= arr[i], difference <= 104`

解法一：动规

```cpp
class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference) {
        int n = arr.size();
        unordered_map<int, int> hash;
        hash[arr[0]] = 1;
        int ret = 1;
        for(int i = 1; i < n; i++)
        {
            hash[arr[i]] = hash[arr[i] - difference] + 1;
            ret = max(ret, hash[arr[i]]);
        }
        return ret;
    }
};
```

## [LCR 093. 最长的斐波那契子序列的长度 - 力扣（LeetCode）](https://leetcode.cn/problems/Q91FMA/)

如果序列 `X_1, X_2, ..., X_n` 满足下列条件，就说它是 *斐波那契式* 的：

- `n >= 3`
- 对于所有 `i + 2 <= n`，都有 `X_i + X_{i+1} = X_{i+2}`

给定一个**严格递增**的正整数数组形成序列 `arr` ，找到 `arr` 中最长的斐波那契式的子序列的长度。如果一个不存在，返回 0 。

*（回想一下，子序列是从原序列 `arr` 中派生出来的，它从 `arr` 中删掉任意数量的元素（也可以不删），而不改变其余元素的顺序。例如， `[3, 5, 8]` 是 `[3, 4, 5, 6, 7, 8]` 的一个子序列）*

 



**示例 1：**

```
输入: arr = [1,2,3,4,5,6,7,8]
输出: 5
解释: 最长的斐波那契式子序列为 [1,2,3,5,8] 。
```

**示例 2：**

```
输入: arr = [1,3,7,11,12,14,18]
输出: 3
解释: 最长的斐波那契式子序列有 [1,11,12]、[3,11,14] 以及 [7,11,18] 。
```

 

**提示：**

- `3 <= arr.length <= 1000`
- `1 <= arr[i] < arr[i + 1] <= 10^9`

解法一：动规

```cpp
class Solution {
public:
    int lenLongestFibSubseq(vector<int>& arr) {
        int n = arr.size();
        unordered_map<int, int> hash;
        vector<vector<int>> dp(n, vector<int>(n, 2));
        for(int i = 0; i < n; i++) hash[arr[i]] = i;
        int ret = 0;
        for(int i = 2; i < n; i++)
        {
            for(int j = 1; j < i; j++)
            {
                int tmp = arr[i] - arr[j];
                if(tmp < arr[j] && hash.count(tmp)) 
                {
                    dp[j][i] = dp[hash[tmp]][j] + 1;
                    ret = max(ret, dp[j][i]); 
                }                          
            }          
        }
        return ret <= 2 ? 0 : ret;
    }
};
```

# DAY 76

## [1027. 最长等差数列 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-arithmetic-subsequence/)

给你一个整数数组 `nums`，返回 `nums` 中最长等差子序列的**长度**。

回想一下，`nums` 的子序列是一个列表 `nums[i1], nums[i2], ..., nums[ik]` ，且 `0 <= i1 < i2 < ... < ik <= nums.length - 1`。并且如果 `seq[i+1] - seq[i]`( `0 <= i < seq.length - 1`) 的值都相同，那么序列 `seq` 是等差的。

 

**示例 1：**

```
输入：nums = [3,6,9,12]
输出：4
解释： 
整个数组是公差为 3 的等差数列。
```

**示例 2：**

```
输入：nums = [9,4,7,2,10]
输出：3
解释：
最长的等差子序列是 [4,7,10]。
```

**示例 3：**

```
输入：nums = [20,1,15,3,10,5,8]
输出：4
解释：
最长的等差子序列是 [20,15,10,5]。
```

 

**提示：**

- `2 <= nums.length <= 1000`
- `0 <= nums[i] <= 500`

解法一：动规

```cpp
class Solution {
public:
    int longestArithSeqLength(vector<int>& nums) {
        int n = nums.size();
        vector<vector<int>> dp(n, vector<int>(n, 2));
        unordered_map<int, int> hash;
        hash[nums[0]] = 0;
        int ret = 2;
        for(int i = 1; i < n; i++)
        {
            for(int j = i + 1; j < n; j++)
            {
                int tmp = 2 * nums[i] - nums[j];
                if(hash.count(tmp)) dp[i][j] = dp[hash[tmp]][i] + 1;
                ret = max(ret, dp[i][j]);
            }
            hash[nums[i]] = i;
        }
        return ret;
    }
};
```

## [446. 等差数列划分 II - 子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/arithmetic-slices-ii-subsequence/description/)

给你一个整数数组 `nums` ，返回 `nums` 中所有 **等差子序列** 的数目。

如果一个序列中 **至少有三个元素** ，并且任意两个相邻元素之差相同，则称该序列为等差序列。

- 例如，`[1, 3, 5, 7, 9]`、`[7, 7, 7, 7]` 和 `[3, -1, -5, -9]` 都是等差序列。
- 再例如，`[1, 1, 2, 5, 7]` 不是等差序列。

数组中的子序列是从数组中删除一些元素（也可能不删除）得到的一个序列。

- 例如，`[2,5,10]` 是 `[1,2,1,***2***,4,1,***5\***,***10***]` 的一个子序列。

题目数据保证答案是一个 **32-bit** 整数。

 

**示例 1：**

```
输入：nums = [2,4,6,8,10]
输出：7
解释：所有的等差子序列为：
[2,4,6]
[4,6,8]
[6,8,10]
[2,4,6,8]
[4,6,8,10]
[2,4,6,8,10]
[2,6,10]
```

**示例 2：**

```
输入：nums = [7,7,7,7,7]
输出：16
解释：数组中的任意子序列都是等差子序列。
```

 

**提示：**

- `1 <= nums.length <= 1000`
- `-231 <= nums[i] <= 231 - 1`

解法一：动规

```cpp
class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        int n = nums.size();
        vector<vector<int>> dp(n, vector<int>(n, 0));
        unordered_map<long long, vector<int>> hash;
        for(int i = 0; i < n; i++) hash[nums[i]].push_back(i);
        int sum = 0;
        for(int i = 2; i < n; i++)
        {
            for(int j = 1; j < i; j++)
            {
                long long tmp = (long long)2 * nums[j] - nums[i];
                if(hash.count(tmp))
                {
                    for(int e : hash[tmp])
                    {
                        if(e < j) dp[j][i] += dp[e][j] + 1;
                    }
                    sum += dp[j][i];
                }
            }
        }
        return sum;
    }
};
```

# DAY77

## [LCR 020. 回文子串 - 力扣（LeetCode）](https://leetcode.cn/problems/a7VOhD/description/)

给定一个字符串 `s` ，请计算这个字符串中有多少个回文子字符串。

具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被视作不同的子串。

 

**示例 1：**

```
输入：s = "abc"
输出：3
解释：三个回文子串: "a", "b", "c"
```

**示例 2：**

```
输入：s = "aaa"
输出：6
解释：6个回文子串: "a", "a", "a", "aa", "aa", "aaa"
```

 

**提示：**

- `1 <= s.length <= 1000`
- `s` 由小写英文字母组成

解法一：动规

```cpp
class Solution {
public:
    int countSubstrings(string s) {
        int n = s.size();
        vector<int> dp(n, 1);
        vector<vector<int>> hash(n, vector<int>(n, 0));
        int sum = 1;
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j < i; j++)
            {
                if(s[i] == s[j] && (hash[j + 1][i - 1] || j + 1 == i || j + 1 == i - 1)) 
                {
                    dp[i] += 1;
                    hash[j][i] = 1;
                }
            }
            sum += dp[i];
        }
        return sum;
    }
};
```

## [409. 最长回文串 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-palindrome/description/)

给定一个包含大写字母和小写字母的字符串 `s` ，返回 *通过这些字母构造成的 **最长的回文串*** 。

在构造过程中，请注意 **区分大小写** 。比如 `"Aa"` 不能当做一个回文字符串。

 

**示例 1:**

```
输入:s = "abccccdd"
输出:7
解释:
我们可以构造的最长的回文串是"dccaccd", 它的长度是 7。
```

**示例 2:**

```
输入:s = "a"
输出:1
```

**示例 3：**

```
输入:s = "aaaaaccc"
输出:7
```

 

**提示:**

- `1 <= s.length <= 2000`
- `s` 只由小写 **和/或** 大写英文字母组成

解法一：动规

```cpp
class Solution {
public:
    int longestPalindrome(string s) {
        int n = s.size();
        unordered_map<int, int> hash;
        for(int i = 0; i < n; i++) hash[s[i]] += 1;
        int sum = 0, flag = 0;
        for(int i = 0; i < n; i++)
        {
            while(hash[s[i]] >= 2)
            {
                sum += 2;
                hash[s[i]] -= 2;
            }
            if(hash[s[i]] > 0) flag = 1;
        }
        return sum + flag;
    }
};
```



## [5. 最长回文子串 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-palindromic-substring/description/)

给你一个字符串 `s`，找到 `s` 中最长的回文子串。

如果字符串的反序与原始字符串相同，则该字符串称为回文字符串。

 

**示例 1：**

```
输入：s = "babad"
输出："bab"
解释："aba" 同样是符合题意的答案。
```

**示例 2：**

```
输入：s = "cbbd"
输出："bb"
```

 

**提示：**

- `1 <= s.length <= 1000`
- `s` 仅由数字和英文字母组成

解法一：动规

```cpp
class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.size();
        vector<vector<int>> hash(n, vector<int>(n, 0));
        int sum = 1;
        string ret = s.substr(0, 1);
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j < i; j++)
            {
                if(s[j] == s[i] && (hash[j + 1][i - 1] || j + 1 == i || j + 1 == i - 1))
                {
                    hash[j][i] = i - j + 1;
                }
                if(hash[j][i] > sum)
                {
                    sum = hash[j][i];
                    ret = s.substr(j, hash[j][i]);
                }
            }
        }
        return ret;
    }
};
```

# DAY78

## [1745. 分割回文串 IV - 力扣（LeetCode）](https://leetcode.cn/problems/palindrome-partitioning-iv/description/)

给你一个字符串 `s` ，如果可以将它分割成三个 **非空** 回文子字符串，那么返回 `true` ，否则返回 `false` 。

当一个字符串正着读和反着读是一模一样的，就称其为 **回文字符串** 。

 

**示例 1：**

```
输入：s = "abcbdd"
输出：true
解释："abcbdd" = "a" + "bcb" + "dd"，三个子字符串都是回文的。
```

**示例 2：**

```
输入：s = "bcbddxy"
输出：false
解释：s 没办法被分割成 3 个回文子字符串。
```

 

**提示：**

- `3 <= s.length <= 2000`
- `s` 只包含小写英文字母。

解法一：动规

```cpp
class Solution {
public:
    bool checkPartitioning(string s) {
        int n = s.size();
        if(n < 3) return false;
        if(n == 3) return true;
        vector<vector<int>> hash(n, vector<int>(n, 0));
        hash[0][0] = 1;
        for(int i = 1; i < n; i++)
        {
            hash[i][i] = 1;
            for(int j = 0; j < i; j++)
            {
                int row = j + 1, col = i - 1;
                if(s[j] == s[i] && (hash[row][col] || row == i || row == col))
                {
                    hash[j][i] = 1;
                }
            }
        }
        for(int j = 1; j < n - 1; j++)
        {
            for(int i = j; i < n - 1; i++)
            {
                if(hash[0][j - 1] && hash[j][i] && hash[i + 1][n - 1]) return true;
            }
        }
        return false;
    }
};
```

## [132. 分割回文串 II - 力扣（LeetCode）](https://leetcode.cn/problems/palindrome-partitioning-ii/description/)

给你一个字符串 `s`，请你将 `s` 分割成一些子串，使每个子串都是回文。

返回符合要求的 **最少分割次数** 。

 

**示例 1：**

```
输入：s = "aab"
输出：1
解释：只需一次分割就可将 s 分割成 ["aa","b"] 这样两个回文子串。
```

**示例 2：**

```
输入：s = "a"
输出：0
```

**示例 3：**

```
输入：s = "ab"
输出：1
```

 

**提示：**

- `1 <= s.length <= 2000`
- `s` 仅由小写英文字母组成

解法一：动规

```cpp
class Solution {
public:
    int minCut(string s) 
    {
        int n = s.size();
        vector<vector<int>> hash(n, vector<int>(n, 0));
        vector<int> dp(n, INT_MAX);
        hash[0][0] = 1;
        for(int i = 1; i < n; i++)
        {
            hash[i][i] = 1;
            for(int j = 0; j < i; j++)
            {
                if(s[j] == s[i] && (j + 1 == i || j + 1 == i - 1 || hash[j + 1][i - 1])) hash[j][i] = 1;
            }
        }
        for(int i = 0; i < n; i++)
        {
            if(hash[0][i]) dp[i] = 0;
            else 
            {
                for(int j = 1; j <= i; j++)
                {
                    if(hash[j][i]) dp[i] = min(dp[i], dp[j - 1] + 1);
                }
            }
        }
        return dp[n - 1];
    }
};
```

# DAY79

## [516. 最长回文子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-palindromic-subsequence/description/)

给你一个字符串 `s` ，找出其中最长的回文子序列，并返回该序列的长度。

子序列定义为：不改变剩余字符顺序的情况下，删除某些字符或者不删除任何字符形成的一个序列。

 

**示例 1：**

```
输入：s = "bbbab"
输出：4
解释：一个可能的最长回文子序列为 "bbbb" 。
```

**示例 2：**

```
输入：s = "cbbd"
输出：2
解释：一个可能的最长回文子序列为 "bb" 。
```

 

**提示：**

- `1 <= s.length <= 1000`
- `s` 仅由小写英文字母组成

解法一：动规

```cpp
class Solution {
public:
    int longestPalindromeSubseq(string s) {
        int n = s.size();
        vector<vector<int>> hash(n, vector<int>(n, 0));

        for(int i = n - 1; i >= 0; i--)
        {
            hash[i][i] = 1;
            for(int j = i + 1; j < n; j++)
            {
                if(s[i] == s[j]) hash[i][j] = hash[i + 1][j - 1] + 2;
                else hash[i][j] = max(hash[i + 1][j], hash[i][j - 1]);
            }
        }
        return hash[0][n - 1];
    }
};
```

## [1312. 让字符串成为回文串的最少插入次数 - 力扣（LeetCode）](https://leetcode.cn/problems/minimum-insertion-steps-to-make-a-string-palindrome/description/)

给你一个字符串 `s` ，每一次操作你都可以在字符串的任意位置插入任意字符。

请你返回让 `s` 成为回文串的 **最少操作次数** 。

「回文串」是正读和反读都相同的字符串。

 

**示例 1：**

```
输入：s = "zzazz"
输出：0
解释：字符串 "zzazz" 已经是回文串了，所以不需要做任何插入操作。
```

**示例 2：**

```
输入：s = "mbadm"
输出：2
解释：字符串可变为 "mbdadbm" 或者 "mdbabdm" 。
```

**示例 3：**

```
输入：s = "leetcode"
输出：5
解释：插入 5 个字符后字符串变为 "leetcodocteel" 。
```

 

**提示：**

- `1 <= s.length <= 500`
- `s` 中所有字符都是小写字母。

解法一：动规

```cpp
class Solution {
public:
    int minInsertions(string s) {
        int n = s.size();
        vector<vector<int>> dp(n, vector<int>(n, 0));

        for(int i = n - 1; i >= 0; i--)
        {
            for(int j = i + 1; j < n; j++)
            {
                if(s[i] == s[j])
                {
                    if(i + 1 < j) dp[i][j] = dp[i + 1][j - 1];
                }
                else 
                {
                    dp[i][j] = min(dp[i + 1][j], dp[i][j - 1]) + 1;
                }
            }
        }
        return dp[0][n - 1];
    }
};
```

# DAY80

## [LCR 095. 最长公共子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/qJnOS7/description/)

给定两个字符串 `text1` 和 `text2`，返回这两个字符串的最长 **公共子序列** 的长度。如果不存在 **公共子序列** ，返回 `0` 。

一个字符串的 **子序列** 是指这样一个新的字符串：它是由原字符串在不改变字符的相对顺序的情况下删除某些字符（也可以不删除任何字符）后组成的新字符串。

- 例如，`"ace"` 是 `"abcde"` 的子序列，但 `"aec"` 不是 `"abcde"` 的子序列。

两个字符串的 **公共子序列** 是这两个字符串所共同拥有的子序列。

 

**示例 1：**

```
输入：text1 = "abcde", text2 = "ace" 
输出：3  
解释：最长公共子序列是 "ace" ，它的长度为 3 。
```

**示例 2：**

```
输入：text1 = "abc", text2 = "abc"
输出：3
解释：最长公共子序列是 "abc" ，它的长度为 3 。
```

**示例 3：**

```
输入：text1 = "abc", text2 = "def"
输出：0
解释：两个字符串没有公共子序列，返回 0 。
```

 

**提示：**

- `1 <= text1.length, text2.length <= 1000`
- `text1` 和 `text2` 仅由小写英文字符组成。

解法一：动规

```cpp
class Solution {
public:
    int longestCommonSubsequence(string text1, string text2) {
        int m =  text1.size(), n = text2.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        text1.insert(0, " "), text2.insert(0, " ");
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(text1[i] == text2[j]) dp[i][j] = dp[i - 1][j - 1] + 1;
                else dp[i][j] = max(dp[i - 1][j], max(dp[i][j - 1], dp[i - 1][j - 1]));
            }
        }
        return dp[m][n];
    }
};
```

## [1035. 不相交的线 - 力扣（LeetCode）](https://leetcode.cn/problems/uncrossed-lines/description/)

在两条独立的水平线上按给定的顺序写下 `nums1` 和 `nums2` 中的整数。

现在，可以绘制一些连接两个数字 `nums1[i]` 和 `nums2[j]` 的直线，这些直线需要同时满足满足：

-  `nums1[i] == nums2[j]`
- 且绘制的直线不与任何其他连线（非水平线）相交。

请注意，连线即使在端点也不能相交：每个数字只能属于一条连线。

以这种方法绘制线条，并返回可以绘制的最大连线数。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2019/04/26/142.png)

```
输入：nums1 = [1,4,2], nums2 = [1,2,4]
输出：2
解释：可以画出两条不交叉的线，如上图所示。 
但无法画出第三条不相交的直线，因为从 nums1[1]=4 到 nums2[2]=4 的直线将与从 nums1[2]=2 到 nums2[1]=2 的直线相交。
```

**示例 2：**

```
输入：nums1 = [2,5,1,2,5], nums2 = [10,5,2,1,5,2]
输出：3
```

**示例 3：**

```
输入：nums1 = [1,3,7,1,7,5], nums2 = [1,9,2,5,1]
输出：2
```

 

**提示：**

- `1 <= nums1.length, nums2.length <= 500`
- `1 <= nums1[i], nums2[j] <= 2000`

解法一：动规

```cpp
class Solution {
public:
    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
        int m = nums1.size(), n = nums2.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        nums1.insert(nums1.begin(), 0), nums2.insert(nums2.begin(), 0);

        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(nums1[i] == nums2[j]) dp[i][j] = dp[i - 1][j - 1] + 1;
                else dp[i][j] = max(dp[i - 1][j], max(dp[i][j - 1], dp[i - 1][j - 1]));
            }
        }
        return dp[m][n];
    }
};
```

# DAY81

## [LCR 097. 不同的子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/21dk04/description/)

给定一个字符串 `s` 和一个字符串 `t` ，计算在 `s` 的子序列中 `t` 出现的个数。

字符串的一个 **子序列** 是指，通过删除一些（也可以不删除）字符且不干扰剩余字符相对位置所组成的新字符串。（例如，`"ACE"` 是 `"ABCDE"` 的一个子序列，而 `"AEC"` 不是）

题目数据保证答案符合 32 位带符号整数范围。

 

**示例 1：**

```
输入：s = "rabbbit", t = "rabbit"
输出：3
解释：
如下图所示, 有 3 种可以从 s 中得到 "rabbit" 的方案。
rabbbit
rabbbit
rabbbit
```

**示例 2：**

```
输入：s = "babgbag", t = "bag"
输出：5
解释：
如下图所示, 有 5 种可以从 s 中得到 "bag" 的方案。 
babgbag
babgbag
babgbag
babgbag
babgbag
```

 

**提示：**

- `0 <= s.length, t.length <= 1000`
- `s` 和 `t` 由英文字母组成

解法一：动规

```cpp
class Solution {
public:
    int numDistinct(string s, string t) {
        int m = s.size(), n = t.size();
        vector<vector<double>> dp(m + 1, vector<double>(n + 1, 0));
        for(int i = 0; i < m; i++) dp[i][0] = 1;

        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                dp[i][j] += dp[i - 1][j];
                if(s[i - 1] == t[j - 1]) dp[i][j] += dp[i - 1][j - 1];
            }
        }
        return dp[m][n];
    }
};
```

## [44. 通配符匹配 - 力扣（LeetCode）](https://leetcode.cn/problems/wildcard-matching/description/)

给你一个输入字符串 (`s`) 和一个字符模式 (`p`) ，请你实现一个支持 `'?'` 和 `'*'` 匹配规则的通配符匹配：

- `'?'` 可以匹配任何单个字符。
- `'*'` 可以匹配任意字符序列（包括空字符序列）。

判定匹配成功的充要条件是：字符模式必须能够 **完全匹配** 输入字符串（而不是部分匹配）。

 

**示例 1：**

```
输入：s = "aa", p = "a"
输出：false
解释："a" 无法匹配 "aa" 整个字符串。
```

**示例 2：**

```
输入：s = "aa", p = "*"
输出：true
解释：'*' 可以匹配任意字符串。
```

**示例 3：**

```
输入：s = "cb", p = "?a"
输出：false
解释：'?' 可以匹配 'c', 但第二个 'a' 无法匹配 'b'。
```

 

**提示：**

- `0 <= s.length, p.length <= 2000`
- `s` 仅由小写英文字母组成
- `p` 仅由小写英文字母、`'?'` 或 `'*'` 组成

解法一：动规

```cpp
class Solution {
public:
    bool isMatch(string s, string p) {
        int m = s.size(), n = p.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        dp[0][0] = 1;
        for(int i = 0; i < n; i++) 
        {
            if(p[i] == '*') dp[0][i + 1] = 1;
            else break;
        }
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if((s[i - 1] == p[j - 1] || p[j - 1] == '?') && dp[i - 1][j - 1])
                {
                    dp[i][j] = 1;
                }
                else if(p[j - 1] == '*') dp[i][j] = dp[i][j - 1] || dp[i - 1][j];
            }
        }
        return dp[m][n];
    }
};
```

# DAY82

## [10. 正则表达式匹配 - 力扣（LeetCode）](https://leetcode.cn/problems/regular-expression-matching/description/)

给你一个字符串 `s` 和一个字符规律 `p`，请你来实现一个支持 `'.'` 和 `'*'` 的正则表达式匹配。

- `'.'` 匹配任意单个字符
- `'*'` 匹配零个或多个前面的那一个元素

所谓匹配，是要涵盖 **整个** 字符串 `s`的，而不是部分字符串。

 

**示例 1：**

```
输入：s = "aa", p = "a"
输出：false
解释："a" 无法匹配 "aa" 整个字符串。
```

**示例 2:**

```
输入：s = "aa", p = "a*"
输出：true
解释：因为 '*' 代表可以匹配零个或多个前面的那一个元素, 在这里前面的元素就是 'a'。因此，字符串 "aa" 可被视为 'a' 重复了一次。
```

**示例 3：**

```
输入：s = "ab", p = ".*"
输出：true
解释：".*" 表示可匹配零个或多个（'*'）任意字符（'.'）。
```

 

**提示：**

- `1 <= s.length <= 20`
- `1 <= p.length <= 20`
- `s` 只包含从 `a-z` 的小写字母。
- `p` 只包含从 `a-z` 的小写字母，以及字符 `.` 和 `*`。
- 保证每次出现字符 `*` 时，前面都匹配到有效的字符

解法一：动规

```cpp
class Solution {
public:
    bool isMatch(string s, string p) {
        int m = s.size(), n = p.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        s = ' ' + s, p = ' ' + p;
        dp[0][0] = 1;
        for(int i = 2; i <= n; i += 2) 
        {
            if(p[i] == '*') dp[0][i] = 1;
            else break;
        }
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(p[j] == '*') dp[i][j] = dp[i][j - 2] || (p[j - 1] == '.' || p[j - 1] == s[i]) && dp[i - 1][j];
                else 
                {
                    dp[i][j] = (s[i] == p[j] || p[j] == '.') && dp[i - 1][j - 1]; 
                }
            }
        }
        return dp[m][n];
    }
};
```

## [LCR 096. 交错字符串 - 力扣（LeetCode）](https://leetcode.cn/problems/IY6buf/description/)

给定三个字符串 `s1`、`s2`、`s3`，请判断 `s3` 能不能由 `s1` 和 `s2` **交织（交错）** 组成。

两个字符串 `s` 和 `t` **交织** 的定义与过程如下，其中每个字符串都会被分割成若干 **非空** 子字符串：

- `s = s1 + s2 + ... + sn`
- `t = t1 + t2 + ... + tm`
- `|n - m| <= 1`
- **交织** 是 `s1 + t1 + s2 + t2 + s3 + t3 + ...` 或者 `t1 + s1 + t2 + s2 + t3 + s3 + ...`

**提示：**`a + b` 意味着字符串 `a` 和 `b` 连接。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/09/02/interleave.jpg)

```
输入：s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
输出：true
```

**示例 2：**

```
输入：s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc"
输出：false
```

**示例 3：**

```
输入：s1 = "", s2 = "", s3 = ""
输出：true
```

 

**提示：**

- `0 <= s1.length, s2.length <= 100`
- `0 <= s3.length <= 200`
- `s1`、`s2`、和 `s3` 都由小写英文字母组成

解法一：动规

```cpp
class Solution {
public:
    bool isInterleave(string s1, string s2, string s3) {
        int m = s1.size(), n = s2.size();
        if(m + n != s3.size()) return false;

        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        s1 = ' ' + s1, s2 = ' ' + s2, s3 = ' ' + s3;
        dp[0][0] = 1;
        for(int i = 1; i <= m; i++)
            if(s1[i] == s3[i]) dp[i][0] = 1;
            else break;
        for(int j = 1; j <= n; j++)
            if(s2[j] == s3[j]) dp[0][j] = 1;
            else break;

        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                dp[i][j] = (s1[i] == s3[i + j] && dp[i - 1][j]) || (s2[j] == s3[i + j] && dp[i][j - 1]);
            }
        }
        return dp[m][n];
    }
};
```

# DAY83

## [712. 两个字符串的最小ASCII删除和 - 力扣（LeetCode）](https://leetcode.cn/problems/minimum-ascii-delete-sum-for-two-strings/description/)

给定两个字符串`s1` 和 `s2`，返回 *使两个字符串相等所需删除字符的 **ASCII** 值的最小和* 。

 

**示例 1:**

```
输入: s1 = "sea", s2 = "eat"
输出: 231
解释: 在 "sea" 中删除 "s" 并将 "s" 的值(115)加入总和。
在 "eat" 中删除 "t" 并将 116 加入总和。
结束时，两个字符串相等，115 + 116 = 231 就是符合条件的最小和。
```

**示例 2:**

```
输入: s1 = "delete", s2 = "leet"
输出: 403
解释: 在 "delete" 中删除 "dee" 字符串变成 "let"，
将 100[d]+101[e]+101[e] 加入总和。在 "leet" 中删除 "e" 将 101[e] 加入总和。
结束时，两个字符串都等于 "let"，结果即为 100+101+101+101 = 403 。
如果改为将两个字符串转换为 "lee" 或 "eet"，我们会得到 433 或 417 的结果，比答案更大。
```

 

**提示:**

- `0 <= s1.length, s2.length <= 1000`
- `s1` 和 `s2` 由小写英文字母组成

解法一：动规

```cpp
class Solution {
public:
    int minimumDeleteSum(string s1, string s2) {
        int m = s1.size(), n = s2.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        s1 = ' ' + s1, s2 = ' ' + s2;
        dp[0][0] = 0;
        for(int i = 1; i <= m; i++) dp[i][0] = s1[i] + dp[i - 1][0];
        for(int j = 1; j <= n; j++) dp[0][j] = s2[j] + dp[0][j - 1];

        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(s1[i] == s2[j]) dp[i][j] = dp[i - 1][j - 1];
                else dp[i][j] = min(dp[i - 1][j] + s1[i], dp[i][j - 1] + s2[j]);
            }
        }
        return dp[m][n];
    }
};
```

## [718. 最长重复子数组 - 力扣（LeetCode）](https://leetcode.cn/problems/maximum-length-of-repeated-subarray/description/)

给两个整数数组 `nums1` 和 `nums2` ，返回 *两个数组中 **公共的** 、长度最长的子数组的长度* 。

 

**示例 1：**

```
输入：nums1 = [1,2,3,2,1], nums2 = [3,2,1,4,7]
输出：3
解释：长度最长的公共子数组是 [3,2,1] 。
```

**示例 2：**

```
输入：nums1 = [0,0,0,0,0], nums2 = [0,0,0,0,0]
输出：5
```

 

**提示：**

- `1 <= nums1.length, nums2.length <= 1000`
- `0 <= nums1[i], nums2[i] <= 100`

解法一：动规

```cpp
class Solution {
public:
    int findLength(vector<int>& nums1, vector<int>& nums2) {
        int m = nums1.size(), n = nums2.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        int ret = 0;
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(nums1[i - 1] == nums2[j - 1]) 
                {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                    ret = max(ret, dp[i][j]);
                }    
            }
        }
        return ret;
    }
};
```

# DAY84

## [背包问题_哔哩哔哩笔试题_牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/3ee42c9155c340588729995561ace594)

有N件物品和一个容量为V的背包。第i件物品的价值是C[i]，重量是W[i]。求解将哪些物品装入背包可使价值总和最大。 

**输入描述:**

```
输入第一行数 N V (1 <=N <=500) (1<= V <= 10000)

输入 N行 两个数字 代表 C W (1 <= C <= 50000, 1 <= W <=1000)
```

**输出描述:**

```
输出最大价值
```

示例1

输入

```
5 10
8 6
10 4
4 2
5 4
5 3
```

输出

```
19
```

解法一：动规

```cpp
#include <iostream>
using namespace std;

const int N = 501;
int C[N], W[N];
int dp[N][N];

int main() 
{
    int n, v;
    cin >> n >> v;
    
    for(int i = 1; i <= n; i++) cin >> C[i] >> W[i];

    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= v; j++)
        {
            dp[i][j] = dp[i - 1][j];
            if(j - W[i] >= 0) 
                dp[i][j] = max(dp[i][j], dp[i - 1][j - W[i]] + C[i]);
        }
    cout << dp[n][v] << endl;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [LCR 101. 分割等和子集 - 力扣（LeetCode）](https://leetcode.cn/problems/NUPfPr/description/)

给定一个非空的正整数数组 `nums` ，请判断能否将这些数字分成元素和相等的两部分。

 

**示例 1：**

```
输入：nums = [1,5,11,5]
输出：true
解释：nums 可以分割成 [1, 5, 5] 和 [11] 。
```

**示例 2：**

```
输入：nums = [1,2,3,5]
输出：false
解释：nums 不可以分为和相等的两部分
```

 



**提示：**

- `1 <= nums.length <= 200`
- `1 <= nums[i] <= 100`

解法一：动规

```cpp
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int n = nums.size(), sum = 0;
        for(int i = 0; i < n; i++) sum += nums[i];
        if(sum % 2) return false;
        int target = sum / 2;
        vector<int> dp(target + 1, 0);
        dp[0] = 1;
        for(int i = 1; i <= n; i++)
            for(int j = target; j >= nums[i - 1]; j--)
                dp[j] = dp[j] || dp[j - nums[i - 1]]; 
                
        return dp[target];
    }
};
```

# DAY 85

## [494. 目标和 - 力扣（LeetCode）](https://leetcode.cn/problems/target-sum/description/)

给你一个非负整数数组 `nums` 和一个整数 `target` 。

向数组中的每个整数前添加 `'+'` 或 `'-'` ，然后串联起所有整数，可以构造一个 **表达式** ：

- 例如，`nums = [2, 1]` ，可以在 `2` 之前添加 `'+'` ，在 `1` 之前添加 `'-'` ，然后串联起来得到表达式 `"+2-1"` 。

返回可以通过上述方法构造的、运算结果等于 `target` 的不同 **表达式** 的数目。

 

**示例 1：**

```
输入：nums = [1,1,1,1,1], target = 3
输出：5
解释：一共有 5 种方法让最终目标和为 3 。
-1 + 1 + 1 + 1 + 1 = 3
+1 - 1 + 1 + 1 + 1 = 3
+1 + 1 - 1 + 1 + 1 = 3
+1 + 1 + 1 - 1 + 1 = 3
+1 + 1 + 1 + 1 - 1 = 3
```

**示例 2：**

```
输入：nums = [1], target = 1
输出：1
```

 

**提示：**

- `1 <= nums.length <= 20`
- `0 <= nums[i] <= 1000`
- `0 <= sum(nums[i]) <= 1000`
- `-1000 <= target <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int target) {
        int n = nums.size();
        int sum = 0;
        for(int e : nums) sum += e;
        int tmp = (target + sum) / 2;
        if(tmp < 0 || (target + sum) % 2) return 0;

        vector<int> dp(tmp + 1, 0);
        dp[0] = 1;

        for(int i = 1; i <= n; i++)
            for(int j = tmp; j >= nums[i - 1]; j--)
                dp[j] += dp[j - nums[i - 1]];
        return dp[tmp];
    }
};
```

## [1049. 最后一块石头的重量 II - 力扣（LeetCode）](https://leetcode.cn/problems/last-stone-weight-ii/description/)

有一堆石头，用整数数组 `stones` 表示。其中 `stones[i]` 表示第 `i` 块石头的重量。

每一回合，从中选出**任意两块石头**，然后将它们一起粉碎。假设石头的重量分别为 `x` 和 `y`，且 `x <= y`。那么粉碎的可能结果如下：

- 如果 `x == y`，那么两块石头都会被完全粉碎；
- 如果 `x != y`，那么重量为 `x` 的石头将会完全粉碎，而重量为 `y` 的石头新重量为 `y-x`。

最后，**最多只会剩下一块** 石头。返回此石头 **最小的可能重量** 。如果没有石头剩下，就返回 `0`。

 

**示例 1：**

```
输入：stones = [2,7,4,1,8,1]
输出：1
解释：
组合 2 和 4，得到 2，所以数组转化为 [2,7,1,8,1]，
组合 7 和 8，得到 1，所以数组转化为 [2,1,1,1]，
组合 2 和 1，得到 1，所以数组转化为 [1,1,1]，
组合 1 和 1，得到 0，所以数组转化为 [1]，这就是最优值。
```

**示例 2：**

```
输入：stones = [31,26,33,21,40]
输出：5
```

 

**提示：**

- `1 <= stones.length <= 30`
- `1 <= stones[i] <= 100`

解法一：动规

```cpp
class Solution {
public:
    int lastStoneWeightII(vector<int>& stones) {
        int n = stones.size();
        int sum = 0;
        for(int e : stones) sum += e;
        int target = sum / 2;

        vector<int> dp(target + 1, 0);

        for(int i = 1; i <= n; i++)
            for(int j = target; j >= stones[i - 1]; j--)
                dp[j] = max(dp[j], dp[j - stones[i - 1]] + stones[i - 1]);
                
        return sum - 2 * dp[target];
    }
};
```

# DAY86

## [【模板】完全背包_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/237ae40ea1e84d8980c1d5666d1c53bc?tpId=230&tqId=38965&ru=/exam/oj)

描述

你有一个背包，最多能容纳的体积是V。

现在有n种物品，每种物品有任意多个，第i种物品的体积为��*v**i* ,价值为��*w**i*。

（1）求这个背包至多能装多大价值的物品？

（2）若背包恰好装满，求至多能装多大价值的物品？

输入描述：

第一行两个整数n和V，表示物品个数和背包体积。

接下来n行，每行两个数��*v**i*和��*w**i*，表示第i种物品的体积和价值。

1≤�,�≤10001≤*n*,*V*≤1000

输出描述：

输出有两行，第一行输出第一问的答案，第二行输出第二问的答案，如果无解请输出0。

示例1

输入：

```
2 6
5 10
3 1
```



输出：

```
10
2
```

```cpp
#include <cstring>
#include <iostream>
#include <vector>
#include <string.h>
#include <memory.h>
using namespace std;

const int N = 1001;

int main() 
{
    int n, v;
    cin >> n >> v;
    vector<int> V(n), W(n);
    for(int i = 0; i < n; i++) cin >> V[i] >> W[i];
    int dp[N][N] = {0};

    for(int i = 1; i <= n; i++)
    {
        for(int j = 0; j <= v; j++)
        {
            dp[i][j] = dp[i - 1][j];
            if(j >= V[i - 1]) dp[i][j] = max(dp[i][j], dp[i][j - V[i - 1]] + W[i - 1]);
        }
    }
    cout << dp[n][v] << endl;

    memset(dp, 0, sizeof dp);
    for(int j = 1; j <= v; j++) dp[0][j] = -1;

    for(int i = 1; i <= n; i++)
    {
        for(int j = 0; j <= v; j++)
        {
            dp[i][j] = dp[i - 1][j];
            if(j >= V[i - 1] && dp[i][j - V[i - 1]] != -1) 
                dp[i][j] = max(dp[i][j], dp[i][j - V[i - 1]] + W[i - 1]);
        }
    }
    cout << (dp[n][v] == -1 ? 0 : dp[n][v]) << endl;
    
}
// 64 位输出请用 printf("%lld")
```

## [LCR 103. 零钱兑换 - 力扣（LeetCode）](https://leetcode.cn/problems/gaM7Ch/description/)

给定不同面额的硬币 `coins` 和一个总金额 `amount`。编写一个函数来计算可以凑成总金额所需的最少的硬币个数。如果没有任何一种硬币组合能组成总金额，返回 `-1`。

你可以认为每种硬币的数量是无限的。

 

**示例 1：**

```
输入：coins = [1, 2, 5], amount = 11
输出：3 
解释：11 = 5 + 5 + 1
```

**示例 2：**

```
输入：coins = [2], amount = 3
输出：-1
```

**示例 3：**

```
输入：coins = [1], amount = 0
输出：0
```

**示例 4：**

```
输入：coins = [1], amount = 1
输出：1
```

**示例 5：**

```
输入：coins = [1], amount = 2
输出：2
```

 

**提示：**

- `1 <= coins.length <= 12`
- `1 <= coins[i] <= 231 - 1`
- `0 <= amount <= 104`

解法一：动规

```cpp
class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {
        int n = coins.size();
        vector<int> dp(amount + 1, 10010);
        dp[0] = 0;

        for(int i = 1; i <= n; i++)
            for(int j = coins[i - 1]; j <= amount; j++)
                    dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
                    
        return dp[amount] == 10010 ? -1 : dp[amount];
    }
};
```

# DAY87

## [518. 零钱兑换 II - 力扣（LeetCode）](https://leetcode.cn/problems/coin-change-ii/description/)

给你一个整数数组 `coins` 表示不同面额的硬币，另给一个整数 `amount` 表示总金额。

请你计算并返回可以凑成总金额的硬币组合数。如果任何硬币组合都无法凑出总金额，返回 `0` 。

假设每一种面额的硬币有无限个。 

题目数据保证结果符合 32 位带符号整数。

 



**示例 1：**

```
输入：amount = 5, coins = [1, 2, 5]
输出：4
解释：有四种方式可以凑成总金额：
5=5
5=2+2+1
5=2+1+1+1
5=1+1+1+1+1
```

**示例 2：**

```
输入：amount = 3, coins = [2]
输出：0
解释：只用面额 2 的硬币不能凑成总金额 3 。
```

**示例 3：**

```
输入：amount = 10, coins = [10] 
输出：1
```

 

**提示：**

- `1 <= coins.length <= 300`
- `1 <= coins[i] <= 5000`
- `coins` 中的所有值 **互不相同**
- `0 <= amount <= 5000`

解法一：动规

```cpp
class Solution {
public:
    int change(int amount, vector<int>& coins) {
        int n = coins.size();
        vector<int> dp(amount + 1, 0);
        dp[0] = 1;

        for(int i = 0; i < n; i++)
            for(int j = coins[i]; j <= amount; j++)
                dp[j] += dp[j - coins[i]];
                
        return dp[amount];
    }
};
```

## [279. 完全平方数 - 力扣（LeetCode）](https://leetcode.cn/problems/perfect-squares/description/)

给你一个整数 `n` ，返回 *和为 `n` 的完全平方数的最少数量* 。

**完全平方数** 是一个整数，其值等于另一个整数的平方；换句话说，其值等于一个整数自乘的积。例如，`1`、`4`、`9` 和 `16` 都是完全平方数，而 `3` 和 `11` 不是。

 

**示例 1：**

```
输入：n = 12
输出：3 
解释：12 = 4 + 4 + 4
```

**示例 2：**

```
输入：n = 13
输出：2
解释：13 = 4 + 9
```

 

**提示：**

- `1 <= n <= 104`

解法一：动规

```cpp
class Solution {
public:
    int numSquares(int n) {
        int m = sqrt(n);
        vector<int> dp(n + 1, 0);
        for(int j = 1; j <= n; j++) dp[j] = 100;
        
        for(int i = 1; i <= m; i++)
            for(int j = i * i; j <= n; j++)
                dp[j] = min(dp[j], dp[j - i * i] + 1);

        return dp[n];
    }
};
```

# DAY88

## [474. 一和零 - 力扣（LeetCode）](https://leetcode.cn/problems/ones-and-zeroes/description/)

给你一个二进制字符串数组 `strs` 和两个整数 `m` 和 `n` 。

请你找出并返回 `strs` 的最大子集的长度，该子集中 **最多** 有 `m` 个 `0` 和 `n` 个 `1` 。

如果 `x` 的所有元素也是 `y` 的元素，集合 `x` 是集合 `y` 的 **子集** 。

 

**示例 1：**

```
输入：strs = ["10", "0001", "111001", "1", "0"], m = 5, n = 3
输出：4
解释：最多有 5 个 0 和 3 个 1 的最大子集是 {"10","0001","1","0"} ，因此答案是 4 。
其他满足题意但较小的子集包括 {"0001","1"} 和 {"10","1","0"} 。{"111001"} 不满足题意，因为它含 4 个 1 ，大于 n 的值 3 。
```

**示例 2：**

```
输入：strs = ["10", "0", "1"], m = 1, n = 1
输出：2
解释：最大的子集是 {"0", "1"} ，所以答案是 2 。
```

 

**提示：**

- `1 <= strs.length <= 600`
- `1 <= strs[i].length <= 100`
- `strs[i]` 仅由 `'0'` 和 `'1'` 组成
- `1 <= m, n <= 100`

解法一：动规

```cpp
class Solution {
public:
    int findMaxForm(vector<string>& strs, int m, int n) {
        int sz = strs.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for(int i = 1; i <= sz; i++)
        {
            int a = 0, b = 0;
            for(auto e : strs[i - 1])
            {
                if(e == '0') a++;
                else b++;
            }
            for(int j = m; j >= a; j--)
                for(int k = n; k >= b; k--)
                    dp[j][k] = max(dp[j][k], dp[j - a][k - b] + 1);
        }
        return dp[m][n];
    }
};
```

## [879. 盈利计划 - 力扣（LeetCode）](https://leetcode.cn/problems/profitable-schemes/description/)

集团里有 `n` 名员工，他们可以完成各种各样的工作创造利润。

第 `i` 种工作会产生 `profit[i]` 的利润，它要求 `group[i]` 名成员共同参与。如果成员参与了其中一项工作，就不能参与另一项工作。

工作的任何至少产生 `minProfit` 利润的子集称为 **盈利计划** 。并且工作的成员总数最多为 `n` 。

有多少种计划可以选择？因为答案很大，所以 **返回结果模** `10^9 + 7` **的值**。

 

**示例 1：**

```
输入：n = 5, minProfit = 3, group = [2,2], profit = [2,3]
输出：2
解释：至少产生 3 的利润，该集团可以完成工作 0 和工作 1 ，或仅完成工作 1 。
总的来说，有两种计划。
```

**示例 2：**

```
输入：n = 10, minProfit = 5, group = [2,3,5], profit = [6,7,8]
输出：7
解释：至少产生 5 的利润，只要完成其中一种工作就行，所以该集团可以完成任何工作。
有 7 种可能的计划：(0)，(1)，(2)，(0,1)，(0,2)，(1,2)，以及 (0,1,2) 。
```

 

**提示：**

- `1 <= n <= 100`
- `0 <= minProfit <= 100`
- `1 <= group.length <= 100`
- `1 <= group[i] <= 100`
- `profit.length == group.length`
- `0 <= profit[i] <= 100`

解法一：动规

```cpp
class Solution {
public:
    int profitableSchemes(int n, int minProfit, vector<int>& group, vector<int>& profit) {
        const int MOD = 1e9 + 7;
        int sz = group.size();
        vector<vector<int>> dp(n + 1, vector<int>(minProfit + 1));

        for(int j = 0; j <= n; j++) dp[j][0] = 1;
        for(int i = 1; i <= sz; i++)
            for(int j = n; j >= group[i - 1]; j--)
                for(int k = minProfit; k >= 0; k--)
                {
                    dp[j][k] += dp[j - group[i - 1]][max(0, k - profit[i - 1])];
                    dp[j][k] %= MOD;
                } 
        return dp[n][minProfit];
    }
};
```

# DAY89

## [LCR 104. 组合总和 Ⅳ - 力扣（LeetCode）](https://leetcode.cn/problems/D0F0SV/description/)

给定一个由 **不同** 正整数组成的数组 `nums` ，和一个目标整数 `target` 。请从 `nums` 中找出并返回总和为 `target` 的元素组合的个数。数组中的数字可以在一次排列中出现任意次，但是顺序不同的序列被视作不同的组合。

题目数据保证答案符合 32 位整数范围。

 

**示例 1：**

```
输入：nums = [1,2,3], target = 4
输出：7
解释：
所有可能的组合为：
(1, 1, 1, 1)
(1, 1, 2)
(1, 2, 1)
(1, 3)
(2, 1, 1)
(2, 2)
(3, 1)
请注意，顺序不同的序列被视作不同的组合。
```

**示例 2：**

```
输入：nums = [9], target = 3
输出：0
```

 

**提示：**

- `1 <= nums.length <= 200`
- `1 <= nums[i] <= 1000`
- `nums` 中的所有元素 **互不相同**
- `1 <= target <= 1000`

解法一：动规

```cpp
class Solution {
public:
    int combinationSum4(vector<int>& nums, int target) {
        vector<double> dp(target + 1, 0);
        dp[0] = 1;

        for(int i = 1; i <= target; i++)
        {
            for(auto e : nums)
                if(i >= e) dp[i] += dp[i - e];
        }
        return dp[target];
    }
};
```

## [96. 不同的二叉搜索树 - 力扣（LeetCode）](https://leetcode.cn/problems/unique-binary-search-trees/description/)

给你一个整数 `n` ，求恰由 `n` 个节点组成且节点值从 `1` 到 `n` 互不相同的 **二叉搜索树** 有多少种？返回满足题意的二叉搜索树的种数。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/01/18/uniquebstn3.jpg)

```
输入：n = 3
输出：5
```

**示例 2：**

```
输入：n = 1
输出：1
```

 

**提示：**

- `1 <= n <= 19`

解法一：动规

```cpp
class Solution {
public:
    int numTrees(int n) {
        vector<int> dp(n + 1, 0);
        dp[0] = 1;
        for(int i = 1; i <= n; i++)
        {
            for(int j = 1; j <= i; j++)
            {
                dp[i] += dp[j - 1] * dp[i - j];
            }
        }
        return dp[n];
    }
};
```

# DAY90

## [733. 图像渲染 - 力扣（LeetCode）](https://leetcode.cn/problems/flood-fill/description/)

有一幅以 `m x n` 的二维整数数组表示的图画 `image` ，其中 `image[i][j]` 表示该图画的像素值大小。

你也被给予三个整数 `sr` , `sc` 和 `newColor` 。你应该从像素 `image[sr][sc]` 开始对图像进行 上色**填充** 。

为了完成 **上色工作** ，从初始像素开始，记录初始坐标的 **上下左右四个方向上** 像素值与初始坐标相同的相连像素点，接着再记录这四个方向上符合条件的像素点与他们对应 **四个方向上** 像素值与初始坐标相同的相连像素点，……，重复该过程。将所有有记录的像素点的颜色值改为 `newColor` 。

最后返回 *经过上色渲染后的图像* 。

 

**示例 1:**

![img](https://assets.leetcode.com/uploads/2021/06/01/flood1-grid.jpg)

```
输入: image = [[1,1,1],[1,1,0],[1,0,1]]，sr = 1, sc = 1, newColor = 2
输出: [[2,2,2],[2,2,0],[2,0,1]]
解析: 在图像的正中间，(坐标(sr,sc)=(1,1)),在路径上所有符合条件的像素点的颜色都被更改成2。
注意，右下角的像素没有更改为2，因为它不是在上下左右四个方向上与初始点相连的像素点。
```

**示例 2:**

```
输入: image = [[0,0,0],[0,0,0]], sr = 0, sc = 0, newColor = 2
输出: [[2,2,2],[2,2,2]]
```

 

**提示:**

- `m == image.length`
- `n == image[i].length`
- `1 <= m, n <= 50`
- `0 <= image[i][j], newColor < 216`
- `0 <= sr < m`
- `0 <= sc < n`

解法一：floodfill算法

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {-1, 1, 0, 0};
    int oldcolor, newcolor;

public:
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
        oldcolor = image[sr][sc];
        newcolor = color;
        if(oldcolor == newcolor) return image;
        queue<pair<int, int>> qu;
        qu.push({sr, sc});

        while(!qu.empty())
        {
            auto [a, b] = qu.front();
            qu.pop();
            image[a][b] = newcolor;
            for(int i = 0; i < 4; i++)
            {
                int tmpx = a + x[i], tmpy = b + y[i];
                if(tmpx >= 0 && tmpx < image.size() && tmpy >= 0 && tmpy < image[0].size() && image[tmpx][tmpy] == oldcolor)
                    qu.push({tmpx, tmpy});
            }
        }
        return image;
    }
};
```

## [200. 岛屿数量 - 力扣（LeetCode）](https://leetcode.cn/problems/number-of-islands/description/)

给你一个由 `'1'`（陆地）和 `'0'`（水）组成的的二维网格，请你计算网格中岛屿的数量。

岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。

此外，你可以假设该网格的四条边均被水包围。

 

**示例 1：**

```
输入：grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
输出：1
```

**示例 2：**

```
输入：grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
输出：3
```

 

**提示：**

- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 300`
- `grid[i][j]` 的值为 `'0'` 或 `'1'`

解法一：flood fill算法

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
    bool vis[300][300];

    int m, n;

public:
    int numIslands(vector<vector<char>>& grid) {
        m = grid.size(), n = grid[0].size();
        int ret = 0;
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
            {
                if(grid[i][j] == '1' && !vis[i][j])
                {
                    ret++;
                    _bfs(grid, i, j);
                }
            }
        return ret;
    }

    void _bfs(vector<vector<char>>& grid, int i, int j)
    {
        queue<pair<int, int>> qu;
        qu.push({i, j});
        while(!qu.empty())
        {
            auto [a, b] = qu.front();
            qu.pop();
            vis[a][b] = true;
            for(int i = 0; i < 4; i++)
            {
                int tmpx = a + x[i], tmpy = b + y[i];
                if(tmpx >= 0 && tmpx < m && tmpy >= 0 && tmpy < n && grid[tmpx][tmpy] == '1' && !vis[tmpx][tmpy])
                {
                    qu.push({tmpx, tmpy});
                    vis[tmpx][tmpy] = true;
                }    
            }
        }
    }
};
```

# DAY91

## [695. 岛屿的最大面积 - 力扣（LeetCode）](https://leetcode.cn/problems/max-area-of-island/description/)

给你一个大小为 `m x n` 的二进制矩阵 `grid` 。

**岛屿** 是由一些相邻的 `1` (代表土地) 构成的组合，这里的「相邻」要求两个 `1` 必须在 **水平或者竖直的四个方向上** 相邻。你可以假设 `grid` 的四个边缘都被 `0`（代表水）包围着。

岛屿的面积是岛上值为 `1` 的单元格的数目。

计算并返回 `grid` 中最大的岛屿面积。如果没有岛屿，则返回面积为 `0` 。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/05/01/maxarea1-grid.jpg)

```
输入：grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,1,1,0,1,0,0,0,0,0,0,0,0],[0,1,0,0,1,1,0,0,1,0,1,0,0],[0,1,0,0,1,1,0,0,1,1,1,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0]]
输出：6
解释：答案不应该是 11 ，因为岛屿只能包含水平或垂直这四个方向上的 1 。
```

**示例 2：**

```
输入：grid = [[0,0,0,0,0,0,0,0]]
输出：0
```

 

**提示：**

- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 50`
- `grid[i][j]` 为 `0` 或 `1`

解法一：flood fill算法

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
public:
    int maxAreaOfIsland(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size(), ret = 0;
        queue<pair<int, int>> qu;
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {      
                if(grid[i][j] == 1)
                {
                    grid[i][j] = 0;
                    int tmp = 0;
                    qu.push({i, j}); 
                    while(!qu.empty())
                    {
                        auto [a, b] = qu.front();
                        qu.pop(), tmp++;
                        for(int i = 0; i < 4; i++)
                        {
                            int tmpx = x[i] + a, tmpy = y[i] + b;
                            if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && grid[tmpx][tmpy])
                            {
                                qu.push({tmpx, tmpy});
                                grid[tmpx][tmpy] = 0;
                            }
                        }
                    }
                    ret = max(ret, tmp);
                }
            }
        }
        return ret;
    }
};
```

## [130. 被围绕的区域 - 力扣（LeetCode）](https://leetcode.cn/problems/surrounded-regions/description/)

给你一个 `m x n` 的矩阵 `board` ，由若干字符 `'X'` 和 `'O'` ，找到所有被 `'X'` 围绕的区域，并将这些区域里所有的 `'O'` 用 `'X'` 填充。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/02/19/xogrid.jpg)

```
输入：board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
输出：[["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
解释：被围绕的区间不会存在于边界上，换句话说，任何边界上的 'O' 都不会被填充为 'X'。 任何不在边界上，或不与边界上的 'O' 相连的 'O' 最终都会被填充为 'X'。如果两个元素在水平或垂直方向相邻，则称它们是“相连”的。
```

**示例 2：**

```
输入：board = [["X"]]
输出：[["X"]]
```

 

**提示：**

- `m == board.length`
- `n == board[i].length`
- `1 <= m, n <= 200`
- `board[i][j]` 为 `'X'` 或 `'O'`

解法一：flood fill算法

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
    int m, n;
public:
    void _solve(vector<vector<char>>& board, int row, int col)
    {
        queue<pair<int, int>> qu;
        qu.push({row, col});
        board[row][col] = '.';
        while(!qu.empty())
        {
            auto [a, b] = qu.front();
            qu.pop();
            for(int i = 0; i < 4; i++)
            {
                int tmpx = x[i] + a, tmpy = y[i] + b;
                if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && board[tmpx][tmpy] == 'O')
                {
                    qu.push({tmpx, tmpy});
                    board[tmpx][tmpy] = '.';
                }
            }
        }
    }
    void solve(vector<vector<char>>& board) {
        m = board.size(), n = board[0].size();
        for(int i = 0; i < m; i++)
        {
            if(board[i][0] == 'O') _solve(board, i, 0);
            if(board[i][n - 1] == 'O') _solve(board, i, n - 1);
        }

        for(int j = 0; j < n; j++)
        {
            if(board[0][j] == 'O') _solve(board, 0, j);
            if(board[m - 1][j] == 'O') _solve(board, m - 1, j);
        }

        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(board[i][j] == 'O') board[i][j] = 'X';
                else if(board[i][j] == '.') board[i][j] = 'O';
            }
        }
    }
};
```

# DAY92

## [1926. 迷宫中离入口最近的出口 - 力扣（LeetCode）](https://leetcode.cn/problems/nearest-exit-from-entrance-in-maze/description/)

给你一个 `m x n` 的迷宫矩阵 `maze` （**下标从 0 开始**），矩阵中有空格子（用 `'.'` 表示）和墙（用 `'+'` 表示）。同时给你迷宫的入口 `entrance` ，用 `entrance = [entrancerow, entrancecol]` 表示你一开始所在格子的行和列。

每一步操作，你可以往 **上**，**下**，**左** 或者 **右** 移动一个格子。你不能进入墙所在的格子，你也不能离开迷宫。你的目标是找到离 `entrance` **最近** 的出口。**出口** 的含义是 `maze` **边界** 上的 **空格子**。`entrance` 格子 **不算** 出口。

请你返回从 `entrance` 到最近出口的最短路径的 **步数** ，如果不存在这样的路径，请你返回 `-1` 。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/06/04/nearest1-grid.jpg)

```
输入：maze = [["+","+",".","+"],[".",".",".","+"],["+","+","+","."]], entrance = [1,2]
输出：1
解释：总共有 3 个出口，分别位于 (1,0)，(0,2) 和 (2,3) 。
一开始，你在入口格子 (1,2) 处。
- 你可以往左移动 2 步到达 (1,0) 。
- 你可以往上移动 1 步到达 (0,2) 。
从入口处没法到达 (2,3) 。
所以，最近的出口是 (0,2) ，距离为 1 步。
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2021/06/04/nearesr2-grid.jpg)

```
输入：maze = [["+","+","+"],[".",".","."],["+","+","+"]], entrance = [1,0]
输出：2
解释：迷宫中只有 1 个出口，在 (1,2) 处。
(1,0) 不算出口，因为它是入口格子。
初始时，你在入口与格子 (1,0) 处。
- 你可以往右移动 2 步到达 (1,2) 处。
所以，最近的出口为 (1,2) ，距离为 2 步。
```

**示例 3：**

![img](https://assets.leetcode.com/uploads/2021/06/04/nearest3-grid.jpg)

```
输入：maze = [[".","+"]], entrance = [0,0]
输出：-1
解释：这个迷宫中没有出口。
```

 

**提示：**

- `maze.length == m`
- `maze[i].length == n`
- `1 <= m, n <= 100`
- `maze[i][j]` 要么是 `'.'` ，要么是 `'+'` 。
- `entrance.length == 2`
- `0 <= entrancerow < m`
- `0 <= entrancecol < n`
- `entrance` 一定是空格子。

解法一：floodfill

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
    int m, n;
    bool vis[100][100];
public:
    int nearestExit(vector<vector<char>>& maze, vector<int>& entrance) {
        m = maze.size(), n = maze[0].size();
        int tx = entrance[0], ty = entrance[1], ret = 0;
        vis[tx][ty] = true;
        queue<pair<int, int>> qu;
        qu.push({tx, ty});
        while(!qu.empty())
        {
            ret++;
            int sz = qu.size();
            for(int i = 0; i < sz; i++)
            {
                auto [a, b] = qu.front();
                qu.pop();
                for(int k = 0; k < 4; k++)
                {
                    int tmpx = a + x[k], tmpy = b + y[k];
                    if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && maze[tmpx][tmpy] == '.' && !vis[tmpx][tmpy])
                    {
                        if(tmpx == 0 || tmpx == m - 1 || tmpy == 0 || tmpy == n - 1) return ret;
                        qu.push({tmpx, tmpy});
                        vis[tmpx][tmpy] = true;
                    }
                }
            }
            
        }
        return -1;
    }
};
```

## [433. 最小基因变化 - 力扣（LeetCode）](https://leetcode.cn/problems/minimum-genetic-mutation/description/)

基因序列可以表示为一条由 8 个字符组成的字符串，其中每个字符都是 `'A'`、`'C'`、`'G'` 和 `'T'` 之一。

假设我们需要调查从基因序列 `start` 变为 `end` 所发生的基因变化。一次基因变化就意味着这个基因序列中的一个字符发生了变化。

- 例如，`"AACCGGTT" --> "AACCGGTA"` 就是一次基因变化。

另有一个基因库 `bank` 记录了所有有效的基因变化，只有基因库中的基因才是有效的基因序列。（变化后的基因必须位于基因库 `bank` 中）

给你两个基因序列 `start` 和 `end` ，以及一个基因库 `bank` ，请你找出并返回能够使 `start` 变化为 `end` 所需的最少变化次数。如果无法完成此基因变化，返回 `-1` 。

注意：起始基因序列 `start` 默认是有效的，但是它并不一定会出现在基因库中。

 

**示例 1：**

```
输入：start = "AACCGGTT", end = "AACCGGTA", bank = ["AACCGGTA"]
输出：1
```

**示例 2：**

```
输入：start = "AACCGGTT", end = "AAACGGTA", bank = ["AACCGGTA","AACCGCTA","AAACGGTA"]
输出：2
```

**示例 3：**

```
输入：start = "AAAAACCC", end = "AACCCCCC", bank = ["AAAACCCC","AAACCCCC","AACCCCCC"]
输出：3
```

 

**提示：**

- `start.length == 8`
- `end.length == 8`
- `0 <= bank.length <= 10`
- `bank[i].length == 8`
- `start`、`end` 和 `bank[i]` 仅由字符 `['A', 'C', 'G', 'T']` 组成

解法一：floodfill算法

```cpp
class Solution {
public:
    int minMutation(string startGene, string endGene, vector<string>& bank) {
        unordered_set<string> hash_bank, hash;
        for(string str : bank) hash_bank.insert(str);
        if(!hash_bank.count(endGene)) return -1;
        int ret = 0;
        string charactor = "ACGT";
        queue<string> qu;
        qu.push(startGene);
        while(!qu.empty())
        {
            ret++;
            int sz = qu.size();
            for(int k = 0; k < sz; k++)
            {
                string fnt = qu.front();
                qu.pop();
                for(int i = 0; i < fnt.size(); i++)
                {
                    for(char ch : charactor)
                    {
                        string tmp = fnt;
                        tmp[i] = ch;
                        if(tmp == endGene) return ret;
                        if(!hash.count(tmp) && hash_bank.count(tmp))
                        {
                            qu.push(tmp);
                            hash.insert(tmp);
                        }
                    }
                }
            }
            
        }
        return -1;
    }
};
```

# DAY 93

## [LCR 108. 单词接龙 - 力扣（LeetCode）](https://leetcode.cn/problems/om3reC/description/)

在字典（单词列表） `wordList` 中，从单词 `beginWord` 和 `endWord` 的 **转换序列** 是一个按下述规格形成的序列：

- 序列中第一个单词是 `beginWord` 。
- 序列中最后一个单词是 `endWord` 。
- 每次转换只能改变一个字母。
- 转换过程中的中间单词必须是字典 `wordList` 中的单词。

给定两个长度相同但内容不同的单词 `beginWord` 和 `endWord` 和一个字典 `wordList` ，找到从 `beginWord` 到 `endWord` 的 **最短转换序列** 中的 **单词数目** 。如果不存在这样的转换序列，返回 0。

 

**示例 1：**

```
输入：beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
输出：5
解释：一个最短转换序列是 "hit" -> "hot" -> "dot" -> "dog" -> "cog", 返回它的长度 5。
```

**示例 2：**

```
输入：beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
输出：0
解释：endWord "cog" 不在字典中，所以无法进行转换。
```

 

**提示：**

- `1 <= beginWord.length <= 10`
- `endWord.length == beginWord.length`
- `1 <= wordList.length <= 5000`
- `wordList[i].length == beginWord.length`
- `beginWord`、`endWord` 和 `wordList[i]` 由小写英文字母组成
- `beginWord != endWord`
- `wordList` 中的所有字符串 **互不相同**

解法一：floodfill算法

```cpp
class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
        unordered_set<string> hash(wordList.begin(), wordList.end());
        unordered_set<string> vis;

        if(hash.empty()) return 0;
        int ret = 1;
        queue<string> qu;
        qu.push(beginWord);
        hash.insert(beginWord);
        while(!qu.empty())
        {
            ret++;
            int sz = qu.size();
            while(sz--)
            {
                string str = qu.front();
                qu.pop();
                for(int i = 0; i < str.size(); i++)
                {
                    string tmp = str;
                    for(char ch = 'a'; ch <= 'z'; ch++)
                    {
                        tmp[i] = ch;
                        if(hash.count(tmp) && !vis.count(tmp))
                        {
                            if(tmp == endWord) return ret;
                            qu.push(tmp);
                            vis.insert(tmp);
                        }
                    }
                }
            }
        }
        return 0;
    }
};
```

## [675. 为高尔夫比赛砍树 - 力扣（LeetCode）](https://leetcode.cn/problems/cut-off-trees-for-golf-event/description/)

你被请来给一个要举办高尔夫比赛的树林砍树。树林由一个 `m x n` 的矩阵表示， 在这个矩阵中：

- `0` 表示障碍，无法触碰
- `1` 表示地面，可以行走
- `比 1 大的数` 表示有树的单元格，可以行走，数值表示树的高度

每一步，你都可以向上、下、左、右四个方向之一移动一个单位，如果你站的地方有一棵树，那么你可以决定是否要砍倒它。

你需要按照树的高度从低向高砍掉所有的树，每砍过一颗树，该单元格的值变为 `1`（即变为地面）。

你将从 `(0, 0)` 点开始工作，返回你砍完所有树需要走的最小步数。 如果你无法砍完所有的树，返回 `-1` 。

可以保证的是，没有两棵树的高度是相同的，并且你至少需要砍倒一棵树。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2020/11/26/trees1.jpg)

```
输入：forest = [[1,2,3],[0,0,4],[7,6,5]]
输出：6
解释：沿着上面的路径，你可以用 6 步，按从最矮到最高的顺序砍掉这些树。
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2020/11/26/trees2.jpg)

```
输入：forest = [[1,2,3],[0,0,0],[7,6,5]]
输出：-1
解释：由于中间一行被障碍阻塞，无法访问最下面一行中的树。
```

**示例 3：**

```
输入：forest = [[2,3,4],[0,0,5],[8,7,6]]
输出：6
解释：可以按与示例 1 相同的路径来砍掉所有的树。
(0,0) 位置的树，可以直接砍去，不用算步数。
```

 

**提示：**

- `m == forest.length`
- `n == forest[i].length`
- `1 <= m, n <= 50`
- `0 <= forest[i][j] <= 109`

解法一：floodfill算法

```cpp
class Solution {
    int m, n;
public:
    int cutOffTree(vector<vector<int>>& forest) {
        m = forest.size(), n = forest[0].size();
        vector<pair<int, int>> trees;
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
                if(forest[i][j] > 1) trees.push_back({i, j});

        sort(trees.begin(), trees.end(), [&](const pair<int, int>& p1, const pair<int, int>& p2)
        {
            return forest[p1.first][p1.second] < forest[p2.first][p2.second];
        });

        int x = 0, y = 0, ret = 0;
        for(auto& [a, b] : trees)
        {
            int step = bfs(forest, x, y, a, b);
            if(step == -1) return -1;
            ret += step;
            x = a, y = b;
        } 
        return ret;
    }

    bool vis[51][51];
    int x[4] = {0, 0, -1, 1};
    int y[4] = {1, -1, 0, 0};

    int bfs(vector<vector<int>>& forest, int bx, int by, int ex, int ey)
    {
        if(bx == ex && by == ey) return 0;
        queue<pair<int, int>> qu;
        memset(vis, 0, sizeof(vis));
        qu.push({bx, by});
        vis[bx][by] = true;
        int ret = 0;
        while(qu.size())
        {
            ret++;
            int sz = qu.size();
            while(sz--)
            {
                auto [a, b] = qu.front();
                qu.pop();
                for(int i = 0; i < 4; i++)
                {
                    int tmpx = a + x[i], tmpy = b + y[i];
                    if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && !vis[tmpx][tmpy] && forest[tmpx][tmpy])
                    {
                        if(tmpx == ex && tmpy == ey) return ret;
                        qu.push({tmpx, tmpy});
                        vis[tmpx][tmpy] = true;
                    }
                }
            }
        }
        return -1;
    }
};
```

# DAY94

## [LCR 107. 01 矩阵 - 力扣（LeetCode）](https://leetcode.cn/problems/2bCMpM/description/)

给定一个由 `0` 和 `1` 组成的矩阵 `mat` ，请输出一个大小相同的矩阵，其中每一个格子是 `mat` 中对应位置元素到最近的 `0` 的距离。

两个相邻元素间的距离为 `1` 。

 

**示例 1：**

![img](https://pic.leetcode-cn.com/1626667201-NCWmuP-image.png)

```
输入：mat = [[0,0,0],[0,1,0],[0,0,0]]
输出：[[0,0,0],[0,1,0],[0,0,0]]
```

**示例 2：**

![img](https://pic.leetcode-cn.com/1626667205-xFxIeK-image.png)

```
输入：mat = [[0,0,0],[0,1,0],[1,1,1]]
输出：[[0,0,0],[0,1,0],[1,2,1]]
```

 

**提示：**

- `m == mat.length`
- `n == mat[i].length`
- `1 <= m, n <= 104`
- `1 <= m * n <= 104`
- `mat[i][j] is either 0 or 1.`
- `mat` 中至少有一个 `0 `

解法一：floodfill

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
public:
    vector<vector<int>> updateMatrix(vector<vector<int>>& mat) {
        int m = mat.size(), n = mat[0].size();
        vector<vector<int>> ret(m, vector<int>(n, -1));
        queue<pair<int, int>> qu;
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
                if(mat[i][j] == 0)
                {
                    qu.push({i, j});
                    ret[i][j] = 0;
                }
        
        
        while(qu.size())
        {
            int sz = qu.size();
            while(sz--)
            {
                auto [a, b] = qu.front();
                qu.pop();
                for(int i = 0; i < 4; i++)
                {
                    int tmpx = a + x[i], tmpy = b + y[i];
                    if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && ret[tmpx][tmpy] == -1)
                    {
                        qu.push({tmpx, tmpy});
                        ret[tmpx][tmpy] = ret[a][b] + 1;
                    }
                }
            }
        }
        return ret;
    }
};
```

## [1020. 飞地的数量 - 力扣（LeetCode）](https://leetcode.cn/problems/number-of-enclaves/description/)

给你一个大小为 `m x n` 的二进制矩阵 `grid` ，其中 `0` 表示一个海洋单元格、`1` 表示一个陆地单元格。

一次 **移动** 是指从一个陆地单元格走到另一个相邻（**上、下、左、右**）的陆地单元格或跨过 `grid` 的边界。

返回网格中 **无法** 在任意次数的移动中离开网格边界的陆地单元格的数量。

 

**示例 1：**

![img](https://assets.leetcode.com/uploads/2021/02/18/enclaves1.jpg)

```
输入：grid = [[0,0,0,0],[1,0,1,0],[0,1,1,0],[0,0,0,0]]
输出：3
解释：有三个 1 被 0 包围。一个 1 没有被包围，因为它在边界上。
```

**示例 2：**

![img](https://assets.leetcode.com/uploads/2021/02/18/enclaves2.jpg)

```
输入：grid = [[0,1,1,0],[0,0,1,0],[0,0,1,0],[0,0,0,0]]
输出：0
解释：所有 1 都在边界上或可以到达边界。
```

 

**提示：**

- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 500`
- `grid[i][j]` 的值为 `0` 或 `1`

解法一：floodfill

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
    
public:
    int numEnclaves(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size();
        queue<pair<int, int>> qu;
        bool vis[m][n];
        memset(vis, 0, sizeof(vis));
        int count = 0;
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(grid[i][j] == 1)
                {
                    count++;
                    if(i == 0 || j == 0 || i == m - 1 || j == n - 1)
                    {
                        qu.push({i, j});
                        vis[i][j] = true;
                    }
                }
            }
        }
        while(qu.size())
        {
            int sz = qu.size();
            count -= sz;
            while(sz--)
            {
                auto [a, b] = qu.front();
                qu.pop();
                for(int i = 0; i < 4; i++)
                {
                    int tmpx = a + x[i], tmpy = b + y[i];
                    if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && grid[tmpx][tmpy] && !vis[tmpx][tmpy])
                    {
                        qu.push({tmpx, tmpy});
                        vis[tmpx][tmpy] = true;
                    }
                }
            }
        }
        return count;
    }
};
```

# DAY95

## [1765. 地图中的最高点 - 力扣（LeetCode）](https://leetcode.cn/problems/map-of-highest-peak/description/)

给你一个大小为 `m x n` 的整数矩阵 `isWater` ，它代表了一个由 **陆地** 和 **水域** 单元格组成的地图。

- 如果 `isWater[i][j] == 0` ，格子 `(i, j)` 是一个 **陆地** 格子。
- 如果 `isWater[i][j] == 1` ，格子 `(i, j)` 是一个 **水域** 格子。

你需要按照如下规则给每个单元格安排高度：

- 每个格子的高度都必须是非负的。
- 如果一个格子是 **水域** ，那么它的高度必须为 `0` 。
- 任意相邻的格子高度差 **至多** 为 `1` 。当两个格子在正东、南、西、北方向上相互紧挨着，就称它们为相邻的格子。（也就是说它们有一条公共边）

找到一种安排高度的方案，使得矩阵中的最高高度值 **最大** 。

请你返回一个大小为 `m x n` 的整数矩阵 `height` ，其中 `height[i][j]` 是格子 `(i, j)` 的高度。如果有多种解法，请返回 **任意一个** 。

 

**示例 1：**

**![img](https://assets.leetcode.com/uploads/2021/01/10/screenshot-2021-01-11-at-82045-am.png)**

```
输入：isWater = [[0,1],[0,0]]
输出：[[1,0],[2,1]]
解释：上图展示了给各个格子安排的高度。
蓝色格子是水域格，绿色格子是陆地格。
```

**示例 2：**

**![img](https://assets.leetcode.com/uploads/2021/01/10/screenshot-2021-01-11-at-82050-am.png)**

```
输入：isWater = [[0,0,1],[1,0,0],[0,0,0]]
输出：[[1,1,0],[0,1,1],[1,2,2]]
解释：所有安排方案中，最高可行高度为 2 。
任意安排方案中，只要最高高度为 2 且符合上述规则的，都为可行方案。
```

 

**提示：**

- `m == isWater.length`
- `n == isWater[i].length`
- `1 <= m, n <= 1000`
- `isWater[i][j]` 要么是 `0` ，要么是 `1` 。
- 至少有 **1** 个水域格子。

解法一：floodfill

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
public:
    vector<vector<int>> highestPeak(vector<vector<int>>& isWater) {
        int m = isWater.size(), n = isWater[0].size();
        queue<pair<int, int>> qu;
        vector<vector<int>> ret(m, vector<int>(n, -1));
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
                if(isWater[i][j] == 1)
                {
                    ret[i][j] = 0;
                    qu.push({i, j});
                }
        while(qu.size())
        {
            int sz = qu.size();
            while(sz--)
            {
                auto [a, b] = qu.front();
                qu.pop();
                for(int i = 0; i < 4; i++)
                {
                    int tmpx = a + x[i], tmpy = b + y[i];
                    if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && ret[tmpx][tmpy] == -1)
                    {
                        ret[tmpx][tmpy] = ret[a][b] + 1;
                        qu.push({tmpx, tmpy});
                    }
                }
            }
        }
        return ret;
    }
};
```

## [1162. 地图分析 - 力扣（LeetCode）](https://leetcode.cn/problems/as-far-from-land-as-possible/description/)

你现在手里有一份大小为 `n x n` 的 网格 `grid`，上面的每个 单元格 都用 `0` 和 `1` 标记好了。其中 `0` 代表海洋，`1` 代表陆地。

请你找出一个海洋单元格，这个海洋单元格到离它最近的陆地单元格的距离是最大的，并返回该距离。如果网格上只有陆地或者海洋，请返回 `-1`。

我们这里说的距离是「曼哈顿距离」（ Manhattan Distance）：`(x0, y0)` 和 `(x1, y1)` 这两个单元格之间的距离是 `|x0 - x1| + |y0 - y1|` 。

 

**示例 1：**

**![img](https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2019/08/17/1336_ex1.jpeg)**

```
输入：grid = [[1,0,1],[0,0,0],[1,0,1]]
输出：2
解释： 
海洋单元格 (1, 1) 和所有陆地单元格之间的距离都达到最大，最大距离为 2。
```

**示例 2：**

**![img](https://assets.leetcode-cn.com/aliyun-lc-upload/uploads/2019/08/17/1336_ex2.jpeg)**

```
输入：grid = [[1,0,0],[0,0,0],[0,0,0]]
输出：4
解释： 
海洋单元格 (2, 2) 和所有陆地单元格之间的距离都达到最大，最大距离为 4。
```

 

**提示：**



- `n == grid.length`
- `n == grid[i].length`
- `1 <= n <= 100`
- `grid[i][j]` 不是 `0` 就是 `1`

解法一：floodfill

```cpp
class Solution {
    int x[4] = {0, 0, 1, -1};
    int y[4] = {1, -1, 0, 0};
public:
    int maxDistance(vector<vector<int>>& grid) {
        int m = grid.size(), n = grid[0].size();
        queue<pair<int, int>> qu;
        vector<vector<int>> vis(m, vector<int>(n, -1));
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(grid[i][j] == 1)
                {
                    qu.push({i, j});
                    vis[i][j] = 0;
                }
            }
        }
        int ret = 0;
        while(qu.size())
        {
            int sz = qu.size();
            while(sz--)
            {
                auto [a, b] = qu.front();
                qu.pop();
                for(int i = 0; i < 4; i++)
                {
                    int tmpx = a + x[i], tmpy = b + y[i];
                    if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && !grid[tmpx][tmpy] && vis[tmpx][tmpy] == -1)
                    {
                        qu.push({tmpx, tmpy});
                        vis[tmpx][tmpy] = vis[a][b] + 1;
                        ret = max(ret, vis[tmpx][tmpy]);
                    }
                }
            }
        }
        return ret == 0 ? -1 : ret;
    }
};
```

# DAY96

## [207. 课程表 - 力扣（LeetCode）](https://leetcode.cn/problems/course-schedule/description/)

你这个学期必须选修 `numCourses` 门课程，记为 `0` 到 `numCourses - 1` 。

在选修某些课程之前需要一些先修课程。 先修课程按数组 `prerequisites` 给出，其中 `prerequisites[i] = [ai, bi]` ，表示如果要学习课程 `ai` 则 **必须** 先学习课程 `bi` 。

- 例如，先修课程对 `[0, 1]` 表示：想要学习课程 `0` ，你需要先完成课程 `1` 。

请你判断是否可能完成所有课程的学习？如果可以，返回 `true` ；否则，返回 `false` 。

 

**示例 1：**

```
输入：numCourses = 2, prerequisites = [[1,0]]
输出：true
解释：总共有 2 门课程。学习课程 1 之前，你需要完成课程 0 。这是可能的。
```

**示例 2：**

```
输入：numCourses = 2, prerequisites = [[1,0],[0,1]]
输出：false
解释：总共有 2 门课程。学习课程 1 之前，你需要先完成课程 0 ；并且学习课程 0 之前，你还应先完成课程 1 。这是不可能的。
```

 

**提示：**

- `1 <= numCourses <= 2000`
- `0 <= prerequisites.length <= 5000`
- `prerequisites[i].length == 2`
- `0 <= ai, bi < numCourses`
- `prerequisites[i]` 中的所有课程对 **互不相同**

解法一：floodfill

```cpp
class Solution {
public:
    bool canFinish(int n, vector<vector<int>>& prerequisites) {
        unordered_map<int, vector<int>> edges;
        vector<int> in(n);
        for(auto& e : prerequisites)
        {
            int a = e[0], b = e[1]; // b -> a
            edges[b].push_back(a);
            in[a]++;
        }

        queue<int> qu;
        for(int i = 0; i < n; i++)
        {
            if(in[i] == 0) qu.push(i);
        }

        while(qu.size())
        {
            int tmp = qu.front();
            qu.pop();
            for(auto t : edges[tmp])
            {
                in[t]--;
                if(!in[t]) qu.push(t);
            }  
        }

        for(int i = 0; i < n; i++)
        {
            if(in[i]) return false;
        }
        return true;
    }
};
```

## [210. 课程表 II - 力扣（LeetCode）](https://leetcode.cn/problems/course-schedule-ii/description/)

现在你总共有 `numCourses` 门课需要选，记为 `0` 到 `numCourses - 1`。给你一个数组 `prerequisites` ，其中 `prerequisites[i] = [ai, bi]` ，表示在选修课程 `ai` 前 **必须** 先选修 `bi` 。

- 例如，想要学习课程 `0` ，你需要先完成课程 `1` ，我们用一个匹配来表示：`[0,1]` 。

返回你为了学完所有课程所安排的学习顺序。可能会有多个正确的顺序，你只要返回 **任意一种** 就可以了。如果不可能完成所有课程，返回 **一个空数组** 。

 

**示例 1：**

```
输入：numCourses = 2, prerequisites = [[1,0]]
输出：[0,1]
解释：总共有 2 门课程。要学习课程 1，你需要先完成课程 0。因此，正确的课程顺序为 [0,1] 。
```

**示例 2：**

```
输入：numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]]
输出：[0,2,1,3]
解释：总共有 4 门课程。要学习课程 3，你应该先完成课程 1 和课程 2。并且课程 1 和课程 2 都应该排在课程 0 之后。
因此，一个正确的课程顺序是 [0,1,2,3] 。另一个正确的排序是 [0,2,1,3] 。
```

**示例 3：**

```
输入：numCourses = 1, prerequisites = []
输出：[0]
```

 

**提示：**

- `1 <= numCourses <= 2000`
- `0 <= prerequisites.length <= numCourses * (numCourses - 1)`
- `prerequisites[i].length == 2`
- `0 <= ai, bi < numCourses`
- `ai != bi`
- 所有`[ai, bi]` **互不相同**

解法一：floodfill

```cpp
class Solution {
public:
    vector<int> findOrder(int n, vector<vector<int>>& prerequisites) {
        unordered_map<int, vector<int>> edges;
        vector<int> in(n), ret;

        for(auto& e : prerequisites)
        {
            int a = e[0], b = e[1]; // b -> a
            edges[b].push_back(a);
            in[a]++;
        }

        queue<int> qu;
        for(int i = 0; i < n; i++)
        {
            if(in[i] == 0)
            {
                qu.push(i);
                ret.push_back(i);
            }
        }
        
        while(qu.size())
        {
            int sz = qu.size();
            while(sz--)
            {
                int tmp = qu.front(); qu.pop();
                for(auto t : edges[tmp])
                {
                    if(--in[t] == 0) 
                    {
                        qu.push(t);
                        ret.push_back(t);
                    }
                }
            }
        }
        for(int i = 0; i < n; i++) 
        {
            if(in[i]) return {};
        }
        return ret;
    }   
};
```



## [LCR 114. 火星词典 - 力扣（LeetCode）](https://leetcode.cn/problems/Jf1JuT/description/)

现有一种使用英语字母的外星文语言，这门语言的字母顺序与英语顺序不同。

给定一个字符串列表 `words` ，作为这门语言的词典，`words` 中的字符串已经 **按这门新语言的字母顺序进行了排序** 。

请你根据该词典还原出此语言中已知的字母顺序，并 **按字母递增顺序** 排列。若不存在合法字母顺序，返回 `""` 。若存在多种可能的合法字母顺序，返回其中 **任意一种** 顺序即可。

字符串 `s` **字典顺序小于** 字符串 `t` 有两种情况：

- 在第一个不同字母处，如果 `s` 中的字母在这门外星语言的字母顺序中位于 `t` 中字母之前，那么 `s` 的字典顺序小于 `t` 。
- 如果前面 `min(s.length, t.length)` 字母都相同，那么 `s.length < t.length` 时，`s` 的字典顺序也小于 `t` 。

 

**示例 1：**

```
输入：words = ["wrt","wrf","er","ett","rftt"]
输出："wertf"
```

**示例 2：**

```
输入：words = ["z","x"]
输出："zx"
```

**示例 3：**

```
输入：words = ["z","x","z"]
输出：""
解释：不存在合法字母顺序，因此返回 "" 。
```

 

**提示：**

- `1 <= words.length <= 100`
- `1 <= words[i].length <= 100`
- `words[i]` 仅由小写英文字母组成

解法一：floodfill算法

```cpp
class Solution {
    unordered_map<char, unordered_set<char>> edges;
    unordered_map<char, int> in;
    bool check;
public:
    string alienOrder(vector<string>& words) {
        for(auto e : words)
            for(auto ch : e)
            {
                in[ch] = 0;
            }

        int n = words.size();
        for(int i = 0; i < n; i++)
        {
            for(int j = i + 1; j < n; j++)
            {
                add(words[i], words[j]);
                if(check) return "";
            }
        }
        queue<char> q;
        for(auto& [a, b] : in)
        {
            if(b == 0) q.push(a);
        }
        string ret;
        while(q.size())
        {
            char t = q.front(); q.pop();
            ret += t;
            for(char ch : edges[t])
            {
                if(--in[ch] == 0) q.push(ch);
            }
        }

        for(auto& [a, b] : in)
        {
            if(b != 0) return "";
        }
        return ret;
    }

    void add(string& s1, string& s2)
    {
        int n = min(s1.size(), s2.size());
        int i = 0;
        for(; i < n; i++)
        {
            if(s1[i] != s2[i])
            {
                char a = s1[i], b = s2[i];
                if(!edges.count(a) || !edges[a].count(b))
                {
                    edges[a].insert(b);
                    in[b]++;
                }
                break;
            }
        }
        if(i == s2.size() && i < s1.size()) check = true;
    }
};
```

# DAY97

## [860. 柠檬水找零 - 力扣（LeetCode）](https://leetcode.cn/problems/lemonade-change/description/)

在柠檬水摊上，每一杯柠檬水的售价为 `5` 美元。顾客排队购买你的产品，（按账单 `bills` 支付的顺序）一次购买一杯。

每位顾客只买一杯柠檬水，然后向你付 `5` 美元、`10` 美元或 `20` 美元。你必须给每个顾客正确找零，也就是说净交易是每位顾客向你支付 `5` 美元。

注意，一开始你手头没有任何零钱。

给你一个整数数组 `bills` ，其中 `bills[i]` 是第 `i` 位顾客付的账。如果你能给每位顾客正确找零，返回 `true` ，否则返回 `false` 。

 

**示例 1：**

```
输入：bills = [5,5,5,10,20]
输出：true
解释：
前 3 位顾客那里，我们按顺序收取 3 张 5 美元的钞票。
第 4 位顾客那里，我们收取一张 10 美元的钞票，并返还 5 美元。
第 5 位顾客那里，我们找还一张 10 美元的钞票和一张 5 美元的钞票。
由于所有客户都得到了正确的找零，所以我们输出 true。
```

**示例 2：**

```
输入：bills = [5,5,10,10,20]
输出：false
解释：
前 2 位顾客那里，我们按顺序收取 2 张 5 美元的钞票。
对于接下来的 2 位顾客，我们收取一张 10 美元的钞票，然后返还 5 美元。
对于最后一位顾客，我们无法退回 15 美元，因为我们现在只有两张 10 美元的钞票。
由于不是每位顾客都得到了正确的找零，所以答案是 false。
```

 

**提示：**

- `1 <= bills.length <= 105`
- `bills[i]` 不是 `5` 就是 `10` 或是 `20` 

解法一：贪心

```cpp
class Solution {
public:
    bool lemonadeChange(vector<int>& bills) {
        int five = 0, ten = 0;
        for(int e : bills)
        {
            if(e == 5) five++;
            else if(e == 10)
            {
                if(five) five--, ten++;
                else return false;
            }
            else
            {
                if(ten && five) ten--, five--;
                else five -= 3;
            }
            if(five < 0) return false;
        }
        return true;
    }
};
```

## [2208. 将数组和减半的最少操作次数 - 力扣（LeetCode）](https://leetcode.cn/problems/minimum-operations-to-halve-array-sum/description/)

给你一个正整数数组 `nums` 。每一次操作中，你可以从 `nums` 中选择 **任意** 一个数并将它减小到 **恰好** 一半。（注意，在后续操作中你可以对减半过的数继续执行操作）

请你返回将 `nums` 数组和 **至少** 减少一半的 **最少** 操作数。

 

**示例 1：**

```
输入：nums = [5,19,8,1]
输出：3
解释：初始 nums 的和为 5 + 19 + 8 + 1 = 33 。
以下是将数组和减少至少一半的一种方法：
选择数字 19 并减小为 9.5 。
选择数字 9.5 并减小为 4.75 。
选择数字 8 并减小为 4 。
最终数组为 [5, 4.75, 4, 1] ，和为 5 + 4.75 + 4 + 1 = 14.75 。
nums 的和减小了 33 - 14.75 = 18.25 ，减小的部分超过了初始数组和的一半，18.25 >= 33/2 = 16.5 。
我们需要 3 个操作实现题目要求，所以返回 3 。
可以证明，无法通过少于 3 个操作使数组和减少至少一半。
```

**示例 2：**

```
输入：nums = [3,8,20]
输出：3
解释：初始 nums 的和为 3 + 8 + 20 = 31 。
以下是将数组和减少至少一半的一种方法：
选择数字 20 并减小为 10 。
选择数字 10 并减小为 5 。
选择数字 3 并减小为 1.5 。
最终数组为 [1.5, 8, 5] ，和为 1.5 + 8 + 5 = 14.5 。
nums 的和减小了 31 - 14.5 = 16.5 ，减小的部分超过了初始数组和的一半， 16.5 >= 31/2 = 15.5 。
我们需要 3 个操作实现题目要求，所以返回 3 。
可以证明，无法通过少于 3 个操作使数组和减少至少一半。
```

 

**提示：**

- `1 <= nums.length <= 105`
- `1 <= nums[i] <= 107`

解法一：贪心

```cpp
class Solution {
public:
    int halveArray(vector<int>& nums) {
        double sum  = 0, ret = 0;
        priority_queue<double> pq;
        for(int e : nums) 
        {
            sum += e;
            pq.push(e);
        }
        double half = sum / 2.0, count = 0;
        while(half > count)
        {
            double tmp = pq.top() / 2; pq.pop();
            count += tmp;
            pq.push(tmp);
            ret++;
        }
        return ret;
    }
};
```

# DAY98

## [179. 最大数 - 力扣（LeetCode）](https://leetcode.cn/problems/largest-number/description/)

给定一组非负整数 `nums`，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数。

**注意：**输出结果可能非常大，所以你需要返回一个字符串而不是整数。

 

**示例 1：**

```
输入：nums = [10,2]
输出："210"
```

**示例 2：**

```
输入：nums = [3,30,34,5,9]
输出："9534330"
```

 

**提示：**

- `1 <= nums.length <= 100`
- `0 <= nums[i] <= 109`

解法一：贪心

```cpp
class Solution {
public:
    string largestNumber(vector<int>& nums) {
        vector<string> tmp;

        for(int e : nums) tmp.push_back(to_string(e));
        string ret;
        sort(tmp.begin(), tmp.end(), [](const string& s1, const string& s2)
        {
            return s1 + s2 > s2 + s1;
        });

        for(string& s : tmp) ret += s;

        return ret[0] == '0' ? "0" : ret;

    }
};
```

## [376. 摆动序列 - 力扣（LeetCode）](https://leetcode.cn/problems/wiggle-subsequence/description/)

如果连续数字之间的差严格地在正数和负数之间交替，则数字序列称为 **摆动序列 。**第一个差（如果存在的话）可能是正数或负数。仅有一个元素或者含两个不等元素的序列也视作摆动序列。

- 例如， `[1, 7, 4, 9, 2, 5]` 是一个 **摆动序列** ，因为差值 `(6, -3, 5, -7, 3)` 是正负交替出现的。
- 相反，`[1, 4, 7, 2, 5]` 和 `[1, 7, 4, 5, 5]` 不是摆动序列，第一个序列是因为它的前两个差值都是正数，第二个序列是因为它的最后一个差值为零。

**子序列** 可以通过从原始序列中删除一些（也可以不删除）元素来获得，剩下的元素保持其原始顺序。

给你一个整数数组 `nums` ，返回 `nums` 中作为 **摆动序列** 的 **最长子序列的长度** 。

 

**示例 1：**

```
输入：nums = [1,7,4,9,2,5]
输出：6
解释：整个序列均为摆动序列，各元素之间的差值为 (6, -3, 5, -7, 3) 。
```

**示例 2：**

```
输入：nums = [1,17,5,10,13,15,10,5,16,8]
输出：7
解释：这个序列包含几个长度为 7 摆动序列。
其中一个是 [1, 17, 10, 13, 10, 16, 8] ，各元素之间的差值为 (16, -7, 3, -3, 6, -8) 。
```

**示例 3：**

```
输入：nums = [1,2,3,4,5,6,7,8,9]
输出：2
```

 

**提示：**

- `1 <= nums.length <= 1000`
- `0 <= nums[i] <= 1000`

解法一：贪心

```cpp
class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        int ret = 1, flag = 0;
        for(int i = 1; i < nums.size(); i++)
        {
            if(nums[i] > nums[i - 1] && (flag == 0 || flag == -1)) 
                ret++, flag = 1;

            else if(nums[i] < nums[i - 1] && (flag == 0 || flag == 1))
                ret++, flag = -1;
        }
        return ret;
    }
};
```

# DAY99

## [300. 最长递增子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-increasing-subsequence/description/)

给你一个整数数组 `nums` ，找到其中最长严格递增子序列的长度。

**子序列** 是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。例如，`[3,6,2,7]` 是数组 `[0,3,1,6,2,2,7]` 的子序列。

 

**示例 1：**

```
输入：nums = [10,9,2,5,3,7,101,18]
输出：4
解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
```

**示例 2：**

```
输入：nums = [0,1,0,3,2,3]
输出：4
```

**示例 3：**

```
输入：nums = [7,7,7,7,7,7,7]
输出：1
```

 

**提示：**

- `1 <= nums.length <= 2500`
- `-104 <= nums[i] <= 104`

 

**进阶：**

- 你能将算法的时间复杂度降低到 `O(n log(n))` 吗?

解法一：贪心

```cpp
class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int n = nums.size();
        vector<int> arr;
        arr.push_back(nums[0]);
        for(int i = 1; i < nums.size(); i++)
        {
            if(nums[i] > arr.back()) arr.push_back(nums[i]);
            else
            {
                int begin = 0, end = arr.size() - 1;
                while(begin < end)
                {
                    int mid = (begin + end) / 2;
                    if(nums[i] > arr[mid]) begin = mid + 1;
                    else end = mid;
                }
                arr[begin] = nums[i];
            }
        }
        return arr.size();
    }
};
```

## [334. 递增的三元子序列 - 力扣（LeetCode）](https://leetcode.cn/problems/increasing-triplet-subsequence/description/)

给你一个整数数组 `nums` ，判断这个数组中是否存在长度为 `3` 的递增子序列。

如果存在这样的三元组下标 `(i, j, k)` 且满足 `i < j < k` ，使得 `nums[i] < nums[j] < nums[k]` ，返回 `true` ；否则，返回 `false` 。

 

**示例 1：**

```
输入：nums = [1,2,3,4,5]
输出：true
解释：任何 i < j < k 的三元组都满足题意
```

**示例 2：**

```
输入：nums = [5,4,3,2,1]
输出：false
解释：不存在满足题意的三元组
```

**示例 3：**

```
输入：nums = [2,1,5,0,4,6]
输出：true
解释：三元组 (3, 4, 5) 满足题意，因为 nums[3] == 0 < nums[4] == 4 < nums[5] == 6
```

 

**提示：**

- `1 <= nums.length <= 5 * 105`
- `-231 <= nums[i] <= 231 - 1`

 

**进阶：**你能实现时间复杂度为 `O(n)` ，空间复杂度为 `O(1)` 的解决方案吗？



解法一：贪心

```cpp
class Solution {
public:
    bool increasingTriplet(vector<int>& nums) {
        int n = nums.size();
        vector<int> arr;
        arr.push_back(nums[0]);
        for(int i = 1; i < n; i++)
        {
            if(nums[i] > arr.back()) arr.push_back(nums[i]);
            else
            {
                int left = 0, right = arr.size() - 1;
                while(left < right)
                {
                    int mid = (left + right) / 2;
                    if(nums[i] > arr[mid]) left = mid + 1;
                    else right = mid;
                }
                arr[left] = nums[i];
            }
            if(arr.size() == 3) return true;
        }
        return false;
    }
};
```

# DAY100

## [674. 最长连续递增序列 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-continuous-increasing-subsequence/description/)

给定一个未经排序的整数数组，找到最长且 **连续递增的子序列**，并返回该序列的长度。

**连续递增的子序列** 可以由两个下标 `l` 和 `r`（`l < r`）确定，如果对于每个 `l <= i < r`，都有 `nums[i] < nums[i + 1]` ，那么子序列 `[nums[l], nums[l + 1], ..., nums[r - 1], nums[r]]` 就是连续递增子序列。

 

**示例 1：**

```
输入：nums = [1,3,5,4,7]
输出：3
解释：最长连续递增序列是 [1,3,5], 长度为3。
尽管 [1,3,5,7] 也是升序的子序列, 但它不是连续的，因为 5 和 7 在原数组里被 4 隔开。 
```

**示例 2：**

```
输入：nums = [2,2,2,2,2]
输出：1
解释：最长连续递增序列是 [2], 长度为1。
```

 

**提示：**

- `1 <= nums.length <= 104`
- `-109 <= nums[i] <= 109`

```cpp
class Solution {
public:
    int findLengthOfLCIS(vector<int>& nums) {
        int ret = 1, tmp = 1;
        for(int i = 1; i < nums.size(); i++)
        {
            if(nums[i] > nums[i - 1]) tmp++;
            else tmp = 1;
            ret = max(ret, tmp);
        }
        return ret;
    }
};
```

## [121. 买卖股票的最佳时机 - 力扣（LeetCode）](https://leetcode.cn/problems/best-time-to-buy-and-sell-stock/description/)

给定一个数组 `prices` ，它的第 `i` 个元素 `prices[i]` 表示一支给定股票第 `i` 天的价格。

你只能选择 **某一天** 买入这只股票，并选择在 **未来的某一个不同的日子** 卖出该股票。设计一个算法来计算你所能获取的最大利润。

返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 `0` 。

 

**示例 1：**

```
输入：[7,1,5,3,6,4]
输出：5
解释：在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
     注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格；同时，你不能在买入前卖出股票。
```

**示例 2：**

```
输入：prices = [7,6,4,3,1]
输出：0
解释：在这种情况下, 没有交易完成, 所以最大利润为 0。
```

 

**提示：**

- `1 <= prices.length <= 105`
- `0 <= prices[i] <= 104`

解法一：贪心

```cpp
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int minNum = INT_MAX, ret = 0;
        for(int i = 0; i < prices.size(); i++)
        {
            ret = max(ret, prices[i] - minNum);
            minNum = min(minNum, prices[i]);
        }

        return ret;
    }
};
```



# DAY101

## [122. 买卖股票的最佳时机 II - 力扣（LeetCode）](https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-ii/description/)

给你一个整数数组 `prices` ，其中 `prices[i]` 表示某支股票第 `i` 天的价格。

在每一天，你可以决定是否购买和/或出售股票。你在任何时候 **最多** 只能持有 **一股** 股票。你也可以先购买，然后在 **同一天** 出售。

返回 *你能获得的 **最大** 利润* 。

 

**示例 1：**

```
输入：prices = [7,1,5,3,6,4]
输出：7
解释：在第 2 天（股票价格 = 1）的时候买入，在第 3 天（股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5 - 1 = 4 。
     随后，在第 4 天（股票价格 = 3）的时候买入，在第 5 天（股票价格 = 6）的时候卖出, 这笔交易所能获得利润 = 6 - 3 = 3 。
     总利润为 4 + 3 = 7 。
```

**示例 2：**

```
输入：prices = [1,2,3,4,5]
输出：4
解释：在第 1 天（股票价格 = 1）的时候买入，在第 5 天 （股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5 - 1 = 4 。
     总利润为 4 。
```

**示例 3：**

```
输入：prices = [7,6,4,3,1]
输出：0
解释：在这种情况下, 交易无法获得正利润，所以不参与交易可以获得最大利润，最大利润为 0 。
```

 

**提示：**

- `1 <= prices.length <= 3 * 104`
- `0 <= prices[i] <= 104`

解法一：贪心

```cpp
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int ret = 0, i = 1, cur = 0;
        for(; i < prices.size(); i++)
        {
            if(prices[i] <= prices[i - 1]) 
            {
                ret += prices[i - 1] - prices[cur];
                cur = i;
            }
        }
        ret += prices[i - 1] - prices[cur];
        return ret;
    }
};
```

## [1005. K 次取反后最大化的数组和 - 力扣（LeetCode）](https://leetcode.cn/problems/maximize-sum-of-array-after-k-negations/description/)

给你一个整数数组 `nums` 和一个整数 `k` ，按以下方法修改该数组：

- 选择某个下标 `i` 并将 `nums[i]` 替换为 `-nums[i]` 。

重复这个过程恰好 `k` 次。可以多次选择同一个下标 `i` 。

以这种方式修改数组后，返回数组 **可能的最大和** 。

 

**示例 1：**

```
输入：nums = [4,2,3], k = 1
输出：5
解释：选择下标 1 ，nums 变为 [4,-2,3] 。
```

**示例 2：**

```
输入：nums = [3,-1,0,2], k = 3
输出：6
解释：选择下标 (1, 2, 2) ，nums 变为 [3,1,0,2] 。
```

**示例 3：**

```
输入：nums = [2,-3,-1,5,-4], k = 2
输出：13
解释：选择下标 (1, 4) ，nums 变为 [2,3,-1,5,4] 。
```

 

**提示：**

- `1 <= nums.length <= 104`
- `-100 <= nums[i] <= 100`
- `1 <= k <= 104`

解法一：贪心

```cpp
class Solution {
public:
    int largestSumAfterKNegations(vector<int>& nums, int k) {
        int count = 0, minCur = 0;
        sort(nums.begin(), nums.end());
        for(int i = 0; i < nums.size(); i++)
        {
            if(nums[i] < 0) count++;
            if(abs(nums[minCur]) > abs(nums[i])) minCur = i;
        }

        if(count == 0)
            while(k--) nums[minCur] *= -1;
                
        if(k <= count)
            for(int i = 0; i < k; i++) nums[i] *= -1;
                
        else
        {
            int i = 0;
            for(; i < count; i++) nums[i] *= -1;

            int tmp = k - count;
            while(tmp--) nums[minCur] *= -1;
        }
        int ret = 0;
        for(int e : nums) ret += e;
        return ret;
    }
};
```

# DAY 102

## [2418. 按身高排序 - 力扣（LeetCode）](https://leetcode.cn/problems/sort-the-people/description/)

给你一个字符串数组 `names` ，和一个由 **互不相同** 的正整数组成的数组 `heights` 。两个数组的长度均为 `n` 。

对于每个下标 `i`，`names[i]` 和 `heights[i]` 表示第 `i` 个人的名字和身高。

请按身高 **降序** 顺序返回对应的名字数组 `names` 。

 

**示例 1：**

```
输入：names = ["Mary","John","Emma"], heights = [180,165,170]
输出：["Mary","Emma","John"]
解释：Mary 最高，接着是 Emma 和 John 。
```

**示例 2：**

```
输入：names = ["Alice","Bob","Bob"], heights = [155,185,150]
输出：["Bob","Alice","Bob"]
解释：第一个 Bob 最高，然后是 Alice 和第二个 Bob 。
```

 

**提示：**

- `n == names.length == heights.length`
- `1 <= n <= 103`
- `1 <= names[i].length <= 20`
- `1 <= heights[i] <= 105`
- `names[i]` 由大小写英文字母组成
- `heights` 中的所有值互不相同

解法一：排序

```cpp
class Solution {
public:
    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
        unordered_map<int, string> hash;
        for(int i = 0; i < names.size(); i++) hash[heights[i]] = names[i];

        sort(heights.begin(), heights.end(), greater<int>());
        vector<string> ret;
        for(int e : heights) ret.push_back(hash[e]);
        return ret;
    }
};
```

## [870. 优势洗牌 - 力扣（LeetCode）](https://leetcode.cn/problems/advantage-shuffle/description/)

给定两个长度相等的数组 `nums1` 和 `nums2`，`nums1` 相对于 `nums2` 的*优势*可以用满足 `nums1[i] > nums2[i]` 的索引 `i` 的数目来描述。

返回 nums1 的**任意**排列，使其相对于 `nums2` 的优势最大化。

 

**示例 1：**

```
输入：nums1 = [2,7,11,15], nums2 = [1,10,4,11]
输出：[2,11,7,15]
```

**示例 2：**

```
输入：nums1 = [12,24,8,32], nums2 = [13,25,32,11]
输出：[24,32,8,12]
```

 

**提示：**

- `1 <= nums1.length <= 105`
- `nums2.length == nums1.length`
- `0 <= nums1[i], nums2[i] <= 109`

```cpp
class Solution {
public:
    vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2) {
        int sz = nums1.size();
        vector<int> index(sz);
        for(int i = 0; i < sz; i++) index[i] = i;

        sort(index.begin(), index.end(), [&](int i, int j)
        {
            return nums2[i] < nums2[j];
        });
        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());

        int start1 = 0, start2 = 0, end = sz - 1;
        vector<int> ret(sz);
        while(start1 < sz)
        {
            if(nums1[start1] <= nums2[start2])
                ret[index[end--]] = nums1[start1++];
            else
                ret[index[start2++]] = nums1[start1++];
        }
        return ret;
    }
};
```

# DAY103

## [409. 最长回文串 - 力扣（LeetCode）](https://leetcode.cn/problems/longest-palindrome/description/)

给定一个包含大写字母和小写字母的字符串 `s` ，返回 *通过这些字母构造成的 **最长的回文串*** 。

在构造过程中，请注意 **区分大小写** 。比如 `"Aa"` 不能当做一个回文字符串。

 

**示例 1:**

```
输入:s = "abccccdd"
输出:7
解释:
我们可以构造的最长的回文串是"dccaccd", 它的长度是 7。
```

**示例 2:**

```
输入:s = "a"
输出:1
```

**示例 3：**

```
输入:s = "aaaaaccc"
输出:7
```

 

**提示:**

- `1 <= s.length <= 2000`
- `s` 只由小写 **和/或** 大写英文字母组成

```cpp
class Solution {
public:
    int longestPalindrome(string s) {
        unordered_map<char, int> hash;
        for(char e : s) hash[e]++;
        int ret = 0;
        for(auto& e : hash)
        {
            int n = e.second / 2;
            ret += 2 * n;
        }
        if(ret < s.size()) return ret + 1;
        return ret;
    }
};
```

##  [942. 增减字符串匹配 - 力扣（LeetCode）](https://leetcode.cn/problems/di-string-match/description/)

由范围 `[0,n]` 内所有整数组成的 `n + 1` 个整数的排列序列可以表示为长度为 `n` 的字符串 `s` ，其中:

- 如果 `perm[i] < perm[i + 1]` ，那么 `s[i] == 'I'` 
- 如果 `perm[i] > perm[i + 1]` ，那么 `s[i] == 'D'` 

给定一个字符串 `s` ，重构排列 `perm` 并返回它。如果有多个有效排列perm，则返回其中 **任何一个** 。

 

**示例 1：**

```
输入：s = "IDID"
输出：[0,4,1,3,2]
```

**示例 2：**

```
输入：s = "III"
输出：[0,1,2,3]
```

**示例 3：**

```
输入：s = "DDI"
输出：[3,2,0,1]
```

 

**提示：**

- `1 <= s.length <= 105`
- `s` 只包含字符 `"I"` 或 `"D"`

```cpp
class Solution {
public:
    vector<int> diStringMatch(string s) {
        int n = s.size();
        vector<int> ret(n + 1);
        int start = 0, finish = n, cur = 0;
        for(char& ch : s)
        {
            if(ch == 'I') ret[cur++] = start++;
            else ret[cur++] = finish--;
        }
        ret[cur] = start;
        return ret;
    }
};
```

# DAY 104

## [455. 分发饼干 - 力扣（LeetCode）](https://leetcode.cn/problems/assign-cookies/description/)

假设你是一位很棒的家长，想要给你的孩子们一些小饼干。但是，每个孩子最多只能给一块饼干。

对每个孩子 `i`，都有一个胃口值 `g[i]`，这是能让孩子们满足胃口的饼干的最小尺寸；并且每块饼干 `j`，都有一个尺寸 `s[j]` 。如果 `s[j] >= g[i]`，我们可以将这个饼干 `j` 分配给孩子 `i` ，这个孩子会得到满足。你的目标是尽可能满足越多数量的孩子，并输出这个最大数值。

 

**示例 1:**

```
输入: g = [1,2,3], s = [1,1]
输出: 1
解释: 
你有三个孩子和两块小饼干，3个孩子的胃口值分别是：1,2,3。
虽然你有两块小饼干，由于他们的尺寸都是1，你只能让胃口值是1的孩子满足。
所以你应该输出1。
```

**示例 2:**

```
输入: g = [1,2], s = [1,2,3]
输出: 2
解释: 
你有两个孩子和三块小饼干，2个孩子的胃口值分别是1,2。
你拥有的饼干数量和尺寸都足以让所有孩子满足。
所以你应该输出2.
```

 

**提示：**

- `1 <= g.length <= 3 * 104`
- `0 <= s.length <= 3 * 104`
- `1 <= g[i], s[j] <= 231 - 1`

解法一：贪心

```cpp
class Solution {
public:
    int findContentChildren(vector<int>& g, vector<int>& s) {
        int ret = 0;
        sort(g.begin(), g.end());
        sort(s.begin(), s.end());
        int i = 0, j = 0;
        while(i < g.size() && j < s.size())
        {
            if(s[j] >= g[i]) ret++, i++, j++;
            else j++;
        }
        return ret;
    }
};
```

## [553. 最优除法 - 力扣（LeetCode）](https://leetcode.cn/problems/optimal-division/description/)

给定一正整数数组 `nums`**，**`nums` 中的相邻整数将进行浮点除法。例如， [2,3,4] -> 2 / 3 / 4 。

- 例如，`nums = [2,3,4]`，我们将求表达式的值 `"2/3/4"`。

但是，你可以在任意位置添加任意数目的括号，来改变算数的优先级。你需要找出怎么添加括号，以便计算后的表达式的值为最大值。

以字符串格式返回具有最大值的对应表达式。

**注意：**你的表达式不应该包含多余的括号。

 

**示例 1：**

```
输入: [1000,100,10,2]
输出: "1000/(100/10/2)"
解释: 1000/(100/10/2) = 1000/((100/10)/2) = 200
但是，以下加粗的括号 "1000/((100/10)/2)" 是冗余的，
因为他们并不影响操作的优先级，所以你需要返回 "1000/(100/10/2)"。

其他用例:
1000/(100/10)/2 = 50
1000/(100/(10/2)) = 50
1000/100/10/2 = 0.5
1000/100/(10/2) = 2
```

 

**示例 2:**

```
输入: nums = [2,3,4]
输出: "2/(3/4)"
解释: (2/(3/4)) = 8/3 = 2.667
可以看出，在尝试了所有的可能性之后，我们无法得到一个结果大于 2.667 的表达式。
```

 

**说明:**

- `1 <= nums.length <= 10`
- `2 <= nums[i] <= 1000`
- 对于给定的输入只有一种最优除法。

解法一：贪心

```cpp
class Solution {

public:
    string optimalDivision(vector<int>& nums) {
        int sz = nums.size();
        if(sz == 1) return to_string(nums[0]);
        if(sz == 2) return to_string(nums[0]) + '/' + to_string(nums[1]);
        string ret;
        ret += to_string(nums[0]) + "/(" + to_string(nums[1]);
        for(int i = 2; i < nums.size(); i++)
            ret += '/' + to_string(nums[i]);          
        ret += ')';
        return ret;
    }
};
```

# DAY105

## [45. 跳跃游戏 II - 力扣（LeetCode）](https://leetcode.cn/problems/jump-game-ii/description/)

给定一个长度为 `n` 的 **0 索引**整数数组 `nums`。初始位置为 `nums[0]`。

每个元素 `nums[i]` 表示从索引 `i` 向前跳转的最大长度。换句话说，如果你在 `nums[i]` 处，你可以跳转到任意 `nums[i + j]` 处:

- `0 <= j <= nums[i]` 
- `i + j < n`

返回到达 `nums[n - 1]` 的最小跳跃次数。生成的测试用例可以到达 `nums[n - 1]`。

 

**示例 1:**

```
输入: nums = [2,3,1,1,4]
输出: 2
解释: 跳到最后一个位置的最小跳跃数是 2。
     从下标为 0 跳到下标为 1 的位置，跳 1 步，然后跳 3 步到达数组的最后一个位置。
```

**示例 2:**

```
输入: nums = [2,3,0,1,4]
输出: 2
```

 

**提示:**

- `1 <= nums.length <= 104`
- `0 <= nums[i] <= 1000`
- 题目保证可以到达 `nums[n-1]`

解法一：贪心

```cpp
class Solution {
public:
    int jump(vector<int>& nums) {
        int sz = nums.size();
        int left = 0, right = 0, maxpos = 0, ret = 0;
        while(left <= right)
        {
            if(maxpos >= sz - 1) return ret;
            for(int i = left; i <= right; i++) maxpos = max(maxpos, nums[i] + i);
            left = right + 1;
            right = maxpos;
            ret++;
        }
        return 0;
    }
};
```

## [55. 跳跃游戏 - 力扣（LeetCode）](https://leetcode.cn/problems/jump-game/description/)

给你一个非负整数数组 `nums` ，你最初位于数组的 **第一个下标** 。数组中的每个元素代表你在该位置可以跳跃的最大长度。

判断你是否能够到达最后一个下标，如果可以，返回 `true` ；否则，返回 `false` 。

 

**示例 1：**

```
输入：nums = [2,3,1,1,4]
输出：true
解释：可以先跳 1 步，从下标 0 到达下标 1, 然后再从下标 1 跳 3 步到达最后一个下标。
```

**示例 2：**

```
输入：nums = [3,2,1,0,4]
输出：false
解释：无论怎样，总会到达下标为 3 的位置。但该下标的最大跳跃长度是 0 ， 所以永远不可能到达最后一个下标。
```

 

**提示：**

- `1 <= nums.length <= 104`
- `0 <= nums[i] <= 105`

解法一：贪心

```cpp
class Solution {
public:
    bool canJump(vector<int>& nums) {
        int sz = nums.size();
        int left = 0, right = 0, maxpos = 0, ret = 0;
        while(left <= right)
        {
            if(maxpos >= sz - 1) return true;
            for(int i = left; i <= right; i++) maxpos = max(maxpos, nums[i] + i);
            left = right + 1;
            right = maxpos;
            ret++;
        }
        return false;
    }
};
```

# DAY 106

## [134. 加油站 - 力扣（LeetCode）](https://leetcode.cn/problems/gas-station/description/)

在一条环路上有 `n` 个加油站，其中第 `i` 个加油站有汽油 `gas[i]` 升。

你有一辆油箱容量无限的的汽车，从第 `i` 个加油站开往第 `i+1` 个加油站需要消耗汽油 `cost[i]` 升。你从其中的一个加油站出发，开始时油箱为空。

给定两个整数数组 `gas` 和 `cost` ，如果你可以按顺序绕环路行驶一周，则返回出发时加油站的编号，否则返回 `-1` 。如果存在解，则 **保证** 它是 **唯一** 的。

 

**示例 1:**

```
输入: gas = [1,2,3,4,5], cost = [3,4,5,1,2]
输出: 3
解释:
从 3 号加油站(索引为 3 处)出发，可获得 4 升汽油。此时油箱有 = 0 + 4 = 4 升汽油
开往 4 号加油站，此时油箱有 4 - 1 + 5 = 8 升汽油
开往 0 号加油站，此时油箱有 8 - 2 + 1 = 7 升汽油
开往 1 号加油站，此时油箱有 7 - 3 + 2 = 6 升汽油
开往 2 号加油站，此时油箱有 6 - 4 + 3 = 5 升汽油
开往 3 号加油站，你需要消耗 5 升汽油，正好足够你返回到 3 号加油站。
因此，3 可为起始索引。
```

**示例 2:**

```
输入: gas = [2,3,4], cost = [3,4,3]
输出: -1
解释:
你不能从 0 号或 1 号加油站出发，因为没有足够的汽油可以让你行驶到下一个加油站。
我们从 2 号加油站出发，可以获得 4 升汽油。 此时油箱有 = 0 + 4 = 4 升汽油
开往 0 号加油站，此时油箱有 4 - 3 + 2 = 3 升汽油
开往 1 号加油站，此时油箱有 3 - 3 + 3 = 3 升汽油
你无法返回 2 号加油站，因为返程需要消耗 4 升汽油，但是你的油箱只有 3 升汽油。
因此，无论怎样，你都不可能绕环路行驶一周。
```

 

**提示:**

- `gas.length == n`
- `cost.length == n`
- `1 <= n <= 105`
- `0 <= gas[i], cost[i] <= 104`

解法一：贪心

```cpp
class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int sz = gas.size();
        for(int i = 0; i < sz; i++)
        {       
            if(gas[i] >= cost[i])
            {
                int num = gas[i] - cost[i], pos = i, step = 1;
                int j = (pos + 1) % sz;
                for(; j != pos; j = (j + 1) % sz, step++)
                {
                    num += gas[j] - cost[j];
                    if(num < 0) break;
                }
                if(j == pos) return pos;
                i += step;
            }
        }
        return -1;
    }
};
```

## [738. 单调递增的数字 - 力扣（LeetCode）](https://leetcode.cn/problems/monotone-increasing-digits/description/)

当且仅当每个相邻位数上的数字 `x` 和 `y` 满足 `x <= y` 时，我们称这个整数是**单调递增**的。

给定一个整数 `n` ，返回 *小于或等于 `n` 的最大数字，且数字呈 **单调递增*** 。

 

**示例 1:**

```
输入: n = 10
输出: 9
```

**示例 2:**

```
输入: n = 1234
输出: 1234
```

**示例 3:**

```
输入: n = 332
输出: 299
```

 

**提示:**

- `0 <= n <= 109`

解法一：贪心

```cpp
class Solution {
public:
    int monotoneIncreasingDigits(int n) {
        string arr = to_string(n);
        int cur = 0;
        for(int i = 1; i < arr.size(); i++)
        {
            if(arr[i - 1] > arr[i])
            {
                cur = i;
                break;
            }
        }
        if(!cur) return n;
        while(cur > 0)
        {
            if(arr[cur] == arr[cur - 1]) cur--;
            else break;
        }
        for(int i = cur; i < arr.size(); i++) arr[i] = '9';
        arr[cur - 1]--;
        for(int i = cur - 1; i > 0; i--)
            if(arr[i - 1] > arr[i]) arr[i] = '9', arr[i - 1]--;
        int ret = atoi(arr.c_str());
        return ret;
    }
};
```

# DAY 107

## [991. 坏了的计算器 - 力扣（LeetCode）](https://leetcode.cn/problems/broken-calculator/description/)

在显示着数字 `startValue` 的坏计算器上，我们可以执行以下两种操作：

- **双倍（Double）：**将显示屏上的数字乘 2；
- **递减（Decrement）：**将显示屏上的数字减 `1` 。

给定两个整数 `startValue` 和 `target` 。返回显示数字 `target` 所需的最小操作数。

 

**示例 1：**

```
输入：startValue = 2, target = 3
输出：2
解释：先进行双倍运算，然后再进行递减运算 {2 -> 4 -> 3}.
```

**示例 2：**

```
输入：startValue = 5, target = 8
输出：2
解释：先递减，再双倍 {5 -> 4 -> 8}.
```

**示例 3：**

```
输入：startValue = 3, target = 10
输出：3
解释：先双倍，然后递减，再双倍 {3 -> 6 -> 5 -> 10}.
```

 

**提示：**

- `1 <= startValue, target <= 109`

解法一：贪心

```cpp
class Solution {
public:
    int brokenCalc(int startValue, int target) {
        int ret = 0, half, addone;
        while(target > startValue)
        {           
            if(target % 2 == 0) target /= 2;
            else target += 1;
            ret++;
        }
        return ret + startValue - target;
    }
};
```

## [LCR 074. 合并区间 - 力扣（LeetCode）](https://leetcode.cn/problems/SsGoHC/description/)

以数组 `intervals` 表示若干个区间的集合，其中单个区间为 `intervals[i] = [starti, endi]` 。请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。

 

**示例 1：**

```
输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
输出：[[1,6],[8,10],[15,18]]
解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
```

**示例 2：**

```
输入：intervals = [[1,4],[4,5]]
输出：[[1,5]]
解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。
```

 

**提示：**

- `1 <= intervals.length <= 104`
- `intervals[i].length == 2`
- `0 <= starti <= endi <= 104`

解法一：贪心

```cpp
class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        vector<pair<int, int>> hash;
        for(auto& v : intervals) hash.push_back({v[0], v[1]});
        sort(hash.begin(), hash.end(), [&](pair<int, int> p1, pair<int, int> p2)
        {
            return p1.first < p2.first;
        });

        vector<vector<int>> ret;
        ret.push_back({hash[0].first, hash[0].second});
        int sz = intervals.size();

        for(int i = 1; i < sz; i++)
        {
            if(ret.back().back() >= hash[i].first)
            {
                auto tmp = {ret.back().front(), max(ret.back().back(), hash[i].second)};
                ret.pop_back();
                ret.push_back(tmp);
            }              
            else ret.push_back({hash[i].first, hash[i].second});
        }
        return ret;
    }
};
```

# DAY 108

## [435. 无重叠区间 - 力扣（LeetCode）](https://leetcode.cn/problems/non-overlapping-intervals/description/)

给定一个区间的集合 `intervals` ，其中 `intervals[i] = [starti, endi]` 。返回 *需要移除区间的最小数量，使剩余区间互不重叠* 。

 

**示例 1:**

```
输入: intervals = [[1,2],[2,3],[3,4],[1,3]]
输出: 1
解释: 移除 [1,3] 后，剩下的区间没有重叠。
```

**示例 2:**

```
输入: intervals = [ [1,2], [1,2], [1,2] ]
输出: 2
解释: 你需要移除两个 [1,2] 来使剩下的区间没有重叠。
```

**示例 3:**

```
输入: intervals = [ [1,2], [2,3] ]
输出: 0
解释: 你不需要移除任何区间，因为它们已经是无重叠的了。
```

 

**提示:**

- `1 <= intervals.length <= 105`
- `intervals[i].length == 2`
- `-5 * 104 <= starti < endi <= 5 * 104`

解法一：贪心

```cpp
class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
        int ret = 0, sz = intervals.size();
        sort(intervals.begin(), intervals.end());
        int begin = intervals[0][0], end = intervals[0][1];
        for(int i = 1; i < sz; i++)
        {
            int a = intervals[i][0], b = intervals[i][1];
            if(a < end)
            {
                end = min(end, b);
                ret++;
            }
            else end = b;
        }
        return ret;
    }
};
```

## [452. 用最少数量的箭引爆气球 - 力扣（LeetCode）](https://leetcode.cn/problems/minimum-number-of-arrows-to-burst-balloons/description/)

有一些球形气球贴在一堵用 XY 平面表示的墙面上。墙面上的气球记录在整数数组 `points` ，其中`points[i] = [xstart, xend]` 表示水平直径在 `xstart` 和 `xend`之间的气球。你不知道气球的确切 y 坐标。

一支弓箭可以沿着 x 轴从不同点 **完全垂直** 地射出。在坐标 `x` 处射出一支箭，若有一个气球的直径的开始和结束坐标为 `x``start`，`x``end`， 且满足  `xstart ≤ x ≤ x``end`，则该气球会被 **引爆** 。可以射出的弓箭的数量 **没有限制** 。 弓箭一旦被射出之后，可以无限地前进。

给你一个数组 `points` ，*返回引爆所有气球所必须射出的 **最小** 弓箭数* 。

 

**示例 1：**

```
输入：points = [[10,16],[2,8],[1,6],[7,12]]
输出：2
解释：气球可以用2支箭来爆破:
-在x = 6处射出箭，击破气球[2,8]和[1,6]。
-在x = 11处发射箭，击破气球[10,16]和[7,12]。
```

**示例 2：**

```
输入：points = [[1,2],[3,4],[5,6],[7,8]]
输出：4
解释：每个气球需要射出一支箭，总共需要4支箭。
```

**示例 3：**

```
输入：points = [[1,2],[2,3],[3,4],[4,5]]
输出：2
解释：气球可以用2支箭来爆破:
- 在x = 2处发射箭，击破气球[1,2]和[2,3]。
- 在x = 4处射出箭，击破气球[3,4]和[4,5]。
```

 



**提示:**

- `1 <= points.length <= 105`
- `points[i].length == 2`
- `-231 <= xstart < xend <= 231 - 1`

解法一：贪心

```cpp
class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) {
        sort(points.begin(), points.end());
        int left = points[0][0], right = points[0][1], ret = 1;
        for(int i = 1; i < points.size(); i++)
        {
            int a = points[i][0], b = points[i][1];
            if(a <= right) left = a, right = min(right, b);
            else left = a, right = b, ret++;
        }
        return ret;
    }
};
```

# DAY109

## [397. 整数替换 - 力扣（LeetCode）](https://leetcode.cn/problems/integer-replacement/)

给定一个正整数 `n` ，你可以做如下操作：

1. 如果 `n` 是偶数，则用 `n / 2`替换 `n` 。
2. 如果 `n` 是奇数，则可以用 `n + 1`或`n - 1`替换 `n` 。

返回 `n` 变为 `1` 所需的 *最小替换次数* 。

 

**示例 1：**

```
输入：n = 8
输出：3
解释：8 -> 4 -> 2 -> 1
```

**示例 2：**

```
输入：n = 7
输出：4
解释：7 -> 8 -> 4 -> 2 -> 1
或 7 -> 6 -> 3 -> 2 -> 1
```

**示例 3：**

```
输入：n = 4
输出：2
```

 

**提示：**

- `1 <= n <= 231 - 1`

解法一：贪心

```cpp
class Solution {
public:
    int integerReplacement(int n) {
        int ret = 0;
        long long val = n;
        while(val != 1)
        {
            if(val % 2 == 0) 
            {
                val /= 2;
                ret++;
            }
            else 
            {
                long long a = (val + 1) / 2, b = (val - 1) / 2;    
                if(a % 2 == 0) val = a;
                if(b % 2 == 0) val = min(val, b);
                ret += 2;
                if(a == 1 || b == 1) break;
            }
        }
        return ret;
    }
};
```

## [354. 俄罗斯套娃信封问题 - 力扣（LeetCode）](https://leetcode.cn/problems/russian-doll-envelopes/description/)

给你一个二维整数数组 `envelopes` ，其中 `envelopes[i] = [wi, hi]` ，表示第 `i` 个信封的宽度和高度。

当另一个信封的宽度和高度都比这个信封大的时候，这个信封就可以放进另一个信封里，如同俄罗斯套娃一样。

请计算 **最多能有多少个** 信封能组成一组“俄罗斯套娃”信封（即可以把一个信封放到另一个信封里面）。

**注意**：不允许旋转信封。

 

**示例 1：**

```
输入：envelopes = [[5,4],[6,4],[6,7],[2,3]]
输出：3
解释：最多信封的个数为 3, 组合为: [2,3] => [5,4] => [6,7]。
```

**示例 2：**

```
输入：envelopes = [[1,1],[1,1],[1,1]]
输出：1
```

 

**提示：**

- `1 <= envelopes.length <= 105`
- `envelopes[i].length == 2`
- `1 <= wi, hi <= 105`

解法一：贪心

```cpp
class Solution {
public:
    int maxEnvelopes(vector<vector<int>>& envelopes) {
        sort(envelopes.begin(), envelopes.end(), [&](auto& v1, auto& v2)
        {
            return v1[0] != v2[0] ? v1[0] < v2[0] : v1[1] > v2[1];
        });
        int sz = envelopes.size();
        vector<int> ret;
        ret.push_back(envelopes[0][1]);
        for(int i = 1; i < sz; i++)
        {
            if(ret.back() < envelopes[i][1]) ret.push_back(envelopes[i][1]);
            else
            {
                int left = 0, right = ret.size() - 1;
                while(left < right)
                {
                    int mid = (left + right) / 2;
                    if(ret[mid] >= envelopes[i][1]) right = mid;
                    else left = mid + 1;
                }
                ret[left] = envelopes[i][1];
            }
        }
        return ret.size();
    }
};
```

# DAY 110

## [1262. 可被三整除的最大和 - 力扣（LeetCode）](https://leetcode.cn/problems/greatest-sum-divisible-by-three/description/)

给你一个整数数组 `nums`，请你找出并返回能被三整除的元素最大和。



 

**示例 1：**

```
输入：nums = [3,6,5,1,8]
输出：18
解释：选出数字 3, 6, 1 和 8，它们的和是 18（可被 3 整除的最大和）。
```

**示例 2：**

```
输入：nums = [4]
输出：0
解释：4 不能被 3 整除，所以无法选出数字，返回 0。
```

**示例 3：**

```
输入：nums = [1,2,3,4,4]
输出：12
解释：选出数字 1, 3, 4 以及 4，它们的和是 12（可被 3 整除的最大和）。
```

 

**提示：**

- `1 <= nums.length <= 4 * 10^4`
- `1 <= nums[i] <= 10^4`

解法一：贪心

```cpp
class Solution {
public:
    int maxSumDivThree(vector<int>& nums) {
        int ret = 0, x1 = 0x3f3f3f3f, x2 = 0x3f3f3f3f, y1 = 0x3f3f3f3f, y2 = 0x3f3f3f3f;
        for(int e : nums) 
        {
            ret += e;
            if(e % 3 == 1)
            {
                if(e < x1) x2 = x1, x1 = e;
                else if(e < x2) x2 = e;
            }
            else if(e % 3 == 2)
            {
                if(e < y1) y2 = y1, y1 = e;
                else if(e < y2) y2 = e;
            }
        }
        if(ret % 3 == 0) return ret;
        else if(ret % 3 == 1) return max(ret - x1, ret - y1 - y2);
        else return max(ret - y1, ret - x1 - x2);
    }
};
```

## [1054. 距离相等的条形码 - 力扣（LeetCode）](https://leetcode.cn/problems/distant-barcodes/description/)

在一个仓库里，有一排条形码，其中第 `i` 个条形码为 `barcodes[i]`。

请你重新排列这些条形码，使其中任意两个相邻的条形码不能相等。 你可以返回任何满足该要求的答案，此题保证存在答案。

 

**示例 1：**

```
输入：barcodes = [1,1,1,2,2,2]
输出：[2,1,2,1,2,1]
```

**示例 2：**

```
输入：barcodes = [1,1,1,1,2,2,3,3]
输出：[1,3,1,3,2,1,2,1]
```

 

**提示：**

- `1 <= barcodes.length <= 10000`
- `1 <= barcodes[i] <= 10000`

解法一：贪心

```cpp
class Solution {
public:
    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
        int sz = barcodes.size();
        unordered_map<int, int> hash;
        int maxcount = 0, maxval = 0;
        for(int e : barcodes) 
        {
            hash[e]++;
            if(hash[e] > maxcount) 
            {
                maxcount = hash[e];
                maxval = e;
            }
        }

        vector<int> ret(sz);
        int index = 0;
        for(int i = 0; i < maxcount; i++)
        {
            ret[index] = maxval;
            index += 2;
        }

        hash.erase(maxval);

        for(auto& [a, b] : hash)
        {
            for(int i= 0; i < b; i++)
            {
                if(index >= sz) index = 1;
                ret[index] = a;
                index += 2;
            }
        }
        return ret;
    }
};
```

# DAY 111

## [767. 重构字符串 - 力扣（LeetCode）](https://leetcode.cn/problems/reorganize-string/description/)

给定一个字符串 `s` ，检查是否能重新排布其中的字母，使得两相邻的字符不同。

返回 *`s` 的任意可能的重新排列。若不可行，返回空字符串 `""`* 。

 

**示例 1:**

```
输入: s = "aab"
输出: "aba"
```

**示例 2:**

```
输入: s = "aaab"
输出: ""
```

 

**提示:**

- `1 <= s.length <= 500`
- `s` 只包含小写字母

解法一：贪心

```cpp
class Solution {
public:
    string reorganizeString(string s) {
        int sz = s.size(), maxCount = 0;
        char maxVal = 0;
        unordered_map<char, int> hash;
        for(char ch : s) 
            if(++hash[ch] > maxCount)
            {
                maxCount = hash[ch];
                maxVal = ch;
            }

        if(maxCount > sz / 2 + sz % 2) return "";

        string ret;
        ret.resize(sz);
        int index = 0;
        for(int i = 0; i < maxCount; i++, index += 2)
            ret[index] = maxVal;

        hash.erase(maxVal);

        for(auto& [a, b] : hash)
            for(int i = 0; i < b; i++, index += 2)
            {
                if(index >= sz) index = 1;
                ret[index] = a;
            }

        return ret;
    }
};
```

## [组队竞赛__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/248ccf8b479c49a98790db17251e39bb)

牛牛举办了一次编程比赛,参加比赛的有3*n个选手,每个选手都有一个水平值a_i.现在要将这些选手进行组队,一共组成n个队伍,即每个队伍3人.牛牛发现队伍的水平值等于该队伍队员中第二高水平值。
  例如:
  一个队伍三个队员的水平值分别是3,3,3.那么队伍的水平值是3
  一个队伍三个队员的水平值分别是3,2,3.那么队伍的水平值是3
  一个队伍三个队员的水平值分别是1,5,2.那么队伍的水平值是2
  为了让比赛更有看点,牛牛想安排队伍使所有队伍的水平值总和最大。
  如样例所示:
  如果牛牛把6个队员划分到两个队伍
  如果方案为:
  team1:{1,2,5}, team2:{5,5,8}, 这时候水平值总和为7.
  而如果方案为:
  team1:{2,5,8}, team2:{1,5,5}, 这时候水平值总和为10.
  没有比总和为10更大的方案,所以输出10.
                                        

输入描述

```
输入的第一行为一个正整数n(1 ≤ n ≤ 10^5)
第二行包括3*n个整数a_i(1 ≤ a_i ≤ 10^9),表示每个参赛选手的水平值.
```

输出描述:

```
输出一个整数表示所有队伍的水平值总和最大值.
```

示例1

输入

```
2
5 2 8 5 1 5
```

输出

```
10
```

解法一：排序

```cpp
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int n;
    cin >> n;
    int sz = 3 * n;
    vector<int> arr(sz);
    for(int i = 0; i < sz; i++) cin >> arr[i];
    sort(arr.begin(), arr.end());

    long long ret = 0;
    while(n--)
    {
        arr.pop_back();
        ret += arr.back();
        arr.pop_back();
    }
    cout << ret;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [删除公共字符_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/f0db4c36573d459cae44ac90b90c6212?tpId=182&tqId=34789&ru=/exam/oj)

输入两个字符串，从第一字符串中删除第二个字符串中所有的字符。例如，输入”They are students.”和”aeiou”，则删除之后的第一个字符串变成”Thy r stdnts.”

输入描述：

每个测试输入包含2个字符串

输出描述：

输出删除后的字符串

示例1

输入：

```
They are students. 
aeiou
```

输出：

```
Thy r stdnts.
```

```cpp
#include <iostream>
#include <string>
using namespace std;

int main() 
{
    string s1, s2;
    getline(cin, s1);
    getline(cin, s2);
    int hash[256] = {0};
    for(char ch : s2) hash[ch]++;
    string ret;
    for(char ch : s1)
        if(hash[ch] == 0) ret += ch;
    cout << ret;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 112

## [排序子序列_牛客笔试题_牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/2d3f6ddd82da445d804c95db22dcc471)

牛牛定义排序子序列为一个数组中一段连续的子序列,并且这段子序列是非递增或者非递减排序的。牛牛有一个长度为n的整数数组A,他现在有一个任务是把数组A分为若干段排序子序列,牛牛想知道他最少可以把这个数组分为几段排序子序列.
  如样例所示,牛牛可以把数组A划分为[1,2,3]和[2,2,1]两个排序子序列,至少需要划分为2个排序子序列,所以输出2                                       

**输入描述:**

```
输入的第一行为一个正整数n(1 ≤ n ≤ 10^5)
第二行包括n个整数A_i(1 ≤ A_i ≤ 10^9),表示数组A的每个数字。
```

输出描述:

```
输出一个整数表示牛牛可以将A最少划分为多少段排序子序列
```

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() //求波峰波谷
{
    int n;
    cin >> n;
    vector<int> arr(n);
    for(int i = 0; i < n; i++) cin >> arr[i];
    int ret = 1;
    for(int i = 1; i < n - 1; i++)
    {
        if(arr[i] > arr[i - 1] && arr[i] > arr[i + 1]
        || arr[i] < arr[i - 1] && arr[i] < arr[i + 1])
        {
            ret++;
            if(n - 3 != i) i++;
        }
    }
    cout << ret;
    return 0;
}
```

## [倒置字符串__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/8869d99cf1264e60a6d9eff4295e5bab)

将一句话的单词进行倒置，标点不倒置。比如 "I like beijing."，经过处理后变为："beijing. like I"。 

  字符串长度不超过100。 

**输入描述:**

```
输入一个仅包含小写字母、空格、'.' 的字符串，长度不超过100。
'.' 只出现在最后一个单词的末尾。
```

**输出描述:**

```
依次输出倒置之后的字符串，以空格分割。
```

输入

```
I like beijing.
```

输出

```
beijing. like I
```

```cpp
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    string s, ret;
    getline(cin, s);
    reverse(s.begin(), s.end());
    int pos = s.find(' '), prvpos = 0;
    while (pos != string::npos) {
        reverse(s.begin() + prvpos, s.begin() + pos);
        prvpos = pos + 1;
        pos = s.find(' ', pos + 1);
    }
    reverse(s.begin() + prvpos, s.end());
    cout << s;
    return 0;
}
```

# DAYA113

## [假设在一个 32 位 little endian 的机器上运__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/b5e03f2361f04631b2eaf567029385c6)

假设在一个 32 位 little endian 的机器上运行下面的程序，结果是多少？

```cpp
#include <stdio.h>
int main(){
  long long a = 1, b = 2, c = 3; 
  printf("%d %d %d\n", a, b, c);  
 return 0;
}
```

- ```
  1,2,3
  ```

- ```
  1,0,2
  ```

- ```
  1,3,2
  ```

- ```
  3,2,1
  ```

![image-20231203083850720](C:\Users\王志豪\AppData\Roaming\Typora\typora-user-images\image-20231203083850720.png)

## [在字符串中找出连续最长的数字串](https://www.nowcoder.com/questionTerminal/2c81f88ecd5a4cc395b5308a99afbbec)



输入一个字符串，返回其最长的数字子串，以及其长度。若有多个最长的数字子串，则将它们全部输出（按原字符串的相对位置） 

  本题含有多组样例输入。

  数据范围：字符串长度 1≤n≤200 1 \le n \le 200 \ 1≤n≤200 ， 保证每组输入都至少含有一个数字
 **输入描述:**

```
输入一个字符串。1<=len(字符串)<=200 
```

**输出描述:**

```
输出字符串中最长的数字字符串和它的长度，中间用逗号间隔。如果有相同长度的串，则要一块儿输出（中间不要输出空格）。
```

解法一：动规

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() 
{
    string arr;
    while(cin >> arr)
    {
        int sz = arr.size();
        vector<int> dp(sz, 0);
        if(arr[0] >= '0' && arr[0] <= '9') dp[0] = 1;
        int num = 0;
        string ret;
        for(int i = 1; i < sz; i++)
        {
            if(arr[i] >= '0' && arr[i] <= '9')
                dp[i] = dp[i - 1] + 1;
            num = max(num, dp[i]);
        }
        int start = 0;
        for(int i = 0; i < sz; i++)
        {
            if(dp[i] == 1) start = i;
            if(dp[i] == num) 
                ret += arr.substr(start, num);
        }
        cout << ret << "," << num << endl;
    }   
}
// 64 位输出请用 printf("%lld")
```

## [数组中出现次数超过一半的数字](https://www.nowcoder.com/practice/e8a1b01a2df14cb2b228b30ee6a92163?tpId=13&tqId=11181&ru=/exam/oj)

给一个长度为 n 的数组，数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。

例如输入一个长度为9的数组[1,2,3,2,2,2,5,4,2]。由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2。

数据范围：�≤50000*n*≤50000，数组中元素的值 0≤���≤100000≤*v**a**l*≤10000

要求：空间复杂度：�(1)*O*(1)，时间复杂度 �(�)*O*(*n*)

输入描述：

保证数组输入非空，且保证有解

示例1

输入：

```
[1,2,3,2,2,2,5,4,2]
```

返回值：

```
2
```

```cpp
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 
     * @param numbers int整型vector 
     * @return int整型
     */
    int MoreThanHalfNum_Solution(vector<int>& numbers) 
    {
        int num = -1, sum = 0;
        for(int e : numbers)
        {
            if(num == -1) num = e, sum++;
            else if(num == e) sum++;
            else if(--sum == 0) num = -1;
        }
        return num;
    }
};
```

# DAY 114

![image-20231207084719301](C:\Users\王志豪\AppData\Roaming\Typora\typora-user-images\image-20231207084719301.png)

![image-20231207094823252](C:\Users\王志豪\AppData\Roaming\Typora\typora-user-images\image-20231207094823252.png)

## [计算糖果_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/02d8d42b197646a5bbd0a98785bb3a34?tpId=122&tqId=33679&ru=/exam/oj)

描述

A,B,C三个人是好朋友,每个人手里都有一些糖果,我们不知道他们每个人手上具体有多少个糖果,但是我们知道以下的信息：
A - B, B - C, A + B, B + C. 这四个数值.每个字母代表每个人所拥有的糖果数.
现在需要通过这四个数值计算出每个人手里有多少个糖果,即A,B,C。这里保证最多只有一组整数A,B,C满足所有题设条件。

输入描述：

输入为一行，一共4个整数，分别为A - B，B - C，A + B，B + C，用空格隔开。 范围均在-30到30之间(闭区间)。

输出描述：

输出为一行，如果存在满足的整数A，B，C则按顺序输出A，B，C，用空格隔开，行末无空格。 如果不存在这样的整数A，B，C，则输出No

```cpp
#include <iostream>
using namespace std;

int main() {
    int arr[4] = {0};
    for (int i = 0; i < 4; i++) cin >> arr[i];
    int a, b, c;
    if ((arr[0] + arr[2]) % 2 != 0 || (arr[1] + arr[3]) % 2 != 0) cout << "No";
    else {
        a = (arr[0] + arr[2]) / 2;
        b = (arr[1] + arr[3]) / 2;
        c = arr[3] - b;
        if(a < 0 || b < 0 || c < 0) cout << "No";
        else cout << a << " " << b << " " << c;
    }
}
// 64 位输出请用 printf("%lld")
```

## [进制转换_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/ac61207721a34b74b06597fe6eb67c52?tpId=85&&tqId=29862&rp=1&ru=/activity/oj&qru=/ta/2017test/question-ranking)

描述

给定一个十进制数M，以及需要转换的进制数N。将十进制数M转化为N进制数

输入描述：

输入为一行，M(32位整数)、N(2 ≤ N ≤ 16)，以空格隔开。

输出描述：

为每个测试实例输出转换后的数，每个输出占一行。如果N大于9，则对应的数字规则参考16进制（比如，10用A表示，等等）



```cpp
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int m, n;
    cin >> m >> n;
    bool flag = false;
    string s, table="0123456789ABCDEF";
    if(m < 0)
    {
        flag = true;
        m *= -1;
    }
    do {
        s += table[m % n];
        m /= n;
    }while (m);
    if(flag) s += '-';
    reverse(s.begin(), s.end());
    cout << s << endl;
}
// 64 位输出请用 printf("%lld")
```

# DAY 115

## [统计回文_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/9d1559511b3849deaa71b576fa7009dc?tpId=122&tqId=33664&ru=/exam/oj)



“回文串”是一个正读和反读都一样的字符串，比如“level”或者“noon”等等就是回文串。花花非常喜欢这种拥有对称美的回文串，生日的时候她得到两个礼物分别是字符串A和字符串B。现在她非常好奇有没有办法将字符串B插入字符串A使产生的字符串是一个回文串。你接受花花的请求，帮助她寻找有多少种插入办法可以使新串是一个回文串。如果字符串B插入的位置不同就考虑为不一样的办法。
例如：
A = “aba”，B = “b”。这里有4种把B插入A的办法：
\* 在A的第一个字母之前: "baba" 不是回文
\* 在第一个字母‘a’之后: "abba" 是回文
\* 在字母‘b’之后: "abba" 是回文
\* 在第二个字母'a'之后 "abab" 不是回文
所以满足条件的答案为2

输入描述：

每组输入数据共两行。 第一行为字符串A 第二行为字符串B 字符串长度均小于100且只包含小写字母

输出描述：

输出一个数字，表示把字符串B插入字符串A之后构成一个回文串的方法数



```cpp
#include <iostream>
#include <string>
using namespace std;

bool is_palindrome(string& str)
{
    int left = 0, right = str.size() - 1;
    while(left < right)
    {
        if(str[left] == str[right]) left++, right--;
        else break;
    }
    if(left < right) return false;
    else return true;
}

int main() {
    string str1, str2;
    cin >> str1 >> str2;
    int count = 0;
    for(int i = 0; i <= str1.size(); i++)
    {
        string tmp = str1;
        tmp.insert(i, str2);
        if(is_palindrome(tmp)) count++;
    }
    cout << count;
}
// 64 位输出请用 printf("%lld")
```

## [连续最大和_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/5a304c109a544aef9b583dce23f5f5db?tpId=182&tqId=34613&ru=/exam/oj)

描述

一个数组有 N 个元素，求连续子数组的最大和。 例如：[-1,2,1]，和最大的连续子数组为[2,1]，其和为 3

输入描述：

输入为两行。 第一行一个整数n(1 <= n <= 100000)，表示一共有n个元素 第二行为n个数，即每个元素,每个整数都在32位int范围内。以空格分隔。

输出描述：

所有连续子数组中和最大的值。

```cpp
#include <climits>
#include <iostream>
#include <vector>

using namespace std;

int main() 
{
    int n, maxNum = INT_MIN, ret = INT_MIN;
    cin >> n;
    vector<int> arr(n);
    for(int i = 0; i < n; i++) 
    {
        cin >> arr[i];
        maxNum = max(arr[i], maxNum + arr[i]);
        ret = max(ret, maxNum);
    }
    cout << ret;
}
// 64 位输出请用 printf("%lld")
```

# DAY 116

## [把字符串转换成整数__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/1277c681251b4372bdef344468e4f26e)

链接：https://www.nowcoder.com/questionTerminal/1277c681251b4372bdef344468e4f26e
来源：牛客网



将一个字符串转换成一个整数，要求不能使用字符串转换整数的库函数。         数值为 0 或者字符串不是一个合法的数值则返回 0 


  数据范围：字符串长度满足 0≤n≤100 0 \le n \le 100 \ 0≤n≤100 
 进阶：空间复杂度 O(1) O(1) \ O(1) ，时间复杂度 O(n) O(n) \ O(n) 

 

  注意： 

  ①字符串中可能出现任意符号，出现除 +/- 以外符号时直接输出 0 

  ②字符串中可能出现 +/- 且仅可能出现在字符串首位。

```cpp
class Solution {
public:
    int StrToInt(string str) 
    {
        bool flag = false;
        if(str[0] == '-') flag = true;
        int ret = 0;
        for(char ch : str)
        {
            if(ch == '+' || ch == '-') continue;
            else if(ch >= '0' && ch <= '9')
            {
                ret = ret * 10 + ch - '0';
            }
            else return 0;
        }
        if(flag) ret *= -1;
        return ret;
    }
};
```

## [不要二_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/1183548cd48446b38da501e58d5944eb?tpId=122&tqId=33662&ru=/exam/oj)

二货小易有一个W*H的网格盒子，网格的行编号为0~H-1，网格的列编号为0~W-1。每个格子至多可以放一块蛋糕，任意两块蛋糕的欧几里得距离不能等于2。
对于两个格子坐标(x1,y1),(x2,y2)的欧几里得距离为:
( (x1-x2) * (x1-x2) + (y1-y2) * (y1-y2) ) 的算术平方根
小易想知道最多可以放多少块蛋糕在网格盒子里。

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() 
{
    int x[4] = {0 ,0, 2, -2};
    int y[4] = {-2, 2, 0, 0};
    int m, n, count = 0;
    cin >> m >> n;
    vector<vector<int>> arr(m, vector<int>(n, 0));
    for(int i = 0; i < m; i++)
        for(int j = 0; j < n; j++)
            if(arr[i][j] == 0)
            {
                arr[i][j] = 1;
                count++;
                for(int z = 0; z < 4; z++)
                {
                    int tmpx = i + x[z], tmpy = j + y[z];
                    if(tmpx >= 0 && tmpx < m && tmpy >= 0 && tmpy < n) 
                        arr[tmpx][tmpy] = 2;
                }
            }
    cout << count;
}
// 64 位输出请用 printf("%lld")
```

# DAY117

## [Fibonacci数列_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/18ecd0ecf5ef4fe9ba3f17f8d00d2d66?tpId=122&tqId=33668&ru=/exam/oj)

描述

Fibonacci数列是这样定义的：
F[0] = 0
F[1] = 1
for each i ≥ 2: F[i] = F[i-1] + F[i-2]
因此，Fibonacci数列就形如：0, 1, 1, 2, 3, 5, 8, 13, ...，在Fibonacci数列中的数我们称为Fibonacci数。给你一个N，你想让其变为一个Fibonacci数，每一步你可以把当前数字X变为X-1或者X+1，现在给你一个数N求最少需要多少步可以变为Fibonacci数。

输入描述：

输入为一个正整数N(1 ≤ N ≤ 1,000,000)

输出描述：

输出一个最小的步数变为Fibonacci数"



```cpp
#include <iostream>

using namespace std;

int main() 
{
    int n;
    cin >> n;
    int left = 0, right = 1, sum = 1, minNum = 0x3f3f3f3f;
    while(sum < n)
    {
        minNum = min(minNum, n - sum);
        left = right;
        right = sum;
        sum = left + right;
    }
    minNum = min(minNum, sum - n);
    cout << minNum;
}
// 64 位输出请用 printf("%lld")
```

## [合法括号序列判断__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/d8acfa0619814b2d98f12c071aef20d4)



给定一个字符串**A**和其长度**n**，请返回一个bool值代表它是否为一个合法的括号串（只能由括号组成）。 

  测试样例： 

```
"(()())",6
返回：true
```

  测试样例： 

```
"()a()()",7
返回：false
```

  测试样例： 

```
"()(()()",7
返回：false
```

```cpp
class Parenthesis {
public:
    bool chkParenthesis(string A, int n) 
    {
        stack<char> st;
        for(char ch : A)
        {
            if(ch == '(') st.push(ch);
            else if(ch == ')')
            {
                if(st.empty()) return false;
                else st.pop();
            }
            else return false;
        }
        if(st.empty()) return true;
        else return false;
    }
};
```

# DAY 118

## [两种排序方法_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/839f681bf36c486fbcc5fcb977ffe432?tpId=122&tqId=33666&ru=/exam/oj)

描述

考拉有n个字符串字符串，任意两个字符串长度都是不同的。考拉最近学习到有两种字符串的排序方法： 1.根据字符串的字典序排序。例如：
"car" < "carriage" < "cats" < "doggies < "koala"
2.根据字符串的长度排序。例如：
"car" < "cats" < "koala" < "doggies" < "carriage"
考拉想知道自己的这些字符串排列顺序是否满足这两种排序方法，考拉要忙着吃树叶，所以需要你来帮忙验证。

输入描述：

输入第一行为字符串个数n(n ≤ 100) 接下来的n行,每行一个字符串,字符串长度均小于100，均由小写字母组成

输出描述：

如果这些字符串是根据字典序排列而不是根据长度排列输出"lexicographically",
如果根据长度排列而不是字典序排列输出"lengths",
如果两种方式都符合输出"both"，否则输出"none"

```cpp
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

bool is_lexicographically(vector<string>& str)
{
    vector<string> tmp(str);
    sort(tmp.begin(), tmp.end(),[&](string& s1, string& s2)
    {
        for(int i = 0; i < s1.size() && i < s2.size(); i++)
        {
            if(s1[i] == s2[i]) continue;
            else return s1[i] < s2[i];
        }
        return s1.size() < s2.size();
    });
    return tmp == str;
}

bool is_lengths(vector<string>& str)
{
    vector<string> tmp(str);
    sort(tmp.begin(), tmp.end(), [&](string& s1, string s2)
    {
        return s1.size() < s2.size();
    });
    return tmp == str;
}
int main() 
{
    int n;
    cin >> n;
    vector<string> str(n);
    for(int i = 0; i < n; i++) cin >> str[i];

    if(is_lengths(str) && is_lexicographically(str)) cout << "both";
    else if(is_lengths(str)) cout << "lengths";
    else if(is_lexicographically(str)) cout << "lexicographically";
    else cout << "none";
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [求最小公倍数__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/d1205615a0904bc39e9e627e7cb9e899)



正整数 a 和正整数 b 的最小公倍数，是指能被 a 和 b 整除的最小的正整数。请你求 a 和 b 的最小公倍数。 

  比如输入5和7，5和7的最小公倍数是35，则需要返回35。 

**输入描述:**

```
输入两个正整数。


1≤a,b≤1000001 \le a,b\le 1000001≤a,b≤100000
```

输出描述:**

```
输出最小公倍数。
```

```cpp
#include <iostream>
using namespace std;

int divisor(int a, int b)
{
    int r;
    while(r = a % b)
    {
        a = b;
        b = r;
    }
    return b;
}

int main() 
{
    long long a, b;
    cin >> a >> b;
    int num = divisor(a, b);
    cout << a * b / num;

    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 119

## [走方格的方案数_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/e2a22f0305eb4f2f9846e7d644dba09b?tpId=37&tqId=21314&ru=/exam/oj)

描述

请计算n*m的棋盘格子（n为横向的格子数，m为竖向的格子数）从棋盘左上角出发沿着边缘线从左上角走到右下角，总共有多少种走法，要求不能走回头路，即：只能往右和往下走，不能往左和往上走。

注：沿棋盘格之间的边缘线行走

数据范围： 1≤�,�≤8 1≤*n*,*m*≤8 

输入描述：

输入两个正整数n和m，用空格隔开。(1≤n,m≤8)

输出描述：

输出一行结果

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() 
{
    int m, n;
    cin >> m >> n;
    vector<vector<int>> board(m + 1, vector<int>(n + 1, 0));
    for(int i = 0; i < m + 1; i++) board[i][0] = 1;
    for(int j = 0; j < n + 1; j++) board[0][j] = 1;

    for(int i = 1; i <= m; i++)
        for(int j = 1; j <= n; j++)
            board[i][j] = board[i - 1][j] + board[i][j - 1];

    cout << board[m][n];
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [井字棋__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/e1bb714eb9924188a0d5a6df2216a3d1)

给定一个二维数组**board**，代表棋盘，其中元素为1的代表是当前玩家的棋子，0表示没有棋子，-1代表是对方玩家的棋子。当一方棋子在横竖斜方向上有连成排的及获胜（及井字棋规则），返回当前玩家是否胜出。 

  测试样例： 

```
[[1,0,1],[1,-1,-1],[1,-1,0]]
返回：true
```

```cpp
class Board {
public:
    bool checkWon(vector<vector<int> > board) {
        int m = board.size(), n = board[0].size();
        int flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0;
        for(int i = 0; i < m; i++)
        {
            if(board[i][0] == 1) flag1++;
            if(board[i][n - 1] == 1) flag2++;
        }
        if(flag1 == 3 || flag2 == 3) return true;
        for(int j = 0; j < n; j++)
        {
            if(board[0][j] == 1) flag3++;
            if(board[m - 1][j] == 1) flag4++;
        }
        if(flag3 == 3 || flag4 == 3) return true;
        if(board[0][0] == 1 && board[1][1] == 1 && board[2][2] == 1)
            return true;
        if(board[0][2] == 1 && board[1][1] == 1 && board[2][0] == 1)
            return true;
        return false;
    }
};
```

## [密码强度等级_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/52d382c2a7164767bca2064c1c9d5361?tpId=37&tqId=21310&ru=/exam/oj)

描述

密码按如下规则进行计分，并根据不同的得分为密码进行安全等级划分。

一、密码长度:
5 分: 小于等于4 个字符
10 分: 5 到7 字符
25 分: 大于等于8 个字符

二、字母:
0 分: 没有字母
10 分: 密码里的字母全都是小（大）写字母
20 分: 密码里的字母符合”大小写混合“

三、数字:
0 分: 没有数字
10 分: 1 个数字
20 分: 大于1 个数字

四、符号:
0 分: 没有符号
10 分: 1 个符号
25 分: 大于1 个符号

五、奖励（只能选符合最多的那一种奖励）:
2 分: 字母和数字
3 分: 字母、数字和符号

5 分: 大小写字母、数字和符号

最后的评分标准:
\>= 90: 非常安全
\>= 80: 安全（Secure）
\>= 70: 非常强
\>= 60: 强（Strong）
\>= 50: 一般（Average）
\>= 25: 弱（Weak）
\>= 0: 非常弱（Very_Weak）

对应输出为：

VERY_SECURE
SECURE
VERY_STRONG
STRONG
AVERAGE
WEAK
VERY_WEAK

请根据输入的密码字符串，进行安全评定。

注：
字母：a-z, A-Z
数字：0-9
符号包含如下： (ASCII码表可以在UltraEdit的菜单view->ASCII Table查看)
!"#$%&'()*+,-./   (ASCII码：0x21~0x2F)
:;<=>?@       (ASCII码：0x3A~0x40)
[\]^_`       (ASCII码：0x5B~0x60)
{|}~        (ASCII码：0x7B~0x7E)

提示:
1 <= 字符串的长度<= 300

输入描述：

输入一个string的密码

输出描述：

输出密码等级

```cpp
#include <iostream>
using namespace std;

int main() {
    string str;
    cin >> str;
    int sum = 0;
    if(str.size() >= 8) sum += 25;
    else if(str.size() >= 5) sum += 10;
    else sum += 5;

    int sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
    for(char ch : str)
    {
        if(ch >= 'a' && ch <= 'z') sum1++;
        else if(ch >= 'A' && ch <= 'Z') sum2++;
        else if(ch >= '0' && ch <= '9') sum3++;
        else sum4++;
    }
    if(sum1 && sum2) sum += 20;
    else if(sum1 || sum2) sum += 10;

    if(sum3 > 1) sum += 20;
    else if(sum3) sum += 10;

    if(sum4 > 1) sum += 25;
    else if(sum4) sum += 10;

    if(sum1 && sum2 && sum3 && sum4) sum += 5;
    else if((sum1 || sum2) && sum3 && sum4) sum += 3;
    else if((sum1 || sum2) && sum3) sum += 2;

    if(sum >= 90) cout << "VERY_SECURE";
    else if(sum >= 80) cout << "SECURE";
    else if(sum >= 70) cout << "VERY_STRONG";
    else if(sum >= 60) cout << "STRONG";
    else if(sum >= 50) cout << "AVERAGE";
    else if(sum >= 25) cout << "WEAK";
    else cout << "VERY_WEAK";

    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 120

## [最近公共祖先_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/70e00e490b454006976c1fdf47f155d9?tpId=8&&tqId=11017&rp=1&ru=/activity/oj&qru=/ta/cracking-the-coding-interview/question-ranking)

描述

将一棵无穷大满二叉树的结点按根结点一层一层地从左往右编号，根结点编号为1。现给定a，b为两个结点。设计一个算法，返回a、b最近的公共祖先的编号。注意其祖先也可能是结点本身。

测试样例：

```
2，3
返回：1
```

```cpp
class LCA {
public:
    int getLCA(int a, int b) {
        vector<int> arr1;
        while(a)
        {
            arr1.push_back(a);
            a /= 2;
        }
        while(b)
        {
            for(int e : arr1) 
                if(e == b) return e;
            b /= 2;
        }
        return 0;
    }
};
```

## [求最大连续bit数_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/4b1658fd8ffb4217bc3b7e85a38cfaf2?tpId=37&&tqId=21309&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

求一个int类型数字对应的二进制数字中1的最大连续数，例如3的二进制为00000011，最大连续2个1

数据范围：数据组数：1≤�≤5 1≤*t*≤5 ，1≤�≤500000 1≤*n*≤500000 

进阶：时间复杂度：�(����) *O*(*l**o**g**n*) ，空间复杂度：�(1) *O*(1) 

输入描述：

输入一个int类型数字

输出描述：

输出转成二进制之后连续1的个数

```cpp
#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int sum = 0, maxSum = 0;
    while(n)
    {
        if(n & 1) sum++;
        else maxSum = max(maxSum, sum), sum = 0;
        n = n >> 1;
    }
    cout << max(maxSum, sum);
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY121

## [二进制插入_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/30c1674ad5694b3f8f0bc2de6f005490?tpId=8&&tqId=11019&rp=1&ru=/activity/oj&qru=/ta/cracking-the-coding-interview/question-ranking)

描述

给定两个32位整数n和m，同时给定i和j，将m的二进制数位插入到n的二进制的第j到第i位,保证n的第j到第i位均为零，且m的二进制位数小于等于i-j+1，其中二进制的位数从0开始由低到高。

测试样例：

```
1024，19，2，6
返回：1100
```

```cpp
class BinInsert {
public:
    int binInsert(int n, int m, int j, int i) {

        return n | m << j;
    }
};
```

## [查找组成一个偶数最接近的两个素数_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/f8538f9ae3f1484fb137789dec6eedb9?tpId=37&&tqId=21283&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

任意一个偶数（大于2）都可以由2个素数组成，组成偶数的2个素数有很多种情况，本题目要求输出组成指定偶数的两个素数差值最小的素数对。

数据范围：输入的数据满足 4≤�≤1000 4≤*n*≤1000 

输入描述：

输入一个大于2的偶数

输出描述：

从小到大输出两个素数

示例1

输入：

```
20
```

输出：

```
7
13
```

```cpp
#include <iostream>
using namespace std;

bool is_prime(int n)
{
    for(int i = 2; i < n; i++)
    {
        if(n % i == 0) return false;
    }
    return true;
}

int main() {
    int n;
    cin >> n;
    int fast = 1, slow = 1;
    int minSub = 0x3f3f3f3f, ti, tj;
    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= n; j++)
        {
            if(is_prime(i) && is_prime(j) && i + j == n)
            {
                if(minSub > abs(i - j))
                {
                    minSub = abs(i - j);
                    ti = i, tj = j;
                }
            }
        }
    }
    cout << ti << endl << tj;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 122

## [参数解析_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/668603dc307e4ef4bb07bcd0615ea677?tpId=37&&tqId=21297&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

在命令行输入如下命令：

xcopy /s c:\\ d:\\e，

各个参数如下：

参数1：命令字xcopy

参数2：字符串/s

参数3：字符串c:\\

参数4: 字符串d:\\e

请编写一个参数解析程序，实现将命令行各个参数解析出来。



解析规则：

1.参数分隔符为空格
2.对于用""包含起来的参数，如果中间有空格，不能解析为多个参数。比如在命令行输入xcopy /s "C:\\program files" "d:\"时，参数仍然是4个，第3个参数应该是字符串C:\\program files，而不是C:\\program，注意输出参数时，需要将""去掉，引号不存在嵌套情况。
3.参数不定长

4.输入由用例保证，不会出现不符合要求的输入

数据范围：字符串长度：1≤�≤1000 1≤*s*≤1000 

进阶：时间复杂度：�(�) *O*(*n*) ，空间复杂度：�(�) *O*(*n*) 

输入描述：

输入一行字符串，可以有空格

输出描述：

输出参数个数，分解后的参数，每个参数都独占一行

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() {
    string str;
    getline(cin, str);
    vector<string> arr;
    string tmp;
    for(int i = 0; i < str.size(); i++)
    {
        if(str[i] == '"')
        {
            i++;
            while(str[i] != '"') tmp += str[i++];
            arr.push_back(tmp);
            tmp.resize(0);
        }
        else if(str[i] == ' ' && tmp.size()) 
        {
            arr.push_back(tmp);
            tmp.resize(0);
        }
        else if(str[i] != ' ') tmp += str[i];
    }
    if(tmp.size()) arr.push_back(tmp);
    cout << arr.size() << endl;
    for(string s : arr) cout << s << endl;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [跳石板_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/4284c8f466814870bae7799a07d49ec8?tpId=85&&tqId=29852&rp=1&ru=/activity/oj&qru=/ta/2017test/question-ranking)

描述

小易来到了一条石板路前，每块石板上从1挨着编号为：1、2、3.......
这条石板路要根据特殊的规则才能前进：对于小易当前所在的编号为K的 石板，小易单次只能往前跳K的一个约数(不含1和K)步，即跳到K+X(X为K的一个非1和本身的约数)的位置。 小易当前处在编号为N的石板，他想跳到编号恰好为M的石板去，小易想知道最少需要跳跃几次可以到达。
例如：
N = 4，M = 24：
4->6->8->12->18->24
于是小易最少需要跳跃5次，就可以从4号石板跳到24号石板

输入描述：

输入为一行，有两个整数N，M，以空格隔开。 (4 ≤ N ≤ 100000) (N ≤ M ≤ 100000)

输出描述：

输出小易最少需要跳跃的步数,如果不能到达输出-1

```cpp
#include <iostream>
#include <vector>
#include <climits>
using namespace std;

void getNum(int num, vector<int>& a)
{
    for(int i = 2; i * i <= num; i++)
    {
        if(num % i == 0) 
        {
            a.push_back(i);
            if(num / i != i) a.push_back(num / i);
        }
    }
}

int main() {
    int m, n;
    cin >> n >> m;
    vector<int> arr(m + 1, -1);
    arr[n] = 0;
    int count = 0;
    for(int i = n; i <= m; i++)
    {
        if(arr[i] == -1) continue;
        vector<int> a;
        getNum(i, a);
        for(int e : a)
        {
            if(i + e > m) continue;
            if(arr[i + e] == -1) arr[i + e] = arr[i] + 1;
            else arr[i + e] = min(arr[i + e], arr[i] + 1);
        }
    }
    cout << arr[m];
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 123

## [计算日期到天数转换_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/769d45d455fe40b385ba32f97e7bcded?tpId=37&&tqId=21296&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

根据输入的日期，计算是这一年的第几天。

保证年份为4位数且日期合法。

进阶：时间复杂度：�(�) *O*(*n*) ，空间复杂度：�(1) *O*(1) 

输入描述：

输入一行，每行空格分割，分别是年，月，日

输出描述：

输出是这一年的第几天

```cpp
#include <iostream>
using namespace std;

int main() {
    int year, month, day;
    cin >> year >> month >> day;
    int sum = 0;
    for(int i = 1; i < month; i++)
    {
        switch(i)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                sum += 31;
                break;
            case 2:
                sum += 28;
                break;
            default:
                sum += 30;
                break;
        }
    }
    sum += day;

    if(((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) && month > 2) sum += 1;
    cout << sum;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [幸运的袋子_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/a5190a7c3ec045ce9273beebdfe029ee?tpId=85&&tqId=29839&rp=1&ru=/activity/oj&qru=/ta/2017test/question-ranking)

描述

一个袋子里面有n个球，每个球上面都有一个号码(拥有相同号码的球是无区别的)。如果一个袋子是幸运的当且仅当所有球的号码的和大于所有球的号码的积。
例如：如果袋子里面的球的号码是{1, 1, 2, 3}，这个袋子就是幸运的，因为1 + 1 + 2 + 3 > 1 * 1 * 2 * 3
你可以适当从袋子里移除一些球(可以移除0个,但是别移除完)，要使移除后的袋子是幸运的。现在让你编程计算一下你可以获得的多少种不同的幸运的袋子。

输入描述：

第一行输入一个正整数n(n ≤ 1000) 第二行为n个数正整数xi(xi ≤ 1000)

输出描述：

输出可以产生的幸运的袋子数

```cpp
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int CountNum(vector<int>& arr, int pos, int sum, int prod)
{
    int count = 0;
    for(int i = pos; i < arr.size(); i++)
    {
        sum += arr[i];
        prod *= arr[i];
        if(sum > prod)
            count += CountNum(arr, i + 1, sum, prod) + 1;
        else if(arr[i] == 1)
            count += CountNum(arr, i + 1, sum, prod);
        else break;

        sum -= arr[i];
        prod /= arr[i];
        while(i < arr.size() - 1 && arr[i] == arr[i + 1]) i++;
    }
    return count;
}

int main() {
    int n;
    cin >> n;
    vector<int> arr(n);
    for(int i = 0; i < n; i++) cin >> arr[i];

    sort(arr.begin(), arr.end());

    cout << CountNum(arr, 0, 0, 1);

    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 124

## [查找输入整数二进制中1的个数_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/1b46eb4cf3fa49b9965ac3c2c1caf5ad?tpId=37&&tqId=21285&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

输入一个正整数，计算它在二进制下的1的个数。

**注意多组输入输出！！！！！！**

数据范围： 1≤�≤231−1 1≤*n*≤231−1 

输入描述：

输入一个整数

输出描述：

计算整数二进制中1的个数

```cpp
#include <iostream>
using namespace std;

int main() {
    int n;
    while(cin >> n)
    {
        int count = 0;
        while(n)
        {
            if(n & 1) count++;
            n >>= 1;
        }
        cout << count << endl;
    }
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [手套_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/365d5722fff640a0b6684391153e58d8?tpId=49&&tqId=29337&rp=1&ru=/activity/oj&qru=/ta/2016test/question-ranking)

描述

在地下室里放着n种颜色的手套，手套分左右手，但是每种颜色的左右手手套个数不一定相同。A先生现在要出门，所以他要去地下室选手套。但是昏暗的灯光让他无法分辨手套的颜色，只能分辨出左右手。所以他会多拿一些手套，然后选出一双颜色相同的左右手手套。现在的问题是，他至少要拿多少只手套(左手加右手)，才能保证一定能选出一双颜色相同的手套。

给定颜色种数n(1≤n≤13),同时给定两个长度为n的数组left,right,分别代表每种颜色左右手手套的数量。数据保证左右的手套总数均不超过26，且一定存在至少一种合法方案。

测试样例：

```
4,[0,7,1,6],[1,5,0,6]
返回：10(解释：可以左手手套取2只，右手手套取8只)
```

```cpp
#include <functional>
class Gloves {
public:
    int findMinimum(int n, vector<int> left, vector<int> right) {
        if(n == 1) return 2;
        int count = 0;
        int minleft = INT_MAX, minright = INT_MAX, countleft = 0, countright = 0;
        for(int i = 0; i < n; i++)
        {
            if(left[i] == 0) count += right[i];
            else if(right[i] == 0) count += left[i];
            else
            {
                countleft += left[i];
                countright += right[i];
                minleft = min(minleft, left[i]);
                minright = min(minright, right[i]);
            }
        }
        countleft -= minleft - 1;
        countright -= minright - 1;   
        return countleft > countright ? countright + count + 1 : countleft + count + 1;
    }
};
```

# DAY125

## [完全数计算_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/7299c12e6abb437c87ad3e712383ff84?tpId=37&&tqId=212)

描述

完全数（Perfect number），又称完美数或完备数，是一些特殊的自然数。

它所有的真因子（即除了自身以外的约数）的和（即因子函数），恰好等于它本身。

例如：28，它有约数1、2、4、7、14、28，除去它本身28外，其余5个数相加，1+2+4+7+14=28。

输入n，请输出n以内(含n)完全数的个数。

数据范围： 1≤�≤5×105 1≤*n*≤5×105 

输入描述：

输入一个数字n

输出描述：

输出不超过n的完全数的个数

```cpp
#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int ret = 0;
    for (int i = 6; i <= n; i++) 
    {
        int sum = 0;
        for (int j = 2; j * j <= i; j++) 
        {
            if (i % j == 0) 
            {
                sum += j;
                sum += i / j;
            }
        }
        if(sum + 1 == i) ret++;
    }
    cout << ret;
}
// 64 位输出请用 printf("%lld")
```

## [扑克牌大小_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/0a92c75f5d6b4db28fcfa3e65e5c9b3f?tpId=49&&tqId=29277&rp=1&ru=/activity/oj&qru=/ta/2016test/question-ranking)

描述

扑克牌游戏大家应该都比较熟悉了，一副牌由54张组成，含3~A，2各4张，小王1张，大王1张。牌面从小到大用如下字符和字符串表示（其中，小写joker表示小王，大写JOKER表示大王）:)
3 4 5 6 7 8 9 10 J Q K A 2 joker JOKER
输入两手牌，两手牌之间用“-”连接，每手牌的每张牌以空格分隔，“-”两边没有空格，如：4 4 4 4-joker JOKER
请比较两手牌大小，输出较大的牌，如果不存在比较关系则输出ERROR

基本规则：
（1）输入每手牌可能是个子，对子，顺子（连续5张），三个，炸弹（四个）和对王中的一种，不存在其他情况，由输入保证两手牌都是合法的，顺子已经从小到大排列；
（2）除了炸弹和对王可以和所有牌比较之外，其他类型的牌只能跟相同类型的存在比较关系（如，对子跟对子比较，三个跟三个比较），不考虑拆牌情况（如：将对子拆分成个子）
（3）大小规则跟大家平时了解的常见规则相同，个子，对子，三个比较牌面大小；顺子比较最小牌大小；炸弹大于前面所有的牌，炸弹之间比较牌面大小；对王是最大的牌；
（4）输入的两手牌不会出现相等的情况。

答案提示：
（1）除了炸弹和对王之外，其他必须同类型比较。
（2）输入已经保证合法性，不用检查输入是否是合法的牌。

（3）输入的顺子已经经过从小到大排序，因此不用再排序了.

数据范围：保证输入合法

输入描述：

输入两手牌，两手牌之间用“-”连接，每手牌的每张牌以空格分隔，“-”两边没有空格，如4 4 4 4-joker JOKER。

输出描述：

输出两手牌中较大的那手，不含连接符，扑克牌顺序不变，仍以空格隔开；如果不存在比较关系则输出ERROR。

```cpp
#include<iostream>
#include<string>
#include<algorithm>

using namespace std;

string FindMax(const string& line)
{
    // 如果存在王炸直接返回王炸
    if (line.find("joker JOKER") != string::npos)
        return "joker JOKER";
    // 分割两手牌
    string poker1 = line.substr(0, line.find('-'));
    string poker2 = line.substr(line.find('-') + 1);
    // 计算两手牌的牌数
    int poker1_cnt = count(poker1.begin(), poker1.end(), ' ') + 1;
    int poker2_cnt = count(poker2.begin(), poker2.end(), ' ') + 1;
    // 获取两手牌各自的第一张牌
    string poker1_first = poker1.substr(0, poker1.find(' '));
    string poker2_first = poker2.substr(0, poker2.find(' '));
    // 按照斗地主规则比较牌的大小
    if (poker1_cnt == poker2_cnt)
    {
        // 只需要比较谁的下标比较大即可
        string str = "345678910JQKA2jokerJOKER";
        if (str.find(poker1_first) > str.find(poker2_first))
            return poker1;
        else
            return poker2;
    }
    // 不满足牌数一样的，判断是否存在炸，是炸的大
    if (poker1_cnt == 4)
        return poker1;
    else if (poker2_cnt == 4)
        return poker2;
    // 不满足牌数一样，还不存在炸的，不能比较
    return "ERROR";
}

int main()
{
    string line;
    while (getline(cin, line))
    {
        cout << FindMax(line) << endl;
    }
    return 0;
}
```

# DAY 126

## [杨辉三角的变形_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/8ef655edf42d4e08b44be4d777edbf43?tpId=37&&tqId=21276&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

![img](https://uploadfiles.nowcoder.com/images/20210617/557336_1623898240633/9AC4B89B5E22854D71DEA4CA6EBD6F9F)

以上三角形的数阵，第一行只有一个数1，以下每行的每个数，是恰好是它上面的数、左上角数和右上角的数，3个数之和（如果不存在某个数，认为该数就是0）。

求第n行第一个偶数出现的位置。如果没有偶数，则输出-1。例如输入3,则输出2，输入4则输出3，输入2则输出-1。

数据范围： 1≤�≤109 1≤*n*≤109 

输入描述：

输入一个int整数

输出描述：

输出返回的int值

```cpp
#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    if (n <= 2) cout << -1;
    else {
        switch ((n - 2) % 4) {
            case 1:
            case 3:
                cout << 2;
                break;
            case 2:
                cout << 3;
                break;
            case 0:
                cout << 4;
                break;
        }
    }

    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [计算某字符出现次数__牛客网 (nowcoder.com)](https://www.nowcoder.com/questionTerminal/a35ce98431874e3a820dbe4b2d0508b1)

写出一个程序，接受一个由字母、数字和空格组成的字符串，和一个字符，然后输出输入字符串中该字符的出现次数。（不区分大小写字母）

  数据范围： 1≤n≤1000 1 \le n \le 1000 \ 1≤n≤1000

 **输入描述:**

```
第一行输入一个由字母、数字和空格组成的字符串，第二行输入一个字符（保证该字符不为空格）。
```

**输出描述:**

```
输出输入字符串中含有该字符的个数。（不区分大小写字母）
```

示例1

输入

```
ABCabc
A
```

输出

```
2
```

```cpp
#include <iostream>
#include <string>
using namespace std;

int main() {
    string str;
    getline(cin, str);
    char ch;
    cin >> ch;
    int ret = 0;
    for (char c : str) 
    {
        if (ch >= 'a' && ch <= 'z') 
        {
            if (ch == c || ch - 32 == c) ret++; 
        }
        else if(ch >= 'A' && ch <= 'Z')
        {
            if(ch == c || ch + 32 == c) ret++;
        }
        else if(ch == c) ret++;
    }
    cout << ret;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY127

## [统计每个月兔子的总数_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/1221ec77125d4370833fd3ad5ba72395?tpId=37&&tqId=21)

描述

有一种兔子，从出生后第3个月起每个月都生一只兔子，小兔子长到第三个月后每个月又生一只兔子。

例子：假设一只兔子第3个月出生，那么它第5个月开始会每个月生一只兔子。

一月的时候有一只兔子，假如兔子都不死，问第n个月的兔子总数为多少？

数据范围：输入满足 1≤�≤31 1≤*n*≤31 

输入描述：

输入一个int型整数表示第n个月

输出描述：

输出对应的兔子总数

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() 
{
    int n;
    cin >> n;
    if(n < 3) cout << 1;
    else  
    {
        int v1 = 0, v2 = 0, v3 = 1;
        for(int i = 3; i <= n; i++)
        {
            v3 += v2;
            int tmp = v3;
            v2 = v1;
            v1 = tmp;   
        }
        cout << v1 + v2 + v3;
    }
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [字符串通配符_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/43072d50a6eb44d2a6c816a283b02036?tpId=37&&tqId=21294&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

问题描述：在计算机中，通配符一种特殊语法，广泛应用于文件搜索、数据库、正则表达式等领域。现要求各位实现字符串通配符的算法。
要求：
实现如下2个通配符：
*：匹配0个或以上的字符（注：能被*和?匹配的字符仅由英文字母和数字0到9组成，下同）
？：匹配1个字符

注意：匹配时不区分大小写。

输入：
通配符表达式；
一组字符串。

输出：

返回不区分大小写的匹配结果，匹配成功输出true，匹配失败输出false

数据范围：字符串长度：1≤�≤100 1≤*s*≤100 

进阶：时间复杂度：�(�2) *O*(*n*2) ，空间复杂度：�(�) *O*(*n*) 

输入描述：

先输入一个带有通配符的字符串，再输入一个需要匹配的字符串

输出描述：

返回不区分大小写的匹配结果，匹配成功输出true，匹配失败输出false

```cpp
#include <iostream>
using namespace std;

bool compare(string str, string com, int i, int j)
{
    while(i < com.size())
    {
        if(com[i] == '*')
        {
            while(i < com.size() && com[i] == '*') i++;
            while(i < com.size() && com[i] == '?') i++, j++;
            while(j < str.size()) 
            {
                while(j < str.size() && (com[i] != str[j] && com[i] + 32 != str[j] && com[i] - 32 != str[j])) j++;
                if(compare(str, com, i, j)) return true;
                else j++; 
            }
        }
        else if(com[i] == '?' || (com[i] == str[j] || com[i] + 32 == str[j] || com[i] - 32 == str[j])) i++, j++;
        else break;
    }
    if(i == com.size() && j == str.size()) return true;
    else return false;
}

int main() 
{
    string str, com;
    cin >> com >> str;
    if(compare(str, com, 0, 0)) cout << "true";
    else cout << "false";
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 128

## [汽水瓶_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/fe298c55694f4ed39e256170ff2c205f?tpId=37&&tqId=21245&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

某商店规定：三个空汽水瓶可以换一瓶汽水，允许向老板借空汽水瓶（但是必须要归还）。

小张手上有n个空汽水瓶，她想知道自己最多可以喝到多少瓶汽水。

数据范围：输入的正整数满足 1≤�≤100 1≤*n*≤100 

注意：本题存在多组输入。输入的 0 表示输入结束，并不用输出结果。

输入描述：

输入文件最多包含 10 组测试数据，每个数据占一行，仅包含一个正整数 n（ 1<=n<=100 ），表示小张手上的空汽水瓶数。n=0 表示输入结束，你的程序不应当处理这一行。

输出描述：

对于每组测试数据，输出一行，表示最多可以喝的汽水瓶数。如果一瓶也喝不到，输出0。

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() 
{
    int n;
    cin >> n;
    while(n != 0)
    {
        int count = 0;
        while(n >= 2)
        {
            int tmp = 0, blank = 0;
            if(n == 2) blank = -1, n++;
            else blank = n % 3;
            count += n / 3;
            tmp = n / 3;
            n = tmp + blank;
        }
        cout << count << endl;
        cin >> n;      
    }
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [查找两个字符串a,b中的最长公共子串_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/181a1a71c7574266ad07f9739f791506?tpId=37&&tqId=21288&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

查找两个字符串a,b中的最长公共子串。若有多个，输出在较短串中最先出现的那个。

注：子串的定义：将一个字符串删去前缀和后缀（也可以不删）形成的字符串。请和“子序列”的概念分开！

数据范围：字符串长度1≤�����ℎ≤300 1≤*l**e**n**g**t**h*≤300 

进阶：时间复杂度：�(�3) *O*(*n*3) ，空间复杂度：�(�) *O*(*n*) 

输入描述：

输入两个字符串

输出描述：

返回重复出现的字符

```cpp
#include <iostream>
#include <variant>
#include <vector>
using namespace std;

int main() 
{
    string str1, str2;
    cin >> str1 >> str2;
    if(str1.size() > str2.size())
    {
        string tmp = str1;
        str1 = str2;
        str2 = tmp;
    }
    vector<string> arr(str1.size() + 1, "");
    for(int i = 1; i <= str1.size(); i++)
    {
        string tmp = str1.substr(0, i);
        while(tmp.size() && tmp.size() > arr[i - 1].size())
        {
            if(str2.find(tmp) == string::npos)
            {
                tmp.erase(tmp.begin());
            }
            else 
            {
                arr[i] = tmp;
                break;
            }
        }
        if(arr[i] == "") arr[i] = arr[i - 1];
    }
    cout << arr[str1.size()];
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 129

## [字符串反转_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/e45e078701ab4e4cb49393ae30f1bb04?tpId=37&&tqId=21235&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

接受一个只包含小写字母的字符串，然后输出该字符串反转后的字符串。（字符串长度不超过1000）

输入描述：

输入一行，为一个只包含小写字母的字符串。

输出描述：

输出该字符串反转后的字符串。

示例1

输入：

```
abcd
```

输出：

```
dcba
```

```cpp
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    string str;
    cin >> str;
    reverse(str.begin(), str.end());
    cout << str;
    return 0;
}
// 64 位输出请用 printf("%lld")
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    string str;
    cin >> str;
    for(int i = 0, j = str.size() - 1; i < j; i++, j--)
    {
        char ch = str[i];
        str[i] = str[j];
        str[j] = ch;
    }
    cout << str;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

## [公共子串计算_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/98dc82c094e043ccb7e0570e5342dd1b?tpId=37&&tqId=21298&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

给定两个只包含小写字母的字符串，计算两个字符串的最大公共子串的长度。

注：子串的定义指一个字符串删掉其部分前缀和后缀（也可以不删）后形成的字符串。

数据范围：字符串长度：1≤�≤150 1≤*s*≤150 

进阶：时间复杂度：�(�3) *O*(*n*3) ，空间复杂度：�(�) *O*(*n*) 

输入描述：

输入两个只包含小写字母的字符串

输出描述：

输出一个整数，代表最大公共子串的长度

示例1

输入：

```
asdfas
werasdfaswer
```

输出：

```
6
```

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() {
    string str1, str2;
    cin >> str1 >> str2;
    if(str1.size() > str2.size()) 
    {
        string tmp = str1;
        str1 = str2;
        str2 = tmp;
    }
    vector<int> dp(str1.size() + 1, 0);
    for(int i = 1; i <= str1.size(); i++)
    {
        string tmp1 = str1.substr(0, i);
        for(int j = 0; j < tmp1.size(); j++)
        {
            string tmp2 = tmp1.substr(j);
            if(tmp2.size() <= dp[i - 1]) break;
            if(str2.find(tmp2) != string::npos) dp[i] = tmp2.size();
        }
        if(dp[i] == 0) dp[i] = dp[i - 1];
    }
    cout << dp[str1.size()];
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY130

## [洗牌_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/5a0a2c7e431e4fbbbb1ff32ac6e8dfa0?tpId=85&&tqId=29848&rp=1&ru=/activity/oj&qru=/ta/2017test/question-ranking)

描述

洗牌在生活中十分常见，现在需要写一个程序模拟洗牌的过程。 现在需要洗2n张牌，从上到下依次是第1张，第2张，第3张一直到第2n张。首先，我们把这2n张牌分成两堆，左手拿着第1张到第n张（上半堆），右手拿着第n+1张到第2n张（下半堆）。接着就开始洗牌的过程，先放下右手的最后一张牌，再放下左手的最后一张牌，接着放下右手的倒数第二张牌，再放下左手的倒数第二张牌，直到最后放下左手的第一张牌。接着把牌合并起来就可以了。 例如有6张牌，最开始牌的序列是1,2,3,4,5,6。首先分成两组，左手拿着1,2,3；右手拿着4,5,6。在洗牌过程中按顺序放下了6,3,5,2,4,1。把这六张牌再次合成一组牌之后，我们按照从上往下的顺序看这组牌，就变成了序列1,4,2,5,3,6。 现在给出一个原始牌组，请输出这副牌洗牌k次之后从上往下的序列。

输入描述：

第一行一个数T(T ≤ 100)，表示数据组数。对于每组数据，第一行两个数n,k(1 ≤ n,k ≤ 100)，接下来有2n行个数a1,a2,...,a2n(1 ≤ ai ≤ 1000000000)。表示原始牌组从上到下的序列。

输出描述：

对于每组数据，输出一行，最终的序列。数字之间用空格隔开，不要在行末输出多余的空格。

示例1

输入：

```
3
3 1
1
2
3
4
5
6
3 2
1
2
3
4
5
6
2 2
1
1
1
1
```

输出：

```
1 4 2 5 3 6
1 5 4 3 2 6
1 1 1 1
```

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() {
    int t;
    cin >> t;
    for(int i = 0; i < t; i++)
    {
        int n, k;
        cin >> n >> k;
        vector<int> arr(2 * n), tmp(2 * n);
        for(int j = 0; j < 2 * n; j++) cin >> arr[j];
        while(k--)
        {
            int cur = 0;
            for(int j = 0, k = n; j < n; j++, k++)
            {
                tmp[cur++] = arr[j];
                tmp[cur++] = arr[k];
            }
            arr = tmp;
        }
        for(auto e : arr) cout << e << ' ';
        cout << endl;
    }
}
// 64 位输出请用 printf("%lld")
```

## [MP3光标位置_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/eaf5b886bd6645dd9cfb5406f3753e15?tpId=37&&tqId=21287&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

MP3 Player因为屏幕较小，显示歌曲列表的时候每屏只能显示几首歌曲，用户要通过上下键才能浏览所有的歌曲。为了简化处理，假设每屏只能显示4首歌曲，光标初始的位置为第1首歌。



现在要实现通过上下键控制光标移动来浏览歌曲列表，控制逻辑如下：

1. 歌曲总数<=4的时候，不需要翻页，只是挪动光标位置。

光标在第一首歌曲上时，按Up键光标挪到最后一首歌曲；光标在最后一首歌曲时，按Down键光标挪到第一首歌曲。

![img](http://uploadfiles.nowcoder.com/images/20151225/60_1451044435725_D0096EC6C83575373E3A21D129FF8FEF)

其他情况下用户按Up键，光标挪到上一首歌曲；用户按Down键，光标挪到下一首歌曲。

![img](http://uploadfiles.nowcoder.com/images/20151225/60_1451044443725_032B2CC936860B03048302D991C3498F)

\2. 歌曲总数大于4的时候（以一共有10首歌为例）：



特殊翻页：屏幕显示的是第一页（即显示第1 – 4首）时，光标在第一首歌曲上，用户按Up键后，屏幕要显示最后一页（即显示第7-10首歌），同时光标放到最后一首歌上。同样的，屏幕显示最后一页时，光标在最后一首歌曲上，用户按Down键，屏幕要显示第一页，光标挪到第一首歌上。

![img](http://uploadfiles.nowcoder.com/images/20151225/60_1451044452440_18E2999891374A475D0687CA9F989D83)

一般翻页：屏幕显示的不是第一页时，光标在当前屏幕显示的第一首歌曲时，用户按Up键后，屏幕从当前歌曲的上一首开始显示，光标也挪到上一首歌曲。光标当前屏幕的最后一首歌时的Down键处理也类似。

![img](http://uploadfiles.nowcoder.com/images/20151225/60_1451044460400_FE5DF232CAFA4C4E0F1A0294418E5660)

其他情况，不用翻页，只是挪动光标就行。

数据范围：命令长度1≤�≤100 1≤*s*≤100 ，歌曲数量1≤�≤150 1≤*n*≤150 

进阶：时间复杂度：�(�) *O*(*n*) ，空间复杂度：�(�) *O*(*n*) 

输入描述：

输入说明：
1 输入歌曲数量
2 输入命令 U或者D

输出描述：

输出说明
1 输出当前列表
2 输出当前选中歌曲

示例1

输入：

```
10
UUUU
```

输出：

```
7 8 9 10
7
```

```cpp
#include <cstring>
#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    string str;
    cin >> str;
    int cur = 1;
    if(n <= 4)
    {
        int cur = 1;
        for(char ch : str)
        {
            if(ch == 'U' && cur == 1) cur = n;
            else if(ch == 'U') cur--;
            else if(ch == 'D' && cur == n) cur = 1;
            else cur++; 
        }
        for(int i = 1; i <= n; i++) cout << i << ' ';
        cout << endl << cur;
    }
    else 
    {
        int arr[4] = {1, 2, 3, 4}, cur = 1;
        for(char ch : str)
        {
            if(ch == 'U' && cur == 1)
            {
                cur = n;
                int tmp = n;
                for(int i = 3; i >= 0; i--) arr[i] = tmp--;
            }
            else if(ch == 'U' && cur == arr[0])
            {
                for(int i = 3; i > 0; i--) arr[i] = arr[i - 1];
                arr[0] = --cur;
            }
            else if(ch == 'D' && cur == n)
            {
                cur = 1;
                for(int i = 1; i <= 4; i++) arr[i - 1] = i;
            }
            else if(ch == 'D' && cur == arr[3])
            {
                for(int i = 0; i < 3; i++) arr[i] = arr[i + 1];
                arr[3] = ++cur;
            }
            else if(ch == 'D') cur++;
            else cur--;
        }
        for(int e : arr) cout << e << ' ';
        cout << endl << cur;
        return 0;
    }
}
// 64 位输出请用 printf("%lld")
```

# DAY131

## [小易的升级之路_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/fe6c73cb899c4fe1bdd773f8d3b42c3d?tpId=49&&tqId=29329&rp=1&ru=/activity/oj&qru=/ta/2016test/question-ranking)

描述

小易经常沉迷于网络游戏.有一次,他在玩一个打怪升级的游戏,他的角色的初始能力值为 a.在接下来的一段时间内,他将会依次遇见n个怪物,每个怪物的防御力为b1,b2,b3...bn. 如果遇到的怪物防御力bi小于等于小易的当前能力值c,那么他就能轻松打败怪物,并 且使得自己的能力值增加bi;如果bi大于c,那他也能打败怪物,但他的能力值只能增加bi 与c的最大公约数.那么问题来了,在一系列的锻炼后,小易的最终能力值为多少?

输入描述：

对于每组数据,第一行是两个整数n(1≤n<100000)表示怪物的数量和a表示小易的初始能力值. 然后输入n行，每行整数,b1,b2...bn(1≤bi≤n)表示每个怪物的防御力

输出描述：

对于每组数据,输出一行.每行仅包含一个整数,表示小易的最终能力值

示例1

输入：

```
3 50
50 105 200
5 20
30 20 15 40 100
```

输出：

```
110
205
```

```cpp
#include <iostream>
#include <vector>
using namespace std;

int maxdivisor(int a, int b)
{
    int divisor = min(a, b);
    while(divisor)
    {
        if(a % divisor == 0 && b % divisor == 0) return divisor;
        divisor--;
    }
    return 0;
}

int main() {
    int n, a;
    while (cin >> n >> a) 
    {
        vector<int> arr(n);
        for(int i = 0; i < n; i++) cin >> arr[i];
        for(int bi : arr)
        {
            if(a >= bi) a += bi;
            else a +=  maxdivisor(a, bi);
        }
        cout << a << endl;
    }
}
// 64 位输出请用 printf("%lld")
```

## [找出字符串中第一个只出现一次的字符_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/e896d0f82f1246a3aa7b232ce38029d4?tpId=37&&tqId=21282&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

找出字符串中第一个只出现一次的字符

数据范围：输入的字符串长度满足 1≤�≤1000 1≤*n*≤1000 

输入描述：

输入一个非空字符串

输出描述：

输出第一个只出现一次的字符，如果不存在输出-1

示例1

输入：

```
asdfasdfo
```

输出：

```
o
```

```cpp
#include <iostream>
#include <queue>
using namespace std;

int main() 
{
    string str;
    cin >> str;
    int arr[128] = {0};
    queue<char> qu;
    for(char ch : str)
    {
        if(arr[ch]++ == 0) qu.push(ch);
    }
    char first = '\0';
    while(qu.size())
    {
        char ch = qu.front();
        qu.pop();
        if(arr[ch] == 1) 
        {
            first = ch;
            break;
        }
    }
    if(first == '\0') cout << -1;
    else cout << first;
    return 0;
}
// 64 位输出请用 printf("%lld")
```

# DAY 132

## [微信红包_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/fbcf95ed620f42a88be24eb2cd57ec54?tpId=49&&tqId=29311&rp=1&ru=/activity/oj&qru=/ta/2016test/question-ranking)

描述

春节期间小明使用微信收到很多个红包，非常开心。在查看领取红包记录时发现，某个红包金额出现的次数超过了红包总数的一半。请帮小明找到该红包金额。写出具体算法思路和代码实现，要求算法尽可能高效。

给定一个红包的金额数组 **gifts** 及它的大小 **n** ，请返回所求红包的金额。

若没有金额超过总数的一半，返回0。

数据范围： 1≤�≤1000 1≤*n*≤1000 ,红包金额满足 1≤�����≤100000 1≤*g**i**f**t**i*≤100000 

示例1

输入：

```
[1,2,3,2,2],5
```

返回值：

```
2
```



示例2

输入：

```
[1,1,2,2,3,3],6
```

返回值：

```
0
```

```cpp
#include <iostream>
#include <vector>
class Gift
{
public:
    int getValue(vector<int> gifts, int n)
    {
        int hash[100000];
        for(int e : gifts) hash[e]++;
        int ret = 0;
        for(int num : gifts)
            if(hash[num] > n / 2) return num;
        return 0;
    }
};
```

## [计算字符串的编辑距离_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/3959837097c7413a961a135d7104c314?tpId=37&&tqId=21275&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

Levenshtein 距离，又称编辑距离，指的是两个字符串之间，由一个转换成另一个所需的最少编辑操作次数。许可的编辑操作包括**将一个字符替换成另一个字符，插入一个字符，删除一个字符**。编辑距离的算法是首先由俄国科学家 Levenshtein 提出的，故又叫 Levenshtein Distance 。

例如：

字符串A: abcdefg

字符串B: abcdef

通过增加或是删掉字符 ”g” 的方式达到目的。这两种方案都需要一次操作。把这个操作所需要的次数定义为两个字符串的距离。

要求：

给定任意两个字符串，写出一个算法计算它们的编辑距离。

数据范围：给定的字符串长度满足 1≤���(���)≤1000 1≤*l**e**n*(*s**t**r*)≤1000 

输入描述：

每组用例一共2行，为输入的两个字符串

输出描述：

每组用例输出一行，代表字符串的距离

示例1

输入：

```
abcdefg
abcdef
```

输出：

```
1
```

```cpp
#include <iostream>
#include <vector>
using namespace std;

int main() 
{
    string str1, str2;
    cin >> str1 >> str2;
    int m = str1.size(), n = str2.size();
    vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
    for(int i = 0; i <= m; i++) dp[i][0] = i;
    for(int j = 0; j <= n; j++) dp[0][j] = j;
    for(int i = 1; i <= m; i++)
        for(int j = 1; j <= n; j++)
        {
            dp[i][j] = str1[i - 1] == str2[j - 1] ? dp[i - 1][j - 1] :
                       min(min(dp[i - 1][j] + 1, dp[i][j - 1] + 1), dp[i - 1][j - 1] + 1);
        }
    cout << dp[m][n];
    return 0;
}
```

# DAY133

## [年终奖_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/72a99e28381a407991f2c96d8cb238ab?tpId=49&&tqId=29305&rp=1&ru=/activity/oj&qru=/ta/2016test/question-ranking)

描述

小东所在公司要发年终奖，而小东恰好获得了最高福利，他要在公司年会上参与一个抽奖游戏，游戏在一个6*6的棋盘上进行，上面放着36个价值不等的礼物，每个小的棋盘上面放置着一个礼物，他需要从左上角开始游戏，每次只能向下或者向右移动一步，到达右下角停止，一路上的格子里的礼物小东都能拿到，请设计一个算法使小东拿到价值最高的礼物。

给定一个6*6的矩阵**board**，其中每个元素为对应格子的礼物价值,左上角为[0,0],请返回能获得的最大价值，保证每个礼物价值大于100小于1000。

```cpp
class Bonus {
public:
    int getMost(vector<vector<int> > board) {
        for(int i = 1; i < 6; i++)
        {
            board[i][0] += board[i - 1][0];
            board[0][i] += board[0][i - 1];
        }
        for(int i = 1; i < 6; i++)
        {
            for(int j = 1; j < 6; j++)
            {
                board[i][j] += max(board[i - 1][j], board[i][j - 1]);
            }
        }
        return board[5][5];
    }
};
```

## [迷宫问题_牛客题霸_牛客网 (nowcoder.com)](https://www.nowcoder.com/practice/cf24906056f4488c9ddb132f317e03bc?tpId=37&&tqId=21266&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking)

描述

定义一个二维数组 N*M ，如 5 × 5 数组下所示：


int maze[5][5] = {
0, 1, 0, 0, 0,
0, 1, 1, 1, 0,
0, 0, 0, 0, 0,
0, 1, 1, 1, 0,
0, 0, 0, 1, 0,
};


它表示一个迷宫，其中的1表示墙壁，0表示可以走的路，只能横着走或竖着走，不能斜着走，要求编程序找出从左上角到右下角的路线。入口点为[0,0],既第一格是可以走的路。

数据范围： 2≤�,�≤10 2≤*n*,*m*≤10 ， 输入的内容只包含 0≤���≤1 0≤*v**a**l*≤1 

输入描述：

输入两个整数，分别表示二维数组的行数，列数。再输入相应的数组，其中的1表示墙壁，0表示可以走的路。数据保证有唯一解,不考虑有多解的情况，即迷宫只有一条通道。

输出描述：

左上角到右下角的最短路径，格式如样例所示。

示例1

输入：

```
5 5
0 1 0 0 0
0 1 1 1 0
0 0 0 0 0
0 1 1 1 0
0 0 0 1 0
```

输出：

```
(0,0)
(1,0)
(2,0)
(2,1)
(2,2)
(2,3)
(2,4)
(3,4)
(4,4)
```

```cpp
#include <iostream>
#include <vector>
using namespace std;
int x[4] = {0, 0, 1, -1};
int y[4] = {1, -1, 0, 0};
int m, n;

bool dfs(vector<vector<int>>& maze, int row, int col)
{
    if(row == m - 1 && col == n - 1) return true;
    for(int i = 0; i < 4; i++)
    {
        int tmpx = row + x[i], tmpy = col + y[i];
        if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && maze[tmpx][tmpy] == 0)
        {
            maze[tmpx][tmpy] = 2;
            if(dfs(maze, tmpx, tmpy))
            {
                return true;
            }
            maze[tmpx][tmpy] = 0;
        }
    }
    return false;
}

void Print(vector<vector<int>>& maze, int a, int b)
{
    printf("(%d,%d)\n", a, b);
    maze[a][b] = 1;
    for(int i = 0; i < 4; i++)
    {
        int tmpx = a + x[i], tmpy = b + y[i];
        if(tmpx >= 0 && tmpy >= 0 && tmpx < m && tmpy < n && maze[tmpx][tmpy] == 2)
        {
            Print(maze, tmpx, tmpy);
        }
    }
}

int main() {
    cin >> m >> n;
    vector<vector<int>> maze(m, vector<int>(n, 0));
    for(int i = 0; i < m; i++)
        for(int j = 0; j < n; j++)
            cin >> maze[i][j];
    maze[0][0] = 2;
    dfs(maze, 0, 0);
    Print(maze, 0, 0);
    return 0;
}
```

