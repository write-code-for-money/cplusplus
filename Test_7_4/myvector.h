#pragma once

#include <iostream>
#include <cassert>
using namespace std;
namespace wzh
{
    template<class T>
    class vector
    {
    public:
        // Vector的迭代器是一个原生指针
        typedef T* iterator;
        typedef const T* const_iterator;
        iterator begin();
        iterator end();
        const_iterator cbegin();
        const_iterator cend() const;
        // construct and destroy
        vector();
        vector(int n, const T& value = T());   //缺省出现在声明，定义不需要写
        template<class InputIterator>
        vector(InputIterator first, InputIterator last);
        vector(const vector<T>& v); // 拷贝构造
        vector<T>& operator= (const vector<T> v);
        ~vector();
        // capacity
        size_t size() const;
        size_t capacity() const;
        void reserve(size_t n);
        void resize(size_t n, const T& value = T());
        ///////////////access///////////////////////////////
        T& operator[](size_t pos);
        const T& operator[](size_t pos)const;
        ///////////////modify/////////////////////////////
        void push_back(const T& x);
        void pop_back();
        void swap(vector<T>& v);
        iterator insert(iterator pos, const T& x);
        iterator erase(iterator pos);
    private:
        iterator _start; // 指向数据块的开始
        iterator _finish; // 指向有效数据的尾
        iterator _endOfStorage; // 指向存储容量的尾
    };

	template<class T>
	void vector<T>::push_back(const T& x)
	{
		if (_finish >= _endOfStorage)
			capacity() == 0 ? reserve(4) : reserve(2 * capacity());
		*_finish = x;
		_finish++;
	}

	template<class T>
	void vector<T>::pop_back()
	{
		if(_finish > _start)
			_finish--;
	}

	template<class T>
	void vector<T>::swap(vector<T>& v)
	{
		vector<T> tmp(v);
		v = *this;
		*this = tmp;
	}

	template<class T>
	typename vector<T>::iterator vector<T>::insert(iterator pos, const T& x)
	{
		size_t index = pos - _start;  
		size_t newSize = size() + 1;  
		if (newSize > capacity())     
		{
			size_t newCapacity = newSize * 2;  
			reserve(newCapacity);
		}

		iterator newPos = _start + index;  
		memmove(newPos + 1, newPos, sizeof(T) * (size() - index));  
		*(_start + index) = x;  
		_finish++;    

		return newPos;
	}

	template<class T>
	typename vector<T>::iterator vector<T>::erase(iterator pos)
	{
		assert(pos < _finish&& pos >= _start);
		memmove(pos, pos + 1, sizeof(T) * (_finish - pos));
		_finish--;
		return _start;
	}

	template<class T>
	T& vector<T>::operator[](size_t pos)
	{
		return _start[pos];
	}

	template<class T>
	const T& vector<T>::operator[](size_t pos) const
	{
		return _start[pos];
	}

	// capacity
	template<class T>
	size_t vector<T>::size() const
	{
		return _finish - _start;
	}

	template<class T>
	size_t vector<T>::capacity() const
	{
		return _endOfStorage - _start;
	}

	template<class T>
	void vector<T>::reserve(size_t n)
	{
		if (capacity() < n)
		{
			int sz = size();
			T* tmp = new T[n];
			std::copy(_start, _finish, tmp);
			delete[] _start;
			_start = tmp;
			_finish = _start + sz;
			_endOfStorage = _start + n;			
		}
	}

	template<class T>
	void vector<T>::resize(size_t n, const T& value)
	{
		if (capacity() < n)
		{
			reserve(n);
			_finish = _start + n;
			for (int i = size(); i < capacity(); i++)
			{
				_start[i] = value;
			}
		}
		else if (size() < n)
		{
			for (int i = size(); i < n; i++)
			{
				_start[i] = value;
			}
			_finish = _start + n;
		}
	}

	template<class T>
	typename vector<T>::iterator vector<T>::begin()
	{
		return _start;
	}

	template<class T>
	typename vector<T>::iterator vector<T>::end()
	{
		return _finish;
	}

	template<class T>
	typename vector<T>::const_iterator vector<T>::cbegin()
	{
		return _finish;
	}

	template<class T>
	typename vector<T>::const_iterator vector<T>::cend() const
	{
		return _start;
	}

	// construct and destroy
	template<class T>
	vector<T>::vector()
		:_start(nullptr)
		, _finish(nullptr)
		, _endOfStorage(nullptr)
	{

	}

	template<class T>
	vector<T>::vector(int n, const T& value)
		:_start(new T[n])
		, _finish(_start + n)
		, _endOfStorage(_start + n)
	{
		for (int i = 0; i < n; i++)
		{
			_start[i] = value;
		}
	}

	template<class T>
	template<class InputIterator>
	vector<T>::vector(InputIterator first, InputIterator last)
		:_start(nullptr)
		, _finish(nullptr)
		, _endOfStorage(nullptr)
	{
		int count = std::distance(first, last);
		_start = new T[count];
		_finish = _start + count;
		_endOfStorage = _start + count;
		for (int i = 0; i < count; i++)
		{
			_start[i] = *first;
			++first;
		}
	}

	template<class T>
	vector<T>::vector(const vector<T>& v)
		:_start(nullptr)
		, _finish(nullptr)
		, _endOfStorage(nullptr)
	{
		int count = v._endOfStorage - v._start;
		_start = new T[count];
		_finish = _start + count;
		_endOfStorage = _start + count;
		for (int i = 0; i < count; i++)
		{
			_start[i] = v._start[i];
		}
	}

	template<class T>
	vector<T>& vector<T>::operator= (const vector<T> v)
	{
		int count = v._endOfStorage - v._start;
		_start = new T[count];
		_finish = _start + count;
		_endOfStorage = _start + count;
		for (int i = 0; i < count; i++)
		{
			_start[i] = v._start[i];
		}
		return *this;
	}

	template<class T>
	vector<T>::~vector()
	{
		delete[] _start;
		_start = nullptr;
		_finish = nullptr;
		_endOfStorage = nullptr;
	}

}