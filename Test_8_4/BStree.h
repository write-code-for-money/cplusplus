#pragma once

//#include <iostream>
////#include <string>
//using namespace std;
//
//template<class K, class V>
//class BSTreeNode
//{
//public:
//	BSTreeNode(const K& key, const V& value)
//		:_key(key), _value(value), _left(nullptr), _right(nullptr)
//	{
//
//	}
//	K _key;
//	V _value;
//	BSTreeNode* _left;
//	BSTreeNode* _right;
//};
//
//
//template<class K, class V>
//class BSTree
//{
//	typedef BSTreeNode<K, V> Node;
//public:
//	bool Insert(const K& key, const V& value)
//	{
//		Node* newNode = new Node(key, value);
//		if (_root == nullptr)
//		{
//			_root = newNode;
//			return true;
//		}
//		Node* cur = _root;
//		Node* parent = nullptr;
//		while (cur)
//		{
//			parent = cur;
//			if (key < cur->_key)
//				cur = cur->_left;
//			else if (key > cur->_key)
//				cur = cur->_right;
//			else
//				return false; // key already exists
//		}
//		if (key < parent->_key)
//			parent->_left = newNode;
//		else
//			parent->_right = newNode;
//		return true;
//	}
//
//	Node* Find(const K& key)
//	{
//		Node* cur = _root;
//		while (cur)
//		{
//			if (key < cur->_key)
//				cur = cur->_left;
//			else if (key > cur->_key)
//				cur = cur->_right;
//			else
//				return cur;
//		}
//		return nullptr; 
//	}
//
//	bool Erase(const K& key)
//	{
//		if (_root == nullptr)
//			return false;
//
//		Node* cur = _root;
//		Node* parent = nullptr;
//		while (cur) 
//		{
//			if (key < cur->_key) 
//			{
//				parent = cur;
//				cur = cur->_left;
//			}
//			else if (key > cur->_key) 
//			{
//				parent = cur;
//				cur = cur->_right;
//			}
//			else 
//			{
//				break;
//			}
//		}
//		
//		if (cur->_left == nullptr) 
//		{
//			if (cur == _root) {
//				_root = cur->_right;
//			}
//			else if (cur == parent->_left) {
//				parent->_left = cur->_right;
//			}
//			else {
//				parent->_right = cur->_right;
//			}
//			delete cur;
//		}
//		else if (cur->_right == nullptr) {
//			if (cur == _root) {
//				_root = cur->_left;
//			}
//			else if (cur == parent->_left) {
//				parent->_left = cur->_left;
//			}
//			else {
//				parent->_right = cur->_left;
//			}
//			delete cur;
//		}
//		else {
//			Node* del = cur->_right;
//			parent = cur;
//			while (del->_left) {
//				parent = del;
//				del = del->_left;
//			}
//			cur->_key = del->_key;
//			cur->_value = del->_value;
//			if (del == parent->_left) {
//				parent->_left = del->_right;
//			}
//			else {
//				parent->_right = del->_right;
//			}
//			delete del;
//		}
//		return true;
//	}
//	void _InOrder(Node* root)
//	{
//		if (root) {
//			_InOrder(root->_left);
//			cout << root->_key << ":" << root->_value << endl;
//			_InOrder(root->_right);
//		}
//	}
//	void InOrder()
//	{
//		_InOrder(_root);
//	}
//private:
//	Node* _root = nullptr;
//};
//
//
//
//
//void TestBSTree()
//{
//	BSTree<string, string> dict;
//	dict.Insert("insert", "插入");
//	dict.Insert("erase", "删除");
//	dict.Insert("left", "左边");
//	dict.Insert("string", "字符串");
//
//	string str;
//	while (cin >> str)
//	{
//		auto ret = dict.Find(str);
//		if (ret)
//		{
//			cout << str << ":" << ret->_value << endl;
//		}
//		else
//		{
//			cout << "单词拼写错误" << endl;
//		}
//	}
//
//	string strs[] = { "苹果", "西瓜", "苹果", "樱桃", "苹果", "樱桃", "苹果", "樱桃", "苹果" };
//	// 统计水果出现的次
//	BSTree<string, int> countTree;
//	for (auto str : strs)
//	{
//		auto ret = countTree.Find(str);
//		if (ret == NULL)
//		{
//			countTree.Insert(str, 1);
//		}
//		else
//		{
//			ret->_value++;
//		}
//	}
//	countTree.InOrder();
//}


//#include <iostream>
//
//using namespace std;
//
//template<class k>
//class BStreeNode
//{
//public:
//	BStreeNode(const k& key)
//		:_key(key), _left(nullptr), _right(nullptr)
//	{}
//
//	k _key;
//	BStreeNode* _left;
//	BStreeNode* _right;
//};
//
//template<class k>
//class BStree
//{
//	typedef BStreeNode<k> Node;
//public:
//	Node* find(const k& key)
//	{
//		Node* cur = _root;
//		while (cur)
//		{
//			if (cur->_key < key)			
//				cur = cur->_right;					
//			else if (cur->_key > key)
//				cur = cur->_left;
//			else
//				return cur;
//		}
//		return nullptr;
//	}
//
//	bool insert(const k& key)
//	{
//		//空树
//		if (_root == nullptr)
//		{
//			Node* newnode = new Node(key);
//			_root = newnode;
//			return true;
//		}
//
//		//不为空
//		Node* cur = _root;
//		Node* parent = nullptr;
//		while (cur)
//		{
//			parent = cur;
//			if (cur->_key < key)
//				cur = cur->_right;
//			else if (cur->_key > key)
//				cur = cur->_left;
//			else
//				return false;
//		}
//		Node* newnode = new Node(key);
//		parent->_key > key ? parent->_left = newnode : parent->_right = newnode;
//		return true;
//	}
//
//	bool erase(const k& key)
//	{
//		//查找该节点及其父亲节点
//		Node* cur = _root;
//		Node* parent = nullptr;
//		while (cur)
//		{
//			if (cur->_key < key)
//			{
//				parent = cur;
//				cur = cur->_right;
//			}
//				
//			else if (cur->_key > key)
//			{
//				parent = cur;
//				cur = cur->_left;
//			}				
//			else
//				break;
//		}
//		if (cur == nullptr)
//			return false;
//		//为叶子节点
//		else if (cur->_left == nullptr && cur->_right == nullptr)
//			delete cur;
//		//只有一个孩子
//		else if ((cur->_left == nullptr && cur->_right) || (cur->_left && cur->_right == nullptr))
//		{
//			if (parent->_left == cur)
//			{
//				cur->_left == nullptr ? parent->_left = cur->_right : parent->_left = cur->_left;
//			}
//			else
//			{
//				cur->_left == nullptr ? parent->_right = cur->_right : parent->_right = cur->_left;
//			}
//			delete cur;
//		}
//		//有两个孩子
//		else if (cur->_left && cur->_right)
//		{
//			Node* del = cur->_right;
//			//找右子树最小节点
//			while (del->_left)
//			{
//				del = del->_left;
//			}
//			//赋值
//			cur->_key = del->_key;
//			//待删除节点的右指向最小节点右
//			cur->_right = del->_right;
//			//删除最小节点
//			delete del;
//		}
//		return true;
//	}
//	
//	void InOrder()
//	{
//		_InOrder(_root);
//		cout << endl;
//	}
//private:
//	Node* _root = nullptr;
//
//	void _InOrder(const Node* root)
//	{
//		if (root == nullptr)
//			return;
//		_InOrder(root->_left);
//		cout << root->_key << ' ';
//		_InOrder(root->_right);
//	}
//};
//
//
//void test1()
//{
//	BStree<int> tree;
//	tree.insert(6);
//	tree.insert(1);
//	tree.insert(7);
//	tree.insert(4);
//	tree.insert(5);
//	tree.insert(8);
//	tree.InOrder();
//	tree.erase(4);
//	tree.InOrder();
//}


#pragma once

#include <iostream>
using namespace std;

template<class K, class V>
class BStreeNode
{
public:
    BStreeNode(const K& key, const V& value)
        :_key(key), _value(value), _left(nullptr), _right(nullptr)
    {}

    K _key;
    V _value;
    BStreeNode* _left;
    BStreeNode* _right;
};

template<class K, class V>
class BStree
{
    typedef BStreeNode<K, V> Node;
public:
    Node* find(const K& key)
    {
        Node* cur = _root;
        while (cur)
        {
            if (cur->_key < key)
                cur = cur->_right;
            else if (cur->_key > key)
                cur = cur->_left;
            else
                return cur;
        }
        return nullptr;
    }

    bool insert(const K& key, const V& value)
    {
        //空树
        if (_root == nullptr)
        {
            Node* newnode = new Node(key, value);
            _root = newnode;
            return true;
        }

        //不为空
        Node* cur = _root;
        Node* parent = nullptr;
        while (cur)
        {
            parent = cur;
            if (cur->_key < key)
                cur = cur->_right;
            else if (cur->_key > key)
                cur = cur->_left;
            else
                return false;
        }
        Node* newnode = new Node(key, value);
        parent->_key > key ? parent->_left = newnode : parent->_right = newnode;
        return true;
    }

    bool erase(const K& key)
    {
        //查找该节点及其父亲节点
        Node* cur = _root;
        Node* parent = nullptr;
        while (cur)
        {
            if (cur->_key < key)
            {
                parent = cur;
                cur = cur->_right;
            }

            else if (cur->_key > key)
            {
                parent = cur;
                cur = cur->_left;
            }
            else
                break;
        }
        if (cur == nullptr)
            return false;
        //为叶子节点
        else if (cur->_left == nullptr && cur->_right == nullptr)
            delete cur;
        //只有一个孩子
        else if ((cur->_left == nullptr && cur->_right) || (cur->_left && cur->_right == nullptr))
        {
            if (parent->_left == cur)
            {
                cur->_left == nullptr ? parent->_left = cur->_right : parent->_left = cur->_left;
            }
            else
            {
                cur->_left == nullptr ? parent->_right = cur->_right : parent->_right = cur->_left;
            }
            delete cur;
        }
        //有两个孩子
        else if (cur->_left && cur->_right)
        {
            Node* del = cur->_right;
            //找右子树最小节点
            while (del->_left)
            {
                del = del->_left;
            }
            //赋值
            cur->_key = del->_key;
            cur->_value = del->_value;
            //待删除节点的右指向最小节点右
            cur->_right = del->_right;
            //删除最小节点
            delete del;
        }
        return true;
    }

    void InOrder()
    {
        _InOrder(_root);
    }
private:
    Node* _root = nullptr;

    void _InOrder(const Node* root)
    {
        if (root == nullptr)
            return;
        _InOrder(root->_left);
        cout << root->_key << ":" << root->_value << endl;
        _InOrder(root->_right);
    }
};

void test1()
{
    BStree<string, string> tree;
    tree.insert("apple", "苹果");
    tree.insert("banana", "香蕉");
    tree.insert("orange", "橘子");
    tree.insert("peach", "桃子");
    tree.insert("pear", "梨");
    tree.InOrder();
}

