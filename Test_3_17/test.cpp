#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//class Date
//{
//public:
//    Date(int year, int month, int day)
//    {
//        _year = year;
//        _month = month;
//        _day = day;
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};
//
//int main()
//{
//    Date s1(2015, 8, 4);
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//        : _year(year)
//        , _month(month)
//        , _day(day)
//    {}
//private:
//    int _year;
//    int _month;
//    int _day;
//};
//
//int main()
//{
//    Date s1(2015, 6, 5);
//    return 0;
//}

//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B(int a, int ref)
//		:_aobj(a)
//		, _ref(ref)
//		, _n(10)
//	{}
//private:
//	A _aobj;  // 没有默认构造函数
//	int& _ref;  // 引用
//	const int _n; // const 
//};
//
//int main()
//{
//	A s1(5);
//	B s2(1, 3);
//	return 0;
//}

//class Time
//{
//public:
//	Time(int hour = 0)
//		:_hour(hour)
//	{
//		cout << "Time()" << endl;
//	}
//private:
//	int _hour;
//};
//
//class Date
//{
//public:
//	Date(int day)
//	{}
//private:
//	int _day;
//	Time _t;
//};
//int main()
//{
//	Date d(1);
//}

//class A
//{
//public:
//    A(int a)
//        :_a1(a)
//        , _a2(_a1)
//    {}
//
//    void Print() {
//        cout << _a1 << " " << _a2 << endl;
//    }
//private:
//    int _a2;
//    int _a1;
//};
//int main() {
//    A aa(1);
//    aa.Print();
//}

//class Date
//{
//public:
//	// 1. 单参构造函数，没有使用explicit修饰，具有类型转换作用
//// explicit修饰构造函数，禁止类型转换---explicit去掉之后，代码可以通过编译
//	/*explicit Date(int year)
//		:_year(year)
//	{}*/
//	// 2. 虽然有多个参数，但是创建对象时后两个参数可以不传递，没有使用explicit修饰，具有类型转换作用
//	// explicit修饰构造函数，禁止类型转换
//	explicit Date(int year, int month = 1, int day = 1)
//	: _year(year)
//	, _month(month)
//	, _day(day)
//	{}
//	Date& operator=(const Date& d)
//	{
//		if (this != &d)
//		{
//			_year = d._year;
//			_month = d._month;
//			_day = d._day;
//		}
//		return *this;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//void main()
//{
//	Date d1(2022);
//	// 用一个整形变量给日期类型对象赋值
//	// 实际编译器背后会用2023构造一个无名对象，最后用无名对象给d1对象进行赋值
//	d1 = 2023;
//	// 将1屏蔽掉，2放开时则编译失败，因为explicit修饰构造函数，禁止了单参构造函数类型转换的作用
//}

//int main()
//{
//	Test();
//	return 0;
//}

//class A
//{
//public:
//	A() 
//	{ 
//		++_scount; 
//	}
//	A(const A& t) 
//	{ 
//		++_scount; 
//	}
//	~A() 
//	{ 
//		--_scount; 
//	}
//	static int GetACount() 
//	{ 
//		return _scount; 
//	}
//private:
//	static int _scount;
//};
//
//int A::_scount = 0;
//
//void main()
//{
//	cout << A::GetACount() << endl;
//	A a1, a2;
//	A a3(a1);
//	cout << A::GetACount() << endl;
//}

//class Date
//{
//public:
//    Date(int year = 2012, int month = 5, int day = 4)
//        : _year(year)
//        , _month(month)
//        , _day(day)
//    {}
//    // d1 << cout; -> d1.operator<<(&d1, cout); 不符合常规调用
//// 因为成员函数第一个参数一定是隐藏的this，所以d1必须放在<<的左侧
//    ostream& operator<<(ostream& _cout)
//    {
//        _cout << _year << "-" << _month << "-" << _day << endl;
//        return _cout;
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};

//int main()
//{
//    Date s1;
//    
//    return 0;
//}

class Date
{
	friend ostream& operator<<(ostream& _cout, const Date& d);
	friend istream& operator>>(istream& _cin, Date& d);
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}
private:
	int _year;
	int _month;
	int _day;
	
};
ostream& operator<<(ostream& _cout, const Date& d)
{
	_cout << d._year << "-" << d._month << "-" << d._day;
	return _cout;
}
istream& operator>>(istream& _cin, Date& d)
{
	_cin >> d._year;
	_cin >> d._month;
	_cin >> d._day;
	return _cin;
}
int main()
{
	Date d;
	cin >> d;
	cout << d << endl;
	return 0;
}