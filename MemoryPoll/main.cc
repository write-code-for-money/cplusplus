#define _CRT_SECURE_NO_WARNINGS 1


#include "memoryPoll.hpp"


int main()
{
	wzh::MemoryPoll<char>* instance = wzh::MemoryPoll<char>::getInstace();
	char* ptr = instance->getMemory(512);
	ptr = (char*)"hello world\n";
	std::cout << ptr << std::endl;
	wzh::MemoryPoll<char>::getInstace()->freeMemory(ptr);
	return 0;
}