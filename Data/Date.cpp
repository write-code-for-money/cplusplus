#define _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

// 获取某年某月的天数
int Date::GetMonthDay(int year, int month)
{
	int day = 0;
	switch(month)
	{
	case 4:
	case 6:
	case 9:
	case 11:
		day = 30;
		break;
	case 2:
		day = 28;
		if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
		{
			day += 1;
		}
		break;
	default:
		day = 31;
		break;
	}
	

	return day;
}

// 全缺省的构造函数
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
}

// 拷贝构造函数
  // d2(d1)
Date::Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
}

// 赋值运算符重载
  // d2 = d3 -> d2.operator=(&d2, d3)
Date& Date::operator=(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
	return *this;

}
// 析构函数
Date::~Date()
{

}

// 日期+天数
Date Date::operator+(int day)
{
	Date d1(*this);
	d1._day += day;
	if (d1._day > 0)
	{
		int tmp = this->GetMonthDay(d1._year, d1._month);
		while (d1._day > tmp)
		{
			d1._month++;
			d1._day -= tmp;
			if (d1._month > 12)
			{
				d1._year++;
				d1._month = 1;
			}
			tmp = this->GetMonthDay(d1._year, d1._month);
		}
	}
	else
	{
		int tmp = this->GetMonthDay(d1._year, d1._month - 1);
		while (d1._day < 0)
		{
			d1._month--;
			d1._day += tmp;
			if (d1._month <= 0)
			{
				d1._year--;
				d1._month = 12;
			}
			tmp = this->GetMonthDay(d1._year, d1._month - 1);
		}
	}
	return d1;
}

// 日期+=天数
Date& Date::operator+=(int day)
{
	*this = *this + day;
	return *this;
}

// 日期-天数
Date Date::operator-(int day)
{
	Date d1(*this);
	d1 += -day;
	return d1;
}

// 日期-=天数
Date& Date::operator-=(int day)
{
	*this = *this - day;
	return *this;
}

// 前置++
Date& Date::operator++()
{
	_day++;
	int tmp = GetMonthDay(_year, _month);
	if (_day > tmp)
	{
		_month++;
		if (_month > 12)
		{
			_year++;
			_month = 1;
		}
		_day = 1;
	}
	return *this;
}

// 后置++
Date Date::operator++(int)
{
	Date d1(*this);
	++(*this);
	return d1;
}

// 后置--
Date Date::operator--(int)
{
	Date d1(*this);
	--(*this);
	return d1;
}
// 前置--
Date& Date::operator--()
{
	_day--;
	if (_day <= 0)
	{
		_month--;
		if (_month <= 0)
		{
			_year--;
			_month = 12;
		}
		_day = GetMonthDay(_year, _month);
	}
	return *this;
}

// >运算符重载
bool Date::operator>(const Date& d)
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year && _month > d._month)
	{
		return true;
	}
	else if (_year == d._year && _month == d._month && _day > d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// ==运算符重载
bool Date::operator==(const Date& d)
{
	return !(*this > d) && !(*this < d);
}

// >=运算符重载
bool Date::operator >= (const Date& d)
{
	return (*this > d) || (*this == d);
}

// <运算符重载
bool Date::operator<(const Date& d)
{
	if (_year < d._year)
	{
		return true;
	}
	else if (_year == d._year && _month < d._month)
	{
		return true;
	}
	else if (_year == d._year && _month == d._month && _day < d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// <=运算符重载
bool Date::operator <= (const Date& d)
{
	return (*this < d) || (*this == d);
}
// !=运算符重载
bool Date::operator != (const Date& d)
{
	return !(*this == d);
}
// 日期-日期 返回天数
int Date::operator-(const Date& d)
{
	int day1 = _day;
	int day2 = d._day;
	int day3 = 0;
	for (int i = 1; i < _month; i++)
	{
		day1 += GetMonthDay(_year, i);
	}
	for (int j = 1; j < d._month; j++)
	{
		day2 += GetMonthDay(d._year, j);
	}
	if (*this > d)
	{
		for (int k = d._year; k < _year; k++)
		{
			if ((k % 4 == 0 && k % 100 != 0) || k % 400 == 0)
			{
				day3 += 366;
			}
			else
			{
				day3 += 365;
			}
		}
		return day3 - day2 + day1;
	}
	else
	{
		for (int k = _year; k < d._year; k++)
		{
			if ((k % 4 == 0 && k % 100 != 0) || k % 400 == 0)
			{
				day3 += 366;
			}
			else
			{
				day3 += 365;
			}
		}
		return -(day3 + day2 - day1);
	}
}		