#pragma once
// 注意：假如实现的哈希表中元素唯一，即key相同的元素不再进行插入
// 为了实现简单，此哈希表中我们将比较直接与元素绑定在一起
#include <iostream>
#include <vector>
using namespace std;

namespace Close_Hash
{
	enum State { EMPTY, EXIST, DELETE };
	template<class K>
	class HashFunc
	{
	public:
		size_t operator()(const K& key)
		{
			return key;
		}
	};
	template<>
	class HashFunc<string>
	{
		size_t operator()(const string& key)
		{
			size_t hash = 0;
			for (auto& ch : key)
			{
				hash = hash * 131 + ch;
			}
			return hash;
		}
	};

	template<class K, class V, class HF = HashFunc<K>>
	class HashTable
	{
		struct Elem
		{
			pair<K, V> _val;
			State _state;
		};

	public:
		HashTable(size_t capacity = 3)
			: _ht(capacity), _size(0), _totalSize(0)
		{
			for (size_t i = 0; i < capacity; ++i)
				_ht[i]._state = EMPTY;
		}

		// 插入
		bool Insert(const pair<K, V>& val)
		{
			CheckCapacity();
			size_t ret = Find(val.first);
			if (ret != -1) return false;
			int hashi = HashFunc(val.first);
			//int flag = _ht.size() * 2;
			int i = 1, cur = hashi;
			while (1)
			{
				if (_ht[hashi % _ht.size()]._state == EMPTY)
				{
					_ht[hashi % _ht.size()] = { val, EXIST };
					_size++;
					_totalSize++;
					break;
				}
				hashi = cur + i * i;
				i++;
			}
			//if (hashi == flag) return false;
			return true;
		}

		// 查找
		size_t Find(const K& key)
		{
			int hashi = HashFunc(key);
			while (hashi < _ht.size() && _ht[hashi]._state != EMPTY)
			{
				if (_ht[hashi]._state != DELETE && _ht[hashi]._val.first == key)
				{
					return hashi;
				}
				hashi++;
			}			
			return -1;
		}

		// 删除
		bool Erase(const K& key)
		{
			size_t ret = Find(key);
			if (ret == -1)
			{
				return false;
			}
			_ht[ret]._state = DELETE;
			_size--;
			return true;
		}

		size_t Size()const
		{
			return _size;
		}

		bool Empty() const
		{
			return _size == 0;
		}

		void Swap(HashTable<K, V>& ht)
		{
			swap(_size, ht._size);
			swap(_totalSize, ht._totalSize);
			_ht.swap(ht._ht);
		}

	private:
		size_t HashFunc(const K& key)
		{
			return HF()(key) % _ht.size();
		}

		void CheckCapacity()
		{
			if (_totalSize * 10 / _ht.size() >= 7)
			{
				HashTable<K, V> ht(_ht.size() * 2);
				for (auto& e : _ht)
				{
					if (e._state == EXIST)
					{
						ht.Insert(e._val);
						/*int hashi = HashFunc(e._val.first);
						ht._ht[hashi] = e;
						ht._size++;
						ht._totalSize++;*/
					}
				}
				Swap(ht);
			}
		}
	private:
		vector<Elem> _ht;
		size_t _size;     //有效元素个数
		size_t _totalSize;  // 哈希表中的所有元素：有效和已删除, 扩容时候要用到
	};
}