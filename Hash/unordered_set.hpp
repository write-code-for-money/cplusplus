#pragma once
#include "openHash.hpp"
using namespace OpenHash;
namespace wzh
{
	template<class K, class HF = HashFunc<K>>
	class unordered_set
	{
		struct KeyOfT
		{
			const K& operator()(const K& data)
			{
				return data;
			}
		};
		typedef HashBucket<K, K, KeyOfT, HF> HT;
		//typedef HashBucketNode<K> Node;
		typedef HBIterator<K, K, KeyOfT, K&, K*, HF> iterator;
	public:
		unordered_set(size_t num = 10)
			:_set(num)
		{}
		unordered_set(const HT& set)
			:_set(set)
		{}
		~unordered_set()
		{}
		iterator begin()
		{
			return _set.begin();
		}
		iterator end()
		{
			return _set.end();
		}
		bool insert(const K& data)
		{
			return _set.Insert(data);
		}
		iterator find(const K& data)
		{
			return _set.Find(data);
		}
		bool erase(const K& data)
		{
			return _set.Erase(data);
		}
		size_t size()
		{
			return _set.Size();
		}
		size_t count()
		{
			return _set.BucketCount();
		}
	private:
		HT _set;
	};
}
