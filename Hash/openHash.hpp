#pragma once
#include <string>
#include <vector>
using namespace std;
namespace OpenHash
{
	template<class T>
	class HashFunc
	{
	public:
		size_t operator()(const T& val)
		{
			return val;
		}
	};

	template<>
	class HashFunc<std::string>
	{
	public:
		size_t operator()(const std::string& s)
		{
			const char* str = s.c_str();
			unsigned int seed = 131; // 31 131 1313 13131 131313
			unsigned int hash = 0;
			while (*str)
			{
				hash = hash * seed + (*str++);
			}
			return hash;
		}
	};

	template<class T>
	struct HashBucketNode
	{
		HashBucketNode(const T& data)
			: _pNext(nullptr)
			, _data(data)
		{}
		HashBucketNode<T>* _pNext;
		T _data;
	};

	const unsigned long primes[28] =
	{
	  53,         97,         193,       389,       769,
	  1543,       3079,       6151,      12289,     24593,
	  49157,      98317,      196613,    393241,    786433,
	  1572869,    3145739,    6291469,   12582917,  25165843,
	  50331653,   100663319,  201326611, 402653189, 805306457,
	  1610612741, 3221225473, 4294967291
	};

	template<class K, class T, class KeyOfT, class HF>
	class HashBucket;

	template<class K, class T, class KeyOfT, class Ref, class Ptr, class HF>
	class HBIterator
	{
		typedef HashBucket<K, T, KeyOfT, HF> HashBucket;
		typedef HashBucketNode<T> Node;
		typedef HBIterator<K, T, KeyOfT, Ref, Ptr, HF> self;
	public:
		HBIterator(Node* node, const HashBucket* ht)
			:_pnode(node), _ht(ht)
		{}
		HBIterator(const self& it)
			:_pnode(it._pnode)
			,_ht(it._ht)
		{}

		Ref operator*()
		{
			return _pnode->_data;
		}
		Ptr operator->()
		{
			return &(_pnode->_data);
		}
		bool operator==(const self& it)
		{
			return _pnode == it._pnode;
		}
		bool operator!=(const self& it)
		{
			return _pnode != it._pnode;
		}
		self operator++()
		{
			if (_pnode->_pNext)
			{
				_pnode = _pnode->_pNext;
			}
			else
			{
				int hashi = HF()(KeyOfT()(_pnode->_data)) % _ht->_table.size();
				hashi++;
				while (hashi < _ht->_table.size())
				{
					if (_ht->_table[hashi])
					{
						_pnode = _ht->_table[hashi];
						break;
					}
					else
					{
						++hashi;
					}
				}
				if (hashi == _ht->_table.size())
				{
					_pnode = nullptr;
				}
			}
			return *this;
		}
	private:
		Node* _pnode;
		const HashBucket* _ht;
	};

	// 本文所实现的哈希桶中key是唯一的
	template<class K, class T, class KeyOfT, class HF = HashFunc<K>>
	class HashBucket
	{
	public:
		friend class HBIterator<K, T, KeyOfT, T&, T*, HF>;
		typedef HashBucketNode<T> Node;
		typedef HashBucket<K, T, KeyOfT, HF> Self;
		typedef HBIterator<K, T, KeyOfT, T&, T*, HF> iterator;
		typedef HBIterator<K, T, KeyOfT, const T&, const T*, HF> const_iterator;

	public:		
		size_t GetNextPrime(size_t num)
		{
			for (auto e : primes)
			{
				if (e > num) return e;
			}
			return primes[27];
		}

		HashBucket(size_t capacity = 10)
			: _table(GetNextPrime(capacity))
			, _size(0)
		{}

		~HashBucket()
		{
			Clear();
		}

		iterator begin()
		{
			for (int i = 0; i < _table.size(); i++)
			{
				if (_table[i])
				{
					return iterator(_table[i], this);
				}
			}
			return iterator(nullptr, this);
		}

		const_iterator begin() const 
		{
			for (int i = 0; i < _table.size(); i++)
			{
				if (_table[i])
				{
					return const_iterator(_table[i], this);
				}
			}
			return const_iterator(nullptr, this);
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		const_iterator end() const 
		{
			return const_iterator(nullptr, this);
		}

		// 哈希桶中的元素不能重复
		pair<iterator, bool> Insert(const T& data)
		{
			KeyOfT keyoft;
			int hashi = HashFunc(keyoft(data));
			Node* p = _table[hashi];
			while (p)
			{
				if (keyoft(p->_data) == keyoft(data)) return make_pair(iterator(nullptr, this), false);
				p = p->_pNext;
			}
			CheckCapacity();
			hashi = HashFunc(keyoft(data));
			Node* node = new Node(data);
			node->_pNext = _table[hashi];
			_table[hashi] = node;
			_size++;
			return make_pair(iterator(node, this), true);
		}

		// 删除哈希桶中为data的元素(data不会重复)
		bool Erase(const K& key)
		{
			KeyOfT keyoft;
			int hashi = HashFunc(key);
			if (_table[hashi] == nullptr) return false;
			Node* cur = _table[hashi];
			if (keyoft(cur->_data) == key)
			{
				_table[hashi] = cur->_pNext;
				delete cur;
				_size--;
				return true;
			}
			else
			{
				Node* prev = cur;
				cur = cur->_pNext;
				while (cur)
				{
					if (keyoft(cur->_data) == key)
					{
						prev->_pNext = cur->_pNext;
						delete cur;
						_size--;
						return true;
					}
					prev = prev->_pNext;
					cur = cur->_pNext;
				}	
				return false;
			}
		}

		iterator Find(const K& key)
		{
			KeyOfT keyoft;
			int hashi = HashFunc(key);
			Node* p = _table[hashi];
			while (p)
			{
				if (keyoft(p->_data) == key) return iterator(p, this);
				p = p->_pNext;
			}
			return end();
		}

		size_t Size()const
		{
			return _size;
		}

		bool Empty()const
		{
			return 0 == _size;
		}

		void Clear()
		{
			for (auto& t : _table)
			{
				while (t != nullptr)
				{
					Erase(KeyOfT()(t->_data));
				}
				if (_size == 0) break;
			}
		}

		size_t BucketCount() const
		{
			return _table.size();
		}

	private:

		size_t HashFunc(const K& key)
		{
			return HF()(key) % _table.size();
		}

		void CheckCapacity()
		{
			if (_size * 10 / _table.size() >= 7)
			{
				_table.resize(GetNextPrime(_table.size()));
				for (int i = 0; i < _table.size(); i++)
				{
					while (_table[i] != nullptr)
					{
						int hashi = HashFunc(KeyOfT()(_table[i]->_data));
						if (hashi == i) break;
						Node* p = _table[i];
						_table[i] = p->_pNext;						
						p->_pNext = _table[hashi];
						_table[hashi] = p;
					}
				}
			}
		}

	private:
		vector<Node*> _table;
		size_t _size;      // 哈希表中有效元素的个数
	};
}
