#pragma once
#include "openHash.hpp"
using namespace OpenHash;
using namespace std;
namespace wzh
{
	template<class K, class V, class HF = HashFunc<K>>
	class unordered_map
	{
	public:
		struct KeyOfT
		{
			const K& operator()(const std::pair<K, V>& data)
			{
				return data.first;
			}
		};
		typedef HashBucket<K, std::pair<K, V>, KeyOfT, HF> HT;
		//typedef HashBucketNode<K> Node;
		typedef typename HashBucket<K, std::pair<K, V>, KeyOfT, HF>::iterator iterator;
	public:
		unordered_map(size_t num = 10)
			:_map(num)
		{}
		unordered_map(const HT& map)
			:_map(map)
		{}
		~unordered_map()
		{}
		iterator begin()
		{
			return _map.begin();
		}
		iterator end()
		{
			return _map.end();
		}
		pair<iterator, bool> insert(const std::pair<K, V>& data)
		{
			return _map.Insert(data);
		}
		iterator find(const K& key)
		{
			return _map.Find(key);
		}
		bool erase(const K& key)
		{
			return _map.Erase(key);
		}
		size_t size()
		{
			return _map.Size();
		}
		size_t count()
		{
			return _map.BucketCount();
		}
	private:
		HT _map;
	};
}
