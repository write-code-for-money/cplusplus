#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>

int main()
{
	const char* s = "0123456789";
	int x = sizeof(s);
	int y = strlen(s);
	std::cout << x << " : " << y << std::endl;
	return 0;
}