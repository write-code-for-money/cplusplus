#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include "Bits_set.h"

void test1()
{
	bits_set<10> bits;
	bits.set(5);
	bits.set(1);
	bits.set(10);
	if (bits.test_bits(10))
	{
		cout << "����" << endl;
	}
	else
	{
		cout << "������" << endl;
	}
	bits.reset(10);
	if (bits.test_bits(10))
	{
		cout << "����" << endl;
	}
	else
	{
		cout << "������" << endl;
	}
}


void test2()
{
	//bits_set<0xFFFFFFFF> bits;
	bits_set<-1> bits;

}

void test3()
{
	BloomFilter<50> bf;
	bf.set("apple");
	bf.set("applc");
	bf.set("banana");
	cout << bf.test("apple") << endl;
	cout << bf.test("applo") << endl;
	cout << bf.test("app") << endl;
}

void test4()
{
	srand(time(0));
	const size_t N = 10000;
	BloomFilter<N> bf;

	std::vector<std::string> v1;
	std::string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";

	for (size_t i = 0; i < N; ++i)
	{
		v1.push_back(url + std::to_string(i));
	}

	for (auto& str : v1)
	{
		bf.set(str);
	}

	// v2��v1�������ַ����������ǲ�һ��
	std::vector<std::string> v2;
	for (size_t i = 0; i < N; ++i)
	{
		std::string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";
		url += std::to_string(999999 + i);
		v2.push_back(url);
	}

	size_t n2 = 0;
	for (auto& str : v2)
	{
		if (bf.test(str))
		{
			++n2;
		}
	}
	cout << "�����ַ���������:" << (double)n2 / (double)N << endl;

	// �������ַ�����
	std::vector<std::string> v3;
	for (size_t i = 0; i < N; ++i)
	{
		string url = "zhihu.com";
		//string url = "https://www.cctalk.com/m/statistics/live/16845432622875";
		url += std::to_string(i + rand());
		v3.push_back(url);
	}

	size_t n3 = 0;
	for (auto& str : v3)
	{
		if (bf.test(str))
		{
			++n3;
		}
	}
	cout << "�������ַ���������:" << (double)n3 / (double)N << endl;
}

void test5()
{
	const size_t N = 10000;
	BloomFilter<N> bf;

	vector<string> v1;
	string url = "https://blog.csdn.net/wzh18907434168?spm=1010.2135.3001.5343";

	for (size_t i = 0; i < N; ++i) 
	{
		v1.push_back(url + to_string(i));
	}

	for (const auto& str : v1) 
	{
		bf.set(str);
	}

	vector<string> v2;
	for (size_t i = 0; i < N; ++i) 
	{
		v2.push_back(url + to_string(456825 + i));
	}

	size_t n2 = 0;
	for (const auto& str : v2) 
	{
		if (bf.test(str)) 
		{
			++n2;
		}
	}
	cout << "�����ַ���������: " << (double)n2 / (double)N << endl;

	vector<string> v3;
	for (size_t i = 0; i < N; ++i) 
	{
		v3.push_back("www.com" + to_string(i + rand()));
	}

	size_t n3 = 0;
	for (const auto& str : v3) 
	{
		if (bf.test(str)) 
		{
			++n3;
		}
	}
	cout << "�������ַ���������: " << (double)n3 / (double)N << endl;
}
int main()
{
	test5();

	return 0;
}