﻿//#include "queue.h"
//#include "stack.h"
#include <stack>
#include <iostream>
#include <vector>
#include <string>
using namespace std;
int evalRPN(vector<string>& tokens) {
    stack<int> st1;
    for (auto& e : tokens)
    {
        if (stoi(e) == stoi("+") || stoi(e) == stoi("/") || stoi(e) == stoi("-") || stoi(e) == stoi("*"))
        {
            int x = st1.top();
            st1.pop();
            int y = st1.top();
            st1.pop();
            switch (e.front())
            {
            case '+':
                st1.push(x + y);
                break;
            case '-':
                st1.push(y - x);
                break;
            case '*':
                st1.push(x * y);
                break;
            case '/':
                st1.push(y / x);
                break;
            }
        }
        else
        {
            st1.push(stoi(e));
        }
    }
    return st1.top();
}

int main()
{
    vector<string> tokens = { "10","6","9","3","+","-11","*","/","*","17","+","5","+" };
    evalRPN(tokens);
    return 0;
}

//int main() {
//    wzh::stack<int> s;
//    s.push(1);
//    s.push(2);
//    s.push(3);
//    cout << "s.top() = " << s.top() << endl;
//    s.pop();
//    cout << "s.top() = " << s.top() << endl;
//    cout << "s.size() = " << s.size() << endl;
//    cout << "s.empty() = " << s.empty() << endl;
//
//    wzh::queue<int> q;
//    q.push(1);
//    q.push(2);
//    q.push(3);
//    cout << "q.front() = " << q.front() << endl;
//    cout << "q.back() = " << q.back() << endl;
//    q.pop();
//    cout << "q.front() = " << q.front() << endl;
//    cout << "q.size() = " << q.size() << endl;
//    cout << "q.empty() = " << q.empty() << endl;
//
//    return 0;
//}