#pragma once
#include <iostream>

template<class T>
struct AVLTreeNode
{
	AVLTreeNode(const T& data = T())
		: _pLeft(nullptr)
		, _pRight(nullptr)
		, _pParent(nullptr)
		, _data(data)
		, _bf(0)
	{}

	AVLTreeNode<T>* _pLeft;
	AVLTreeNode<T>* _pRight;
	AVLTreeNode<T>* _pParent;
	T _data;
	int _bf;   // 节点的平衡因子
};


// AVL: 二叉搜索树 + 平衡因子的限制
template<class T>
class AVLTree
{
	typedef AVLTreeNode<T> Node;
public:
	AVLTree()
		: _pRoot(nullptr)
	{}

	// 在AVL树中插入值为data的节点
	bool Insert(const T& data)
	{
        Node* pNewNode = new Node(data);
        if (_pRoot == nullptr)
        {
            _pRoot = pNewNode;
            return true;
        }

        Node* pParent = nullptr;
        Node* pCur = _pRoot;
        while (pCur)
        {
            if (data < pCur->_data)
            {
                pParent = pCur;
                pCur = pCur->_pLeft;
            }
            else if (data > pCur->_data)
            {
                pParent = pCur;
                pCur = pCur->_pRight;
            }
            else
            {
                delete pNewNode;  // 已存在相同值的节点，不插入
                return false;
            }
        }

        if (data < pParent->_data)
            pParent->_pLeft = pNewNode;
        else
            pParent->_pRight = pNewNode;

        pNewNode->_pParent = pParent;

        // 更新平衡因子
        while (pParent)
        {
            if (pNewNode == pParent->_pLeft)
                pParent->_bf--;
            else
                pParent->_bf++;

            if (pParent->_bf == 0)
                break;
            else if (abs(pParent->_bf) == 2)
            {
                if (pParent->_bf == 2 && pNewNode->_bf == 1)
                    RotateL(pParent);
                else if (pParent->_bf == -2 && pNewNode->_bf == -1)
                    RotateR(pParent);
                else if (pParent->_bf == 2 && pNewNode->_bf == -1)
                    RotateLR(pParent);
                else if (pParent->_bf == -2 && pNewNode->_bf == 1)
                    RotateRL(pParent);
                break;
            }

            pNewNode = pParent;
            pParent = pParent->_pParent;
        }

        return true;
	}

	// AVL树的验证
	bool IsAVLTree()
	{
		return _IsAVLTree(_pRoot);
	}

private:
	// 根据AVL树的概念验证pRoot是否为有效的AVL树
	bool _IsAVLTree(Node* pRoot)
	{
		if (pRoot == nullptr)
			return true;

		int leftHeight = _Height(pRoot->_pLeft);
		int rightHeight = _Height(pRoot->_pRight);

		int bf = rightHeight - leftHeight;
		if (bf != pRoot->_bf)
		{
			std::cout << "平衡因子错误" << std::endl;
			return false;
		}

		if (abs(bf) > 1)
		{
			std::cout << "平衡因子不满足要求" << std::endl;
			return false;
		}

		return _IsAVLTree(pRoot->_pLeft) && _IsAVLTree(pRoot->_pRight);
	}
	size_t _Height(Node* pRoot)
	{
		if (pRoot == nullptr)
			return 0;

		size_t leftHeight = _Height(pRoot->_pLeft);
		size_t rightHeight = _Height(pRoot->_pRight);

		return (leftHeight > rightHeight ? leftHeight : rightHeight) + 1;
	}
	// 右单旋
	void RotateR(Node* pParent)
	{
		Node* pSubL = pParent->_pLeft;
		Node* pSubLR = pSubL->_pRight;

		pParent->_pLeft = pSubLR;
		if (pSubLR)
			pSubLR->_pParent = pParent;

		pSubL->_pRight = pParent;
		pSubL->_pParent = pParent->_pParent;
		pParent->_pParent = pSubL;

		if (_pRoot == pParent)
			_pRoot = pSubL;
		else
		{
			if (pSubL->_pParent->_pLeft == pParent)
				pSubL->_pParent->_pLeft = pSubL;
			else
				pSubL->_pParent->_pRight = pSubL;
		}
	}
	// 左单旋
	void RotateL(Node* pParent)
	{
		Node* pSubR = pParent->_pRight;
		Node* pSubRL = pSubR->_pLeft;

		pParent->_pRight = pSubRL;
		if (pSubRL)
			pSubRL->_pParent = pParent;

		pSubR->_pLeft = pParent;
		pSubR->_pParent = pParent->_pParent;
		pParent->_pParent = pSubR;

		if (_pRoot == pParent)
			_pRoot = pSubR;
		else
		{
			if (pSubR->_pParent->_pLeft == pParent)
				pSubR->_pParent->_pLeft = pSubR;
			else
				pSubR->_pParent->_pRight = pSubR;
		}
	}
	// 右左双旋
	void RotateRL(Node* pParent)
	{
		Node* pSubR = pParent->_pRight;
		Node* pSubRL = pSubR->_pLeft;

		int bf = pSubRL->_bf;
		RotateR(pParent->_pRight);
		RotateL(pParent);

		if (bf == 1)
			pParent->_bf = -1;
		else if (bf == -1)
			pSubR->_bf = 1;
	}
	// 左右双旋
	void RotateLR(Node* pParent)
	{
		Node* pSubL = pParent->_pLeft;
		Node* pSubLR = pSubL->_pRight;

		int bf = pSubLR->_bf;
		RotateL(pParent->_pLeft);
		RotateR(pParent);

		if (bf == 1)
			pSubL->_bf = -1;
		else if (bf == -1)
			pParent->_bf = 1;
	}

private:
	Node* _pRoot;
};



void Test1()
{
	AVLTree<int>  AVL;
	AVL.Insert(4);
	AVL.Insert(2);
	AVL.Insert(5);
	AVL.Insert(7);
	std::cout << AVL.IsAVLTree() << std::endl;
}